//
//  BuzzCaseWishListViewController.m
//  QNavigator
//
//  Created by Mac on 9/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuzzCaseWishListViewController.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

@implementation BuzzCaseWishListViewController
@synthesize buzzCaseViewPersonImage;
@synthesize buzzCaseViewProductImage;
@synthesize buzzCaseViewTitleLabel;
@synthesize buzzCaseViewCommentLabel;
@synthesize buzzCaseViewMoreInfo;
@synthesize datadict;
@synthesize titleLabel;
@synthesize buzzyCaseScrollView;
@synthesize m_headLineLBL,whoWhatTextView,Whowhattitletext;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{   [buzzCaseViewMoreInfo release];
    [buzzCaseViewPersonImage release];
    [buzzCaseViewProductImage release];
    [buzzCaseViewTitleLabel release];
    [buzzCaseViewCommentLabel release];
    [buzzyCaseScrollView release];
    [m_headLineLBL release];
    [whoWhatTextView release];
    [Whowhattitletext release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{   
    
    
    buzzCaseViewPersonImage=[[AsyncImageView   alloc] initWithFrame:CGRectMake(12, 90, 50, 50)];
    [self.view addSubview:buzzCaseViewPersonImage];
    buzzCaseViewProductImage=[[AsyncImageView alloc] initWithFrame:CGRectMake(25, 55, 242, 210)];
    [buzzCaseViewProductImage setContentMode:UIViewContentModeScaleAspectFill];
    [buzzyCaseScrollView addSubview:buzzCaseViewProductImage];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"the dict value is %@",datadict);
    CGFloat yHeight=0;
    if (datadict && [datadict isKindOfClass:[NSDictionary class]]) 
    {
        m_headLineLBL.text=[[datadict objectForKey:@"wsBuzz"] objectForKey:@"headline"];
        whoWhatTextView.text = [self formatWhoWhatText:[datadict objectForKey:@"wsBuzz"]];
        
        Whowhattitletext.text=[self formatWhoWhattitle:[datadict objectForKey:@"wsBuzz"]];
        
        if ([whoWhatTextView.text length] > 0) {
            [Whowhattitletext sizeToFit];
            Whowhattitletext.frame=CGRectMake(Whowhattitletext.frame.origin.x, Whowhattitletext.frame.origin.y, Whowhattitletext.frame.size.width, Whowhattitletext.contentSize.height);
            
            [whoWhatTextView sizeToFit];
            whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
            yHeight = whoWhatTextView.frame.size.height;
            [buzzyCaseScrollView setContentSize:CGSizeMake(buzzyCaseScrollView.frame.size.width, buzzyCaseScrollView.frame.size.height+yHeight-20)];
        }
    }else {
        whoWhatTextView.text = @"";
        Whowhattitletext.text = @"";
        m_headLineLBL.text = @"";
    }

    
    [buzzCaseViewMoreInfo setHidden:NO];
    [buzzCaseViewTitleLabel setText:[NSString stringWithFormat:@"%@ said:",[[datadict objectForKey:@"originatorData"] objectForKey:@"firstName"]]];

    NSString *temp=[NSString stringWithFormat:@"%@",[[datadict objectForKey:@"originatorData"] objectForKey:@"comments"]];
    if ((temp == nil) || ([temp isEqualToString:@"(null)"])) {
    	temp=[NSString stringWithFormat:@"%@",[[datadict objectForKey:@"wishlist"] objectForKey:@"comments"]];
	}
    if ((temp == nil) || ([temp isEqualToString:@"(null)"])) {
    	temp=[NSString stringWithFormat:@"%@",[[datadict objectForKey:@"wsBuzz"] objectForKey:@"comments"]];
	}
    if ([temp isEqualToString:@"(null)"]) {
        [buzzCaseViewCommentLabel setText:@"No Comment"];
    }
    else
        [buzzCaseViewCommentLabel setText:temp];
    
    [buzzCaseViewProductImage loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,[[datadict objectForKey:@"wsBuzz"] objectForKey:@"imageUrl"]]]];
    [buzzCaseViewPersonImage loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,[[datadict objectForKey:@"originatorData"] objectForKey:@"profileThumbPicUrl"]]]];
    [self.titleLabel setText:self.title];

}

-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle
{
    //    if ([[datatitle objectForKey:@"oped"] length] > 0)
    //        return [datatitle objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [datatitle objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@"•%@:\n",@"Who"];
    }
    data = [datatitle objectForKey:@"what"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"What"];
        
    }
    data = [datatitle objectForKey:@"when"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"When"];
    }
    data = [datatitle objectForKey:@"where"];
    if ([data length] > 0) {
        if ([data length]<=24)
        {
            [string appendFormat:@"•%@:\n",@"Where"];
        }
        else
        {
            [string appendFormat:@"•%@:\n\n",@"Where"];
        }        
    }
    data = [datatitle objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"How"];
        
    }
    data = [datatitle objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"Why"];
    }
    return string;
    
}

- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict
{
    
    if ([[dataDict objectForKey:@"oped"] length] > 0)
        return [dataDict objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [dataDict objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",data];
    }
    data = [dataDict objectForKey:@"what"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"when"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",[self dateReFormated:data]];
    }
    data = [dataDict objectForKey:@"where"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
        
    }
    return string;
}

-(NSString*)dateReFormated:(NSString*)date
{
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    if ([date intValue])
    {
        NSArray *mainarray=[date componentsSeparatedByString:@" "];
        NSArray *timearray=[[mainarray objectAtIndex:1]componentsSeparatedByString:@":"];
        NSString *timeformate=[NSString stringWithFormat:@"%@ %@:%@",[mainarray objectAtIndex:0],[timearray objectAtIndex:0],[timearray objectAtIndex:1]];
        NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
        [dateFormate setDateFormat:@"YYYY:mm:dd HH:mm"];
        NSDate *dateValue=[dateFormate dateFromString:timeformate];
        [dateFormate setDateFormat:@"MMM dd, HH:mm a"];
        [string appendFormat:@"%@",[dateFormate stringFromDate:dateValue]];
        [dateFormate release]; 
        //return string;
    }
    else
    {
        [string appendFormat:@"%@",date];  
    }
    return string;
}

-(IBAction) m_goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    self.buzzyCaseScrollView=nil;
    self.m_headLineLBL=nil;
    self.whoWhatTextView=nil;
    self.Whowhattitletext=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
