//
//  WishListViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tblWishList.h"
#import "WEPopoverController.h"
#import "AsyncImageView.h"
#import "UIGridView.h"
#import "UIGridViewDelegate.h"
@interface WishListViewController : UIViewController <UITextViewDelegate,UIActionSheetDelegate,WEPopoverControllerDelegate, UIPopoverControllerDelegate,UIAlertViewDelegate>
{
	IBOutlet UITableView *m_tblWishList;
	UIView *m_customEntryView;
	NSManagedObjectContext *_context; 
	IBOutlet UIButton *m_editButton;
	UIView	*m_moreDetailView;
	UIScrollView *m_scrollView;
	NSMutableArray *l_ArrayWishlist;
	UILabel *m_lblBrandName,*m_lblDiscount,*m_lblCategory,*m_lblAddress,*m_lblPhoneNo,*m_lblStoreHours;
	UIButton *l_closeBtn;
	UIImageView *m_imageView;
	UITextView *m_txtViewComments;
	UIView *m_popUpView;
	WEPopoverController *popoverController;
	NSInteger currentPopoverCellIndex;
	Class popoverClass;
	CGRect frame;
	BOOL isFromFriendsView;
	NSString *m_friendsMailString;
	UIImageView *m_imgViewAddressIcon;
	UIImageView *m_imgViewPhoneIcon;
	UIImageView *m_imgViewTimeIcon;
	UIView *m_greenCommentBox;
    

    int tableSelectedIndex;
    UIView *footerView;
    int selectedIndex;
    
}
@property(nonatomic,retain)UIView *footerView;
@property (nonatomic, retain) IBOutlet UIGridView *table;

@property CGRect frame;
@property (nonatomic,retain) IBOutlet UIView *m_greenCommentBox;
@property (nonatomic,retain) IBOutlet UIImageView *m_imgViewAddressIcon;
@property (nonatomic,retain) IBOutlet UIImageView *m_imgViewPhoneIcon;
@property (nonatomic,retain) IBOutlet UIImageView *m_imgViewTimeIcon;
@property (nonatomic,retain) IBOutlet UIImageView *m_imageView;
@property (nonatomic,retain) IBOutlet UIButton *l_closeBtn;
@property (nonatomic,retain) IBOutlet UILabel *m_lblBrandName;
@property (nonatomic,retain) IBOutlet UILabel *m_lblDiscount;
@property (nonatomic,retain) IBOutlet UILabel *m_lblCategory;
@property (nonatomic,retain) IBOutlet UILabel *m_lblAddress;
@property (nonatomic,retain) IBOutlet UILabel *m_lblPhoneNo;
@property (nonatomic,retain) IBOutlet UILabel *m_lblStoreHours;
@property (nonatomic,retain) IBOutlet UITextView *m_txtViewComments;

@property (nonatomic,retain) IBOutlet UIView *m_popUpView;
@property (nonatomic,retain) NSMutableArray *l_ArrayWishlist;


@property (nonatomic,retain) UITableView *m_tblWishList;
@property (nonatomic,retain) NSManagedObjectContext *context;
@property (nonatomic,retain) UIView	*m_moreDetailView;
@property (nonatomic,retain) UIScrollView *m_scrollView;
@property (nonatomic, retain) WEPopoverController *popoverController;
@property  BOOL isFromFriendsView;
@property (nonatomic,retain) NSString *m_friendsMailString;

// GDL: Not implemented, so we comment this out.
//- (IBAction)showPopover:(id)sender;
-(IBAction)buzzCaseMoreInfoCloseButtonAction:(id)sender;

-(void)requestFetching;
-(IBAction)btnCloseAction:(id)sender;
-(IBAction) m_goToBackView;

-(IBAction)addBtnPressed:(id)sender;
-(void)btnMoreInfoAction:(id)sender;
@end
