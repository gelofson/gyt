//
//  YahooFriendsViewController.m
//  QNavigator
//
//  Created by softprodigy on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "YahooFriendsViewController.h"
#import "Reachability.h"
#import "JSON.h"
#import "ShopbeeAPIs.h"
#import "LoadingIndicatorView.h"
#import "AsyncImageView.h"
#import "Constants.h"
#import "AuditingClass.h"
#import "QNavigatorAppDelegate.h"
#import "NewsDataManager.h"

# define SectionHeaderHeight 30


@implementation YahooFriendsViewController
@synthesize session;
@synthesize m_strVerificationCode;
@synthesize m_tableView;
@synthesize m_friendsDetails;
@synthesize m_textField;
@synthesize m_verificationView;
@synthesize tempValueArray;
@synthesize temp_ArrCountIds;
@synthesize m_shopBeeArray;
@synthesize m_nonshopBeeArray;
@synthesize tmpArrshopbeeAndNonShopbee;
@synthesize m_string_email;
@synthesize m_ArrAllEmails;
@synthesize shouldShowVerificationView;


NSMutableDictionary	*oauthResponse;
QNavigatorAppDelegate *l_appDelegate;
ShopbeeAPIs *l_request;
int verify;
int b;
int count;
int AddButtonTag;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


- (void)viewDidLoad 
{
	[super viewDidLoad];
	m_ArrAllEmails=[[NSMutableArray alloc] init];
	b=0;
	m_shopBeeArray=[[NSMutableArray alloc] init];
	m_nonshopBeeArray=[[NSMutableArray alloc] init];
	//	NSLog(@"m_strVerificationCode: %@",m_strVerificationCode);
	//	
	//[[NSUserDefaults standardUserDefaults] setObject:@"verifier" forKey:verifier];
	
	
	[self.view addSubview:m_verificationView];
	m_verificationView.hidden=YES;
	
	
	////[m_textField resignFirstResponder];
	//[m_verificationView setHidden:YES];
	Reachability* reachability = [Reachability reachabilityWithHostName:@"yahoo.com"];
	//[reachability setHostName:@"www.yahoo.com"];    // set your host name here
	NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
	
	if(remoteHostStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Yahoo connect"
														message:@"Error: No internet connection" 
													   delegate:self 
											  cancelButtonTitle:@"OK" 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	
	session = [[YOSSession sessionWithConsumerKey:@"dj0yJmk9OVZQS3ZQVXZUTTVyJmQ9WVdrOVIzRnJVRlo1TjJzbWNHbzlNakE1T0RReU1qZzJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yZQ--"//@"dj0yJmk9ZEtIZ0tSeVdOaHY2JmQ9WVdrOWRrcDJkbXh2TkdrbWNHbzlPRFExTWpVME5qSS0mcz1jb25zdW1lcnNlY3JldCZ4PTk5"  
								andConsumerSecret:@"2e0c982475c23da68af9a7378bd3526aa9259c3b"//@"3e7a62ea0cf8c1e80d5f41b9aea5dc4a0a0b5f98"  
								 andApplicationId:@"GqkPVy7k"]retain] ; 
	
	//[session setVerifier:[[NSUserDefaults standardUserDefaults] valueForKey:@"verifier"]];
	[session setVerifier:m_textField.text];
	
	
	BOOL hasSession = [session resumeSession];
	
	if(hasSession == FALSE)
	{  
		session.webViewClass=self;//required to push to webview class without closing the app
		[m_verificationView setHidden:NO];
		[session sendUserToAuthorizationWithCallbackUrl:nil];
		
		
		//[session setVerifier:@"12345"];
		//[session sendUserToAuthorizationWithCallbackUrl:nil];  
	}
	else
	{  
		
		[[LoadingIndicatorView SharedInstance] startLoadingView:self];
		
		[self sendRequests];
		
	}  
	
	
	//[m_verificationView setHidden:NO];
	
}


-(IBAction)btnOkAction:(id)sender
{
	[m_textField resignFirstResponder];
	[m_verificationView setHidden:YES];
	Reachability* reachability = [Reachability reachabilityWithHostName:@"yahoo.com"];
	//[reachability setHostName:@"www.yahoo.com"];    // set your host name here
	NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
	
	if(remoteHostStatus == NotReachable) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Yahoo connect"
														message:@"Error:No internet connection" 
													   delegate:self 
											  cancelButtonTitle:@"OK" 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	//dj0yJmk9MGQxTXRicVl4cmlIJmQ9WVdrOVFVbFlOVEJsTkc4bWNHbzlNVFl4TlRNeU1EYzJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD03MQ--
	//5f0c3cf605cfae06391a9348f5c2489e821bb708
	//AIX50e4o
	session = [[YOSSession sessionWithConsumerKey:@"dj0yJmk9OVZQS3ZQVXZUTTVyJmQ9WVdrOVIzRnJVRlo1TjJzbWNHbzlNakE1T0RReU1qZzJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yZQ--"//@"dj0yJmk9ZEtIZ0tSeVdOaHY2JmQ9WVdrOWRrcDJkbXh2TkdrbWNHbzlPRFExTWpVME5qSS0mcz1jb25zdW1lcnNlY3JldCZ4PTk5"  
								andConsumerSecret:@"2e0c982475c23da68af9a7378bd3526aa9259c3b"//@"3e7a62ea0cf8c1e80d5f41b9aea5dc4a0a0b5f98"  
								 andApplicationId:@"GqkPVy7k"]retain] ; 
	
	//[session setVerifier:[[NSUserDefaults standardUserDefaults] valueForKey:@"verifier"]];
	[session setVerifier:m_textField.text];
	
	BOOL hasSession = [session resumeSession];
	
	if(hasSession == FALSE)
	{  
		session.webViewClass=self;//required to push to webview class without closing the app
		[session sendUserToAuthorizationWithCallbackUrl:nil];
		
		//[session setVerifier:@"12345"];
		//[session sendUserToAuthorizationWithCallbackUrl:nil];  
	}
	else
	{  
		//[self sendRequestsForSocialProfile];
		[self sendRequests];
		
		
	}  
	
}

-(void)btnAddAllAction:(id)sender
{
	NSString *tmp_mail;
	NSMutableArray *tmp_array;
	NSString *customerId;
	tmp_array=[[NSMutableArray alloc] init];
	for (int i=0; i<[m_shopBeeArray count];i++)
	{
		tmp_mail=[[m_shopBeeArray objectAtIndex:i]valueForKey:@"email"];
		[tmp_array addObject:tmp_mail];
	}
	
	NSString *strJson=[NSString stringWithFormat:@"%@",[tmp_array JSONRepresentation]];
	
	customerId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	l_request=[[ShopbeeAPIs alloc] init];
	[l_request sendRequestToAddFriend:@selector(requestCallBackMethodForAddFriend:responseData:) tempTarget:self custId:customerId tmpUids:strJson];
	[l_request release];
	l_request=nil;
	[tmp_array release];
	tmp_array=nil;
	AddButtonTag=-1;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"YahooViewFriends"
                                                    withAction:@"btnAddAllAction"
                                                     withLabel:nil
                                                     withValue:nil];
}


-(void)btnAddAction:(id)sender
{
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	
	NSString *tempString= [NSString stringWithFormat:@"[\"%@\"]",[[m_shopBeeArray objectAtIndex:[sender tag]] valueForKey:@"email"]];
	NSLog(@"%@",tempString);
	
	AddButtonTag=[sender tag];
	[l_request sendRequestToAddFriend:@selector(requestCallBackMethodForAddFriend:responseData:) tempTarget:self custId:customerID tmpUids:tempString];//[m_shopBeeArray objectAtIndex:[sender tag]]
	[l_request release];
	l_request=nil;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"YahooViewFriends"
                                                    withAction:@"btnAddAction"
                                                     withLabel:nil
                                                     withValue:nil];
	
}



-(IBAction)btnbackAction:(id)sender
{
	//l_appDelegate.countVal=2;
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
	
	if (shouldShowVerificationView)
	{
		shouldShowVerificationView=FALSE;
		m_verificationView.hidden=NO;
	}
	
	
	//[[NSUserDefaults standardUserDefaults] setObject:@"verifier" forKey:m_strVerificationCode];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */
-(void)sendRequestsForSocialProfile
{
	verify=1;
	YQLQueryRequest *request = [YQLQueryRequest requestWithSession:session];  
	
	//NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"SELECT * from social.contacts WHERE guid=me"];
	//NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from geo.places where text=\"sfo\""];  
	//NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.connections where owner_guid=me"];  
	//H5JLRYJ6JM56JLLXIWEDL2KNIU
	//SELECT * from social.contacts WHERE guid=me
	//select * from social.profile where guid=me
	//NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.relationships where owner_guid=me"];
	//SELECT * FROM social.contacts(0, 500) WHERE guid=me
	//@"select * from social.profile where guid in(select guid from social.connections where owner_guid=me)"];
	//select * from social.profile where guid in(select guid from social.connections where owner_guid=me
	//NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"SELECT nickname from social.profile WHERE guid in(select guid from social.connection where owner_guid=me)"];
	//	NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.profile where guid in (select guid from social.contacts where guid=me)"];
	NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.profile.image where guid in (select guid from social.connections where owner_guid=me) and size='32x32'"];
	
	//NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.profile where guid=me"];
	[request query:structuredProfileLocationQuery withDelegate:self];  
	
	
}


- (void)sendRequests {  
	verify=2;
	// initialize a user request for the logged-in user     
	YOSUserRequest *request = [YOSUserRequest requestWithSession:session];  
	// fetch the user's profile data  
	//   fetchContactSyncRevision
	//YOSResponseData *userProfileResponse = 
	//[request fetchProfileWithDelegate:self];  
	[request fetchContactsWithStart:0 andCount:1000 withDelegate:self];
	//[request fetchConnectionUpdatesWithStart:1 andCount:10 withDelegate:self];
	//[request fetchConnectionProfilesWithStart:1 andCount:10 withDelegate:self];
	
	//	fetchConnectionUpdatesWithStart
	//[request fetchContactSyncRevision:2 withDelegate:self];
	//	[request fetchContactWithID:@"H3JZ5YUVXBJ4LVRL4N3EMXPAWI" withDelegate:self];
	//H3JZ5YUVXBJ4LVRL4N3EMXPAWI
}  

- (void)requestDidFinishLoading:(YOSResponseData *)data {  
	
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	if(verify==1)
	{
		NSDictionary *rspData = [data.responseText JSONValue];  
		NSLog(@"%@",rspData);
		NSDictionary *queryData = [rspData objectForKey:@"query"];  
		NSDictionary *results = [queryData objectForKey:@"results"];  
		
		NSLog(@"resultsssss: %@",results);
		
		//       NSMutableArray	*tempArray=[[NSMutableArray alloc]init];
		//		
		//		for(int i=0;i<[m_friendsDetails count];i++)
		//		{
		//			[tempArray addObject:[NSString stringWithFormat:@"%@",[[m_friendsDetails objectAtIndex:i]valueForKey:@"guid"]]];
		//		}
		
		//..[self sendRequests];
	}
	else if(verify==2) 
	{
		//   parse the response text string into a dictionary  
		temp_ArrCountIds=[[NSMutableArray alloc] init];
		NSDictionary *rspData = [data.responseText JSONValue];  
		//NSLog(@"%@",rspData);
		tempValueArray=[[rspData allValues] retain];
		NSLog(@"%@",tempValueArray);
		NSLog(@"%d",[[[tempValueArray objectAtIndex:0] valueForKey:@"contact"]count]);
		
		NSMutableArray	*tmpArrEmail=[[NSMutableArray alloc]init];
		tmpArrshopbeeAndNonShopbee=[[NSMutableArray alloc]init];
		BOOL isEmailExists;
		
		
		for(int i=0;i<[[[tempValueArray objectAtIndex:0] valueForKey:@"contact"] count];i++)
		{
			NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
			isEmailExists=FALSE;
			for(int a=0;a<[[[[[tempValueArray objectAtIndex:0]valueForKey:@"contact"]objectAtIndex:i] valueForKey:@"fields"] count];a++)
			{
				
				if([[[[[[[tempValueArray objectAtIndex:0]valueForKey:@"contact"]objectAtIndex:i] valueForKey:@"fields"] objectAtIndex:a] valueForKey:@"type"] isEqualToString:@"email"])
				{
					isEmailExists=TRUE;
					[tmpArrEmail addObject:[[[[[[tempValueArray objectAtIndex:0] valueForKey:@"contact"] objectAtIndex:i]valueForKey:@"fields"]objectAtIndex:a] objectForKey:@"value"]];
					[dic setValue:[[[[[[tempValueArray objectAtIndex:0] valueForKey:@"contact"] objectAtIndex:i]valueForKey:@"fields"]objectAtIndex:a] objectForKey:@"value"] forKey:@"email"];
				}
				
				if([[[[[[[tempValueArray objectAtIndex:0]valueForKey:@"contact"]objectAtIndex:i] valueForKey:@"fields"] objectAtIndex:a] valueForKey:@"type"] isEqualToString:@"name"] && isEmailExists==TRUE)
				{
					NSLog(@"name : %@",[[[[[[[tempValueArray objectAtIndex:0] valueForKey:@"contact"] objectAtIndex:i]valueForKey:@"fields"]objectAtIndex:a] objectForKey:@"value"]objectForKey:@"givenName"]);
					
					[dic setValue:[[[[[[[tempValueArray objectAtIndex:0] valueForKey:@"contact"] objectAtIndex:i]valueForKey:@"fields"]objectAtIndex:a] objectForKey:@"value"]objectForKey:@"givenName"] forKey:@"name"];
					
				}
				
			}
			
			if (isEmailExists)
			{
				[tmpArrshopbeeAndNonShopbee addObject:dic];
			}
			
			[dic release];
			
		}
		//NSLog(@"dic :%@",dic);
		NSString *tmpStrEmailIds; 
		tmpStrEmailIds=[NSString stringWithFormat:@"listids=%@",[tmpArrEmail JSONFragment]];
		NSLog(@"tmpStrEmailIds : %@",tmpStrEmailIds);
		
		
		
		
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		l_request=[[ShopbeeAPIs alloc] init];
		[[LoadingIndicatorView SharedInstance] startLoadingView:self];
		[l_request getRegisteredUsers:@selector(requestCallBackMethodForListIds:responseData:) tempTarget:self customerid:customerID listids:tmpStrEmailIds];
		
		//		[l_request mapFaceBookID:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID facebookid:str];
		
		
		//[arrGuid release];
		[l_request release];
		l_request=nil;
	}
}  
#pragma mark -
#pragma mark Callback methods
- (void)requestDidFailWithError:(YOSResponseData *)error
{
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	NSLog(@"some error occurred in yahoo api");
	
}
-(void)requestCallBackMethodForAddFriend:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	NSInteger Code=[responseCode integerValue];
	
    if (Code == 401 || Code == 500) {
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
        return;
    }
    
	if(AddButtonTag==-1)
	{
		if (Code==200)
		{
			//[self showAlertView:@"Friend request has been sent to all friends." alertMessage:nil];	
			
			[m_shopBeeArray removeAllObjects];
			[m_tableView reloadData];
			
		}
		else if(Code==500)
		{
			//	[self showAlertView:@"Some error occured" alertMessage:@"Friend already added"];	
		}
	}
	else
	{
		if (Code==200) 
		{
			//	[self showAlertView:@"Friend Request sent" alertMessage:@"Friend request has been sent." ];
			[m_shopBeeArray removeObjectAtIndex:AddButtonTag];
			
			[m_tableView reloadData];
		}
		else if(Code==500)
		{
            // GDL: Removed since we don't appear to implement this method.
			//[self showAlertView:@"Some error occured" alertMessage:@"Friend already added !"];	
		}
	}
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[tempString release];
	tempString=nil;
}	




-(void)requestCallBackMethodForListIds:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	int  temp_responseCode=[responseCode intValue];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	if(temp_responseCode == 200)
	{
		NSString *str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding]autorelease];
		
		SBJSON *objJson=[SBJSON new];
		
		NSMutableArray *response=[objJson objectWithString:str];
		
		NSLog(@"response: %@",response);
		NSLog(@"count:-%d",[[[response objectAtIndex:0] valueForKey:@"nonShopbee"]count]);
		NSMutableArray	*tempArrayNonshopbee=[[NSMutableArray alloc]init];
		
		for(int i=0;i<[[[response objectAtIndex:0] valueForKey:@"nonShopbee"]count];i++)
		{
			[tempArrayNonshopbee addObject:[[[response objectAtIndex:0]valueForKey:@"nonShopbee"]objectAtIndex:i]];
		}
		
		NSLog(@"nonShopbee : %@\n",tempArrayNonshopbee);
		
		NSMutableArray	*tempArrayShopBee=[[NSMutableArray alloc]init];
		
		for(int i=0;i<[[[response objectAtIndex:0] valueForKey:@"shopbee"]count];i++)
		{
			[tempArrayShopBee addObject:[[[response objectAtIndex:0]valueForKey:@"shopbee"]objectAtIndex:i]];
		}
		
		NSLog(@"FashionGram :%@\n",tempArrayShopBee);
		NSLog(@"complete array : %d",[tmpArrshopbeeAndNonShopbee count]);
		
		
		//NSArray *arr = [tempArray_Uids objectAtIndex:0];
		
		BOOL tempStatus;
		tempStatus=FALSE;
		
		for(int j=0;j<[tmpArrshopbeeAndNonShopbee count];j++)
		{
			for(int i=0;i<[tempArrayNonshopbee count];i++)
			{
				if([[[tmpArrshopbeeAndNonShopbee objectAtIndex:j]valueForKey:@"email"] isEqualToString:(NSString *)[tempArrayNonshopbee objectAtIndex:i]])
				{
					tempStatus=TRUE;
					break;
				}
				else
				{
					tempStatus=FALSE;
					
				}
			}
			if(tempStatus == TRUE)
			{
				[m_nonshopBeeArray addObject:[tmpArrshopbeeAndNonShopbee objectAtIndex:j]];
				tempStatus=FALSE;
			}
			else
			{
				[m_shopBeeArray addObject:[tmpArrshopbeeAndNonShopbee objectAtIndex:j]];
			}
		}
		[m_tableView reloadData];
	} else {
        NSLog(@"YahooFriendsViewController responseCode = %d", temp_responseCode);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	
}	


-(void)showPicker:(id)sender
{
	
	if(count==1)
	{
		for(int i=0;i<[m_nonshopBeeArray count];i++)
		{
			[m_ArrAllEmails addObject:[[m_nonshopBeeArray objectAtIndex:i] objectForKey:@"email"]];
		}
	}
	else
	{
		m_string_email=[[[m_nonshopBeeArray objectAtIndex:[sender tag]] objectForKey:@"email"]retain];
	}
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}


#pragma mark -
#pragma mark MailComposer delegates
// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet
{
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	//NSMutableArray *toRecipients=[[NSMutableArray alloc]init];
	
	
	if(count==1)
	{
		[picker setBccRecipients:m_ArrAllEmails];
		count=0;
	}
	else
	{
		[picker setToRecipients:[NSArray arrayWithObject:m_string_email]];
		[m_string_email release];
		m_string_email=nil;
	}
	[picker setSubject:kTellAFriendMailSubject];
	
	NSString *emailBody;
	emailBody=kTellAFriendMailBody;
	
	//[toRecipients release];
	//toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	[self presentModalViewController:picker animated:YES];
	
    [picker release];
	picker=nil;
	
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{	
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	//m_lblRecipient.text=@"";
	//m_lblRecipient.hidden=TRUE;
	[self dismissModalViewControllerAnimated:YES];
	
}
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}



-(void)btnInviteAction:(id)sender
{
	NSLog(@"invite user from yahoo\n");
}

#pragma mark -
#pragma mark  table view methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section                              
{
	
	NSLog(@"table row count:-%d",[m_nonshopBeeArray count]);
	
	if(section==0)
	{
		return [m_shopBeeArray count];    
	}
	else
	{
		return [m_nonshopBeeArray count];
	}
	
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	//NSArray *tmp_array;
	//	 tmp_array=[[[[m_nonshopBeeArray objectAtIndex:0] valueForKey:@"contact"] objectAtIndex:indexPath.row]valueForKey:@"fields"];
	
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell %@",indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: SimpleTableIdentifier];
	
	if (cell == nil) 
	{ 
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
		
		cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow_in_add_friend"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		CGRect frame;
		frame.size.width=30; frame.size.height=30;
		frame.origin.x=5; frame.origin.y=10;
		
		AsyncImageView* asyncImage = [[[AsyncImageView alloc]
									   initWithFrame:frame] autorelease];
		asyncImage.tag = 999;
		[cell.contentView addSubview:asyncImage];
		
		UILabel *temp_lblFriendName=[[UILabel alloc]init];
		temp_lblFriendName.frame=CGRectMake(45,6,185,25);
		temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
		temp_lblFriendName.tag=102;
		temp_lblFriendName.backgroundColor=[UIColor clearColor];
		temp_lblFriendName.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
	}
	else 
	{
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		UIImageView *tempImageview=(UIImageView *)[asyncImage viewWithTag:101];
		[tempImageview setImage:nil];
		[(UILabel *)[cell.contentView viewWithTag:102] setText:@""];
	}
	
	if(indexPath.section==0)
	{		
		
		if([[m_shopBeeArray objectAtIndex:indexPath.row]valueForKey:@"name"])
		{
			[(UILabel *)[cell.contentView viewWithTag:102] setText:[[m_shopBeeArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
		}
		else
		{
			[(UILabel *)[cell.contentView viewWithTag:102] setText:[[m_shopBeeArray objectAtIndex:indexPath.row]valueForKey:@"email"]];
		}
		
		UIButton *temp_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_addButton setImage:[UIImage imageNamed:@"add_in_add_friend_button.png"] forState:UIControlStateNormal];
		temp_addButton.tag=indexPath.row;
		[temp_addButton addTarget: self action:@selector(btnAddAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addButton.frame=CGRectMake(230,13,42,23);
		[cell.contentView addSubview:temp_addButton];
		
	}
	else
	{
		
		if([[m_nonshopBeeArray objectAtIndex:indexPath.row]valueForKey:@"name"])
		{
			[(UILabel *)[cell.contentView viewWithTag:102] setText:[[m_nonshopBeeArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
		}
		else
		{
			[(UILabel *)[cell.contentView viewWithTag:102] setText:[[m_nonshopBeeArray objectAtIndex:indexPath.row]valueForKey:@"email"]];
		}
		
		
		
		UIButton *temp_inviteButton=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_inviteButton setImage:[UIImage imageNamed:@"btn_invite"] forState:UIControlStateNormal];
		temp_inviteButton.tag=indexPath.row;
		[temp_inviteButton addTarget: self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
		temp_inviteButton.frame=CGRectMake(250,13,43,24);
		[cell.contentView addSubview:temp_inviteButton];
		
	}
	
	return cell;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	if (section==0) 
	{
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 350, SectionHeaderHeight)];
		tmp_view.backgroundColor=[UIColor colorWithRed:.78f green:.79f blue:.79f alpha:1.0];
		[tmp_view autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:16];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%d friends are on FashionGram", m_shopBeeArray.count];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:0.78f green:0.79f blue:0.79f alpha:1.0];
		tmp_headerLabel.textColor=[UIColor whiteColor];
		[tmp_view addSubview:tmp_headerLabel];
		
		
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_view.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"add_all.png"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnAddAllAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(240,4,50,23);
		[tmp_view addSubview:temp_addAll];
		
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return tmp_view;
	}
	else 
	{
		
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 350, SectionHeaderHeight)];
		tmp_headerForSecondSection.backgroundColor=[UIColor colorWithRed:.78f green:.79f blue:.79f alpha:1.0];
		[tmp_headerForSecondSection autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)] ;
		[tmp_headerLabel autorelease];
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:16 ];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%d friends not on FashionGram",m_nonshopBeeArray.count];
		tmp_headerLabel.textColor=[UIColor whiteColor];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:0.78f green:0.79f blue:0.79f alpha:1.0];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
		
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_headerForSecondSection.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"btn_inviteAll"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnInviteAllAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(260,5,43,20);
		[tmp_headerForSecondSection addSubview:temp_addAll];
		
		
		
		return tmp_headerForSecondSection;
		
		
	}
}


-(void)btnInviteAllAction:(id)sender
{
	count=1;
	[self performSelector:@selector(showPicker:)];
	
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 
	
	if (section==0 && m_shopBeeArray.count>0)
		return 30;
	else if(section==0 && m_shopBeeArray.count==0)
		return 0;
	if (section==1 && m_nonshopBeeArray.count>0)
		return 30;
	else if(section==1 && m_nonshopBeeArray.count==0)
		return 0;
	return 0;
	
	
} 




/*
 -(void)sendRequests {  
 YQLQueryRequest *request = [YQLQueryRequest requestWithSession:session];  
 
 NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"SELECT * from social.contacts WHERE guid=me"];
 //NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from geo.places where text=\"sfo\""];  
 //NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.connections where owner_guid=me"];  
 //H5JLRYJ6JM56JLLXIWEDL2KNIU
 //SELECT * from social.contacts WHERE guid=me
 //select * from social.profile where guid=me
 //NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.relationships where owner_guid=me"];
 //SELECT * FROM social.contacts(0, 500) WHERE guid=me
 //@"select * from social.profile where guid in(select guid from social.connections where owner_guid=me)"];
 //select * from social.profile where guid in(select guid from social.connections where owner_guid=me
 //NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"SELECT nickname from social.profile WHERE guid in(select guid from social.connection where owner_guid=me)"];
 //NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select * from social.profile where guid in (select guid from social.contacts where guid=me)"];
 
 [request query:structuredProfileLocationQuery withDelegate:self];  
 }  
 
 -(void)requestDidFinishLoading:(YOSResponseData *)data {  
 NSLog(@"%d",data);
 
 NSDictionary *rspData = [data.responseText JSONValue];  
 NSLog(@"%@",rspData);
 NSDictionary *queryData = [rspData objectForKey:@"query"];  
 NSDictionary *results = [queryData objectForKey:@"results"];  
 
 NSLog(@"results: %@", [results description]);  
 NSLog(@"resultsssss: %@",results);
 //m_friendsDetails = [[NSMutableArray alloc] initWithArray:results];
 
 
 //	NSLog(@"uids : %@",[[[results objectForKey:@"profile"] objectAtIndex:0] valueForKey:@"guid"]);
 
 
 NSMutableArray	*tempArray=[[NSMutableArray alloc]init];
 
 for(int i=0;i<[m_friendsDetails count];i++)
 {
 [tempArray addObject:[NSString stringWithFormat:@"%@",[[m_friendsDetails objectAtIndex:i]valueForKey:@"guid"]]];
 }
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release all retained IBOutlets.
    self.m_verificationView = nil;
    self.m_textField = nil;
    self.m_tableView = nil;
}

- (void)dealloc {
    [session release];
    [m_strVerificationCode release];
    [m_tableView release];
    [m_friendsDetails release];
    [m_textField release];
    [m_verificationView release];
    [tempValueArray release];
    [temp_ArrCountIds release];
    [m_shopBeeArray release];
    [m_nonshopBeeArray release];
    [tmpArrshopbeeAndNonShopbee release];
    [m_string_email release];
    [m_ArrAllEmails release];
    
    [super dealloc];
}

@end
