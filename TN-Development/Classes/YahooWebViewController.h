//
//  YahooWebViewController.h
//  QNavigator
//
//  Created by sukhwinder on 28/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface YahooWebViewController : UIViewController {

	UIWebView *m_webview;
	NSURL *strUrlToLoad;
	UIActivityIndicatorView *m_activityIndicator;
	id m_yahooFrndsview;
	
}
@property (nonatomic,retain) id m_yahooFrndsview;
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView *m_activityIndicator;
@property (nonatomic,retain) IBOutlet UIWebView *m_webview;
@property (nonatomic,retain) NSURL *strUrlToLoad;

-(IBAction)btnDoneAction:(id)sender;

- (IBAction)btnBackAction:(id)sender;


@end
