//
//  ImageCacheManager.h
//  ooVoo
//
//  Created by Nava Carmon on 12/21/10.
//  Copyright 2010 ooVoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PictureRequest.h"

@interface ImageLoaderManager : NSObject {
    NSOperationQueue *loaderQueue;
}

+ (ImageLoaderManager*)sharedManager;

- (void) addLoaderRequest:(PictureRequest *)request;
@end
