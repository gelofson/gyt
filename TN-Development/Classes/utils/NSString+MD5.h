//
//  NSString+MD5.h
//  TinyNews
//
//  Created by Nava Carmon on 2/17/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString *) md5;
@end
