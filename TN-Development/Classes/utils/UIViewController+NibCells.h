@interface UIViewController (NibCells)


- (UITableViewCell *)loadTableViewCellFromNibNamed:(NSString *)nibName;
- (UITableViewCell *)loadReusableTableViewCellFromNibNamed:(NSString *)nibName;

+ (UITableViewCell *)loadTableViewCellFromNibNamed:(NSString *)nibName withOwner:(NSObject *)owner;
+ (UITableViewCell *)loadReusableTableViewCellFromNibNamed:(NSString *)nibName withOwner:(NSObject *)owner;

@end
