#import "GeoSelectAnnotation.h"

@implementation GeoSelectAnnotation

@synthesize coordinate;
@synthesize geoCoder;
@synthesize placeMark;

-(void) findAddress {

	 /*Geocoder Stuff*/
	 MKReverseGeocoder * gc=[[MKReverseGeocoder alloc] initWithCoordinate:coordinate];
	 self.geoCoder=gc;
	[gc release]; gc = nil;
	 self.geoCoder.delegate=self;
	 [self.geoCoder start];
 
}


-(NSString *) subtitle {
	NSString * str = [self getFormatedAddressSubtitle];
	if ((str == nil) || ([str length] < 2)) {
		str = [NSString stringWithFormat:@"lat:%lf, long:%lf", coordinate.latitude,coordinate.longitude];
	}
	return str;
}

-(NSString *) title {
	NSString * str = [self getFormatedAddressTitle];
	if ((str == nil) || ([str length] < 2)) {
		str = @"Location";
	}
	return str;
}
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    NSLog(@"GeoSelectAnnotation:setCoordinate:");
    coordinate = newCoordinate;
    self.placeMark = nil;
	if (self.geoCoder != nil) {
        [self.geoCoder cancel];
    }
    [self findAddress];
}

-(id)init
{
	self = [super init];
	if (self) {
		placeMark = nil;
		geoCoder = nil;
	}
    return self;
}


- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error{
	NSLog(@"Reverse Geocoder Errored on annotation=%d",self);
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reverse Geocoding Failure" 
		message:[NSString stringWithFormat:@"Unable to get the street address to your location (lat:%lf, long:%lf) at this time. However you can continue to use the application.", geocoder.coordinate.latitude, geocoder.coordinate.longitude] 
		 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	/*[alert show];*/ [alert release];	
	
} 

 
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark{
	NSLog(@"Reverse Geocoder completed on %d", self);
	if(!placemark)  {
		return;
	}

	MKPlacemark * pm = [[MKPlacemark alloc] initWithCoordinate:self.coordinate addressDictionary:[placemark addressDictionary]];
	self.placeMark = pm;
	[pm release]; pm=nil;
	
	[self.geoCoder cancel]; 
	
}

-(NSString *) getFormatedAddressTitle {
	if (placeMark == nil)
		return @"";

	NSMutableString* str = [[NSMutableString alloc] initWithCapacity:200];
	if(placeMark.subThoroughfare != nil) {
		[str appendString:placeMark.subThoroughfare];
	}

	if(placeMark.thoroughfare != nil) {
		if (str.length > 0) {
			[str appendString:@" "];
		}
		[str appendString:placeMark.thoroughfare];
	}
		
	if(placeMark.subLocality != nil) {
		if (str.length > 0) {
			[str appendString:@" "];
		}
		[str appendString:placeMark.subLocality];
	}
	
	NSString * retStr = [NSString stringWithString:str];
	[str release]; str = nil;
	NSLog(@"->%lf,%lf,title=%@",coordinate.latitude, coordinate.longitude,retStr);

	return retStr;
}

-(NSString *) getFormatedAddressSubtitle {
	if (placeMark == nil)
		return @"";
	NSMutableString* str = [[NSMutableString alloc] initWithCapacity:200];
	
	if(placeMark.locality != nil) {
		if (str.length > 0) {
			[str appendString:@" "];
		}
		[str appendString:placeMark.locality];
	}
	
	if(placeMark.administrativeArea != nil) {
		if (str.length > 0) {
			[str appendString:@" "];
		}
		[str appendString:placeMark.administrativeArea];
	}

	if(placeMark.postalCode != nil) {
		if (str.length > 0) {
			[str appendString:@" "];
		}
		[str appendString:placeMark.postalCode];
	}

	if(placeMark.countryCode != nil) {
		if (str.length > 0) {
			[str appendString:@" "];
		}
		[str appendString:placeMark.countryCode];
	}
	
	NSString * retStr = [NSString stringWithString:str];
	[str release]; str = nil;
	NSLog(@"->%lf,%lf,subtitle=%@",coordinate.latitude, coordinate.longitude,retStr);

	return retStr;
}


- (void)dealloc {
    geoCoder.delegate = nil;
    self.geoCoder=nil;

	if (placeMark != nil) {
		[placeMark release]; placeMark = nil;
	}

    [super dealloc];
}


@end


