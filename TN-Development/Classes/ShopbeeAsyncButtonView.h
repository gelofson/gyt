//
//  ShopbeeAsyncButtonView.h
//  QNavigator
//
//  Created by Bharat Biswal on 08/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ShopbeeAsyncButtonView : UIView 
{
	//could instead be a subclass of UIImageView instead of UIView, depending on what other features you want to 
	// to build into this class?
	id m_target;
	NSURLConnection* connection; //keep a reference to the connection so we can cancel download in dealloc
	NSMutableData* data; //keep reference to the data so we can collect it as it downloads
	int m_tagValue;
	UIButton *m_btnIcon;
	BOOL isCatView;
    NSInteger tag;
	//but where is the UIImage reference? We keep it in self.subviews - no need to re-code what we have in the parent class
}

@property BOOL isCatView;
@property(assign,nonatomic)NSInteger tag;
@property (nonatomic,retain) NSMutableData* data;

@property (nonatomic,retain) id m_target;

@property (nonatomic,retain) UIButton *m_btnIcon;

- (void)loadImageFromURL:(NSURL*)url target:(id)target action:(SEL)selector btnText:(NSString *)btnText;
- (UIImage*) image;
- (void) cancelRequest;
@end
