//
//  ProfileSettingsViewController.m
//  QNavigator
//
//  Created by softprodigy on 30/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyPageSettingViewController.h"
#import "MyPageUserGuideViewController.h"
#import "TOSViewController.h"
#import"Constants.h"
#import "MyPageFeedBackViewController.h"
#import "LoginHomeViewController.h"
#import "QNavigatorAppDelegate.h"
#import "LoginHomeViewController.h"
#import"PrivacySettingsViewController.h"
#import"ApplicationSettingsViewController.h"
#import "BigSalesAdsViewController.h"
#import "ContentViewController.h"
#import "NewsDataManager.h"
#import "LoadingIndicatorView.h"
#import "ShopbeeAPIs.h"

LoadingIndicatorView *l_indicatorView;
ShopbeeAPIs *l_request;
#define SectionHeaderHeight 36

@implementation MyPageSettingViewController

@synthesize m_mutResponseData;
@synthesize m_activityIndicator;
@synthesize m_intResponseStatusCode;
@synthesize signoutPopup;

NSURLConnection *l_theConnection;
QNavigatorAppDelegate *l_appDelegate;
PrivacySettingsViewController *l_privacyObj;

- (void) viewWillAppear:(BOOL)animated
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *custID= [prefs objectForKey:@"userName"];
    [l_indicatorView startLoadingView:self];
    [l_request getUserInfo:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:custID loginId:custID];

}

#pragma mark custom methods
-(IBAction)m_popView{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section	{	
	if (section==0) {
		return [m_profileSetting count];
	}	
	else {
		return [m_profileSetting1 count];
	}
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *cellidentifier=@"cell";
	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
	
	UILabel *temp_lblFriendName=[[UILabel alloc]init];
	temp_lblFriendName.frame=CGRectMake(10,12,200,25);    // for setting the frame of the tableview cell
	temp_lblFriendName.font=[UIFont fontWithName:kHelveticaBoldFont size:16];
	if (indexPath.section==0) {
        temp_lblFriendName.text=[m_profileSetting objectAtIndex:indexPath.row];
		temp_lblFriendName.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	}
	else if(indexPath.section==1){ 
		temp_lblFriendName.text=[m_profileSetting1 objectAtIndex:indexPath.row];	
		temp_lblFriendName.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	}
	
    
	
    if (cell == nil)
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier] autorelease];
	
    if (!cell.accessoryView)
        cell.accessoryView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_friends"]] autorelease];
    
    
	//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_with_arrow_in_add_friend.png"]];	
//    UIImageView *arrowImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow.png"]];
//    [arrowImageView setFrame:CGRectMake(275, 8, 20, 21)];
//    [cell.contentView addSubview:arrowImageView];
	cell.selectionStyle=UITableViewCellSelectionStyleNone;
	[cell.contentView addSubview:temp_lblFriendName];
	
	[temp_lblFriendName release];
    //[arrowImageView release];
   // arrowImageView=nil;
	temp_lblFriendName=nil;
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	return 2;
	
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
    
	if (section==0) 
	{
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, SectionHeaderHeight)];
        
        UIImageView *tmp_view_backrgound=[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"darkgrey_bar.png"]] autorelease];
        
        [tmp_view addSubview:tmp_view_backrgound];
		tmp_view.backgroundColor=[UIColor clearColor];
        
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,3, 320, 30)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kHelveticaBoldBFont size:18];
		tmp_headerLabel.text=@"   Settings";
		
		tmp_headerLabel.backgroundColor=[UIColor clearColor]; //[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		tmp_headerLabel.textColor=[UIColor blackColor];
		
		[tmp_view addSubview:tmp_headerLabel];
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return [tmp_view autorelease];
		
	}
	else 
	{
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
        
        UIImageView *tmp_view_backrgound=[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"darkgrey_bar.png"]] autorelease];

        [tmp_headerForSecondSection addSubview:tmp_view_backrgound];
		tmp_headerForSecondSection.backgroundColor=[UIColor clearColor];
		
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,3, 320, 30)] ;
		
		tmp_headerLabel.font=[UIFont  fontWithName:kHelveticaBoldBFont size:16];
		tmp_headerLabel.text=@"   About Tiny News";
		tmp_headerLabel.textColor=[UIColor blackColor];
		tmp_headerLabel.backgroundColor=[UIColor clearColor]; //[UIColor colorWithRed:0.76f green:0.76f blue:0.76f alpha:1.0];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return [tmp_headerForSecondSection autorelease];
		
        
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section==0) 
	{
		
//        if (indexPath.row==0) {
//            NSLog(@"for profile setting && row is %i",(indexPath.row));
//            ProfileSettingViewController * vc =[[ProfileSettingViewController alloc]initWithNibName:@"ProfileSettingViewController" bundle:[NSBundle mainBundle]];
//            [self.navigationController pushViewController:vc animated:YES ];
//            [vc release];
//            vc=nil;
//        }
       // else
            
        if(indexPath.row==0)
        {
            NSLog(@"for profile info setting && row is %i",(indexPath.row));
            ProfileinfoSettingViewController * vc=[[ProfileinfoSettingViewController alloc]initWithNibName:@"ProfileinfoSettingViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:vc animated:YES];
            [vc release];
            vc=nil;
        }
        /*else if(indexPath.row==2)
        {
            NSLog(@"for alert message setting && row is %i",(indexPath.row));
            m_AlertMessageSettingVC=[[AlertMessageSettingViewController alloc] initWithNibName:@"AlertMessageSettingViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:m_AlertMessageSettingVC animated:YES];
            [m_AlertMessageSettingVC release];
            m_AlertMessageSettingVC=nil;
        }*/
//		else if(indexPath.row==2)           //else if(indexPath.row==3)
//		{
//			l_privacyObj=[[PrivacySettingsViewController alloc] initWithNibName:@"PrivacySettingsViewController" bundle:[NSBundle mainBundle]];
//			[self.navigationController pushViewController:l_privacyObj animated:YES];
//			[l_privacyObj release];
//			l_privacyObj=nil;
//		}
//		else if(indexPath.row==3)        
//		{
//			ApplicationSettingsViewController * appSet=[[ApplicationSettingsViewController alloc] initWithNibName:@"ApplicationSettingsViewController" bundle:[NSBundle mainBundle]];
//			[self.navigationController pushViewController:appSet animated:YES];
//			[appSet release];
//			appSet=nil;
//		}
	}
	else if(indexPath.section==1)
	{
        /*
         US341 - Removing UserGuide and Feedback screens
		if (indexPath.row==0)
		{
			MyPageUserGuideViewController *MyPageUserGuide=[[MyPageUserGuideViewController alloc]initWithNibName:@"MyPageUserGuideViewController" bundle:[NSBundle mainBundle]];
			[self.navigationController pushViewController:MyPageUserGuide animated:YES ];
			[MyPageUserGuide release];
			MyPageUserGuide=nil;
            
		}
		else if(indexPath.row==1)
		{
			MyPageFeedBackViewController *MyPageFeedBack=[[MyPageFeedBackViewController alloc]initWithNibName:@"MyPageFeedBackViewController" bundle:[NSBundle mainBundle]];
			[self.navigationController pushViewController:MyPageFeedBack animated:YES ];
			[MyPageFeedBack release];
			MyPageFeedBack=nil;
		}
        
		else */ if (indexPath.row==0)
		{
			TOSViewController *temp_tosViewCtrl=[[TOSViewController alloc]init];
			temp_tosViewCtrl.m_strTypeOfData=@"1";
			[self presentModalViewController:temp_tosViewCtrl animated:YES];
			[temp_tosViewCtrl release];
			temp_tosViewCtrl=nil;
		}
		else if (indexPath.row==1)
		{
			TOSViewController *temp_tosViewCtrl=[[TOSViewController alloc]init];
			temp_tosViewCtrl.m_strTypeOfData=@"0";
			[self presentModalViewController:temp_tosViewCtrl animated:YES];
			[temp_tosViewCtrl release];
			temp_tosViewCtrl=nil;
			
		}
		else if (indexPath.row == 2) {
			
			//Bharat: 11/10/2011: Invoking webcall in a thread.
			signoutPopup.hidden = NO;

			/*
Bharat: 11/10/11: This code is being moved to connectionDidFinishLoading, as we wait on worker thread to finish logout web call.
			UINavigationController * tempCtrl = [l_appDelegate.tabBarController.viewControllers objectAtIndex:0];
			BigSalesAdsViewController * tempBigSalesCtrl = [tempCtrl.viewControllers objectAtIndex:0];
			
			
			if (tempBigSalesCtrl.l_isConnectionActive) {
				[tempBigSalesCtrl.l_theConnection cancel];
				tempBigSalesCtrl.l_isConnectionActive = FALSE;
			}
			
            l_appDelegate.isLogged = NO;
			l_appDelegate.m_loginHomeViewCtrl.isUserLoggedOut = TRUE;
			[l_appDelegate.tabBarController.view removeFromSuperview];
			l_appDelegate.userInfoDic = nil;
			l_appDelegate.isMyPageShow = YES;
			l_appDelegate.isFittingRoomShown = YES;
			*/
		}
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 
	return 30;
} 

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */
//- (IBAction)slideMenuButtonTouched
//{
//    menuWasPressed = YES;
//    [super slideMenuButtonTouched];
//}




#pragma mark -----------------

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    self.trackedViewName = @"Settings";

    
   // menuWasPressed=NO;
	//Bharat: 11/21/11: Added Application Settings to capture font
    //m_profileSetting=[[NSArray alloc] initWithObjects:@"Profile settings",@"Personal info settings",@"Privacy settings",nil];
    m_profileSetting=[[NSArray alloc] initWithObjects:@"Personal info settings",nil];

//    m_profileSetting=[[NSArray alloc] initWithObjects:@"Profile settings",@"Personal info settings",@"Alert message settings",@"Privacy settings",nil];
    /*
     US341 - Removing UserGuide and Feedback screens

	m_profileSetting1=[[NSArray alloc]initWithObjects:@"User guide",@"Give us feedback/report bugs",@"Privacy Policy",@"Terms of Use",@"Logout",nil];
     */
    m_profileSetting1=[[NSArray alloc]initWithObjects:@"Privacy Policy",@"Terms of Use",@"Sign out", nil];
    
    l_indicatorView=[LoadingIndicatorView SharedInstance];
	
	l_request=[[ShopbeeAPIs alloc] init];
    
 
    
    
	[super viewDidLoad];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (IBAction)signOut:(id)sender
{
    signoutPopup.hidden = YES;
    [self sendRequestForLogout];
}

- (IBAction)cancel:(id)sender
{
    signoutPopup.hidden = YES;
}


// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
	self.m_activityIndicator = nil;
	self.m_mutResponseData = nil;
    
    // none
}


- (void)dealloc {
    
	[m_ProfileSettingVC release];
    [m_ProfileinfoSettingVC release];
    [m_AlertMessageSettingVC release];
    
	[m_activityIndicator release];
    [m_mutResponseData release];
    [l_request release];
    [super dealloc];
}

-(void)sendRequestForLogout
{
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
        
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=logout&custid=%@",kServerUrl,customerID];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"logout url=[%@]",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//NSLog(@"device used:%@\n",[[UIDevice currentDevice] model]);
		
		if([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
		{
			NSLog(@"No need to send token message\n");
		}
		else
		{
			NSDictionary *tmp_deviceDetails=[NSDictionary dictionaryWithObjectsAndKeys:tnApplication.m_strDeviceId,@"did",tnApplication.m_strDeviceToken,@"deviceToken",nil];
			NSString *temp_strJson=[NSString stringWithFormat:@"devicedetails=%@",[tmp_deviceDetails JSONFragment]];
			[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		}
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"MenuViewController:sendRequestForLogout: Request sent to get data");
		}
		
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
	
}

#pragma mark -
#pragma mark Connection response methods

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseStatusCode = [httpResponse statusCode];
	
	NSArray * all = [NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
    NSLog(@"MenuViewController: didReceiveResponse: How many Cookies: %d", all.count);
	
	[[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:all forURL:[NSURL URLWithString:
																		  [NSString stringWithFormat:@"%@/salebynow/", kServerUrl]] mainDocumentURL:nil];
	
	NSArray * availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:
								  [NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
	
	NSLog(@"MenuViewController: didReceiveResponse: no. of cookies: %d",availableCookies.count);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory =[paths objectAtIndex:0];
	NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:@"cookieFile.plist"];
	NSDictionary *temp_dict;
	
	for (NSHTTPCookie *cookie in availableCookies)
	{
        temp_dict = [NSDictionary dictionaryWithObjectsAndKeys:
                     [NSString stringWithFormat:@"%@",kServerUrl], NSHTTPCookieOriginURL,
                     cookie.name, NSHTTPCookieName,
                     cookie.path, NSHTTPCookiePath,
                     cookie.value, NSHTTPCookieValue,
                     nil];
		[temp_dict writeToFile:writableStatePath atomically:YES];
        
		NSLog(@"MenuViewController: didReceiveResponse: [%@]",temp_dict);
        
		
	}
	
	NSLog(@"MenuViewController: didReceiveResponse: m_intResponseStatusCode=[%d]",m_intResponseStatusCode);
	
	[m_mutResponseData setLength:0];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[m_mutResponseData appendData:data];
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	self.view.userInteractionEnabled=TRUE;
	
	if(m_intResponseStatusCode==200)
	{
		NSLog(@"MenuViewController: connectionDidFinishLoading: response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if ([prefs boolForKey:@"fbRegistration"]) {
            [prefs setBool:NO forKey:@"fbRegistration"];
            [tnApplication removeOAuthData];
            [tnApplication.m_session logout];
            [prefs setBool:YES forKey:@"wasLogout"];
        }
        
        [prefs setBool:NO forKey:kRegistrationKey];
        tnApplication.isUserLoggedInAfterLoggedOutState=FALSE;
        [tnApplication logout];
    } else if (m_intResponseStatusCode == 401){
        NSLog(@"Logged out, ignored the 401 session expired");
        //[[NewsDataManager sharedManager] showErrorByCode:m_intResponseStatusCode fromSource:NSStringFromClass([self class])];
    } else {
		NSLog(@"MenuViewController: connectionDidFinishLoading: response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:@"Logout failure!" message:@"Wrong username/password. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[networkDownAlert show];
		[networkDownAlert release];
		networkDownAlert=nil;
	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	self.view.userInteractionEnabled=TRUE;
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
}


-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSLog(@"response code is %d",[responseCode intValue]);
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_indicatorView stopLoadingView];
	
	if([responseCode intValue]==200)
	{
        NSArray *tmpArray=[[tempString JSONValue]retain];
		NSLog(@"the array is %@",tmpArray);
		l_appDelegate.userInfoDic=[tmpArray objectAtIndex:0];
		
		[tmpArray release];
		tmpArray=nil;
		
		[tempString release];
		tempString=nil;
	}
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    
    
	
}

@end
