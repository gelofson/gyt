//
//  GetLocationAfterDistance.m
//  QNavigator
//
//  Created by softprodigy  on 22/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GetLocationAfterDistance.h"
#import "LoginViewController.h"
#import "QNavigatorAppDelegate.h"
#import "MyCLController.h"
#import "DBManager.h"

@implementation GetLocationAfterDistance

@synthesize m_locationManager;
@synthesize m_tmpLatitude;
@synthesize m_tmpLongitude;

float temp_lat;
float temp_long;

QNavigatorAppDelegate *l_appDelegate;

BOOL l_showPrompt;
BOOL l_flagUpdateLocation;


- (void)initializeMembers
{
	//l_appDelegate=(QNavigatorAppDelegate  *)[[UIApplication sharedApplication]delegate];
	
	if(!m_locationManager)
	{
		self.m_locationManager=[[CLLocationManager alloc]init];
		self.m_locationManager.distanceFilter = 600;
		self.m_locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
		self.m_locationManager.delegate = self; //
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
		[self.m_locationManager startUpdatingLocation];
		
		l_showPrompt=TRUE;
		
	}
	
}
/*
-(void)showUpdateAlert
{
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"User location updated!" message:@"Do you want to update application data?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO",nil]autorelease];
	[dataReloadAlert show];
}*/

#pragma mark -
#pragma mark Location Manager Methods

//Called when the location is updated
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
		
	temp_lat=oldLocation.coordinate.latitude;
	temp_long=oldLocation.coordinate.longitude;
	
	if(l_flagUpdateLocation==FALSE) //just to check for first time location update
	{
		m_tmpLatitude=newLocation.coordinate.latitude;
		m_tmpLongitude=newLocation.coordinate.longitude;
		
		temp_lat=newLocation.coordinate.latitude;
		temp_long=newLocation.coordinate.longitude;
		
	}
		
	l_flagUpdateLocation=TRUE;
	
	CLLocation *oldCLLocation = [[CLLocation alloc] initWithLatitude:m_tmpLatitude longitude:m_tmpLongitude];
	
	float tempDistance=[oldCLLocation distanceFromLocation:newLocation];
	
	NSLog(@"Distance i meters: %f", tempDistance);
	
	[oldCLLocation release];
	
	if(tempDistance >=10000 && l_showPrompt==TRUE) //prompt after 10 km
	{
		l_showPrompt=FALSE;
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"User location updated!" message:@"Do you want to update application data?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
		[dataReloadAlert show];
	}

		
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"fail to update location");
	
}

#pragma mark -
#pragma mark Alert view delegates

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if ([alertView.title isEqualToString:@"User location updated!"] && buttonIndex==1)
	{
		UIAlertView *temp_reloadAlert=[[[UIAlertView alloc]initWithTitle:@"Please wait!!" message:@"The application reloads data. This may take few minutes." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease];
		[temp_reloadAlert show];
		
		//if user presses yes to reload data then update location variables
		m_tmpLatitude=temp_lat;
		m_tmpLongitude=temp_long;
				
		l_appDelegate.isReloadingData=TRUE;
		l_showPrompt=TRUE;
		
		[[MyCLController sharedInstance].locationManager startUpdatingLocation];
		
	}
	else 
	{
		m_tmpLatitude=temp_lat;
		m_tmpLongitude=temp_long;
		
		l_appDelegate.isReloadingData=FALSE;
		l_showPrompt=TRUE;
	}
	
}


#pragma mark -
#pragma mark Login methods
-(void)showLoginScreenOnSessionExpire:(id)sender
{
	LoginViewController *temp_loginScreen=[[LoginViewController alloc]init];
	//temp_loginScreen.m_strUsername=@"";
	//temp_loginScreen.m_strPassword=@"";
	[sender presentModalViewController:temp_loginScreen animated:NO];
	[temp_loginScreen release];
	temp_loginScreen=nil;
	
}

// GDL: Changed everything below.

#pragma mark - memory management

-(void)dealloc {
    [m_locationManager release];
	
	[super dealloc];
}

@end