//
//  ViewMorePicsMyPage.m
//  QNavigator
//
//  Created by softprodigy on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewMorePicsMyPage.h"
#import<QuartzCore/QuartzCore.h>
#import "QNavigatorAppDelegate.h"
#import "PhotosLoadingView.h"
#import "Constants.h"
#import "ShopbeeAPIs.h"
#import "NewsDataManager.h"

@implementation ViewMorePicsMyPage

@synthesize m_nameLabel;

@synthesize m_pageControl;

@synthesize m_photoView;

@synthesize m_pgcntrlphotos;
@synthesize m_headlineArray;
@synthesize  m_value;

@synthesize m_lblDiscount;

@synthesize m_lblSale;

@synthesize m_msgTypeBuzzCase;

@synthesize m_leftButton;

@synthesize m_rightButton;
@synthesize m_photoscrollView;
@synthesize m_headLineLBL;
@synthesize whoWhatTextView;
@synthesize Whowhattitletext;
QNavigatorAppDelegate *l_appDelegate;

PhotosLoadingView* asyncImage2;
ShopbeeAPIs *l_request;

BOOL l_isConnectionActive;

NSURLConnection  *l_theConnection;

int temp_responseCode,counter;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{	
	counter=0;
	[super viewDidLoad];
    m_mutResponseData =[[NSMutableData alloc]init];
	[[m_photoscrollView layer]setCornerRadius:4.0f];
	[[m_photoscrollView layer] setMasksToBounds:YES];
	m_pageControl.currentPage=0;
    
	l_isConnectionActive=FALSE;
	[m_pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
	
    CGRect frame=CGRectMake(50, 25, 200, 200);
	//frame.size.width=270; frame.size.height=170;
//	frame.origin.x=5; frame.origin.y=5;
	
	asyncImage2 = [[[PhotosLoadingView alloc] initWithFrame:frame] autorelease];
	//NSLog(@"%@",[[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"imageUrls"]objectAtIndex:0]);
	
	NSMutableString *temp_strUrl;
	
	temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"photoUrl"]];
	
	NSLog(@"%@",temp_strUrl);
	NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
	
	[asyncImage2 loadImageFromURL:tempLoadingUrl tempImage:nil];
	[m_photoscrollView addSubview:asyncImage2];
    
    UIColor *color=[UIColor colorWithPatternImage:[UIImage imageNamed:@"lightgrey_bar.png"]];
    
    m_nameLabel.backgroundColor=color;
    

	
	NSLog(@"%@",[m_pgcntrlphotos objectAtIndex:m_value]);
	if([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"buzzId"] length]==0)
	{
		m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"productName"] ];
		m_lblDiscount.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"discountInfo"];
		m_lblSale.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"tagLine"];
		
	}
	else 
	{
        m_nameLabel.text = [[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"];
/*		if ([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"] caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame)
		{
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"]];
		} 
        else if ([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"] caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"]];
		}*/
		//m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"]];
		m_lblDiscount.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"bodyMessage"];
		m_lblSale.text=@"";
	}
    NSDictionary *tmp_dict=[[m_headlineArray objectAtIndex:0] valueForKey:@"wsBuzz"];
    m_headLineLBL.text=[tmp_dict objectForKey:@"headline"];
    CGFloat yHeight=0;
    whoWhatTextView.text=[self formatWhoWhatText:tmp_dict];
    Whowhattitletext.text=[self formatWhoWhattitle:tmp_dict];
    if ([whoWhatTextView.text length] > 0) 
    {
        [Whowhattitletext sizeToFit];
       
        Whowhattitletext.frame=CGRectMake(Whowhattitletext.frame.origin.x, Whowhattitletext.frame.origin.y, Whowhattitletext.frame.size.width, Whowhattitletext.contentSize.height);
        
        [whoWhatTextView sizeToFit];
        whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
        yHeight =whoWhatTextView.frame.size.height + 10;
        
    }
 else {
    whoWhatTextView.text = @"";
    Whowhattitletext.text = @"";
    m_headLineLBL.text = @"";
}
    [m_photoscrollView setContentSize:CGSizeMake(m_photoscrollView.frame.size.width, m_photoscrollView.frame.size.height+yHeight)];
    
	[m_pageControl setNumberOfPages:[m_pgcntrlphotos count]];
	[m_pageControl setCurrentPage:m_value];
    
    [self changepagecontrolImage];
    	NSString *temp_strUrlStr=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_pgcntrlphotos objectAtIndex:counter] valueForKey:@"photoUrl"]];
	//NSURL *tempLoadingUrlStr = [NSURL URLWithString:temp_strUrlStr];
	[self sendRequestToLoadImages:temp_strUrlStr];
	
}
-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle
{
    //    if ([[datatitle objectForKey:@"oped"] length] > 0)
    //        return [datatitle objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [datatitle objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@"•%@:\n",@"Who"];
    }
    data = [datatitle objectForKey:@"what"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"What"];
        
    }
    data = [datatitle objectForKey:@"when"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"When"];
    }
    data = [datatitle objectForKey:@"where"];
    if ([data length] > 0) {
        if ([data length]<=24)
        {
            [string appendFormat:@"•%@:\n",@"Where"];
        }
        else
        {
            [string appendFormat:@"•%@:\n\n",@"Where"];
        }        
    }
    data = [datatitle objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"How"];
        
    }
    data = [datatitle objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"Why"];
    }
    return string;
    
}

- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict
{
    
    if ([[dataDict objectForKey:@"oped"] length] > 0)
        return [dataDict objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [dataDict objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",data];
    }
    data = [dataDict objectForKey:@"what"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"when"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",[self dateReFormated:data]];
    }
    data = [dataDict objectForKey:@"where"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
        
    }
    return string;
}

-(NSString*)dateReFormated:(NSString*)date
{
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    if ([date intValue])
    {
        NSArray *mainarray=[date componentsSeparatedByString:@" "];
        NSArray *timearray=[[mainarray objectAtIndex:1]componentsSeparatedByString:@":"];
        NSString *timeformate=[NSString stringWithFormat:@"%@ %@:%@",[mainarray objectAtIndex:0],[timearray objectAtIndex:0],[timearray objectAtIndex:1]];
        NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
        [dateFormate setDateFormat:@"YYYY:mm:dd HH:mm"];
        NSDate *dateValue=[dateFormate dateFromString:timeformate];
        [dateFormate setDateFormat:@"MMM dd, HH:mm a"];
        [string appendFormat:@"%@",[dateFormate stringFromDate:dateValue]];
        [dateFormate release]; 
        //return string;
    }
    else
    {
        [string appendFormat:@"%@",date];  
    }
    return string;
}


-(void)viewDidAppear:(BOOL)animated
{
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark Custom pagecontrol image methos

-(void)changepagecontrolImage
{
    int i=1;
    //UIImageView *image;
    for (UIView *view in[m_pageControl subviews])
    {
        if ([view isKindOfClass:[UIImageView class]])
        {
           UIImageView* image=(UIImageView *)view;
            image.tag=i;
        }
        i++;
    }
    for(int i=0;i<[m_pgcntrlphotos count];i++)
    {
       UIImageView* image=(UIImageView *)[self.view viewWithTag:i+1];
        if (i==m_pageControl.currentPage)
        {
            [image setImage:[UIImage imageNamed:@"dark_dot.png"]];
        }
        else
        {
            [image setImage:[UIImage imageNamed:@"light_dot.png"]];
        }
    }

  
}

#pragma mark custom methods
- (IBAction) changePage:(id)sender{
	
	int page = m_pageControl.currentPage;
	NSLog(@"value is %i",page);
	  pageNO = page; // to get the current page on which the control is.

   [self changepagecontrolImage];

}


-(void)leftButtonAction
{
    //NSDictionary *l_leftdict;//18aug
	if(m_value>0)
	{
		m_value--;
		NSLog(@"the value for m_value is %i ",m_value);
	}
	if(m_value==0)
	{
		//m_leftButton.userInteractionEnabled=NO;
		m_leftButton.enabled=FALSE;
		
	}
	else 
	{
		//m_leftButton.userInteractionEnabled=YES;
		//m_rightButton.userInteractionEnabled=YES;
		m_leftButton.enabled=TRUE;
		m_rightButton.enabled=TRUE;
		
	}
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/123_%d.jpg",documentsDirectory,m_value];
	NSLog(@"tempEmail %@",tmpPath);
	NSFileManager *tempFileManager=[NSFileManager defaultManager];
	
	
	
	NSLog(@"the vale for value is %i ",m_value);
	int page = m_pageControl.currentPage;
	pageNO = page;
	
	if (pageNO>0)
	{
        pageNO--;
		
	}
	//l_leftdict=[m_pgcntrlphotos objectAtIndex:m_value]; //18aug
	
	NSMutableString *temp_strUrl;
	
	temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"photoUrl"]];
	
	NSLog(@"%@",temp_strUrl);
	NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
	
	if ([tempFileManager fileExistsAtPath:tmpPath])
	{
		[asyncImage2 loadImageFromURL:tempLoadingUrl tempImage:[UIImage imageWithContentsOfFile:tmpPath]];
	}
	else
	{
		[asyncImage2 loadImageFromURL:tempLoadingUrl tempImage:nil];
	}
    l_request=[[ShopbeeAPIs alloc]init];
    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"id"]intValue]];
	[l_request release];
    l_request=nil;
	
	if([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"buzzId"] length]==0)
	{
		
		m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"productName"]];
		m_lblDiscount.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"discountInfo"];
		m_lblSale.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"tagLine"];
		
	}
	else 
	{//messageType
        m_nameLabel.text = [[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"];
/*		if ([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"] caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame)
		{
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"]];
		} else if ([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"] caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"]];
		}*/
		//m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"]];
		m_lblDiscount.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"bodyMessage"];
		m_lblSale.text=@"";
		//m_nameLabel.text=@"buzz";
	}
	
	[m_pageControl setCurrentPage:pageNO];
   [self changepagecontrolImage];
}

-(IBAction) m_leftButtonAction // for changing the image in the image view as well as shifting the page control when the left arrow is clicked
{
    [self leftButtonAction];	
}

-(void)rightButtonAction
{
	//NSDictionary *l_rightdict;//18aug
	NSLog(@"the vale for value is %i ",m_value);
	
    
	if (m_value<([m_pgcntrlphotos count]-1 ) ) 
	{
		
		m_value++;
		NSLog(@"the value for mvalue is %i",m_value);
	}
	if (m_value==([m_pgcntrlphotos count]-1)) 
	{
		//m_rightButton.userInteractionEnabled=NO;
		m_rightButton.enabled=FALSE;
		
		
	}
	else 
	{
        //	m_rightButton.userInteractionEnabled=YES;
        //	m_leftButton.userInteractionEnabled=YES;
		m_rightButton.enabled=TRUE;
		m_leftButton.enabled=TRUE;
		
	}
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/123_%d.jpg",documentsDirectory,m_value];
	NSLog(@"tempEmail %@",tmpPath);
	NSFileManager *tempFileManager=[NSFileManager defaultManager];
	int page = m_pageControl.currentPage;
    
	pageNO = page;
	
    
	if (pageNO<[m_pageControl numberOfPages]) 
        
	{
        
		pageNO++;
		
	}
	//l_rightdict=[m_pgcntrlphotos objectAtIndex:m_value];//18aug 
    
	//CGRect frame;
    //	frame.size.width=100; frame.size.height=100;
    //	frame.origin.x=10; frame.origin.y=15;
    //	
    //	AsyncImageView* asyncImage = [[[AsyncImageView alloc] initWithFrame:frame] autorelease];
	//NSLog(@"%@",[[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"imageUrls"]objectAtIndex:0]);
	
	NSMutableString *temp_strUrl;
	
	temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"photoUrl"]];
	
	NSLog(@"%@",temp_strUrl);
	NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
	
	if ([tempFileManager fileExistsAtPath:tmpPath])
	{
		[asyncImage2 loadImageFromURL:tempLoadingUrl tempImage:[UIImage imageWithContentsOfFile:tmpPath]];
	}
	else 
	{
		[asyncImage2 loadImageFromURL:tempLoadingUrl tempImage:nil];
	}
	l_request=[[ShopbeeAPIs alloc]init];
    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"id"]intValue]];
	[l_request release];
    l_request=nil;

	
	if([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"buzzId"] length]==0)
	{
		m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"productName"]];
		m_lblDiscount.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"discountInfo"];
		m_lblSale.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"tagLine"];
		
	}
	else 
	{
        m_nameLabel.text = [[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"];
/*		if ([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"] caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame)
		{
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"]];
		} else if ([[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"] caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"]];
		} else if([[[m_pgcntrlphotos objectAtIndex:m_value] valueForKey:@"messageType"] caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
			m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"]];
		}*/
		//m_nameLabel.text=[NSString stringWithFormat:@"  %@",[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"messageType"]];
		m_lblDiscount.text=[[m_pgcntrlphotos objectAtIndex:m_value]valueForKey:@"bodyMessage"];//bodyMessage
		m_lblSale.text=@"";
	}
	[m_pageControl setCurrentPage:pageNO];
    
    [self changepagecontrolImage];
}

-(IBAction) m_rightButtonAction
{ // for changing the image in the image view as well as shifting the page control when the right arrow is clicked
    [self rightButtonAction];	
}

-(void)viewWillAppear:(BOOL)animated
{

	if ([m_pageControl currentPage]==0) 
	{
		//m_leftButton.userInteractionEnabled=NO;
		m_leftButton.enabled=FALSE;
		
	}
	 
    if ([m_pgcntrlphotos count]==1) 
	{
		//m_leftButton.userInteractionEnabled=NO;
		//m_rightButton.userInteractionEnabled=NO;
		m_leftButton.enabled=FALSE;
		m_rightButton.enabled=FALSE;
		
	}
	 
    if(m_pageControl.currentPage==([m_pgcntrlphotos count]-1)) 
	{
		//m_rightButton.userInteractionEnabled=NO;
		m_rightButton.enabled=FALSE;
		
	}

    UISwipeGestureRecognizer *recogniser=[[UISwipeGestureRecognizer alloc] init]; // add a swipe gesture recogniser
	recogniser.direction=UISwipeGestureRecognizerDirectionLeft;
	[recogniser addTarget:self action:@selector(handleSwipeFrom:)];
	[self.view addGestureRecognizer:recogniser];
	[recogniser release];
	recogniser=nil;
	
	UISwipeGestureRecognizer *recogniser1=[[UISwipeGestureRecognizer alloc] init];
	recogniser1.direction=UISwipeGestureRecognizerDirectionRight;
	[recogniser1 addTarget:self action:@selector(handleSwipeFrom:)];
	[self.view addGestureRecognizer:recogniser1];
	[recogniser1 release];
	recogniser1=nil;

}

-(void)viewWillDisappear:(BOOL)animated
{
	if (l_isConnectionActive)
	{
		[l_theConnection cancel];
		l_isConnectionActive=FALSE;
	}
	
	//remove the saved images to avoid inconsistency
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
		
	//delete the category sales images saved in temproray directory on applcation termination
	//NSError **tempErr;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
	//directoryContentsAtPath:tempPath];
	NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	if (onlyJPGs) 
	{	
		for (int i = 0; i < [onlyJPGs count]; i++) {
			//NSLog(@"Directory Count: %i", [onlyJPGs count]);
			NSString *contentsOnly = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [onlyJPGs objectAtIndex:i]];
			[fileManager removeItemAtPath:contentsOnly error:nil];
		}
	}
	
	
}

-(IBAction) m_goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)sendRequestToLoadImages:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:25.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
		
		
		//[theRequest setHTTPBody:[imageUrl dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if(l_isConnectionActive)
			[l_theConnection cancel];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		l_isConnectionActive=TRUE;
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		l_isConnectionActive=FALSE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}

#pragma mark-
#pragma mark buzzy method

-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {		
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
		
		
		NSDictionary *tmp_dict=[[tmp_array objectAtIndex:0] valueForKey:@"wsBuzz"];
        m_headLineLBL.text=[tmp_dict objectForKey:@"headline"];
        CGFloat yHeight=0;
        whoWhatTextView.text=[self formatWhoWhatText:tmp_dict];
        Whowhattitletext.text=[self formatWhoWhattitle:tmp_dict];
        if ([whoWhatTextView.text length] > 0) 
        {
            [Whowhattitletext sizeToFit];
            
            Whowhattitletext.frame=CGRectMake(Whowhattitletext.frame.origin.x, Whowhattitletext.frame.origin.y, Whowhattitletext.frame.size.width, Whowhattitletext.contentSize.height);
            
            [whoWhatTextView sizeToFit];
            whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
            yHeight =whoWhatTextView.frame.size.height + 10;
            
        }
        else {
            whoWhatTextView.text = @"";
            Whowhattitletext.text = @"";
            m_headLineLBL.text = @"";
        }
        [m_photoscrollView setContentSize:CGSizeMake(m_photoscrollView.frame.size.width, m_photoscrollView.frame.size.height+yHeight)];
        
        NSLog(@"response string: %@",tmp_dict);
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
        
}


#pragma mark -
#pragma mark connection methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[m_mutResponseData setLength:0];
	
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	temp_responseCode= [httpResponse statusCode];
	NSLog(@"%d",temp_responseCode);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[m_mutResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	l_isConnectionActive=FALSE;
	
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	self.view.userInteractionEnabled=TRUE;
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];

}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	l_isConnectionActive=FALSE;
	
	if(m_mutResponseData!=nil&&temp_responseCode==200)// typeofresponse=0: response is in json, to get data
	{
		//NSLog(@"%@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
		 
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
		NSLog(@"Doc Directory______Path_____%@",pathDoc);
		
		
		NSMutableString *tempEmail=[NSMutableString stringWithFormat:@"%@/123_%d.jpg",pathDoc,counter];
		NSLog(@"tempEmail %@",tempEmail);
		
		//[l_indicatorView startLoadingView:self];
		
		[m_mutResponseData writeToFile:tempEmail atomically:YES];
		[temp_string release];
		temp_string=nil;
		
		counter++;
		if (counter<[m_pgcntrlphotos count]) 
		{
			
			NSString *temp_strUrlStr=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_pgcntrlphotos objectAtIndex:counter] valueForKey:@"photoUrl"]];
			
			[self sendRequestToLoadImages:temp_strUrlStr];
		}
	}
	
		
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_lblSale = nil;
    self.m_lblDiscount = nil;
    self.m_nameLabel = nil;
    self.m_pageControl = nil;
    self.m_photoView = nil;
    self.m_leftButton = nil;
    self.m_rightButton = nil;
    self.m_photoscrollView=nil;
    self.m_headLineLBL=nil;
    self.whoWhatTextView=nil;
    self.Whowhattitletext=nil;
}

- (void)dealloc {
	[m_nameLabel release];
	[m_lblSale release];
	[m_lblDiscount release];
	[m_pageControl release];
	[m_photoView release];
    [m_pgcntrlphotos release];
    [m_msgTypeBuzzCase release];
	[m_mutResponseData release];	
	[m_leftButton release];
	[m_rightButton release];
    [m_photoscrollView release];
    [m_headlineArray release];
    [m_headLineLBL release];
    [whoWhatTextView release];
    [Whowhattitletext release];
    [super dealloc];
}

#pragma mark swipe methods
- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	if(recognizer.direction==UISwipeGestureRecognizerDirectionRight)
	{
		[self leftButtonAction];
	}
	else
	{
		[self rightButtonAction];
	}
	
}

@end
                                                                                                                                                                                                                                                                                