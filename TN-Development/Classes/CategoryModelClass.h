//
//  CategoryModelClass.h
//  QNavigator
//
//  Created by Soft Prodigy on 27/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CategoryModelClass : NSObject 
{
	NSString *m_strTitle;
	NSString *m_strId;
	NSString *m_strDiscount;
	NSString *m_strAddress;
	NSString *m_strDealUrl;
	NSString *m_strStoreWebsite;
	NSString *m_strStartRedemptionAt;
	NSString *m_strEndRedemptionAt;
	NSString *m_strOverview;
	NSString *m_strImageUrl;
	NSString *m_strImageUrl2;
	NSString *m_strImageUrl3;
	NSString *m_strLatitude;
	NSString *m_strLongitude;
	NSString *m_strMallImageUrl;
	NSString *m_strDistance;
	NSString *m_strProductName;
	NSString *m_strStoreHours;
	NSString *m_strBrandName;
	NSString *m_strDescription;
	UIImage *m_imgItemImage;
	UIImage *m_imgItemImage2;
	UIImage *m_imgItemImage3;
	
	NSString *m_strPhoneNo;
}

@property(nonatomic,retain)NSString *m_strDescription;
@property(nonatomic,retain)NSString *m_strTitle;
@property(nonatomic,retain)NSString *m_strDiscount;
@property(nonatomic,retain)NSString *m_strAddress;
@property(nonatomic,retain)NSString *m_strStoreWebsite;
@property(nonatomic,retain)NSString *m_strEndRedemptionAt;
@property(nonatomic,retain)NSString *m_strStartRedemptionAt;
@property(nonatomic,retain)NSString *m_strDealUrl;
@property(nonatomic,retain)NSString *m_strOverview;
@property(nonatomic,retain)NSString *m_strImageUrl;
@property(nonatomic,retain)NSString *m_strLatitude;
@property(nonatomic,retain)NSString *m_strLongitude;
@property(nonatomic,retain)NSString *m_strMallImageUrl;
@property(nonatomic,retain)NSString *m_strImageUrl2;
@property(nonatomic,retain)NSString *m_strImageUrl3;
@property(nonatomic,retain)NSString *m_strDistance;
@property(nonatomic,retain)NSString *m_strProductName;
@property(nonatomic,retain)NSString *m_strStoreHours;
@property(nonatomic,retain)NSString *m_strBrandName;
@property(nonatomic,retain)UIImage *m_imgItemImage;
@property(nonatomic,retain)UIImage *m_imgItemImage2;
@property(nonatomic,retain)UIImage *m_imgItemImage3;
@property(nonatomic,retain)NSString *m_strId;

@property(nonatomic,retain)NSString *m_strPhoneNo;

@end
