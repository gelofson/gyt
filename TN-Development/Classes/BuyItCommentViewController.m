//
//  BuyItCommentViewController.m
//  QNavigator
//
//  Created by softprodigy on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuyItCommentViewController.h"
#import<QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"FittingRoomUseCasesViewController.h"
#import"LoadingIndicatorView.h"
#import"ShopbeeAPIs.h"
#import"FittingRoomMoreViewController.h"
#import"AsyncImageView.h"
#import"QNavigatorAppDelegate.h"

#import "NewsDataManager.h"

@implementation BuyItCommentViewController

FittingRoomUseCasesViewController *l_Obj;

ShopbeeAPIs *l_requestObj;

LoadingIndicatorView *l_buyItCommentIndicatorView;

QNavigatorAppDelegate *l_appDelegate;

@synthesize m_CallBackViewController;

@synthesize m_textView;

@synthesize m_MainArray;

@synthesize m_PersonName;

@synthesize m_Subject;

@synthesize m_HeaderLabel;

@synthesize m_Dictionary;

@synthesize m_FavourString;
@synthesize m_personImage;

//
//@synthesize m_Imageview;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidAppear:(BOOL)animated
{
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[m_textView.layer  setCornerRadius:4.0f];
	[m_textView.layer setMasksToBounds:YES];
	m_textView.font=[UIFont fontWithName:kFontName size:14];
        
	NSDictionary *tmp_dict = [[m_MainArray objectAtIndex:0] valueForKey:@"wsMessage"];
    
    // GDL: m_Type is not retained. I was releasing it.
	m_Type=[tmp_dict valueForKey:@"messageType"];
    
	m_messageId=[[tmp_dict valueForKey:@"id"] intValue];
	
	m_Subject.text=[tmp_dict valueForKey:@"bodyMessage"];
    m_Subject.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
	m_PersonName.text=[[m_MainArray objectAtIndex:0] valueForKey:@"originatorName"];
    m_PersonName.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	m_UserID=[prefs valueForKey:@"userName"];
    
    // GDL: Was getting crash here. Sending isEqualToString message to deallocated instance.
    // GDL: That's because I was releasing m_Type in dealloc without retaining it here.
    m_HeaderLabel.font = [UIFont fontWithName:kMyriadProBoldFont size:19];
	l_buyItCommentIndicatorView=[LoadingIndicatorView SharedInstance];
	m_Dictionary=[[NSMutableDictionary alloc] init];
//	AsyncImageView* SenderImage = [[[AsyncImageView alloc]
//									initWithFrame:CGRectMake(14,95,85,85)] autorelease];
//	//Bharat: 11/23/11: DE13 - Show image2 first
//	NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_MainArray objectAtIndex:0]valueForKey:@"productThumbImageUrl"]];
//	id wsProductDictionary = [[m_MainArray objectAtIndex:0] valueForKey:@"wsProduct"];
//	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
//		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
//		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
//				([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
//			temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
//		}	
//	}

//	NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,85,85]];
    
    senderImage = [[UIImageView alloc]
                                    initWithFrame:CGRectMake(14,95,85,85)] ;
	//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
	//[SenderImage loadImageFromURL:tempLoadingUrl ];
	[self.view addSubview:senderImage];
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [senderImage setImage:self.m_personImage];
}
#pragma mark -
#pragma mark Custom methods
-(void)doneClick{
	[(UIButton*)[self.view viewWithTag:1] setUserInteractionEnabled:YES];
	[(UIButton*)[self.view viewWithTag:2] setUserInteractionEnabled:YES];
	[m_textView resignFirstResponder];	
	[self animateViewDownward];
	
}
-(IBAction) goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];	
}

-(IBAction) BtnSendAction
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	//l_ViewRequest=[[FittingRoomMoreViewController alloc] init];
	NSString *ResponseWords=[m_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([ResponseWords isEqualToString:@""]) 
	{
		[self showAlertView:@"" alertMessage:@"Please enter your comments." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else 
	{
		[m_Dictionary setValue:ResponseWords forKey:@"responseWords"];
		[m_Dictionary setObject:[NSNumber numberWithInt:m_messageId] forKey:@"messageId"];
		//[m_Dictionary setObject:m_Type forKey:@"type"];
		[m_Dictionary setObject:m_FavourString forKey:@"isFavour"];
		[l_buyItCommentIndicatorView startLoadingView:self];
		[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserID msgresponse:m_Dictionary];
		
	}
		
	//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Your message sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alert  show];
//	[alert setTag:1];
//	[alert release];
}
-(void)addTopBarOnKeyboard
{	
	[(UIButton*)[self.view viewWithTag:1] setUserInteractionEnabled:NO];
	[(UIButton*)[self.view viewWithTag:2] setUserInteractionEnabled:NO];
	if(doneBackground)
	{
		[doneBackground removeFromSuperview];
		[doneBackground release];
		doneBackground=nil;
	}
	if(doneButton)
	{
		[doneButton removeFromSuperview];
		[doneButton release];
		doneButton=nil;
	}
	
	doneBackground=[[UIImageView alloc]initWithFrame:CGRectMake(0,285,320,40)];
	//[doneBackground setTag:1001];
	doneBackground.image=[UIImage imageNamed:@"bar-for-wheel.png"];
	[self.view addSubview:doneBackground];
	
	doneButton = [[UIButton alloc]initWithFrame:CGRectMake(255,290, 54, 28)];
	//[doneButton retain];
//	[doneButton setTag:1002];
	[doneButton setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
	[doneButton addTarget:self action:@selector(doneClick)
		 forControlEvents:UIControlEventTouchUpInside];	
	[self.view addSubview:doneButton];
	
}
-(IBAction)BtnCancelAction{
	[self.navigationController popToViewController:m_CallBackViewController animated:YES];
    //[self showAlertView:nil alertMessage:@"Do you want to send your vote anyway?" tag:2 cancelButtonTitle:@"NO" otherButtonTitles:@"OK"];
}

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles ,nil];
	tempAlert.tag=Tagvalue;
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

#pragma mark Animation methods

-(void)animateViewUpward
{
	[UIView beginAnimations: @"upView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,-80, 320,460);
	[UIView commitAnimations];
}

-(void)animateViewDownward
{
	[UIView beginAnimations: @"downView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,0, 320,460);
	[UIView commitAnimations];
	doneButton.hidden=YES;
	doneBackground.hidden=YES;
}
#pragma mark -
#pragma mark call back methods
-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_buyItCommentIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:1 cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
	}
	else 
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

	[tempString release];
	tempString=nil;
}


#pragma mark -
#pragma mark textView delegate
//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//	//[self addTopBarOnKeyboard];
//	//[self animateViewUpward];
//}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
	[self addTopBarOnKeyboard];	
	[self animateViewUpward];
	return YES;
	
}
-(BOOL) textViewShouldEndEditing:(UITextView *)textView
{
	[self animateViewDownward];
	[m_textView resignFirstResponder];
	return YES;
}

// Override to allow orientations other than the default portrait orientation.
/*- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
*/
 
#pragma mark -
#pragma mark alert view delegates
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (alertView.tag==1) {
						
		if (buttonIndex==0) 
		{
			//l_Obj=[[FittingRoomUseCasesViewController alloc] init];
			//[self.navigationController pushViewController:l_Obj animated:YES];
			//[l_Obj release];
			//l_Obj=nil;
			[self.navigationController popToViewController:m_CallBackViewController animated:YES];
		}
	}
	if (alertView.tag==2) {
		if (buttonIndex==0) {
			[self.navigationController popViewControllerAnimated:YES];
		}
		else if(buttonIndex==1)
		{
			[m_Dictionary setValue:@"" forKey:@"responseWords"];
			[m_Dictionary setValue:m_Type forKey:@"type"];
			[m_Dictionary setObject:m_FavourString forKey:@"isFavour"];
			[m_Dictionary setValue:[NSNumber numberWithInt:m_messageId]  forKey:@"messageId"];
			[l_buyItCommentIndicatorView startLoadingView:self];
			[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserID msgresponse:m_Dictionary];
		}
	}
	if (alertView.tag==3) {
		if (buttonIndex==0) {
			[self.navigationController popToRootViewControllerAnimated:YES];
		}
	}
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_textView = nil;
    self.m_PersonName = nil;
    self.m_Subject = nil;
    self.m_MainArray = nil;
    self.m_HeaderLabel = nil;
}


- (void)dealloc {
    [m_textView release];
    [doneBackground release];
    [doneButton release];
    [m_MainArray release];
    [m_PersonName release];
    [m_Subject release];
    //[m_UserID release];
    [m_Dictionary release];
    [m_personImage release];
    // GDL: m_Type is not retained. It is retained by m_MainArray
    //[m_Type release];
    [senderImage release];
    [m_FavourString release];
    [m_CallBackViewController release];
    
    [super dealloc];
}


@end
