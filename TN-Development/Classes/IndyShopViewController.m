//
//  IndyShopViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 30/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "IndyShopViewController.h"
#import "QNavigatorAppDelegate.h"
#import "CategoryModelClass.h"
#import "Constants.h"
#import "SBJSON.h"
#import "ATIconDownloader.h"
#import "AuditingClass.h"
#import "StreetMallMapViewController.h"
#if 0 //USE_PREGROUPON_INDY_SHOP_IMPLEMENTATION
#import "WhereBox.h"
#else
#import "GrouponWhereBox.h"
#endif
#import "ShopbeeAPIs.h"
#import "LoadingIndicatorView.h"
#import "AsyncImageView.h"
#import "NewsDataManager.h"
// GDL
#import "UISearchBar+ContentInset.h"

@implementation IndyShopViewController

@synthesize m_tableView;
@synthesize m_moreDetailView;
@synthesize m_activityIndicator;
@synthesize m_mutIndyShopResponseData;
@synthesize m_theConnection;
@synthesize m_isConnectionActive;
@synthesize m_isMallMapConnectionActive;
@synthesize m_mallMapConnection;
@synthesize m_locationManager;
@synthesize m_intResponseCode;
@synthesize m_scrollView;
@synthesize m_lblCaption;
@synthesize isCurrentIndyShopList;
@synthesize m_searchBar;
@synthesize m_tblSearchResults;
@synthesize m_exceptionPage;


CategoryModelClass *l_categoryModelObj;

int pageNumber;
int totalPages;




CustomTopBarView *l_customView;

CLLocation *l_cllocation;




NSMutableArray *l_arrSearchResults;

QNavigatorAppDelegate *l_appDelegate;

CategoryModelClass *l_objIndyShopModel;

//temp variables
NSString *temp_strCity;
NSString *temp_strState;
NSString *temp_strZip;
NSString *temp_strAdd2;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

#pragma mark -
#pragma mark UIView methods

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	l_appDelegate.m_SelectedMainPageClassObj=self;
	l_appDelegate.m_intCurrentView=1;//1 : type of view = indy shop
	
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	//send request to load data when session resumes after expiration
	if(l_appDelegate.m_isDataReloadOnSessionExpire==YES)
	{
		l_appDelegate.m_isDataReloadOnSessionExpire=NO;
		[self sendRequestForIndyShopData:l_appDelegate.m_intIndyPageNumber];	
		
	}
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	m_tblSearchResults.hidden=YES;
	
	self.m_locationManager = [[CLLocationManager alloc] init];
	self.m_locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
	self.m_locationManager.distanceFilter = 80.0;
	

	//Bharat: 11/19/11: Moving search bar by 8 pixels
	//m_moreDetailView=[[UIView alloc]initWithFrame:CGRectMake(8, 135, 305,230)];//13,142,295,195)];
	m_moreDetailView=[[UIView alloc]initWithFrame:CGRectMake(8, 127, 305,238)];//13,142,295,195)];
	m_moreDetailView.backgroundColor=[UIColor colorWithRed:0 green:.51f blue:.57f alpha:1.0];
	
	m_isConnectionActive=FALSE;
	
	//Bharat: 11/9/2011: Changing font of caption to GillSans-Bold
	m_lblCaption.font=[UIFont fontWithName:kFontName size:16];
	//m_lblCaption.font=[UIFont fontWithName:kGillSansFont size:16];
	
	m_mutIndyShopResponseData=[[NSMutableData alloc] init];
	l_arrSearchResults=[[NSMutableArray alloc]init];
	
	/* search bar background setting */


	/* Bharat: Comment out search bars on sales page, and replace with grey bar 
	 	(to expunge a source of bugs we cannot fix related to search) 
    	Bharat: 11/8/2011: extending width of search bar to beyond display region by 100 pixels
     	and then will set the contentInset to non-displayable region, thus masking the search bar.
		Resetting the width back to 320 will make the search-bar appear as before.	
	 */
	//m_searchBar.frame=CGRectMake(0, 99, 320, 32);
	//Bharat: 11/19/11: Moving search bar by 8 pixels
    //m_searchBar.frame=CGRectMake(0, 99, 320+200, 32);
    m_searchBar.frame=CGRectMake(0, 91, 320+200, 32);

	UITextField *searchField = nil;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	
    //Bharat: 11/8/2011: setting the contentInset to (frameWidth - 100)
    // If frameWidth is greater than (320+100), then search bar will be out of visible area to the right.
	//[m_searchBar setContentInset:UIEdgeInsetsMake(5, 220, 5, 5)];
    [m_searchBar setContentInset:UIEdgeInsetsMake(5, m_searchBar.frame.size.width-100, 5, 5)]; // outside

	[m_searchBar setText:@""];
	//Bharat: 11/9/2011: Let Caption field occupy full width relative to the width of the m_searchBar object.
	// This will help defer auto-shrink of font-size till limit exceeds furthure
	CGRect aFrame = m_lblCaption.frame;
	aFrame.size.width = m_searchBar.frame.size.width-200;
	m_lblCaption.frame = aFrame;
	
	/***********************************/

	//l_appDelegate.m_intCurrentView.tag=50;
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed following two methods.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_exceptionPage = nil;
    self.m_tblSearchResults = nil;
    self.m_lblCaption = nil;
    self.m_searchBar = nil;
    self.m_tableView = nil;
    self.m_activityIndicator = nil;
}

- (void)dealloc {
    
    [m_tableView release];
    [m_moreDetailView release];
    [m_activityIndicator release];
    [m_mutIndyShopResponseData release];
    
    // This is autoreleased
    //[m_theConnection release];
    
    // I expect this was already released.
    //[m_mallMapConnection release];
    
    [m_locationManager release];
    [m_scrollView release];
    [m_searchBar release];
    [m_lblCaption release];
    [m_tblSearchResults release];
    [m_exceptionPage release];
    
    [super dealloc];
}

#pragma mark Custom Methods
-(void)btnLookInsideAction:(int)tagg
{
	l_objIndyShopModel=[l_appDelegate.m_arrIndyShopData objectAtIndex:tagg];//[sender tag]];
	
	//[self.m_locationManager startUpdatingLocation];
			
	//------------------------------------------------------------
	AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
	[temp_objAudit initializeMembers];
	NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
	[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
	
	if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
	{
		//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Look inside button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",l_objIndyShopModel.m_strId,@"productId",nil];
		
		NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
								 @"Look inside button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",l_objIndyShopModel.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
		
		[temp_objAudit.m_arrAuditData addObject:temp_dict];
		//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
	}
	
	if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
	{
		[temp_objAudit sendRequestToSubmitAuditData];
	}
	//------------------------------------------------------------
	
	l_appDelegate.m_intIndyShopIndex=tagg;//[sender tag];
		
	[m_tableView setUserInteractionEnabled:FALSE];
	
	for(UIView *temp_view in m_moreDetailView.subviews)
	{
		[temp_view removeFromSuperview];
	}
	
	UIButton *temp_btnClose=[UIButton buttonWithType:UIButtonTypeCustom];
	[temp_btnClose setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
	[temp_btnClose addTarget:self action:@selector(btnCloseAction:) forControlEvents:UIControlEventTouchUpInside];
	[temp_btnClose setFrame:CGRectMake(260,-10,50,50)];
	[m_moreDetailView addSubview:temp_btnClose];
	
	UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(5,5,90,90)];
	//[temp_imgView setImage:l_objIndyShopModel.m_imgItemImage]; //@"clothes.png"]];
	[temp_imgView setImage:[UIImage imageNamed:@"loading.png"]];
	temp_imgView.contentMode=UIViewContentModeScaleAspectFit;
	
	if(l_objIndyShopModel.m_imgItemImage3!=nil)
	{
		[temp_imgView setImage:l_objIndyShopModel.m_imgItemImage3];
	}
	else if(l_objIndyShopModel.m_imgItemImage3==nil)
	{
		if(l_objIndyShopModel.m_imgItemImage!=nil)
		{
			[temp_imgView setImage:l_objIndyShopModel.m_imgItemImage];
			l_objIndyShopModel.m_imgItemImage3=l_objIndyShopModel.m_imgItemImage;
			[l_appDelegate.m_arrIndyShopData replaceObjectAtIndex:tagg withObject:l_objIndyShopModel];
		}
		else {
			[temp_imgView setImage:[UIImage imageNamed:@"no-image"]];
		}

	}
	
	[m_moreDetailView addSubview:temp_imgView];
	[temp_imgView release];
	temp_imgView=nil;
	
	UILabel *temp_lblBrandName=[[UILabel alloc]initWithFrame:CGRectMake(102,10,150,20)];
	//[temp_lblBrandName setText:l_objIndyShopModel.m_strBrandName];
	
	if(l_objIndyShopModel.m_strBrandName.length>0)
		[temp_lblBrandName setText:l_objIndyShopModel.m_strBrandName];
	else
		[temp_lblBrandName setText:@"Brand: N/A"];
	
	[temp_lblBrandName setTextColor:[UIColor whiteColor]];
	[temp_lblBrandName setBackgroundColor:[UIColor clearColor]];
	[temp_lblBrandName setFont:[UIFont fontWithName:kArialBoldFont size:16]];
	[m_moreDetailView addSubview:temp_lblBrandName];
	[temp_lblBrandName release];
	temp_lblBrandName=nil;
	
	UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(102,38,150,20)]; //190,5,85,20)];
	[temp_lblDiscount setText:l_objIndyShopModel.m_strDiscount]; //@"10% discount"];
	[temp_lblDiscount setTextColor:[UIColor yellowColor]];
	[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
	[temp_lblDiscount setFont:[UIFont fontWithName:kFontName size:16]];
	[m_moreDetailView addSubview:temp_lblDiscount];
	[temp_lblDiscount release];
	temp_lblDiscount=nil;
	
	//-------calculate distance-------
	l_cllocation=[[CLLocation alloc]initWithLatitude:[l_objIndyShopModel.m_strLatitude floatValue] longitude:[l_objIndyShopModel.m_strLongitude floatValue]];
	CLLocation *tempCurrentCL=[[CLLocation alloc] initWithLatitude:[l_appDelegate.m_objGetCurrentLocation m_latitude] longitude:[l_appDelegate.m_objGetCurrentLocation m_longitude]];
	
	float tempDistance=[l_cllocation distanceFromLocation:tempCurrentCL];
	
	NSString *temp_strDistance;
	
	if (tempDistance > 400) {
		temp_strDistance=[NSString stringWithFormat:@"%.2f miles away",tempDistance * 0.000622f];
	}
	else {
		temp_strDistance=[NSString stringWithFormat:@"%.0f steps away",tempDistance];
	}

	[l_cllocation release];
	[tempCurrentCL release];
	
	//---------------------------------
	
	UIButton *temp_btnMapIt=[UIButton buttonWithType:UIButtonTypeCustom];
	[temp_btnMapIt setImage:[UIImage imageNamed:@"map_it.png"] forState:UIControlStateNormal];
	[temp_btnMapIt addTarget:self action:@selector(btnMapItAction:) forControlEvents:UIControlEventTouchUpInside];
	[temp_btnMapIt setFrame:CGRectMake(102,52,50,53)];
	[m_moreDetailView addSubview:temp_btnMapIt];
	
	UILabel *temp_lblDistance=[[UILabel alloc]initWithFrame:CGRectMake(160,68,140,20)];
	[temp_lblDistance setText:temp_strDistance];//[m_locationManager distanceToLocation:l_cllocation]];
	//[m_locationManager distanceAndDirectionTo:l_cllocation]];
	//[NSString stringWithFormat:@"(%.2f meters away)",([l_objCatModel.m_strDistance floatValue] * 1609.344f)]];
	[temp_lblDistance setTextColor:[UIColor yellowColor]];
	[temp_lblDistance setBackgroundColor:[UIColor clearColor]];
	[temp_lblDistance setFont:[UIFont fontWithName:kFuturaItalicFont size:16]];
	[m_moreDetailView addSubview:temp_lblDistance];
	[temp_lblDistance release];
	temp_lblDistance=nil;
	//-------------
	
	UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,97,280,20)];
	[temp_lblTitle setText:l_objIndyShopModel.m_strTitle];
	[temp_lblTitle setTextColor:[UIColor whiteColor]];
	[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
	[temp_lblTitle setFont:[UIFont fontWithName:kArialBoldFont size:15]];
	[m_moreDetailView addSubview:temp_lblTitle];
	[temp_lblTitle release];
	temp_lblTitle=nil;
	
	
	//---------------------------scrollview------------------------------------
	m_scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(5,115,285,110)];
	
	//m_scrollView.delegate = self;
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	
	//m_scrollView.delaysContentTouches=NO;
	//m_scrollView.contentSize = CGSizeMake(52,53);
	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	UIImageView *temp_imgViewAddress=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 11, 10)];
	temp_imgViewAddress.image=[UIImage imageNamed:@"address_icon.png"];
	[m_scrollView addSubview:temp_imgViewAddress];
	[temp_imgViewAddress release];
	temp_imgViewAddress=nil;
	
	UILabel *temp_lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(20,0,250,40)];
	[temp_lblAddress setText:l_objIndyShopModel.m_strAddress];
	[temp_lblAddress setTextColor:[UIColor whiteColor]];
	[temp_lblAddress setNumberOfLines:2];
	[temp_lblAddress setBackgroundColor:[UIColor clearColor]];
	[temp_lblAddress setFont:[UIFont fontWithName:kFontName size:13]];
	[m_scrollView addSubview:temp_lblAddress];
	[temp_lblAddress release];
	temp_lblAddress=nil;
	
	UIImageView *temp_imgViewPhone=[[UIImageView alloc]initWithFrame:CGRectMake(5, 41, 8, 12)];
	temp_imgViewPhone.image=[UIImage imageNamed:@"phone_icon.png"];
	[m_scrollView addSubview:temp_imgViewPhone];
	[temp_imgViewPhone release];
	temp_imgViewPhone=nil;
	
	UILabel *temp_lblPhone=[[UILabel alloc]initWithFrame:CGRectMake(20,38,275,20)];
	
	if(l_objIndyShopModel.m_strPhoneNo.length>0)
	{
		[temp_lblPhone setText:l_objIndyShopModel.m_strPhoneNo];
	}
	else 
	{
		[temp_lblPhone setText:@"Phone: N/A"];
	}
	
	[temp_lblPhone setTextColor:[UIColor whiteColor]];
	[temp_lblPhone setBackgroundColor:[UIColor clearColor]];
	[temp_lblPhone setFont:[UIFont fontWithName:kFontName size:13]];
	[m_scrollView addSubview:temp_lblPhone];
	[temp_lblPhone release];
	temp_lblPhone=nil;
	
	UIImageView *temp_imgViewHours=[[UIImageView alloc]initWithFrame:CGRectMake(5, 61, 10, 10)];
	temp_imgViewHours.image=[UIImage imageNamed:@"hours_icon.png"];
	[m_scrollView addSubview:temp_imgViewHours];
	[temp_imgViewHours release];
	temp_imgViewHours=nil;
	
	UILabel *temp_lblHours=[[UILabel alloc]initWithFrame:CGRectMake(20,56,275,20)];
	if (l_objIndyShopModel.m_strStoreHours.length>0)
	{
		[temp_lblHours setText:l_objIndyShopModel.m_strStoreHours];
	}
	else {
		[temp_lblHours setText:@"Store hours: N/A"];
	}
	
	[temp_lblHours setTextColor:[UIColor whiteColor]];
	[temp_lblHours setBackgroundColor:[UIColor clearColor]];
	[temp_lblHours setFont:[UIFont fontWithName:kFontName size:13]];
	[m_scrollView addSubview:temp_lblHours];
	[temp_lblHours release];
	temp_lblHours=nil;
	
	UITextView *temp_txtViewOverview=[[UITextView alloc]initWithFrame:CGRectMake(10,70,0,0)];
	[temp_txtViewOverview setText:l_objIndyShopModel.m_strOverview];//l_objCatModel.m_strOverview]; // @"Overview\n -Testing 1\n -Testing 2"];
	[temp_txtViewOverview setTextColor:[UIColor whiteColor]];
	[temp_txtViewOverview setFont:[UIFont fontWithName:kFontName size:13]];
	[temp_txtViewOverview setBackgroundColor:[UIColor clearColor]];
	[temp_txtViewOverview setEditable:FALSE];
	[temp_txtViewOverview setScrollEnabled:FALSE];
	[m_scrollView addSubview:temp_txtViewOverview];
	
	
	[temp_txtViewOverview setFrame:CGRectMake(10,70,270,temp_txtViewOverview.contentSize.height)];
	
	[temp_txtViewOverview setFrame:CGRectMake(10,70,270,temp_txtViewOverview.contentSize.height)];
	
	//[temp_txtViewOverview setFrame:CGRectMake(10,70,270,temp_txtViewOverview.contentSize.height)];
	
	m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,
										  m_scrollView.frame.size.height + temp_txtViewOverview.contentSize.height);
	
	[temp_txtViewOverview release];
	temp_txtViewOverview=nil;
	
	[m_moreDetailView addSubview:m_scrollView];
	
	
	[m_scrollView release];
	m_scrollView=nil;
	[self.view addSubview:m_moreDetailView];	
	
	//-------------------------------------------------
	
		
}

-(void)btnCloseAction:(id)sender
{
	[m_moreDetailView removeFromSuperview];
	[m_tableView setUserInteractionEnabled:YES];
	l_appDelegate.m_intIndyShopIndex=-1;
	
	[self.tabBarController.tabBar setUserInteractionEnabled:TRUE];
}

-(void)btnMapItAction:(id)sender
{
	[self performSelector:@selector(checkForMallMapExistence)];
	
	//StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
//	[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
//	[temp_streetMapViewCtrl release];
//	temp_streetMapViewCtrl=nil;
//	
//	[CATransaction begin];
//	CATransition *animation = [CATransition animation];
//	animation.type = kCATransitionFromLeft;
//	animation.duration = 0.6;
//	//animation.delegate=self;
//	[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
//	[[l_appDelegate.m_customView layer] addAnimation:animation forKey:@"Slide"];
//	[l_appDelegate.m_customView setHidden:YES];
//	[CATransaction commit];
}

#pragma mark -
#pragma mark Web Service methods
-(void)sendRequestForIndyShopData:(int)temp_indyPageNumber
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[[LoadingIndicatorView SharedInstance] startLoadingView:self];
		
		
		l_appDelegate.m_intIndyPageNumber=temp_indyPageNumber;
		
		NSMutableString *temp_url;
		
#if 0 //USE_PREGROUPON_INDY_SHOP_IMPLEMENTATION
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllActiveItemsByCategory&did=%@&lng=%f&lat=%f&num=%d&pagenum=%d&cat=%@&dist=50",kServerUrl,l_appDelegate.m_strDeviceId,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude,10,temp_indyPageNumber,l_appDelegate.m_strCategoryName];
#else
		NSString * grouponCatName = @"shopping";
		temp_url=[NSMutableString stringWithFormat:
			@"https://api.groupon.com/v2/now_deals.json?client_id=cc35475ee57e712759abdc2702c5ca9f7cfe5478&visitor_id=%@&lng=%f&lat=%f&limit=100&categories[]=%@",
			l_appDelegate.m_strDeviceId,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude,grouponCatName];
#endif
				
		[temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
		
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		//NSLog(@"%@",temp_url);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_url dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setHTTPShouldHandleCookies:YES];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if(m_isConnectionActive)
			[m_theConnection cancel];
		
		m_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		[m_theConnection start];
		m_isConnectionActive=TRUE;
		
		if(m_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	if (connection == m_mallMapConnection) 
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		long long temp_length=[response expectedContentLength];
		
		[m_mallMapConnection cancel];
		m_isMallMapConnectionActive=FALSE;
		
		StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
		
		if (temp_length ==0)
		{
			temp_streetMapViewCtrl.m_isShowMallMapTab=FALSE;	
		}
		else {
			temp_streetMapViewCtrl.m_isShowMallMapTab=TRUE;	
		}
		
		[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
		[temp_streetMapViewCtrl release];
		temp_streetMapViewCtrl=nil;
		
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromLeft;
		animation.duration = 0.6;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
		[CATransaction commit];
		
		
	}
	else 
	{
		m_intResponseCode = [httpResponse statusCode];
		NSLog(@"%d",m_intResponseCode);
		[m_mutIndyShopResponseData setLength:0];	
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[m_mutIndyShopResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	[self.view setUserInteractionEnabled:TRUE];
	m_isConnectionActive=FALSE;
	
	if (m_intResponseCode==401)
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
		
	}
	else if(m_mutIndyShopResponseData!=nil && m_intResponseCode==200)
	{
		//NSLog(@"%@",[[[NSString alloc]initWithData:m_mutIndyShopResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutIndyShopResponseData encoding:NSUTF8StringEncoding];
				
		SBJSON *temp_objSBJson=[[SBJSON alloc]init];
						
		//NSLog(@"%d",[temp_arrResponse count]);
		
		CategoryModelClass *temp_objIndyShops;
		
#if 0 //USE_PREGROUPON_INDY_SHOP_IMPLEMENTATION
		NSArray *temp_arrResponse =[[NSArray alloc]initWithArray:[temp_objSBJson objectWithString:temp_string]];
		for (int i=0;i<[temp_arrResponse count]; i++)
		{
			NSDictionary *temp_dictIndy=[temp_arrResponse objectAtIndex:i];
			
			//NSLog(@"%@",temp_dictIndy);
			
			//NSLog(@"%@",[[temp_dictAd objectForKey:@"location"] objectForKey:@"latitude"]);
			
			l_appDelegate.m_intTotalIndyPages=[[temp_dictIndy objectForKey:@"noOfPages"]intValue];
			l_appDelegate.m_intTotalIndyRecords=[[temp_dictIndy objectForKey:@"totalRecords"]intValue];
			
			temp_objIndyShops=[[CategoryModelClass alloc]init];
			temp_objIndyShops.m_strLatitude=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"latitude"];
			temp_objIndyShops.m_strLongitude=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"longitude"];
			temp_objIndyShops.m_strMallImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictIndy objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]];
			temp_objIndyShops.m_strStoreHours=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"storeHours"];
			temp_objIndyShops.m_strPhoneNo=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"phoneNumber"];
			
			temp_strAdd2=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"address2"];
			
			if(temp_strAdd2.length>0)
				temp_objIndyShops.m_strAddress=[NSString stringWithFormat:@"%@, %@",[[temp_dictIndy objectForKey:@"location"] objectForKey:@"address1"],temp_strAdd2];
			else
				temp_objIndyShops.m_strAddress=[NSString stringWithFormat:@"%@",[[temp_dictIndy objectForKey:@"location"] objectForKey:@"address1"]];
			
			temp_strCity=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"city"];
			temp_strState=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"state"];
			temp_strZip=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"zip"];
			
			if(temp_strCity.length>0)
			{
				temp_objIndyShops.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objIndyShops.m_strAddress,temp_strCity];
			}
			
			if(temp_strState.length>0)
			{
				temp_objIndyShops.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objIndyShops.m_strAddress,temp_strState];
			}
			
			if(temp_strZip.length>0)
			{
				temp_objIndyShops.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objIndyShops.m_strAddress,temp_strZip];
			}
			
			//reuse distance for street name to be shown on category rows : address1 will be used
			//temp_objIndyShops.m_strDistance=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"distance"];
			temp_objIndyShops.m_strDistance=[[temp_dictIndy objectForKey:@"location"] objectForKey:@"address1"];
			
			temp_objIndyShops.m_strTitle=[[temp_dictIndy objectForKey:@"product"] objectForKey:@"productTagLine"];
			temp_objIndyShops.m_strDiscount=[[temp_dictIndy objectForKey:@"product"] objectForKey:@"discountInfo"];
			temp_objIndyShops.m_strOverview=[[temp_dictIndy objectForKey:@"product"] objectForKey:@"termsAndConditions"];
			temp_objIndyShops.m_strProductName=[[temp_dictIndy objectForKey:@"product"] objectForKey:@"productName"];
			temp_objIndyShops.m_strBrandName=[[temp_dictIndy objectForKey:@"product"] objectForKey:@"brandName"];
			
			temp_objIndyShops.m_strId=[NSString stringWithFormat:@"%@",[[temp_dictIndy objectForKey:@"product"] objectForKey:@"id"]];
			
			NSArray *temp_arrImgAdUrls=[[temp_dictIndy objectForKey:@"product"] objectForKey:@"imageUrls"];
			
			NSString *temp_str=@"";
			
			if (temp_arrImgAdUrls.count>0)
			{
				temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:0]];
				temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				//NSLog(@"Indy shop image url: %@ ",temp_str);
				temp_objIndyShops.m_strImageUrl=temp_str;
				if(temp_arrImgAdUrls.count>=3)
				{
					temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:2]];
					temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
					//NSLog(@"Indy shop image url3: %@",temp_str);
					temp_objIndyShops.m_strImageUrl3=temp_str;
				}
				
			}
			
			[l_appDelegate.m_arrIndyShopData addObject:temp_objIndyShops];
			
			[temp_objIndyShops release];
			temp_objIndyShops=nil;
			
		}
		[temp_arrResponse release];
		temp_arrResponse=nil;
			
#else
		NSDictionary *temp_dictResponse = [temp_objSBJson objectWithString:temp_string];
		NSArray * nowDealsArray = [temp_dictResponse objectForKey:@"nowDeals"];

		NSString * grouponCatName = @"shopping";
		BOOL categoryFoundShouldBreakNow = NO;

		if ((nowDealsArray) && ([nowDealsArray count] > 0)) {
			for (int i=0; i< [nowDealsArray count]; i++) {
				
				if (categoryFoundShouldBreakNow == YES)
					break;
				
				NSDictionary * dict = [nowDealsArray objectAtIndex:i];
				if ((dict != nil) && ([dict objectForKey:@"category"] != nil)) {
					NSString * catId = [[dict objectForKey:@"category"] objectForKey:@"id"];
					if ((catId) && ([catId caseInsensitiveCompare:grouponCatName] == NSOrderedSame)) {
						categoryFoundShouldBreakNow = YES;

						temp_objIndyShops = nil;
						NSArray * dealsArray = [dict objectForKey:@"deals"];

						if ((dealsArray) && ([dealsArray count] > 0)) {
							
							l_appDelegate.m_intTotalIndyPages=1;
							l_appDelegate.m_intTotalIndyRecords=[dealsArray count];
			
							for (int j=0; j < [dealsArray count]; j++) {
								NSDictionary * dataDict = [dealsArray objectAtIndex:j];
								if ((dataDict) && ([dataDict count] > 0)) {
									temp_objIndyShops = [[CategoryModelClass alloc]init];
									
									temp_objIndyShops.m_strStartRedemptionAt = [[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"startRedemptionAt"];
                                    if ((temp_objIndyShops.m_strStartRedemptionAt == nil) ||
												([temp_objIndyShops.m_strStartRedemptionAt isKindOfClass:[NSString class]] != YES))
                                        temp_objIndyShops.m_strStartRedemptionAt = @"";
                                    

									temp_objIndyShops.m_strEndRedemptionAt = [[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"endRedemptionAt"];
                                    if ((temp_objIndyShops.m_strEndRedemptionAt == nil) ||
												([temp_objIndyShops.m_strEndRedemptionAt isKindOfClass:[NSString class]] != YES))
                                        temp_objIndyShops.m_strEndRedemptionAt = @"";

									id latId = [[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"lat"];

									id lngId = [[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"lng"];

									NSLog(@"Class of lat/lng object=[%@,%@] value=[%@,%@]",[latId class], [lngId class],
											[NSString stringWithFormat:@"%f",[latId floatValue]],
											[NSString stringWithFormat:@"%f",[lngId floatValue]]);

									temp_objIndyShops.m_strLatitude = [NSString stringWithFormat:@"%f",[latId floatValue]]; 
									temp_objIndyShops.m_strLongitude = [NSString stringWithFormat:@"%f", [lngId floatValue]];
									
									// ????
									//temp_objIndyShops.m_strStoreHours=TBD
									temp_objIndyShops.m_strDistance=  [NSString stringWithFormat:@"%f",
										[[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"distanceFromOriginInMiles"] floatValue]];
									temp_objIndyShops.m_strPhoneNo= [[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"phoneNumber"];
									
									temp_objIndyShops.m_strAddress=[NSString stringWithFormat:@"%@ %@, %@, %@ %@",
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"streetAddress1"],
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"streetAddress2"],
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"city"],
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"state"],
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"postalCode"]];
									
										
									temp_strCity=
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"city"];
									temp_strState=
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"state"];
									temp_strZip=
										[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"postalCode"];
									
																	
									temp_objIndyShops.m_strMallImageUrl=[dataDict objectForKey:@"largeImageUrl"];
									temp_objIndyShops.m_strDealUrl=[dataDict objectForKey:@"dealUrl"];
                                    if ((temp_objIndyShops.m_strDealUrl == nil) || 
												([temp_objIndyShops.m_strDealUrl isKindOfClass:[NSString class]] != YES))
                                        temp_objIndyShops.m_strDealUrl = @"";
									temp_objIndyShops.m_strTitle=[dataDict objectForKey:@"title"];
									temp_objIndyShops.m_strDiscount=[dataDict objectForKey:@"subtitle"];

									NSMutableString * mutString = [[NSMutableString alloc] init];
									NSArray * strArr = [[[dataDict objectForKey:@"options"] objectAtIndex:0] 
										objectForKey:@"details"];
									for (id aObj in strArr) {
										if ([aObj objectForKey:@"description"] != nil) {
											[mutString appendString:[aObj objectForKey:@"description"]];
											[mutString appendString:@" "];
										}
									}
									temp_objIndyShops.m_strOverview=[NSString stringWithFormat:@"%@",mutString];
									[mutString release];
									
										temp_objIndyShops.m_strStoreWebsite=[[dataDict objectForKey:@"merchant"] objectForKey:@"websiteUrl"];
                                    if ((temp_objIndyShops.m_strStoreWebsite == nil) ||
												([temp_objIndyShops.m_strStoreWebsite isKindOfClass:[NSString class]] != YES))
                                        temp_objIndyShops.m_strStoreWebsite = @"";
                                    
									temp_objIndyShops.m_strProductName=[[dataDict objectForKey:@"merchant"] objectForKey:@"name"];
									temp_objIndyShops.m_strBrandName=[[dataDict objectForKey:@"merchant"] objectForKey:@"name"];
									temp_objIndyShops.m_strDescription=[dataDict objectForKey:@"announcementTitle"];
									
									temp_objIndyShops.m_strId=[dataDict objectForKey:@"id"];
									
									NSLog(@"%@",temp_objIndyShops.m_strId);
															
									temp_objIndyShops.m_strImageUrl=[dataDict objectForKey:@"mediumImageUrl"];
									temp_objIndyShops.m_strImageUrl2=[dataDict objectForKey:@"largeImageUrl"];

									NSLog(@"%@->loc[%@,%@]",temp_objIndyShops.m_strProductName, temp_objIndyShops.m_strLatitude, temp_objIndyShops.m_strLongitude);
									
									
									[l_appDelegate.m_arrIndyShopData addObject:temp_objIndyShops];
									
									[temp_objIndyShops release];
									temp_objIndyShops=nil;

								}
							}
						}
						
					}
				}
			}
		}
#endif
		
		[temp_objSBJson release];
		temp_objSBJson=nil;
		
		[temp_string release];
		temp_string=nil;
		
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			[m_activityIndicator stopAnimating];
			[self.view setUserInteractionEnabled:TRUE];
			m_isConnectionActive=FALSE;
		
		[m_tableView reloadData];
		
		[self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:1.0];
	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	m_isConnectionActive=FALSE;
	m_isMallMapConnectionActive=FALSE;
	self.view.userInteractionEnabled=TRUE;
	[m_activityIndicator stopAnimating];

    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];

	// inform the user
   // NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	//NSLog(@"Exit :didFailWithError");
	
}
#pragma mark -
#pragma mark Check Mall Map Existence

-(void)checkForMallMapExistence
{
	if(m_isMallMapConnectionActive)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_isMallMapConnectionActive=FALSE;
		[m_mallMapConnection cancel];
	}
	
	//checks from which view user comes in mall map view
	if(l_appDelegate.m_intCurrentView==1)//1: represents indy shop view
	{
		if(l_appDelegate.m_arrIndyShopData.count>0 && l_appDelegate.m_intIndyShopIndex!=-1)
		{
			CategoryModelClass *temp_objIndyShop=[l_appDelegate.m_arrIndyShopData objectAtIndex:l_appDelegate.m_intIndyShopIndex];
			
			[self sendRequestToCheckMallMap:temp_objIndyShop.m_strMallImageUrl];
		}
		
	}
	
	
}

-(void)sendRequestToCheckMallMap:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if (m_isMallMapConnectionActive==TRUE)
		{
			[m_mallMapConnection cancel];
		}
		
		m_mallMapConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[m_mallMapConnection start];
		m_isMallMapConnectionActive=TRUE;
		
		if(m_mallMapConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		m_isMallMapConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}


#pragma mark Table View Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 85.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	
	if (tableView==m_tableView)
	{
			if((l_appDelegate.m_arrIndyShopData.count > 0) && (l_appDelegate.m_intTotalIndyPages > 1) && (l_appDelegate.m_arrIndyShopData.count < l_appDelegate.m_intTotalIndyRecords))
				return l_appDelegate.m_arrIndyShopData.count+1;
			else
				return l_appDelegate.m_arrIndyShopData.count;
	}
	else if(tableView==m_tblSearchResults)
	{
		if(pageNumber < totalPages)
		{
			return l_arrSearchResults.count+1;
		}
		else 
		{
			return l_arrSearchResults.count;
		}
	}
    
    // GDL: We shouln't get here without returning something.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if (tableView==m_tableView)
	{
		
		UITableViewCell *newCell = nil;
		NSString *strIdentifier= [NSString stringWithFormat:@"Cell %@",indexPath];
		
		newCell=[tableView dequeueReusableCellWithIdentifier:strIdentifier];
		
		if(newCell==nil)
			newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier]autorelease];
		
		if(indexPath.row == l_appDelegate.m_arrIndyShopData.count)
			newCell.selectionStyle=UITableViewCellSelectionStyleGray;
		else
			newCell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		for (UIView *temp_view in newCell.contentView.subviews )
		{
			if([temp_view isKindOfClass:[UIButton class]])
			{
				[temp_view removeFromSuperview];
			}
			if([temp_view isKindOfClass:[UILabel class]])
			{
				[temp_view removeFromSuperview];
			}
			if([temp_view isKindOfClass:[UIImageView class]])
			{
				[temp_view removeFromSuperview];
			}
		}
		
		if(indexPath.row < l_appDelegate.m_arrIndyShopData.count)
		{
		
		l_objIndyShopModel=[l_appDelegate.m_arrIndyShopData objectAtIndex:indexPath.row];
		
		//making Main Title for TableViewCell
		UILabel *temp_lblTitle=[[UILabel alloc]init];
		temp_lblTitle.frame=CGRectMake(130,2.5,167,36);
		temp_lblTitle.lineBreakMode = UILineBreakModeWordWrap;
		temp_lblTitle.numberOfLines = 2;
		temp_lblTitle.font=[UIFont fontWithName:kFontName size:16];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=l_objIndyShopModel.m_strTitle;
		//Bharat : 12/05/11: US28
		//temp_lblTitle.textColor=[UIColor colorWithRed:0.33f green:0.33f blue:0.33f alpha:1.0];
		temp_lblTitle.textColor=[UIColor whiteColor];
		
		//making Discount Title for TableViewCell
		UILabel *temp_lblDiscount=[[UILabel alloc]init];
		temp_lblDiscount.frame=CGRectMake(130,40,167,36);
		temp_lblDiscount.lineBreakMode = UILineBreakModeWordWrap;
		temp_lblDiscount.numberOfLines = 2;
		temp_lblDiscount.font=[UIFont fontWithName:kFontName size:16];
		temp_lblDiscount.backgroundColor=[UIColor clearColor];
		temp_lblDiscount.text=l_objIndyShopModel.m_strDiscount;
		//Bharat : 12/05/11: US28
		//temp_lblDiscount.textColor=[UIColor redColor];
		temp_lblDiscount.textColor=[UIColor colorWithRed:255.0f/255.0f green:248.0f/255.0f blue:37.0f/255.0f alpha:1.0];
			
			
			//making distance parameter for TableViewCell
			//UILabel *temp_lblDistance=[[UILabel alloc]init];
	//		temp_lblDistance.frame=CGRectMake(82,50,138,25);
	//		temp_lblDistance.font=[UIFont fontWithName:kFontName size:13]; //kGillSansFont size:13];
	//		temp_lblDistance.backgroundColor=[UIColor clearColor];
	//		temp_lblDistance.textColor=[UIColor colorWithRed:0.62f green:0.62f blue:0.62f alpha:1.0];
	//		temp_lblDistance.text=l_objIndyShopModel.m_strDistance;
			
			//-------calculate distance-------
	//		l_cllocation=[[CLLocation alloc]initWithLatitude:[l_objIndyShopModel.m_strLatitude floatValue] longitude:[l_objIndyShopModel.m_strLongitude floatValue]];
	//		CLLocation *tempCurrentCL=[[CLLocation alloc] initWithLatitude:[l_appDelegate.m_objGetCurrentLocation m_latitude] longitude:[l_appDelegate.m_objGetCurrentLocation m_longitude]];
	//		
	//		float tempDistance=[l_cllocation distanceFromLocation:tempCurrentCL];
	//		
	//		if (tempDistance > 400) 
	//		{
	//			temp_lblDistance.text=[NSString stringWithFormat:@"%.2f miles away",tempDistance * 0.000622f];
	//		}
	//		else {
	//			temp_lblDistance.text=[NSString stringWithFormat:@"%.0f steps away",tempDistance];
	//		}
	//		
	//		[l_cllocation release];
	//		[tempCurrentCL release];
	//		
	//		//-----------------------------------------------------------------
			
			
		
		// making UIImageView for TableView Cell
		UIImageView *temp_bgView=[[UIImageView alloc]init];
		temp_bgView.frame=CGRectMake(0,0,301,81);
		temp_bgView.contentMode=UIViewContentModeScaleAspectFit;
		temp_bgView.image=[UIImage imageNamed:@"blue_boxbg2"];
			
		
		// making UIImageView for TableView Cell
		UIImageView *temp_imageView=[[UIImageView alloc]init];
		temp_imageView.frame=CGRectMake(6,4,120,72);
		temp_imageView.tag=50;
		temp_imageView.contentMode=UIViewContentModeScaleAspectFit;
		
		if(l_objIndyShopModel.m_imgItemImage)
		{
			temp_imageView.image = l_objIndyShopModel.m_imgItemImage;
		}
		else 
		{
			temp_imageView.image=[UIImage imageNamed:@"groupon_loading_icon"];
		}	
		
		/*
		// making custom UIButton
		UIButton *temp_btnLookInside=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_btnLookInside setImage:[UIImage imageNamed:@"look_inside.png"] forState:UIControlStateNormal];
		[temp_btnLookInside setTitle:@"➤ More info" forState:UIControlStateNormal];
		[temp_btnLookInside setTitle:@"➤ More info" forState:UIControlStateHighlighted];
		[temp_btnLookInside.titleLabel setTextColor:[UIColor colorWithRed:180.0f/255.0f green:219.0f/255.0f blue:242.0f/255.0f alpha:1.0]];
		[temp_btnLookInside addTarget: self action:@selector(btnLookInsideAction:) forControlEvents:UIControlEventTouchDown];
		temp_btnLookInside.frame=CGRectMake(180,62,120,14);
		temp_btnLookInside.tag=indexPath.row;
		*/
		//making Discount Title for TableViewCell
		//adding Custom Views to tableView Cell
		[newCell.contentView addSubview:temp_bgView];
		[newCell.contentView addSubview:temp_imageView];
		//[newCell.contentView addSubview:temp_imageView2];
		[newCell.contentView addSubview:temp_lblTitle];
		[newCell.contentView addSubview:temp_lblDiscount];
			//[newCell.contentView addSubview:temp_lblDistance];
		//[newCell.contentView addSubview:temp_btnLookInside];
		
		/*
		UILabel *temp_lblMoreInfo=[[UILabel alloc]init];
		temp_lblMoreInfo.frame=CGRectMake(192,62,120,16);
		temp_lblMoreInfo.font=[UIFont fontWithName:kFontName size:16];
		temp_lblMoreInfo.backgroundColor=[UIColor clearColor];
		temp_lblMoreInfo.text=@"➤ More info";
		temp_lblMoreInfo.textColor= [UIColor colorWithRed:180.0f/255.0f green:219.0f/255.0f blue:242.0f/255.0f alpha:1.0];
			
		[newCell.contentView addSubview:temp_lblMoreInfo];
		[temp_lblMoreInfo release];
		*/

		//newCell.backgroundView =[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"blue_boxbg2"]]autorelease];
		newCell.backgroundView =[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"somethingnotavailablerightnow"]]autorelease];
		
		[temp_bgView release];
		[temp_imageView release];
		[temp_lblTitle release];
		[temp_lblDiscount release];
			//[temp_lblDistance release];
		//[temp_imageView2 release];
		}
		else if(indexPath.row==l_appDelegate.m_arrIndyShopData.count)
		{
			newCell.backgroundView =[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"main_bg_old.png"]]autorelease];
			
			//making load more label for last cell
			UILabel *temp_lblLoadMore=[[UILabel alloc]init];
			temp_lblLoadMore.frame=CGRectMake(85,20,200,25);
			temp_lblLoadMore.font=[UIFont fontWithName:kFontName size:16];
			temp_lblLoadMore.backgroundColor=[UIColor clearColor];
			temp_lblLoadMore.text=@"Load more items..";
			[newCell.contentView addSubview:temp_lblLoadMore];
			[temp_lblLoadMore release];
			
			UILabel *temp_lblStatus=[[UILabel alloc]init];
			temp_lblStatus.frame=CGRectMake(85,40,200,25);
			temp_lblStatus.font=[UIFont fontWithName:kFontName size:15];
			temp_lblStatus.backgroundColor=[UIColor clearColor];
			temp_lblStatus.text=[NSString stringWithFormat:@"(%d of %d items)",l_appDelegate.m_arrIndyShopData.count,l_appDelegate.m_intTotalIndyRecords];
			temp_lblStatus.textColor=[UIColor grayColor];
			[newCell.contentView addSubview:temp_lblStatus];
			[temp_lblStatus release];
			
			
			UIImage *backImg=[UIImage imageNamed:@"main_bg_selected.png"];
			newCell.selectedBackgroundView =[[[UIImageView alloc] init] autorelease];
			((UIImageView *)newCell.selectedBackgroundView).image=backImg;
		}
		
		return newCell;
	}
	else if(tableView==m_tblSearchResults) //table for search results needs to be filled
	{
		UITableViewCell *newCell;
		
		NSLog(@"cellforRowAtIndexPath for row: %d",indexPath.row);
		
		
		if (indexPath.row < l_arrSearchResults.count)
		{
			NSString *strIdentifier= [NSString stringWithFormat:@"Cell %@",indexPath];
			l_categoryModelObj= [l_arrSearchResults objectAtIndex:indexPath.row];
			
			newCell=[tableView dequeueReusableCellWithIdentifier:strIdentifier];
			//add objects only if cell is nil (no need to add in case of resuable cell)
			if(newCell==nil)
			{
				newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier]autorelease];
				newCell.selectionStyle = UITableViewCellSelectionStyleNone;	
				
				AsyncImageView *temp_imageView=[[[AsyncImageView alloc]initWithFrame:CGRectMake(5, 4, 70, 70)] autorelease];
				[temp_imageView setTag:50];
				[newCell.contentView addSubview:temp_imageView];
				
				//making Main Title for TableViewCell
				UILabel *temp_lblTitle=[[UILabel alloc]init];
				temp_lblTitle.frame=CGRectMake(82,6,200,25);
				temp_lblTitle.font=[UIFont fontWithName:kFontName size:16];
				temp_lblTitle.backgroundColor=[UIColor clearColor];
				//temp_lblTitle.text=l_objCatModel.m_strTitle;
				temp_lblTitle.tag=51;
				temp_lblTitle.textColor=[UIColor colorWithRed:0.33f green:0.33f blue:0.33f alpha:1.0];
				[newCell.contentView addSubview:temp_lblTitle];
				[temp_lblTitle release];
				
				//making Discount Title for TableViewCell
				UILabel *temp_lblDiscount=[[UILabel alloc]init];
				temp_lblDiscount.frame=CGRectMake(82,30,200,25);
				temp_lblDiscount.font=[UIFont fontWithName:kFontName size:16];
				temp_lblDiscount.backgroundColor=[UIColor clearColor];
				//temp_lblDiscount.text=l_objCatModel.m_strDiscount;
				temp_lblDiscount.tag=52;
				temp_lblDiscount.textColor=[UIColor redColor];
				[newCell.contentView addSubview:temp_lblDiscount];
				[temp_lblDiscount release];
				
				
				//making distance parameter for TableViewCell
				UILabel *temp_lblDistance=[[UILabel alloc]init];
				temp_lblDistance.frame=CGRectMake(82,50,138,25);
				temp_lblDistance.tag=53;
				temp_lblDistance.font=[UIFont fontWithName:kFontName size:13]; //kGillSansFont size:13];
				temp_lblDistance.backgroundColor=[UIColor clearColor];
				temp_lblDistance.textColor=[UIColor colorWithRed:0.62f green:0.62f blue:0.62f alpha:1.0];
				//temp_lblDistance.text=l_objCatModel.m_strDistance;
				[newCell.contentView addSubview:temp_lblDistance];
				[temp_lblDistance release];
				
			}
			else
			{
				AsyncImageView *asyncImage = (AsyncImageView *)[newCell.contentView viewWithTag:50];
				UIImageView *tempImageview=(UIImageView *)[asyncImage viewWithTag:101];
				[tempImageview setImage:nil];
				
				[(UILabel *)[newCell.contentView viewWithTag:51] setText:@""];
				[(UILabel *)[newCell.contentView viewWithTag:52] setText:@""];
			}
			
			//l_objCatModel= [l_arrSearchResults objectAtIndex:indexPath.row];
			
			//-------calculate distance-------
			l_cllocation=[[CLLocation alloc]initWithLatitude:[l_categoryModelObj.m_strLatitude floatValue] longitude:[l_categoryModelObj.m_strLongitude floatValue]];
			CLLocation *tempCurrentCL=[[CLLocation alloc] initWithLatitude:[l_appDelegate.m_objGetCurrentLocation m_latitude] longitude:[l_appDelegate.m_objGetCurrentLocation m_longitude]];
			
			float tempDistance=[l_cllocation distanceFromLocation:tempCurrentCL];
			
			if (tempDistance > 400) 
			{
				((UILabel *)[newCell.contentView viewWithTag:53]).text=[NSString stringWithFormat:@"%.2f miles away",tempDistance * 0.000622f];
			}
			else {
				((UILabel *)[newCell.contentView viewWithTag:53]).text=[NSString stringWithFormat:@"%.0f steps away",tempDistance];
			}
			
			[l_cllocation release];
			[tempCurrentCL release];
			
			//		//-----------------------------------------------------------------
			
			
			[(AsyncImageView *)[newCell.contentView viewWithTag:50] loadImageFromURL:[NSURL URLWithString:l_categoryModelObj.m_strImageUrl]];
			[(UILabel *)[newCell.contentView viewWithTag:51] setText:l_categoryModelObj.m_strTitle];
			[(UILabel *)[newCell.contentView viewWithTag:52] setText:l_categoryModelObj.m_strDiscount];
			
			
			newCell.backgroundView =[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"moreInfo.png"]]autorelease];
			
			
		}
		else if(indexPath.row == l_arrSearchResults.count)
		{
			newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LoadMoreCell"]autorelease];
			newCell.selectionStyle = UITableViewCellSelectionStyleNone;	
			
			newCell.backgroundView =[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"main_bg_old.png"]]autorelease];
			
			//making load more label for last cell
			UILabel *temp_lblLoadMore=[[UILabel alloc]init];
			temp_lblLoadMore.frame=CGRectMake(85,20,200,25);
			temp_lblLoadMore.font=[UIFont fontWithName:kFontName size:16];
			temp_lblLoadMore.backgroundColor=[UIColor clearColor];
			temp_lblLoadMore.text=@"Load more items..";
			[newCell.contentView addSubview:temp_lblLoadMore];
			[temp_lblLoadMore release];		
			
		}
		
		return newCell;
		
		
	}
    
    // GDL: We shouldn't be getting here without having returned something.
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if (tableView==m_tableView)
	{
		
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
			
		if(indexPath.row==l_appDelegate.m_arrIndyShopData.count)
		{
			l_appDelegate.m_intIndyPageNumber++;	
			[self sendRequestForIndyShopData:l_appDelegate.m_intIndyPageNumber];
		}
		else {
			//[self btnLookInsideAction:indexPath.row];
#if 0 //USE_PREGROUPON_INDY_SHOP_IMPLEMENTATION
			WhereBox *tmp_whereBox=[[WhereBox alloc] init];
#else
			GrouponWhereBox *tmp_whereBox=[[GrouponWhereBox alloc] init];
#endif
			tmp_whereBox.m_productImage=((UIImageView *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:50]).image;
			tmp_whereBox.isFromIndyShopView=YES;
			tmp_whereBox.tagg=indexPath.row;
			[self.navigationController pushViewController:tmp_whereBox animated:YES];
			
			[CATransaction begin];
			CATransition *animation = [CATransition animation];
			animation.type = kCATransitionFromRight;
			animation.duration = 0.3;
			//animation.delegate=self;
			[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
			[CATransaction commit];
			
			[tmp_whereBox release];
			tmp_whereBox=nil;
			
		}
	}
	else if(tableView==m_tblSearchResults)
	{
		if(indexPath.row == l_appDelegate.m_arrIndyShopData.count && ++pageNumber <= totalPages)
		{
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			
			ShopbeeAPIs	*tmp_request=[[ShopbeeAPIs alloc] init];
			[tmp_request getSearchResults:@selector(requestCallBackMethod:responseData:) tempTarget:self searchText:m_searchBar.text pageNo:[NSString stringWithFormat:@"%d",pageNumber]];
			[tmp_request release];
		}
		else if(indexPath.row < l_appDelegate.m_arrIndyShopData.count)
		{
#if 0 //USE_PREGROUPON_INDY_SHOP_IMPLEMENTATION
			WhereBox *tmp_whereBox1=[[WhereBox alloc] init];
#else
			GrouponWhereBox *tmp_whereBox1=[[GrouponWhereBox alloc] init];
#endif
			tmp_whereBox1.m_productImage= ((UIImageView *)[(AsyncImageView *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:50] viewWithTag:101]).image ;
			tmp_whereBox1.isFromIndyShopView=YES;
			tmp_whereBox1.tagg=indexPath.row;
			[self.navigationController pushViewController:tmp_whereBox1 animated:YES];
			
			[CATransaction begin];
			CATransition *animation = [CATransition animation];
			animation.type = kCATransitionFromLeft;
			animation.duration = 0.3;
			//animation.delegate=self;
			[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
			[CATransaction commit];
			
			[tmp_whereBox1 release];
			tmp_whereBox1=nil;
		}
		
		
	}
	
}


#pragma mark -
#pragma mark Icon Downloader Methods

- (void)startIconDownload:(CategoryModelClass *)temp_objCategory forIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < l_appDelegate.m_arrIndyShopData.count)
	{
		ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
		iconDownloader.id_ = indexPath;
	
		[iconDownloader openConnection:temp_objCategory.m_strImageUrl withOtherUrlString:temp_objCategory.m_strImageUrl3  withCallBackTarget:indexPath withDelegate:self];
	}
}

-(void) imageDownloadFinish:(UIImage*)image_ withId:(NSIndexPath*)id_
{
	//if(id_.row < [appDelegate.arrSearchResults count])
	//{
	if (id_.section == 0) 
	{
		CategoryModelClass *temp_objCat= [l_appDelegate.m_arrIndyShopData  objectAtIndex:id_.row];
		if (image_) 
		{
			[temp_objCat setM_imgItemImage:image_];
		}
		else 
		{
			[temp_objCat setM_imgItemImage:[UIImage imageWithContentsOfFile:
											[[NSBundle mainBundle] pathForResource:@"no-image" ofType:@"png"]]];
		}
		
		[l_appDelegate.m_arrIndyShopData replaceObjectAtIndex:id_.row withObject:temp_objCat];
		UITableViewCell *cell = [self.m_tableView cellForRowAtIndexPath:id_];
		
		// Display the newly loaded image
		[(UIImageView*)[cell.contentView viewWithTag:50] setImage: image_];
		[self.m_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:id_] withRowAnimation:UITableViewRowAnimationNone];
	}
	
	else 
	{
		//NSLog(@"image download finished ");
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:id_.row inSection:0];
		CategoryModelClass *temp_objCat= [l_appDelegate.m_arrIndyShopData  objectAtIndex:indexPath.row];
		
		[temp_objCat setM_imgItemImage3:image_];
		
		[l_appDelegate.m_arrIndyShopData replaceObjectAtIndex:indexPath.row withObject:temp_objCat];
		
			
	}

}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows
{
	if ([l_appDelegate.m_arrIndyShopData count] > 0 && m_tableView.hidden==NO)
	{
		m_exceptionPage.hidden = YES;
		NSArray *visiblePaths = [m_tableView indexPathsForVisibleRows];
		for (NSIndexPath *indexPath in visiblePaths)
		{
			if(indexPath.row < l_appDelegate.m_arrIndyShopData.count)
			{
				CategoryModelClass *temp_objIndyModel = [l_appDelegate.m_arrIndyShopData objectAtIndex:indexPath.row];
				
				if (!temp_objIndyModel.m_imgItemImage) // avoid the app icon download if the app already has an icon
				{
					[self startIconDownload:temp_objIndyModel forIndexPath:indexPath];
				}
			}
		}
	} else {
		[m_exceptionPage setImage:[UIImage imageNamed:@"exception_alarm clock"]];
		m_exceptionPage.hidden = NO;
	}
}


// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	//if(self.typeOfData==0)
	[self loadImagesForOnscreenRows];
}


#pragma mark Search bar delegate methods

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
	
	/* search bar background setting */
	//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField = nil;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		//[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"gray_searchbox.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	/***********************************/
	
	[UIView beginAnimations:@"previousAd" context:nil];
	[UIView setAnimationDuration:0.4f];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[m_searchBar setContentInset:UIEdgeInsetsMake(5, 5, 5, 5)];
	[m_searchBar setShowsCancelButton:YES];
	[m_lblCaption setHidden:YES];
	
	[m_moreDetailView removeFromSuperview];
	
	//[UIView set forView:m_searchBar cache:YES];
	//[UIView setAnimationTransition:UIViewAnimatio forView:m_imgViewSaleAd cache:YES];
	[UIView commitAnimations];
	return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[l_arrSearchResults removeAllObjects];
	[m_tableView setHidden:YES];
	[m_tblSearchResults setHidden:NO];
	
	isCurrentIndyShopList=NO;
	
	pageNumber=1;
	
	
	[searchBar resignFirstResponder];
	
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	ShopbeeAPIs	*tmp_request=[[ShopbeeAPIs alloc] init];
	[tmp_request getSearchResults:@selector(requestCallBackMethod:responseData:) tempTarget:self searchText:searchBar.text pageNo:@"1"];
	[tmp_request release];
	
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
	//[m_tableView setHidden:YES];
	//[m_logosScrollView setHidden:NO];
	
	/* search bar background setting */
	//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField = nil;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		//[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	/***********************************/
	
	
	[m_searchBar setContentInset:UIEdgeInsetsMake(5, 220, 5, 5)];
	[m_searchBar setText:@""];
	
	[m_searchBar setShowsCancelButton:NO];
	[m_lblCaption setHidden:NO];
	[searchBar resignFirstResponder];
	
	
}

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
#if 0 //USE_PREGROUPON_INDY_SHOP_IMPLEMENTATION
	[l_appDelegate.m_customView setUserInteractionEnabled:TRUE];
	//[l_catLoadingView stopLoadingView];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	if ([responseCode intValue]==401)
	{
		[m_tblSearchResults setHidden:YES];
		isCurrentIndyShopList=YES;
		[m_tableView setHidden:NO];
		
		UIAlertView *temp_timeoutAlert=[[UIAlertView alloc]initWithTitle:@"Session timeout!" message:
										@"Session expired. Please login again to continue." delegate:
										nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_timeoutAlert show];
		[temp_timeoutAlert release];
		temp_timeoutAlert=nil;
		
		[m_tableView reloadData];
		
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		//[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
		
		
	}
	else if(responseData!=nil && [responseCode intValue]==200)
	{	
		[m_tblSearchResults setHidden:NO];
		isCurrentIndyShopList=NO;
		[m_tableView setHidden:YES];
		
		NSString *temp_string=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
		
		
		NSArray *temp_arrResponse =[temp_string JSONValue];
		if (temp_arrResponse.count>0)
		{
			[m_exceptionPage setHidden:YES];
			[m_tblSearchResults setHidden:NO];
			
			totalPages=[[[temp_arrResponse objectAtIndex:0] valueForKey:@"noOfPages"] intValue];
			temp_arrResponse =[[temp_arrResponse objectAtIndex:0] valueForKey:@"searchData"];
		}
		else 
		{
			[m_exceptionPage setHidden:NO];
			[m_tblSearchResults setHidden:YES];
			//UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"No data found. Please try with different search text." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			//			[tempAlert show];
			//			[tempAlert release];
			//			tempAlert=nil;
			//							
			totalPages=0;
			
		}
		
		CategoryModelClass *temp_objCatSales;
		
		NSDictionary *temp_dictCat;
		
		for (int i=0;i<[temp_arrResponse count]; i++)
		{
			temp_dictCat=[temp_arrResponse objectAtIndex:i];
			//NSLog(@"%@",temp_dictCat);
			temp_objCatSales=[[CategoryModelClass alloc]init];
			temp_objCatSales.m_strLatitude=[[temp_dictCat objectForKey:@"location"] objectForKey:@"latitude"];
			temp_objCatSales.m_strLongitude=[[temp_dictCat objectForKey:@"location"] objectForKey:@"longitude"];
			temp_objCatSales.m_strMallImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictCat objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]];
			
			//reuse distance for street name to be shown on category rows : address1 will be used
			//temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"distance"];
			temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"];
			
			temp_objCatSales.m_strStoreHours=[[temp_dictCat objectForKey:@"location"] objectForKey:@"storeHours"];
			temp_objCatSales.m_strPhoneNo=[[temp_dictCat objectForKey:@"location"] objectForKey:@"phoneNumber"];
			
			temp_strAdd2=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address2"];
			
			if(temp_strAdd2.length>0)
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"],temp_strAdd2];
			else
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"]];
			
			
			temp_strCity=[[temp_dictCat objectForKey:@"location"] objectForKey:@"city"];
			temp_strState=[[temp_dictCat objectForKey:@"location"] objectForKey:@"state"];
			temp_strZip=[[temp_dictCat objectForKey:@"location"] objectForKey:@"zip"];
			
			if(temp_strCity.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strCity];
			}
			
			if(temp_strState.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strState];
			}
			
			if(temp_strZip.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strZip];
			}
			
			temp_objCatSales.m_strTitle=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productTagLine"];
			temp_objCatSales.m_strDiscount=[[temp_dictCat objectForKey:@"product"] objectForKey:@"discountInfo"];
			temp_objCatSales.m_strOverview=[[temp_dictCat objectForKey:@"product"] objectForKey:@"termsAndConditions"];
			temp_objCatSales.m_strProductName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productName"];
			temp_objCatSales.m_strBrandName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"brandName"];
			temp_objCatSales.m_strDescription=[[temp_dictCat objectForKey:@"product"] objectForKey:@"description"];
			
			//@"brandName"];
			
			temp_objCatSales.m_strId=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"product"] objectForKey:@"id"]];
			
			NSLog(@"%@",temp_objCatSales.m_strId);
			
			NSArray *temp_arrImgAdUrls=[[temp_dictCat objectForKey:@"product"] objectForKey:@"imageUrls"];
			NSString *temp_str=@"";
			if (temp_arrImgAdUrls.count>0)
			{
				temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:0]];
				temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				NSLog(@"%@",temp_str);
				temp_objCatSales.m_strImageUrl=temp_str;
				
				if(temp_arrImgAdUrls.count>=3)
				{
					temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:2]];
					temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
					//NSLog(@"Cat sale image url3: %@",temp_str);
					temp_objCatSales.m_strImageUrl3=temp_str;
				}
				
			}
			
			[l_arrSearchResults addObject:temp_objCatSales];
			
			[temp_objCatSales release];
			temp_objCatSales=nil;
		}
		
		
		//logic to interchange results data with category data because category array is used on many views
		//so better to replace category sales array content
		//replace only after first request else wrong data will be replaced on second request
		
		if (pageNumber==1)
		{
			l_appDelegate.m_arrBackupIndyShop=[[NSMutableArray arrayWithArray:l_appDelegate.m_arrIndyShopData] retain];
			
		}
		
		l_appDelegate.m_arrIndyShopData=[l_arrSearchResults retain];
		[m_tblSearchResults reloadData];
		
		
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Server error!" message:@"Internal error occurred. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tempAlert show];
		[tempAlert release];
		tempAlert=nil;
		
	}
	
#endif
}



@end 
