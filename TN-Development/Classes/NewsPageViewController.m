//
//  NewsPageViewController.m
//  TinyNews
//
//  Created by Nava Carmon on 30/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewsPageViewController.h"
#import "PushNotificationViewController.h"
#import "UIView+Badge.h"
#import "UIViewController+NibCells.h"
#import "NewsDataManager.h"
#import "CategoryData.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "MoreButton.h"
#import "DetailPageViewController.h"
#import "LoadingIndicatorView.h"
#import "NewsMoreViewController.h"

#import "GAI.h"


@interface NewsPageViewController ()
- (void) openFilter;

@end

@implementation NewsPageViewController

@synthesize notificationButton, m_exceptionPage, m_exceptionButton, newsTable, detailController, moreController, tableCells;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.trackedViewName = @"News Page";
    }
    return self;
}

//view notification tray;
-(IBAction)viewNotification {//**
    PushNotificationViewController *notificationTray=[[PushNotificationViewController alloc] init];
    [self.navigationController   pushViewController:notificationTray animated:YES];
    [notificationButton removeBadge];
    [[[[[self tabBarController] tabBar] items] 
      objectAtIndex:0] setBadgeValue:nil];
    [notificationTray release];
}

- (void) updateNotifications
{
    
}

- (IBAction)slideMenuButtonTouched
{
    menuWasPressed = YES;
    [super slideMenuButtonTouched];
}

- (void)viewDidLoad
{
    [m_exceptionButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 8, 5)];
    m_exceptionButton.hidden = YES;
    m_exceptionPage.hidden = YES;
    menuWasPressed = NO;
    currentFilter = POPULAR;
    [[NewsDataManager sharedManager] getActiveCategories:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePopular:) name:@"UpdatePopular" object:nil];
    [m_exceptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [m_exceptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [m_exceptionButton setTitle:@"No Label" forState:UIControlStateNormal];
    [m_exceptionButton setTitle:@"No Label" forState:UIControlStateHighlighted];
	
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.newsTable.bounds.size.height, self.view.frame.size.width, self.newsTable.bounds.size.height)];
		view.delegate = self;
		[self.newsTable addSubview:view];
		_refreshHeaderView = view;
		[view release];
		
	}
	
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
}

- (void) updatePopular:(NSNotification *)nf
{
    [[NewsDataManager sharedManager] cleanActiveCategories];
    [[NewsDataManager sharedManager] getActiveCategories:self];
}

- (void) updateCategories
{
    [self clickOnPopular:nil];
    [[NewsDataManager sharedManager] getNotificationsCount:self];
}

- (void) viewWillAppear:(BOOL)animated
{
    /*if ([[NewsDataManager sharedManager] thereAreCategories]) {
        if (!menuWasPressed) {
            [self openFilter];
            [[NewsDataManager sharedManager] getNotificationsCount:self];
        } else {
            menuWasPressed = NO;
        }
    }*/
}

- (void) openFilter
{
    [self.tableCells removeAllObjects];
    switch (currentFilter) {
        case POPULAR:
            [self clickOnPopular:nil];
            break;
        case FOLLOWING:
            [self clickOnFollowing:nil];
            break;
        case FRIENDS:
            [self clickOnFriends:nil];
            break;
        case MINE:
            [self clickOnMine:nil];
            break;
        default:
            break;
    }
}

- (void) updateNotifications:(NSString *) count
{
    if ([count length] > 0)
        [notificationButton setBadge:count];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
	_refreshHeaderView=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) noData
{
    [[LoadingIndicatorView SharedInstance] stopLoadingView];
    [self.tableCells removeAllObjects];
    [m_exceptionPage setHidden:NO];
    if (currentFilter != FOLLOWING && currentFilter != MINE) {
        [m_exceptionButton setHidden:NO];
    }
}

- (void) updateData
{
    [[LoadingIndicatorView SharedInstance] stopLoadingView];

    [newsTable reloadData];
    
    [self doneLoadingTableViewData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSMutableArray *) tableCells
{
    if (!tableCells) {
        self.tableCells = [[[NSMutableArray alloc] init] autorelease];
    }
    return tableCells;
}

#pragma mark UITableView delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[NewsDataManager sharedManager] countActiveCategories];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"NewsCell";
    
    UITableViewCell *cell = nil;
    NSInteger ind = [self.tableCells indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary   *dict = obj;
        
        return [[dict objectForKey:@"indexPath"] intValue] == indexPath.row;
    }];
    
    if (ind != NSNotFound) {
        NSDictionary *dict = [self.tableCells objectAtIndex:ind];
        cell = [dict objectForKey:@"cell"];
    } else {
//    cell = [tableView dequeueReusableCellWithIdentifier:cellId];
//    if (nil == cell) {
        cell = [self loadReusableTableViewCellFromNibNamed:cellId];
        [self.tableCells addObject:[NSDictionary dictionaryWithObjectsAndKeys:cell, @"cell", [NSNumber numberWithInt:indexPath.row], @"indexPath", nil]];
    }
    for (int i = 0; i < 3; i++) {
        AsyncButtonView *asyncButton = (AsyncButtonView *)[cell.contentView viewWithTag:i + 1];
        UILabel *aLabel = (UILabel *)[cell.contentView viewWithTag:i + 11];

        [asyncButton setHidden:YES];
        [asyncButton resetButton];
        asyncButton.cropImage = YES;
        [aLabel setHidden:YES];
    }
    
    //UIButton *sectionTitle = (UIButton *)[cell.contentView viewWithTag:20];
    //UIImageView *sectionLine = (UIImageView *)[cell.contentView viewWithTag:30];
    MoreButton *arrow = (MoreButton *)[cell.contentView viewWithTag:40];
    
    
    NSString *titleString = [[NewsDataManager sharedManager] getCategoryIdAtIndex:indexPath.row];
    [arrow setTitle:titleString forState:UIControlStateNormal];
    [arrow setTitle:titleString forState:UIControlStateHighlighted];
    /*sectionTitle.text = titleString;
    sectionTitle.font = [UIFont fontWithName:kMyriadProRegularFont size:18];
    CGSize  size = [sectionTitle.text sizeWithFont:sectionTitle.font minFontSize:18 actualFontSize:nil forWidth:320 lineBreakMode:UILineBreakModeTailTruncation];
    CGRect rct = sectionTitle.frame;  
    
    rct.size.width = size.width;
    sectionTitle.frame = rct;

    rct = sectionLine.frame;
    rct.origin.x = sectionTitle.frame.origin.x + size.width + 10;
    rct.size.width = 300 - rct.origin.x;
    
    sectionLine.frame = rct;*/
    
    [arrow setData:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:indexPath.row], @"index", nil]];
    [arrow addTarget:self action:@selector(moreButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    CategoryData *catData = [[NewsDataManager sharedManager] getCategoryDataByIndex:indexPath.row];
    
    if (catData) {
        int numItems = MIN(3, [catData.categoryData count]);
        
        [arrow setHidden:NO];
        if (catData.totalRecords <= 3) {
            //[arrow setHidden:YES];
            [arrow setBackgroundImage:[UIImage imageNamed:@"news-headline-normal.png"] forState:UIControlStateNormal];
            [arrow setBackgroundImage:[UIImage imageNamed:@"news-headline-bgrd.png"] forState:UIControlStateHighlighted];
            arrow.userInteractionEnabled = NO;
        } else {
            [arrow setBackgroundImage:[UIImage imageNamed:@"news-headline-bgrd-normal.png"] forState:UIControlStateNormal];
            [arrow setBackgroundImage:[UIImage imageNamed:@"news-headline-bgrd-selected.png"] forState:UIControlStateHighlighted];
            arrow.userInteractionEnabled = YES;
        }
        
        for (int i = 0; i < numItems; i++) {
            NSDictionary *dict = [catData.categoryData objectAtIndex:i];
            
            AsyncButtonView *asyncButton = (AsyncButtonView *)[cell.contentView viewWithTag:i + 1];
            UILabel *aLabel = (UILabel *)[cell.contentView viewWithTag:i + 1 + 10];
           
            [asyncButton setHidden:NO];
            [aLabel setHidden:NO];
            
            NSString * Url = [NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[dict valueForKey:@"productThumbImageUrl"],192,192];
            
            NSURL * temp_loadingUrl = [NSURL URLWithString:Url];
            asyncButton.messageTag = [[dict valueForKey:@"messageId"] intValue];
            asyncButton.typeString = titleString;
            [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[dict valueForKey:@"senderName"]];
            
            [asyncButton setButtonData:[NSDictionary dictionaryWithObject:catData forKey:@"category"]];
            aLabel.text = [dict valueForKey:@"headline"];

        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170.;
}

- (void) showController
{

	[self.navigationController pushViewController:self.detailController animated:YES];
//@@@@ To be changed later
//    [[NewsDataManager sharedManager] getOneCategoryData:self.detailController type:self.detailController.m_Type filter:currentFilter number:24 numPage:1 addToExisting:NO];
}

- (void) IconClickedAction:(id) sender
{
	self.detailController = [[[DetailPageViewController alloc] init] autorelease];
    CategoryData *catData = [((MoreButton *) sender).data objectForKey:@"category"];
    self.detailController.m_data = catData;
    self.detailController.m_Type = catData.type;
    self.detailController.m_messageId = ((MoreButton *) sender).tag;
    self.detailController.filter = currentFilter;

    
    // Now check if the message is posted by current user, if so set the minetrack flag to yes
    int index = [catData indexOfMessage:self.detailController.m_messageId];
    
    NSDictionary *aData = [catData messageData:index];
    
    NSDictionary *tmp_Dict=[aData valueForKey:@"wsMessage"];
	NSString *custID=[tmp_Dict valueForKey:@"sentFromCustomer"];
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	if ([[custID lowercaseString] isEqualToString:[[prefs valueForKey:@"userName"] lowercaseString] ])
    {
        // Message is posted by current user
        [self.detailController setMineTrack:YES];
    }else{
        // This is not current user's message
        [self.detailController setMineTrack:NO];
    }
    
    self.detailController.curPage = 1;
    //@@@@ To be changed later
//    self.detailController.fetchingData = YES;
    self.detailController.m_CallBackViewController = self;
    [self showController];
/*
	self.detailController = [[[FittingRoomMoreViewController alloc] init] autorelease];
	self.detailController.m_CallBackViewController=self;		
    if (currentFilter == MINE) {
        self.detailController.MineTrack=YES;
    } else {
        self.detailController.MineTrack=NO;
    }
    self.detailController.GetPinnFlag = YES;
    self.detailController.m_messageId = ((UIButton *)sender).tag;
    
    [[NewsDataManager sharedManager] getStoryByMessageId:self message:((UIButton *)sender).tag];
*/    
}

/*
- (void) updateMessageData:(NSArray *)dataArray
{
    NSDictionary *tmp_dict=[[dataArray objectAtIndex:0] valueForKey:@"wsMessage"] ;
    NSString *type=[tmp_dict valueForKey:@"messageType"];
    
    CategoryData *data = [[NewsDataManager sharedManager] getCategoryDataByType:type];
    
    self.detailController.m_dataArray = dataArray;
    self.detailController.m_messageIDsArray = data.messageIDs;
    self.detailController.m_Type = type;
    self.detailController.curPage = 1;
    self.detailController.filter = currentFilter;
    [self showController];
}
*/
- (IBAction)moreButtonClicked:(MoreButton *)sender
{
    
    
    
    NSInteger row = [[sender.data objectForKey:@"index"] intValue];
    
    
    
    CategoryData *catData = [[NewsDataManager sharedManager] getCategoryDataByIndex:row];
    self.moreController = [[[NewsMoreViewController alloc] initWithNibName:@"NewsMoreViewController" bundle:[NSBundle mainBundle]] autorelease];
    self.moreController.m_Type = catData.type;
    self.moreController.m_data = catData;
    self.moreController.curPage = 1;
    self.moreController.filter = currentFilter;
    self.moreController.m_CallBackViewController=self;
    self.moreController.fetchingData = YES;

	[self.navigationController pushViewController:self.moreController animated:YES];
    [[NewsDataManager sharedManager] getOneCategoryData:self.moreController type:self.moreController.m_Type filter:currentFilter number:24 numPage:1 addToExisting:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"NewsPage"
                                                    withAction:@"moreButtonClicked"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction) clickOnPopular:(id) sender
{
    [m_exceptionButton setHidden:YES];
    [m_exceptionPage setHidden:YES];
    currentFilter = POPULAR;
    popularButton.selected = YES;
    friendsButton.selected = NO;
    followingButton.selected = NO;
    mineButton.selected = NO;
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    [[NewsDataManager sharedManager] getAllCategoriesData:self filter:POPULAR number:3 numPage:1 addToExisting:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"NewsPage"
                                                    withAction:@"clickOnPopular"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction) clickOnFriends:(id) sender
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height > 480)
    {
        [m_exceptionPage setImage:[UIImage imageNamed:@"exception_ADD FRIEND IN MESSAGE_iphone5"]];
        [m_exceptionButton setFrame:CGRectMake(28, 365 + 88, 265, 39)];
    }else{
    [m_exceptionPage setImage:[UIImage imageNamed:@"exception_ADD FRIEND IN MESSAGE"]];
        [m_exceptionButton setFrame:CGRectMake(28, 365, 265, 39)];
    }
    
    
    [m_exceptionButton setHidden:YES];
    [m_exceptionPage setHidden:YES];
    [m_exceptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [m_exceptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [m_exceptionButton setTitle:@"Add friends" forState:UIControlStateNormal];
    [m_exceptionButton setTitle:@"Add friends" forState:UIControlStateHighlighted];
    currentFilter = FRIENDS;
    popularButton.selected = NO;
    friendsButton.selected = YES;
    followingButton.selected = NO;
    mineButton.selected = NO;
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    [[NewsDataManager sharedManager] getAllCategoriesData:self filter:FRIENDS number:3 numPage:1 addToExisting:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"NewsPage"
                                                    withAction:@"clickOnFriends"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction) clickOnFollowing:(id) sender
{
    [m_exceptionPage setHidden:YES];
    [m_exceptionButton setHidden:YES];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height > 480)
    {
        [m_exceptionPage setImage:[UIImage imageNamed:@"exception_following_iphone5"]];
        [m_exceptionButton setFrame:CGRectMake(28, 365 + 88, 265, 39)];
    }else{
    [m_exceptionPage setImage:[UIImage imageNamed:@"exception_following"]];
        [m_exceptionButton setFrame:CGRectMake(28, 365, 265, 39)];
    }
    
    currentFilter = FOLLOWING;
    popularButton.selected = NO;
    friendsButton.selected = NO;
    followingButton.selected = YES;
    mineButton.selected = NO;
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    [[NewsDataManager sharedManager] getAllCategoriesData:self filter:FOLLOWING number:3 numPage:1 addToExisting:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"NewsPage"
                                                    withAction:@"clickOnFollowing"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction) clickOnMine:(id) sender
{
    [m_exceptionPage setHidden:YES];
    [m_exceptionButton setHidden:YES];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height > 480)
    {
        [m_exceptionPage setImage:[UIImage imageNamed:@"exception_paparazzi_iphone5"]];
        [m_exceptionButton setFrame:CGRectMake(28, 365 + 88, 265, 39)];
    }else{
    [m_exceptionPage setImage:[UIImage imageNamed:@"exception_paparazzi"]];
        [m_exceptionButton setFrame:CGRectMake(28, 365, 265, 39)];        
    }

    [m_exceptionButton setTitle:@"Take pictures" forState:UIControlStateNormal];
    [m_exceptionButton setTitle:@"Take pictures" forState:UIControlStateHighlighted];
    currentFilter = MINE;
    popularButton.selected = NO;
    friendsButton.selected = NO;
    followingButton.selected = NO;
    mineButton.selected = YES;
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    [[NewsDataManager sharedManager] getAllCategoriesData:self filter:MINE number:3 numPage:1 addToExisting:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"NewsPage"
                                                    withAction:@"clickOnMine"
                                                     withLabel:nil
                                                     withValue:nil];
}

-(IBAction)exceptionButtonAction:(id)sender 
{
    if (currentFilter == MINE) {
        [tnApplication goToPage:SHARE_TAB_INDEX];
    } else if (currentFilter == FRIENDS) {
        [tnApplication goToPage:FIND_FRIENDS_TAB_INDEX];
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
    [self openFilter];
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.newsTable];
	_reloading = NO;
	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}


- (void) dealloc
{
    [newsTable release];
    [notificationButton release];
    [m_exceptionPage release];//**
    [m_exceptionButton release];//**
    [detailController release];
    [tableCells release];
	_refreshHeaderView = nil;
    [super dealloc];
}
@end
