//
//  ProfileinfoSettingViewController.h
//  QNavigator
//
//  Created by softprodigy on 31/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"DBManager.h"

@interface ProfileinfoSettingViewController : UIViewController<UITextViewDelegate> {
	UITextView *m_theAboutMe;
	UITextView *m_interest;
	UITextView *m_favouriteBrand;
	UITextView *m_favouriteQuotation;
	NSDictionary *m_ProfileInfoDict;
	NSDictionary *m_getDatabaseData;
    UITextView *currentText;
}

@property(nonatomic,retain) IBOutlet UITextView *m_theAboutMe;
@property(nonatomic,retain) IBOutlet UITextView *m_interest;
@property(nonatomic,retain) IBOutlet UITextView *m_favouriteBrand;
@property(nonatomic,retain) IBOutlet  UITextView *m_favouriteQuotation;
@property(nonatomic,retain) IBOutlet  UITextView *currentText;

-(IBAction)btnCancelAction:(id)sender;
-(IBAction)btnSaveAction:(id)sender;
-(IBAction) sendDataToDatabase;
-(void)animateViewUpward;
-(void)animateViewDownward;
-(IBAction)btnBackAction:(id)sender;

// GDL: Not implemented, so I'll comment it out.
//-(IBAction) m_ShowActivityIndicator;

@end
