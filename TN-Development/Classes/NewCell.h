//
//  NewCell.h
//  TinyNews
//
//  Created by jay kumar on 5/31/13.
//
//

#import <UIKit/UIKit.h>
#import "UIGridViewCell.h"
#import "AsyncButtonView.h"
@interface NewCell : UIGridViewCell
{

}
@property (nonatomic, retain) IBOutlet AsyncButtonView *thumbnail;
@property (nonatomic, retain) IBOutlet UILabel *headline;
@end
