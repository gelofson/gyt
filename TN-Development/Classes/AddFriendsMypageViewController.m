//
//  AddFriendsMypageViewController.m
//  QNavigator
//
//  Created by softprodigy on 22/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddFriendsMypageViewController.h"
#import "QNavigatorAppDelegate.h"
#import "AuditingClass.h"
#import "Constants.h"
#import "TwitterFriendsViewController.h"
#import "TwitterProcessing.h"
#import "WhereBox.h"
#import "ShopbeeFriendsViewController.h"
#import "YahooFriendsViewController.h"
#import "GoogleFriendsViewController.h"
#import "Constants.h"
#import"BuzzButtonUseCase.h"
#import "LoadingIndicatorView.h"
#import "MyPageViewController.h"
#import "ViewAFriendViewController.h"
#import "InviteFriendsViaEmail.h"
#import "NewsDataManager.h"

@implementation AddFriendsMypageViewController

@synthesize m_btnTellAFriend;
@synthesize m_arrUserInfo;
@synthesize m_scrollView;
@synthesize searchbar;
@synthesize tblView;
@synthesize activity;
@synthesize feedBackPage;

QNavigatorAppDelegate *l_appDelegate;


#pragma mark -
#pragma mark Custom

-(IBAction) m_goToBackView
{
    if (isSearching)
    {
        [searchbar setText:@""];
        [m_scrollView setHidden:NO];
        [tblView setHidden:YES];
        [btnBack setHidden:YES];
        isSearching = !isSearching;
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)m_addAdrressBookFriend
{
    optionClick = YES;
	m_addFriend=[[AddAddressBookFriends alloc]initWithNibName:@"AddAddressBookFriends" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:m_addFriend animated:YES];
	[m_addFriend release];
	m_addFriend=nil;
	
}

-(IBAction)btnTellAFriendAction:(id)sender //for displaying the mail interface when the user clicks on the add friend from email button
{
	InviteFriendsViaEmail * vc = [[InviteFriendsViaEmail alloc] init];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];

#if 0
BHARAT: COMMENTED OUT AS PART OF US236:TA122 fixes
    optionClick = YES;
	[self performSelector:@selector(showPicker)];
#endif
}

-(IBAction)addFacebookfriends//**
{   
    [self facebookYesButtonClicked];
#if 0
    if (m_exceptionPage==nil) {//constant
        m_exceptionPage=[[UIImageView alloc] initWithFrame:CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y-searchbar.frame.size.height, tblView.frame.size.width, tblView.frame.size.height+searchbar.frame.size.height)];
        [m_exceptionPage setUserInteractionEnabled:YES];
        m_exceptionPageYesButton=[[UIButton alloc] initWithFrame:CGRectMake(10, 295, 140, 35)];
        [m_exceptionPageYesButton setImage:[UIImage imageNamed:@"btn_yes_blut_slide12_if-not-logged-into-twitter.png"] forState:UIControlStateNormal];
        m_exceptionPageNoButton=[[UIButton alloc] initWithFrame:CGRectMake(170, 295, 140, 35)];
        [m_exceptionPageNoButton setImage:[UIImage imageNamed:@"btn_no_yellow_slide12_if-not-logged-into-twitter.png"] forState:UIControlStateNormal];
        [m_exceptionPageNoButton addTarget:self action:@selector(exceptionPageNoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [m_exceptionPage addSubview:m_exceptionPageNoButton];
        [m_exceptionPage addSubview:m_exceptionPageYesButton];
        [m_exceptionPage setHidden:YES];
        [self.view addSubview:m_exceptionPage];
    }//constant end
    
    //variable
    [m_exceptionPage setImage:[UIImage imageNamed:@"facebook_exception.png"]];
    [m_exceptionPageYesButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [m_exceptionPageYesButton addTarget:self action:@selector(facebookYesButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [m_scrollView setHidden:YES];
    [m_exceptionPage setHidden:NO];
    //variable end
#endif   
}
-(void)facebookYesButtonClicked{//**
    [m_exceptionPage setHidden:YES];//constant
    [m_scrollView setHidden:NO];//constant

    optionClick = YES;
	ShopbeeFriendsViewController *shopbee_obj=[[ShopbeeFriendsViewController alloc]init];
	[shopbee_obj performSelector:@selector(setSelectedButton1:)];
	[self.navigationController pushViewController:shopbee_obj animated:YES];
	[shopbee_obj release];
	shopbee_obj=nil;


}
-(void)exceptionPageNoButtonPressed{
    [m_exceptionPage setHidden:YES];
    [m_scrollView setHidden:NO];


}
-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}

//<<<<<<< HEAD
-(IBAction)loadMoreClick
{
    if (endIndex < 300)
    {
        startIndex = endIndex+1;
        endIndex+=30;        
    }
    else
    {
     //   [btnLoadMore setHidden:YES];  
        [tblView setFrame:CGRectMake(0.0, 120.0, 320.0, 291.0)];
    }
    [self sendSearchRequest];
}

//=======
//>>>>>>> Naveed's updates  Exception pages, wishlist, searching bug fix,
#pragma mark -
#pragma mark MailComposer methods

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];
	[picker setSubject:kTellAFriendMailSubject];
	
	NSString *emailBody;
	emailBody=kTellAFriendMailBody;
	
	[picker setToRecipients:toRecipients];
	[toRecipients release];
	toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	[self presentModalViewController:picker animated:YES];
	
    [picker release];
	picker=nil;
	
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			//------------------------------------------------------------
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{	
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	[self dismissModalViewControllerAnimated:YES];
	
}
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(IBAction)btnFindTwitterFrnd:(id)sender//**
{
    [self twitterYesButtonClicked];
#if 0
    if (m_exceptionPage==nil) {//constant
        m_exceptionPage=[[UIImageView alloc] initWithFrame:CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y-searchbar.frame.size.height, tblView.frame.size.width, tblView.frame.size.height+searchbar.frame.size.height)];
        [m_exceptionPage setUserInteractionEnabled:YES];
        m_exceptionPageYesButton=[[UIButton alloc] initWithFrame:CGRectMake(180, 295, 73, 28)];
        [m_exceptionPageYesButton setImage:[UIImage imageNamed:@"blue_yes_btn.png"] forState:UIControlStateNormal];
        
        m_exceptionPageNoButton=[[UIButton alloc] initWithFrame:CGRectMake(70, 295, 72, 26)];
        [m_exceptionPageNoButton setImage:[UIImage imageNamed:@"orange_cancel_btn.png"] forState:UIControlStateNormal];
        [m_exceptionPageNoButton addTarget:self action:@selector(exceptionPageNoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [m_exceptionPage addSubview:m_exceptionPageNoButton];
        [m_exceptionPage addSubview:m_exceptionPageYesButton];
        [m_exceptionPage setHidden:YES];
        [self.view addSubview:m_exceptionPage];
    }//constant end
    
    //variable
    [m_exceptionPage setImage:[UIImage imageNamed:@"exception_image.png"]];
    [m_exceptionPageYesButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [m_exceptionPageYesButton addTarget:self action:@selector(twitterYesButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [m_scrollView setHidden:YES];
    [m_exceptionPage setHidden:NO];
#endif
    //variable end
}
-(void)twitterYesButtonClicked{//**

    [m_exceptionPage setHidden:YES];//constant
    [m_scrollView setHidden:NO];//constant
        

    optionClick = YES;
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator startLoadingView:self];
    
	
	TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
	temp_twitterPross.m_addFrndMypageVC=self;
	[temp_twitterPross btnTellFriendByTwitterAction:2];	

}


-(void)gotAllFriendsFromTwitter:(NSArray *)tempFriendsArray
{
	NSLog(@"%@",tempFriendsArray);
	
	TwitterFriendsViewController *Twitter_obj=[[TwitterFriendsViewController alloc]init];
	Twitter_obj.m_arrTwitterFriends=[NSArray arrayWithArray:tempFriendsArray];
	[self.navigationController pushViewController:Twitter_obj animated:YES];
	[Twitter_obj release];
	Twitter_obj=nil;
	
	
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */



-(IBAction)btnTellFriendByYahooAction:(id)sender
{
	//YahooFriendsViewController *tempYahooObj=[[YahooFriendsViewController alloc]init];
    //	[self.navigationController pushViewController:tempYahooObj animated:YES];
    //	[tempYahooObj release];
    //	tempYahooObj=nil;
    
    optionClick = YES;
	YahooFriendsViewController *tempYahooObj=[[YahooFriendsViewController alloc]init];
	[self.navigationController pushViewController:tempYahooObj animated:YES];
	[tempYahooObj release];
	tempYahooObj=nil;
	
}	

-(void)btnTellFriendByGmailAction:(id)sender
{
	GoogleFriendsViewController *google_obj=[[GoogleFriendsViewController alloc]init];
	[self.navigationController pushViewController:google_obj animated:YES];
	[google_obj release];
	google_obj=nil;
}


-(IBAction)btnTellFriendByHotmailAction:(id)sender
{
    
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This feature is currently not supported." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tempAlert show];
	[tempAlert release];
	tempAlert=	nil;
}


#pragma mark -
-(void)viewDidAppear:(BOOL)animated
{
}


-(void)viewWillAppear:(BOOL)animated
{	
    optionClick = NO;
    
    [self setBackButton];
}

-(void)setBackButton
{
    if (!l_appDelegate.isAddFriendLoadFromTab) 
    {
		[btnBack setHidden:NO];
    }
	else 
    {
        if (isSearching)
        {
            [btnBack setHidden:NO];
        }
        else
            [btnBack setHidden:YES];			
    }
	
	// Always hide back button, because we removed "Add Friends" from My Page Tab.
	// This view shows in Add Friends Tab.
	btnBack.hidden = YES;
}

- (void)viewDidLoad 
{
	[super viewDidLoad];
    self.trackedViewName = @"Find Friends Page";
   
    searchbar.tintColor=[UIColor colorWithRed:243/255.0 green:242/255.0 blue:246/255.0 alpha:1.0];
    
    for (UIView *view in searchbar.subviews)
    {
        //if ([[view.class description]isEqualToString:@"UINavigationButton"])
        if([view isKindOfClass:[UIButton class]])
        {
            
            
           
           [((UIButton *)view) setImage:[UIImage imageNamed:@"New_cancel_btn.png"] forState:UIControlStateNormal];
                      

        }
    }
    
    isSearching = NO;
    [tblView setHidden:YES];
    
   l_appDelegate.objAddFriendsMypageViewController = self;
	
	m_scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0,120,320,333)];
	
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	
	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
    
    /*UIButton *temp_btnShopbee = [UIButton buttonWithType:UIButtonTypeRoundedRect];
     temp_btnShopbee.frame = CGRectMake(13, 20, 292, 45); // position in the parent view and set the size of the button
     [temp_btnShopbee setTitle:@"Search all FashionGram users" forState:UIControlStateNormal];
     //	[temp_btnShopbee setImage:[UIImage imageNamed:@"email.png"] forState:UIControlStateNormal];
     [temp_btnShopbee addTarget:self action:@selector(btnShopbeeAction:) forControlEvents:UIControlEventTouchUpInside];
     [m_scrollView addSubview:temp_btnShopbee];
     */
	
	UIButton *temp_btnEmail = [UIButton buttonWithType:UIButtonTypeCustom];
    [temp_btnEmail setFrame:CGRectMake(10, 5, 82, 101)]; // position in the parent view and set the size of the button
    [temp_btnEmail setImage:[UIImage imageNamed:@"New_email_icon.png"] forState:UIControlStateNormal];	
    [temp_btnEmail addTarget:self action:@selector(btnTellAFriendAction:) forControlEvents:UIControlEventTouchUpInside];
    [m_scrollView addSubview:temp_btnEmail];
	
#if 0
Bharat: COMMENTED OUT AFTER US236:TA122 fixes
	UIButton *temp_btnAddressbook = [UIButton buttonWithType:UIButtonTypeCustom];
    temp_btnAddressbook.frame = CGRectMake(120, 5, 82, 101); // position in the parent view and set the size of the button
    [temp_btnAddressbook setImage:[UIImage imageNamed:@"addressbook.png"] forState:UIControlStateNormal];
    [temp_btnAddressbook addTarget:self action:@selector(m_addAdrressBookFriend) forControlEvents:UIControlEventTouchUpInside];
    [m_scrollView addSubview:temp_btnAddressbook];
#endif
	
	UIButton *temp_btnYahoo = [UIButton buttonWithType:UIButtonTypeCustom];
    //temp_btnYahoo.frame = CGRectMake(13, 210, 292, 45); // position in the parent view and set the size of the button
    [temp_btnYahoo setFrame:CGRectMake(120, 5, 82, 101)];// position in the parent view and set the size of the button
    [temp_btnYahoo setImage:[UIImage imageNamed:@"yahoo_icon.png"] forState:UIControlStateNormal]; 
    [temp_btnYahoo addTarget:self action:@selector(btnTellFriendByYahooAction:) forControlEvents:UIControlEventTouchUpInside];
    [m_scrollView addSubview:temp_btnYahoo];
	
	
//	UIButton *temp_btnGoogle = [UIButton buttonWithType:UIButtonTypeCustom];
//    //temp_btnGoogle.frame = CGRectMake(13, 260, 292, 45); // position in the parent view and set the size of the button
//    [temp_btnGoogle setFrame:CGRectMake(230, 5, 82, 101)];// position in the parent view and set the size of the button
//    
//    [temp_btnGoogle setImage:[UIImage imageNamed:@"gmail_icons@2x.png"] forState:UIControlStateNormal];     
//	
//    [temp_btnGoogle addTarget:self action:@selector(btnTellFriendByGmailAction:) forControlEvents:UIControlEventTouchUpInside];
//    [m_scrollView addSubview:temp_btnGoogle];
	
//    UIButton *temp_btnTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
//    temp_btnTwitter.frame = CGRectMake(10, 125,82, 101); // position in the parent view and set the size of the button
//    [temp_btnTwitter setImage:[UIImage imageNamed:@"twitter_icons.png"] forState:UIControlStateNormal];
//    [temp_btnTwitter addTarget:self action:@selector(btnFindTwitterFrnd:) forControlEvents:UIControlEventTouchUpInside];
//    [m_scrollView addSubview:temp_btnTwitter];

    
	UIButton *temp_btnFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    temp_btnFacebook.frame = CGRectMake(230, 5, 82, 101);//CGRectMake(10, 125,82, 101); // position in the parent view and set the size of the button
    [temp_btnFacebook setImage:[UIImage imageNamed:@"facebook_icon.png"] forState:UIControlStateNormal];
    [temp_btnFacebook addTarget:self action:@selector(addFacebookfriends) forControlEvents:UIControlEventTouchUpInside];
    [m_scrollView addSubview:temp_btnFacebook];
	
	/*UIButton *temp_btnHotmail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
     temp_btnHotmail.frame = CGRectMake(13, 320, 292, 45); // position in the parent view and set the size of the button
     [temp_btnHotmail setImage:[UIImage imageNamed:@"hotmail.png"] forState:UIControlStateNormal];
     // add targets and actions
     [temp_btnHotmail addTarget:self action:@selector(btnTellFriendByHotmailAction:) forControlEvents:UIControlEventTouchUpInside];
     // add to a view
     [m_scrollView addSubview:temp_btnHotmail];*/
	
	m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width, 350.0);	//500	//-50);	
	[self.view addSubview:m_scrollView];
    //	[m_scrollView release];
    //	m_scrollView=nil;	
}

// GDL: Changed everything below.

#pragma mark - memory management

-(void)viewWillDisappear:(BOOL)animated
{
    if (!l_appDelegate.isAddFriendLoadFromTab && optionClick)
        l_appDelegate.isAddFriendLoadFromTab = NO;
    else
        l_appDelegate.isAddFriendLoadFromTab = YES;
}

- (void)viewDidUnload 
{   
    [super viewDidUnload];
    
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];
    
    // Release retained IBOutlets.
    self.m_btnTellAFriend = nil;
    
    // Is this created in viewDidLoad?
 	[m_addFriend release];
	m_addFriend=nil;
}

- (void)dealloc {
    [m_exceptionPage release];//**
    [m_exceptionPageNoButton release];//**
    [m_exceptionPageYesButton release];//**
    [feedBackPage release];//**
    [m_btnTellAFriend release];
    [m_addFriend release];
    [m_arrUserInfo release];
    [m_scrollView release];
    [searchbar release];
    [tblView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Searchbar Delegates 
#pragma mark

-(void)sendSearchRequest
{
    [activity startAnimating];
    if (m_mutResponseData)
    {
        [m_mutResponseData release];
        m_mutResponseData = nil;
    }
    
    m_mutResponseData =[[NSMutableData alloc]init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *customerID= [prefs objectForKey:@"userName"];
    
    NSMutableString *temp_url;
    temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=shopbeeUsersSearch&custid=%@&searchtxt=%@&startindex=%d&endindex=%d",kServerUrl,customerID,searchbar.text,startIndex,endIndex];
    
    [temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
    
    temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",temp_url);
    
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
    
    NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
    
    //[theRequest setHTTPBody:[temp_url dataUsingEncoding:NSUTF8StringEncoding]];
    [theRequest setAllHTTPHeaderFields:headerFieldsDict];
    [theRequest setHTTPMethod:@"GET"];
    
    
    NSURLConnection *l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
    
    [l_theConnection start];
    
    
    if(l_theConnection)
    {
        NSLog(@"Request sent to get data");
    }
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    isSearchBarEditing=YES;  

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar 
{   
    
    [tblView setHidden:NO];
    [feedBackPage setHidden:YES];
    
    isSearching = YES;
    //[btnBack setHidden:NO];
    [activity startAnimating];
    startIndex = 0;
    endIndex = 30;
//<<<<<<< HEAD
    
//=======
    [arrData removeAllObjects];
    [tblView reloadData];
//>>>>>>> Naveed's updates  Exception pages, wishlist, searching bug fix,
    [self sendSearchRequest];
    
    [tblView setHidden:NO];
    [m_scrollView setHidden:YES];
    
    [self.searchbar resignFirstResponder];
    isSearchBarEditing=NO;
    [self enableCancelButton:searchBar];
    //	[self.searchbar setShowsCancelButton:YES animated:YES];	
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{   
    //[tblView setHidden:YES];

    if (isSearchBarEditing) {
        [self.searchbar resignFirstResponder];
        
        [self enableCancelButton:searchBar];
       
        
    }
    else
    {   
        [feedBackPage setHidden:YES];
       
        [self.searchbar resignFirstResponder];
        [tblView setHidden:YES];

        [m_scrollView setHidden:NO];
        [self viewWillAppear:YES];
    
    }
    [searchBar setText:@""];
    isSearchBarEditing=NO;
    //	[self.searchbar setShowsCancelButton:NO animated:YES];		
}
//implemented to enable the canel button all the time

- (void)enableCancelButton:(UISearchBar *)aSearchBar {
    for (id subview in [aSearchBar subviews]) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setEnabled:TRUE];
        }
    }  
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	
}

/*- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
 {
 [m_searchBar setShowsCancelButton:YES animated:YES];
 
 return YES;
 
 }*/

#pragma mark -
#pragma mark Connection response methods
#pragma mark

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[m_mutResponseData setLength:0];
	
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	responseResult= [httpResponse statusCode];
	NSLog(@"AddFriends %d",responseResult);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{   
    
    if (responseResult == 200 ) {
        NSString *temp_string=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
        
        SBJSON *temp_objSBJson=[[SBJSON alloc]init];
        NSArray *temp_arrResponse =[[NSArray alloc]initWithArray:[temp_objSBJson objectWithString:temp_string]];
        NSLog(@"%@",temp_arrResponse);
        if ([temp_arrResponse count] > 0)
        {
            [feedBackPage setHidden:YES];
            NSDictionary *dictMain = [temp_arrResponse objectAtIndex:0];
            // The found people are divided intto two parts: nonFriends and friends.
            // We will display both.
            NSArray *arrNonFriends = [dictMain valueForKey:@"nonFriends"];
            NSLog(@"nonFriends count: %d",[arrNonFriends count]);
            NSArray *arrFriends = [dictMain valueForKey:@"friends"];
            NSLog(@"friends count: %d",[arrFriends count]);
            
            /*if (arrData)
             {
             [arrData removeAllObjects];
             [arrData release];
             arrData = nil;
             }*/
            if(startIndex > 1)
            {
                NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:arrData];
                [arrData removeAllObjects];
                [arrData release];
                arrData = nil;
                
                arrData = [[NSMutableArray alloc] init];
                for (int i=0; i<[arr count]; i++)
                    [arrData addObject:[arr objectAtIndex:i]];
                
                for (int i=0; i<[arrNonFriends count]; i++)
                    [arrData addObject:[arrNonFriends objectAtIndex:i]];
                
            }
            else
            {
                arrData = [[NSMutableArray alloc] init];
                for (int i=0; i<[arrNonFriends count]; i++)
                    [arrData addObject:[arrNonFriends objectAtIndex:i]];
                for (int i=0; i<[arrFriends count]; i++)
                    [arrData addObject:[arrFriends objectAtIndex:i]];
                
            }
            [self loadSearchData];
        }
        else
        {
            [tblView reloadData];
            [activity stopAnimating];
        }
        if ([arrData count]<=0) {
            [tblView setHidden:YES];
            [feedBackPage setHidden:NO];
        }
    } else
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [activity stopAnimating];
    NSLog(@"....%@",[error description]);
}

-(void)loadSearchData
{
    /*if (arrImage)
     {
     [arrImage removeAllObjects];
     [arrImage release];
     arrImage = nil;
     }*/
    
    if (startIndex == 0)
    {
        arrImage = [[NSMutableArray alloc] init];        
    }
    
    for (int i=0; i<[arrData count]; i++)
    {
        NSDictionary *dict = [arrData objectAtIndex:i];
        if ([[dict valueForKey:@"profileThumbPicUrl"] length] > 0) 
        {
            NSString *strURL = [NSString stringWithFormat:@"%@%@",kServerUrl,[dict valueForKey:@"profileThumbPicUrl"]];
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
            UIImage *img = [UIImage imageWithData:imgData];
            [arrImage addObject:img];
        }
        else
        {
            UIImage *img = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"no-image" ofType:@"png"]];
            [arrImage addObject:img];
        }
    }
    
   
    [tblView reloadData];
    [activity stopAnimating];
}

#pragma mark Table View Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 68.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *newCell;
    NSString *strIdentifier= [NSString stringWithFormat:@"Cell %@",indexPath];
    
    newCell=[tableView dequeueReusableCellWithIdentifier:strIdentifier];

     if (indexPath.row<[arrData count]) {
         if(newCell==nil)
             newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier]autorelease];
         
        newCell.selectionStyle = UITableViewCellSelectionStyleNone;
//		newCell.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_with_arrow_in_add_friend.png"]];
        UIImageView *backimageView=[[UIImageView alloc]init];
        backimageView.frame=CGRectMake(10.0,-1.0,300.0,70.0);//(5.0,0.0,310.0,70.0);
        backimageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"white_bar_without_arrow" ofType:@"png"]];
        [newCell.contentView addSubview:backimageView];
        [backimageView release];
        
        NSDictionary *dict = [arrData objectAtIndex:indexPath.row];
        
        UILabel *lblfnm=[[UILabel alloc]init];
        lblfnm.frame=CGRectMake(80,18,250,25);
        lblfnm.font=[UIFont fontWithName:kFontName size:14];
        lblfnm.backgroundColor=[UIColor clearColor];
        lblfnm.text= [NSString stringWithFormat:@"%@ %@", [dict valueForKey:@"firstName"], [dict valueForKey:@"lastName"]];
        //lblfnm.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
         lblfnm.textColor=[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0];
         //lblfnm.textColor=[UIColor blackColor];
        [newCell.contentView addSubview:lblfnm];
        [lblfnm release];
        
//        UILabel *lbllnm=[[UILabel alloc]init];
//        lbllnm.frame=CGRectMake(80,38,210,25);
//        lbllnm.font=[UIFont fontWithName:kFontName size:16];
//        lbllnm.backgroundColor=[UIColor clearColor];
//        lbllnm.text=[dict valueForKey:@"lastName"];
//        lbllnm.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
//        [newCell.contentView addSubview:lbllnm];
//        [lbllnm release];
        
        UIImageView *imageView=[[UIImageView alloc]init];
		imageView.frame=CGRectMake(15,10,55,55);
        imageView.tag=50;
        imageView.contentMode=UIViewContentModeScaleAspectFit;    
        imageView.image = [arrImage objectAtIndex:indexPath.row];
        [newCell.contentView addSubview:imageView];
        [imageView release];
        
         UIImageView *lineImageView=[[UIImageView alloc]init];
         [lineImageView setFrame:CGRectMake(0, 67, 320, 1)];
         [lineImageView setImage:[UIImage imageNamed:@"grey_line.png"]];
         [newCell.contentView addSubview:lineImageView];
         [lineImageView release];
         
         if (!newCell.accessoryView)
             newCell.accessoryView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_friends"]] autorelease];
         
        return newCell;
    }
    if (newCell==nil) {
         newCell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
   
    newCell.selectionStyle = UITableViewCellSelectionStyleNone;	
	//newCell.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_with_arrow_in_add_friend.png"]];
	UIImageView *backimageView=[[UIImageView alloc]init];
	backimageView.frame=CGRectMake(5.0,-1.0,310.0,70.0);

	backimageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"white_bar_without_arrow" ofType:@"png"]];
	[newCell.contentView addSubview:backimageView];
	[backimageView release];
    
    NSDictionary *dict = [arrData objectAtIndex:indexPath.row];
    
	UILabel *lblfnm=[[UILabel alloc]init];
	lblfnm.frame=CGRectMake(80,18,250,25);
	lblfnm.font=[UIFont fontWithName:kFontName size:14];
	lblfnm.backgroundColor=[UIColor clearColor];
	lblfnm.text= [NSString stringWithFormat:@"%@ %@", [dict valueForKey:@"firstName"], [dict valueForKey:@"lastName"]];
	//lblfnm.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
   // lblfnm.textColor=[UIColor blackColor];
    lblfnm.textColor=[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0];
	[newCell.contentView addSubview:lblfnm];
	[lblfnm release];
	
//        UILabel *lbllnm=[[UILabel alloc]init];
//        lbllnm.frame=CGRectMake(80,38,210,25);
//        lbllnm.font=[UIFont fontWithName:kFontName size:16];
//        lbllnm.backgroundColor=[UIColor clearColor];
//        lbllnm.text=[dict valueForKey:@"lastName"];
//        lbllnm.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
//        [newCell.contentView addSubview:lbllnm];
//        [lbllnm release];
    
    UIImageView *imageView=[[UIImageView alloc]init];
    imageView.frame=CGRectMake(15,10,55,55);
    imageView.tag=50;
    imageView.contentMode=UIViewContentModeScaleAspectFit;    
    imageView.image = [arrImage objectAtIndex:indexPath.row];
    [newCell.contentView addSubview:imageView];
    [imageView release];
    
    UIImageView *lineImageView=[[UIImageView alloc]init];
    [lineImageView setFrame:CGRectMake(0, 67, 320, 1)];
    [lineImageView setImage:[UIImage imageNamed:@"grey_line.png"]];
    [newCell.contentView addSubview:lineImageView];
    [lineImageView release];

    if (!newCell.accessoryView)
        newCell.accessoryView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_friends"]] autorelease];

    
//=======
    [newCell addSubview:backimageView];
    //making load more label for last cell
    UILabel *temp_lblLoadMore=[[UILabel alloc]init];
    temp_lblLoadMore.frame=CGRectMake(85,20,200,25);
    temp_lblLoadMore.font=[UIFont fontWithName:kFontName size:16];
    temp_lblLoadMore.backgroundColor=[UIColor clearColor];
    temp_lblLoadMore.text=@"Load more items..";
    [newCell addSubview:temp_lblLoadMore];
    [temp_lblLoadMore release];		
 
   
//>>>>>>> Naveed's updates  Exception pages, wishlist, searching bug fix,
    return newCell;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row>=[arrData count]) {
        if (endIndex < 300)
        {
            startIndex = endIndex+1;
            endIndex+=30;        
        }
        else
        {
             
            [tblView setFrame:CGRectMake(0.0, 120.0, 320.0, 291.0)];
        }
        [self sendSearchRequest];

    }
    else{
        ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
        NSDictionary *tmp_dictionary=[arrData objectAtIndex:indexPath.row];
        tempViewAFriend.m_Email=[[arrData objectAtIndex:indexPath.row] valueForKey:@"emailid"];
        tempViewAFriend.m_strLbl1=[tmp_dictionary valueForKey:@"firstName"];
        tempViewAFriend.messageLayout = NO;
        [self.navigationController pushViewController:tempViewAFriend animated:YES];
        [tempViewAFriend release];
        tempViewAFriend=nil;
    }
    
    
   /* MyPageViewController *objMyPageViewController = [[MyPageViewController alloc] initWithNibName:@"MyPageViewController" bundle:nil];
    objMyPageViewController.userID = [[arrData objectAtIndex:indexPath.row] valueForKey:@"emailid"];
    [self.navigationController pushViewController:objMyPageViewController animated:YES];
    [objMyPageViewController release];*/
}

@end
