//
//  CustomTopBarView.h
//  QNavigator
//
//  Created by Soft Prodigy on 25/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CategoryButtonsDelegate
//Implemented in Big sales ads view
-(void)categoriesButtonAction:(id)sender;

// GDL: These are needed by CustomTopBarView's categoriesDelegate.
-(void)switchIndyShopListingView;
-(void)switchLogosListingView;

@end

@interface CustomTopBarView : UIView <UIScrollViewDelegate>
{
	UIImageView *m_imgViewLogo;
	UIImageView *m_imgViewBlueBar;
	NSArray *m_arrCategories;
	int m_intSelectedCategory;

	// GDL: Added protocol to id.
    id <CategoryButtonsDelegate> categoriesDelegate;
    
	UIScrollView *m_scrollView;
	UIPageControl *m_pageControl;
    int numberOfBarButtons;
}

@property (nonatomic,retain) UIImageView *m_imgViewLogo;
@property (nonatomic,retain) UIImageView *m_imgViewBlueBar;
@property (nonatomic,copy) NSArray *m_arrCategories;

// GDL: Added protocol to id.
@property (nonatomic,retain)id <CategoryButtonsDelegate> categoriesDelegate;
@property (nonatomic,retain)UIScrollView *m_scrollView;
@property (nonatomic,retain)UIPageControl *m_pageControl;
@property int m_intSelectedCategory;

-(void)addCategoryBarButtons;
-(void)selectCategory:(int)catTag;
-(void)createScrollBar;
-(void)scrollToBigSaleIcon;
-(void)deselectCustomBarButtons;

@end
