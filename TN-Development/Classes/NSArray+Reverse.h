//
//  NSArray+Reverse.h
//  TinyNews
//
//  Created by Nava Carmon on 17/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Reverse)
- (NSArray *)reversedArray; 

@end
