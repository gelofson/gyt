//
//  MyFriendViewController.m
//  TinyNews
//
//  Created by jay kumar on 5/25/13.
//
//

#import "MyFriendViewController.h"
#import "LoadingIndicatorView.h"
#import <QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"QNavigatorAppDelegate.h"
#import"ViewAFriendViewController.h"
#import"ShopbeeAPIs.h"
#import"JSON.h"
#import"AsyncImageView.h"
#import "NewsDataManager.h"
#import "Cell.h"
#import "NewCell.h"


@interface MyFriendViewController ()

@end

@implementation MyFriendViewController
@synthesize table;
@synthesize m_friendsArray;
@synthesize footerView;
@synthesize m_image;
LoadingIndicatorView *l_indicater;
QNavigatorAppDelegate *l_appDelegate;

ShopbeeAPIs *l_request;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.table.tableFooterView = [self gridFooterView];
    
    l_indicater=[LoadingIndicatorView SharedInstance];
    
    [l_indicater startLoadingView:self];
	l_request=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	[l_request getFriendsList:@selector(requestCallBackMethodForFriendsList:responseData:) tempTarget:self customerid:customerID loginId:customerID];
	[l_request release];
	l_request=nil;

    
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark- Custom Button
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(IBAction)moreclicked:(id)sender
{

}


#pragma mark- CallBack Methods

-(void)requestCallBackMethodForFriendsList:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_indicater stopLoadingView];
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
	if ([responseCode intValue]==200)
	{
        NSArray *friendsArray = [tempString JSONValue];
        NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"requestType"
                                                                         ascending:NO] autorelease];
        NSSortDescriptor *sortDescriptor2 = [[[NSSortDescriptor alloc] initWithKey:@"firstname"
                                                                         ascending:YES selector:@selector(caseInsensitiveCompare:)] autorelease];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, sortDescriptor2, nil];
        
        self.m_friendsArray = [friendsArray sortedArrayUsingDescriptors:sortDescriptors];
        [table reloadData];
		[l_indicater stopLoadingView];
		CGRect r;
		if ([m_friendsArray count] == 0) {
			self.table.hidden = YES;
			[feedBackPage setImage:[UIImage imageNamed:@"exception_alone"]];
			feedBackPage.hidden = NO;
            //m_addFriendButton.hidden = NO;
            
		} else {
			self.table.hidden = NO;
			//self.m_searchBar.hidden = NO;
			feedBackPage.hidden = YES;
           // m_addFriendButton.hidden = YES;
		}
		
		
	}
	
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]!=-1)
	{
		[l_indicater stopLoadingView];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
}




#pragma mark- UIGridViewDelegate

- (UIView *) gridFooterView
{
    if (!self.footerView) {
        self.footerView = [[[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 30)] autorelease];
        
    }
    return  self.footerView;
}



- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 80;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 120;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 4;
}


- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid
{
	return [m_friendsArray count];
}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
	NewCell *cell = (NewCell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[[NewCell alloc] init] autorelease];
	}
	
    NSInteger indexInArray = rowIndex * 4 + columnIndex;
	
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_friendsArray   objectAtIndex:indexInArray]valueForKey:@"imageUrl"]];
    cell.headline.text= [[m_friendsArray objectAtIndex:indexInArray]valueForKey:@"firstname"];
    cell.thumbnail.messageTag = indexInArray+1;
    cell.thumbnail.cropImage = YES;
    cell.thumbnail.maskImage = YES;
    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:[[m_friendsArray objectAtIndex:indexInArray]valueForKey:@"email"]];
    
	return cell;
}

- (void) ProductBtnClicked:(id)sender
{
    int tag=((UIButton *)sender).tag-1;
    
    ViewAFriendViewController *tmp_obj=[[ViewAFriendViewController alloc] init];//initWithNibName:@"ViewAFriendViewController" bundle:[NSBundle mainBundle]];
	tmp_obj.m_strLbl1 = [[m_friendsArray objectAtIndex:tag]valueForKey:@"firstname"];
    
	tmp_obj.m_Email=[[m_friendsArray objectAtIndex:tag]valueForKey:@"email"];
	tmp_obj.m_RequestTypeString=[[m_friendsArray objectAtIndex:tag] valueForKey:@"requestType"];
	[tmp_obj.m_strLbl1 retain];
	tmp_obj.m_personImage=m_image;
	tmp_obj.LevelTrackFriendsView=0;
    tmp_obj.messageLayout = NO;
	[self.navigationController pushViewController:tmp_obj animated:YES];
	[tmp_obj release];
	tmp_obj=nil;

    
    
    
    
    
    
    /*
     NSLog(@"%d clicked", ((UIButton *)sender).tag);
     self.detailController = [[[FittingRoomMoreViewController alloc] init] autorelease];
     self.detailController.m_CallBackViewController=self;
     if (filter == MINE) {
     self.detailController.MineTrack=YES;
     } else {
     self.detailController.MineTrack=NO;
     }
     self.detailController.GetPinnFlag = YES;
     self.detailController.m_messageId = ((UIButton *)sender).tag;
     
     [[NewsDataManager sharedManager] getStoryByMessageId:self message:((UIButton *)sender).tag];
     */
//    self.detailController = [[[DetailPageViewController alloc] init] autorelease];
//    self.detailController.m_data = self.m_data;
//    self.detailController.m_Type = self.m_data.type;
//    self.detailController.m_messageId = ((UIButton *) sender).tag;
//    //self.detailController.m_messageIDsArray = self.m_data.messageIDs;
//    self.detailController.filter = filter;
//    self.detailController.fetchingData = NO;
//    self.detailController.mineTrack = (filter == MINE);
//    self.detailController.curPage = curPage;
//    self.detailController.m_CallBackViewController = self;
//    [self showController];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    [table release];
    [m_friendsArray release];
    [footerView release];
    [super dealloc];
}
@end
