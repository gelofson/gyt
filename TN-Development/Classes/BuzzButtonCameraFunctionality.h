//
//  BuzzButtonCameraFunctionality.h
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ContentViewController.h"
#import "GPUImage.h"

//#define PLACEHOLDER_COMMENTS_MESSAGE @"< Comments go here >"
#define PLACEHOLDER_COMMENTS_MESSAGE @""

@interface BuzzButtonCameraFunctionality : ContentViewController <UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIActionSheetDelegate > {
	UIImagePickerController *m_imgPicker;
	UIImageView *m_userImage;
	UITextView *l_textView;
	UITextView *m_buttomTextView;
	UITextView *m_buttomTextView2;
	UILabel *m_LabCountChar;
	UIImageView *m_imageView;
	UIImageView *m_imageViewCrop;

	UISlider *m_sliderZooming;
	float scale;
	float scaleValue;
	float previousScaleValue;	
	
	UIImage *m_buzzImage;
	UISlider *smoothnessSlider;
    UISlider *brightnessSlider;
    UISlider *saturationSlider;
    
    int smoothness;
    float gamma,initialDistance;
    float saturation;
	UIView *m_beautyView; 
	UIView *m_beautyViewBorder;
	
	CGPoint touch1,touch2;
	
	UIImageView *m_bottomBar;
	UIActivityIndicatorView *m_indicatorView;
	UIButton *btnEdit;
	UIButton *btnPreview;
	UIButton *btnUndo;
	UIView *m_ViewTopTextView;
	NSMutableArray *m_arrWithUndoDict;	
	NSDictionary *m_ZoomDict;
	NSDictionary *m_BeautyDict;
	NSDictionary *m_CommentDict;
	NSString *m_strType;
	NSData *m_personData;
	UIImage	*m_personImage;
	UILabel *m_lblselectAnImg;
	UIButton *m_sendBtn;
	UIView *m_zoomSliderView;
	UILabel *m_previewViewlabel3;
	UIView *m_requiredView;
	UIButton *m_ZoomBtn;
    UIView *greyOverlay;
	UIScrollView *m_scrollView;
	BOOL shouldUseEditedImageOfPhoto;
    
	BOOL wasCameraPickerActiveThenSkipCamLaunch;
    NSMutableDictionary *metadata;
    NSString *moviePath;
    NSURL *movieURL;
    // Video-related
    IBOutlet UIView *videoView;
    IBOutlet UIButton *sourceSwitch;
    IBOutlet UIButton *shootButton;
    IBOutlet UIButton *recordButton;
    IBOutlet UIButton *cameraButton;
    IBOutlet UIButton *flashButton;
    IBOutlet UIButton *playButton;
    IBOutlet UIView *headerView;
    IBOutlet UIView *shareView;
    IBOutlet UIView *sharePhotoView;
    IBOutlet UIView *movieEnclosureView;
    IBOutlet UIImageView *thumbnailForMovie;
    IBOutlet UIView *defaultView;
    BOOL    sourceCamera, recordingVideo, rearCamera, flashOn;
    BOOL    playing;
    UIScrollView *_filterScrollView;
    UIButton *_filtersToggleButton;
    GPUImageOutput<GPUImageInput> *filter;
    int selectedFilter;
    UIImageView *_filtersBackgroundImageView;
    GPUImagePicture *staticPicture;
    UIButton *m_BeautyBtn;
    MPMoviePlayerController *moviePlayerController;
}

@property BOOL shouldUseEditedImageOfPhoto;
@property BOOL wasCameraPickerActiveThenSkipCamLaunch;

@property (nonatomic,retain) IBOutlet UIScrollView *m_scrollView;
@property (nonatomic,retain) IBOutlet UIView *m_requiredView;
@property (nonatomic,retain) IBOutlet UILabel *m_previewViewlabel3;
@property (nonatomic,retain) IBOutlet UIView *m_zoomSliderView;
@property (nonatomic,retain) IBOutlet UIView *m_beautyViewBorder;
@property (nonatomic,retain) IBOutlet UIButton *m_sendBtn;
@property (nonatomic,retain) IBOutlet UILabel *m_lblselectAnImg;
@property (nonatomic,retain)NSString *m_strType;
@property (nonatomic,retain)NSDictionary *m_ZoomDict;
@property (nonatomic,retain)NSDictionary *m_BeautyDict;
@property (nonatomic,retain)NSDictionary *m_CommentDict;
@property (nonatomic,retain) NSMutableArray *m_arrWithUndoDict;
@property (nonatomic,retain) IBOutlet UIView *m_ViewTopTextView;
@property (nonatomic,retain) IBOutlet	UIButton *btnEdit;
@property (nonatomic,retain) IBOutlet	UIButton *btnPreview;
@property (nonatomic,retain) IBOutlet	UIButton *btnUndo;
@property (nonatomic,retain) UIActivityIndicatorView *m_indicatorView;
@property (nonatomic,retain) IBOutlet UIImageView *m_bottomBar;
@property (nonatomic,retain) IBOutlet UIImageView *m_imageViewCrop;
@property (nonatomic, retain)IBOutlet UIView *m_beautyView;
@property (nonatomic, retain)IBOutlet UISlider *m_sliderZooming;
@property (nonatomic, retain)IBOutlet UIImageView *m_imageView;
@property(nonatomic,retain)IBOutlet UITextView *m_buttomTextView;
@property(nonatomic,retain)IBOutlet UITextView *m_buttomTextView2;
@property (nonatomic,retain) IBOutlet UIImageView *m_userImage;
@property(nonatomic,retain)IBOutlet UITextView *l_textView;
@property(nonatomic,retain)IBOutlet	UILabel *m_LabCountChar;
@property(nonatomic, retain)IBOutlet UIButton *m_ZoomBtn;
@property(nonatomic, retain)IBOutlet UIButton *m_BeautyBtn;
@property(nonatomic, retain) NSMutableDictionary *metadata;
@property(nonatomic, retain)UIView *greyOverlay;
@property(nonatomic, retain)UIView *defaultView;

@property (nonatomic, retain) UIImage *m_buzzImage;
@property (nonatomic, retain) IBOutlet UISlider *smoothnessSlider;
@property (nonatomic, retain) IBOutlet UISlider *brightnessSlider;
@property (nonatomic, retain) IBOutlet UISlider *saturationSlider;

// Video-related
@property (nonatomic, retain) IBOutlet UIView *videoView;
@property (nonatomic, retain) IBOutlet UIView *headerView;
@property (nonatomic, retain) IBOutlet UIView *shareView;
@property (nonatomic, retain) IBOutlet UIView *sharePhotoView;
@property (nonatomic, retain) IBOutlet UIButton *sourceSwitch;
@property (nonatomic, retain) IBOutlet UIButton *shootButton;
@property (nonatomic, retain) IBOutlet UIButton *recordButton;
@property (nonatomic, retain) IBOutlet UIButton *cameraButton;
@property (nonatomic, retain) IBOutlet UIButton *flashButton;
@property (nonatomic, retain) IBOutlet UIButton *playButton;
@property (nonatomic, copy)  NSString *moviePath;
@property (nonatomic, copy)  NSURL *movieURL;
@property (nonatomic, retain) MPMoviePlayerController *moviePlayerController;

// Filters - related
@property (nonatomic, retain) IBOutlet UIScrollView *filterScrollView;
@property (nonatomic, retain) IBOutlet UIButton *filtersToggleButton;
@property (nonatomic, retain) IBOutlet UIImageView *filtersBackgroundImageView;

@property (nonatomic, retain) UIImagePickerController *m_imgPicker;

- (IBAction) chooseImage:(id) sender;
- (IBAction) smoothness: (UISlider *)slider;
- (IBAction) brightness: (UISlider *)slider;
- (IBAction) saturation: (UISlider *)slider;


-(IBAction)btnPhotoLibrary:(id)sender;
-(IBAction)btnRetakeAction:(id)sender;
-(IBAction)btnCancelAction:(id)sender;
- (IBAction)takePicture:(id)sender;
-(IBAction)btnEditAction:(id)sender;
-(IBAction)btnPreviewAction:(id)sender;
-(IBAction)btnUndoAction:(id)sender;
-(void)zoomValueChanged:(NSNumber *)zoomvalue;
-(IBAction) smoothnessforUndo: (NSNumber *)smoothnessSliderValue;
-(IBAction) brightnessforUndo: (NSNumber *)brightnessSliderValue;
-(IBAction) saturationforUndo: (NSNumber *)saturationSliderValue;
-(IBAction)btnSendAction:(id)sender;
-(void)reset;
- (IBAction)sendToRunway:(id)sender;
-(IBAction)btnBeautyAction:(id)sender;
-(IBAction)btnZoomAction:(id)sender;
- (void) highlightButton:(NSNumber *)number;
- (IBAction) goToSlider:(id)sender;
- (IBAction)startRecording:(id)sender;
//- (IBAction)offFlash:(id)sender;
- (IBAction)rotateCamera:(id)sender;
- (IBAction)trashMovie:(id)sender;
- (IBAction)shareMovie:(id)sender;
- (IBAction)play:(id)sender;
- (void) removePicker;
-(IBAction) toggleFilters:(UIButton *)sender;

@end
