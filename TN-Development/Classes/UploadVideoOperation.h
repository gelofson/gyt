//
//  UploadVideoOperation.h
//  TinyNews
//
//  Created by Nava Carmon on 2/7/13.
//
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequestDelegate.h"
@interface UploadVideoOperation : NSOperation <ASIHTTPRequestDelegate>
{
    id delegate;
    NSString *urlStr;
    NSString *tempPath;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic, copy) NSString *urlString;
@property (nonatomic, copy) NSString *tempPath;

@end
