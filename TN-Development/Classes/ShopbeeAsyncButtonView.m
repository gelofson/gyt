//
//  ShopbeeAsyncButtonView.m
//  QNavigator
//
//  Created by Bharat Biswal on 08/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ShopbeeAsyncButtonView.h"
#import<QuartzCore/QuartzCore.h>
#import "CategoriesViewController.h"
#import "Constants.h"

@implementation ShopbeeAsyncButtonView
@synthesize m_target;
@synthesize m_btnIcon;
@synthesize isCatView;
@synthesize data;
@synthesize tag;
SEL l_selector;

- (void)dealloc  {
	[connection cancel]; //in case the URL is still downloading
	[connection release];
	//if(data)
//	{
//		data = nil;
//		[data release];
//    }
    // GDL: added these.
    // This was already released.
    //[m_target release];
    
    //[m_btnIcon release];
    
    [super dealloc];
}


- (void)loadImageFromURL:(NSURL*)url target:(id)target action:(SEL)selector btnText:(NSString *)btnText
{
	if (connection!=nil) 
	{ 
		[connection release]; 
	} //in case we are downloading a 2nd image
	if (data!=nil) 
	{ 
		[data release];
	}
	
	m_target=target;
	l_selector=selector;
	
	for	(UIView *tempView in [self subviews]) 
	{
		//then this must be another image, the old one is still in subviews
		[tempView removeFromSuperview]; //so remove it (releases it also)

	}
	
	UIImageView * bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shopbee package"]];
	[bgView setFrame:self.bounds];
	[self addSubview:bgView];
	[bgView release];
	
	
	m_btnIcon = [UIButton buttonWithType:UIButtonTypeCustom];
	[m_btnIcon setFrame:CGRectMake(6.5,6.5,132.0,58.86)];
	[m_btnIcon setBackgroundImage:[UIImage imageNamed:@"shopbee_loading_icon"]  forState:UIControlStateNormal];
	[m_btnIcon setTag:self.tag];
    [m_btnIcon addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:m_btnIcon];
	
	UILabel *tmp_label=[[UILabel alloc]initWithFrame:CGRectMake(2,66,141,40)];
	tmp_label.font=[UIFont fontWithName:kFontName size:16];
	tmp_label.numberOfLines=2;
	tmp_label.lineBreakMode=UILineBreakModeWordWrap;
	tmp_label.textAlignment=UITextAlignmentCenter;
	tmp_label.adjustsFontSizeToFitWidth = YES;
	tmp_label.minimumFontSize = 12.0f;
	tmp_label.backgroundColor=[UIColor clearColor];
	tmp_label.textColor=[UIColor whiteColor];
	tmp_label.text=btnText;	
	[self addSubview:tmp_label];
	[tmp_label release];
	tmp_label=nil;	
	
	
	NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	//
	
	data = [[NSMutableData alloc] initWithCapacity:2048]; 
	
	connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
	//[connection start];
	//[connection setValue:tagg forKey:@"tag"];
	//TODO error handling, what if connection is nil?
}

//Crash here

//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	
	
	//if (data != nil) {
//	[data release],data = nil;
	//			NSLog(@"DATA IS NOT NULL");
	//}
	if (data==nil) 
	{ 
		NSLog(@"DATA IS NIL");
		data = [[NSMutableData alloc] initWithCapacity:2048]; 
	} 
	
	[data appendData:incrementalData];
}

- (void)cancelRequest {
    
    [connection cancel];
    
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection 
{ 
	//so self data now has the complete image 
	//NSLog(@"%@",[connection valueForKey:@"tag"]);
	
	
	
	//if ([[self subviews] count]>0) 
//	{
//		//then this must be another image, the old one is still in subviews
//		//[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
//	}
		
	UIImage *tempImage=[UIImage imageWithData:data];
	if (tempImage==nil)
	{
		tempImage=[UIImage imageNamed:@"no-image"];
	}
	
	[m_btnIcon setBackgroundImage:tempImage  forState:UIControlStateNormal];
	
	//because we dont want border in logos view
	if (!isCatView)//[m_target isKindOfClass:[CategoriesViewController class]]) 
	{
		[m_btnIcon.layer setBorderWidth:2.0f];
		[m_btnIcon.layer setBorderColor:[[UIColor whiteColor]CGColor]];
	}
	
	[connection release];
	connection=nil;
	
	[data release]; //don't need this any more, its in the UIButton now
	data=nil;
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
{
	[connection release];
	connection=nil;
	
	[m_btnIcon setBackgroundImage:[UIImage imageNamed:@"no-image"]  forState:UIControlStateNormal];
	[m_btnIcon addTarget:m_target action:l_selector forControlEvents:UIControlEventTouchUpInside];
	
	//because we dont want border in logos view
	if (!isCatView)//[m_target isKindOfClass:[CategoriesViewController class]]) 
	{
		[m_btnIcon.layer setBorderWidth:2.0f];
		[m_btnIcon.layer setBorderColor:[[UIColor whiteColor]CGColor]];
	}
}

- (UIImage*) image
{
	for (UIView *tempView in [self subviews])
	{
		if ([tempView isKindOfClass:[UIButton class]])
		{
			return [(UIButton *)tempView imageForState:UIControlStateNormal];
		}
	}
	
	//if ([[self subviews] count]>0)
//	{
//		return [(UIButton *)[[self subviews] objectAtIndex:0] imageForState:UIControlStateNormal];
//	}
	return nil;
}



@end
