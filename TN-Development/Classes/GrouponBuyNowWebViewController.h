//
//  GrouponBuyNowWebViewController.h
//  QNavigator
//
//  Created by Bharat Biswal on 12/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrouponBuyNowWebViewController : UIViewController
{
    NSString * urlString;
    NSString * titleString;
    IBOutlet UIWebView * grouponWebView;
    IBOutlet UILabel * pageTitleLabel;
}

@property (nonatomic, retain) IBOutlet UIWebView * grouponWebView;
@property (nonatomic, retain) IBOutlet UILabel * pageTitleLabel;

@property (nonatomic, retain) NSString * urlString;
@property (nonatomic, retain) NSString * titleString;

-(IBAction) backAction:(id)sender;
-(IBAction) grouponButtonClicked: (id) sender;

@end
