//
//  BuzzButtonCameraFunctionality.m
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.

#import "BuzzButtonCameraFunctionality.h"
#import "QNavigatorAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <Math.h>
#import "GLImage.h"
#import "UploadImageAPI.h"
#import "LoadingIndicatorView.h"
#import "BuzzButtonShareNowFunctionality.h"
#import "Constants.h"
#import "BuzzButtonHeadlineFunctionality.h"
// GDL: We use this, so we should import this.
#import "JSON.h"
#import "NSMutableDictionary+ImageMetadata.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Context.h"
#import "Constants.h"
#import "FontsDef.h"
#import <MediaPlayer/MediaPlayer.h>
#import "MenuViewController.h"
#import "GrayscaleContrastFilter.h"
#import "GPUImageContrastFilter.h"

@interface BuzzButtonCameraFunctionality (PRIVATE)
- (void)updateBasicTextInLabel:(UILabel*)l;
-(void) loadFilters;
-(void) prepareStaticFilter;
-(void) prepareFilter;
- (void) resetFilters;

@end

@implementation BuzzButtonCameraFunctionality

QNavigatorAppDelegate *l_appDelegate;
UploadImageAPI *l_buzzRequest;

@synthesize wasCameraPickerActiveThenSkipCamLaunch;
@synthesize m_sendBtn;
@synthesize m_lblselectAnImg;
@synthesize m_userImage;
@synthesize l_textView;
@synthesize m_LabCountChar;
@synthesize m_buttomTextView;
@synthesize m_buttomTextView2;
@synthesize m_imageView;
@synthesize m_sliderZooming;
@synthesize smoothnessSlider;
@synthesize brightnessSlider;
@synthesize saturationSlider;
@synthesize m_buzzImage;
@synthesize m_imageViewCrop;
@synthesize m_bottomBar;
@synthesize m_indicatorView;
@synthesize m_beautyView;
@synthesize m_BeautyBtn;
@synthesize btnEdit;
@synthesize btnPreview;
@synthesize btnUndo;
@synthesize m_ViewTopTextView;
@synthesize m_arrWithUndoDict;
@synthesize m_ZoomDict;
@synthesize m_BeautyDict;
@synthesize m_CommentDict;
@synthesize m_strType;
@synthesize m_beautyViewBorder;
@synthesize	 m_zoomSliderView;
@synthesize m_previewViewlabel3;
@synthesize m_requiredView;
@synthesize m_scrollView;
@synthesize m_ZoomBtn;
@synthesize metadata;
@synthesize greyOverlay;
@synthesize shouldUseEditedImageOfPhoto;
@synthesize defaultView;
@synthesize m_imgPicker;
// Filters-related
@synthesize filterScrollView = _filterScrollView;
@synthesize filtersToggleButton = _filtersToggleButton;
@synthesize filtersBackgroundImageView = _filtersBackgroundImageView;

// Video-related
@synthesize videoView, sourceSwitch, shootButton, headerView, recordButton, cameraButton, flashButton, shareView, sharePhotoView, moviePath, moviePlayerController, movieURL, playButton;

//BOOL isCaptureImage;
UIView *temp_View;
UIView *preview_view;
//UIView *m_view;

UILabel *Preview_ViewLabel;

UIImageView *l_commentCheckMark;

int checkUndoOrZoom;
int check;

BOOL isRetakeImage;


float l_zoomValue, l_brightnessValue, l_smoothnessValue, l_saturationValue;

NSString *str_textView;
int count_text1=140;

BOOL isEditViewVisible;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
		wasCameraPickerActiveThenSkipCamLaunch = NO;
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    self.view = self.defaultView;
    [super viewDidLoad];
    self.trackedViewName = @"Make Report";

	[self.m_scrollView setFrame:CGRectMake(0,0,320,320)];
	[self.m_imageView setFrame:CGRectMake(0,0,320,320)];

	[self.m_buttomTextView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:BUZZBUTTONCAMERAFUNCTIONALITY_COMMENTVIEW_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:BUZZBUTTONCAMERAFUNCTIONALITY_COMMENTVIEW_FONT_SIZE_KEY]]];
	self.m_buttomTextView.text = PLACEHOLDER_COMMENTS_MESSAGE;
	[self.m_buttomTextView2 setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:BUZZBUTTONCAMERAFUNCTIONALITY_COMMENTVIEW_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:BUZZBUTTONCAMERAFUNCTIONALITY_COMMENTVIEW_FONT_SIZE_KEY]]];
	self.m_buttomTextView2.text = PLACEHOLDER_COMMENTS_MESSAGE;
	//[m_lblselectAnImg setUserInteractionEnabled:TRUE];
//	[m_sendBtn setUserInteractionEnabled:TRUE];
	m_lblselectAnImg.hidden=YES;
	//NSLog(@"m_strType: %@",m_strType);
	m_arrWithUndoDict=[[NSMutableArray alloc] init];
	m_ViewTopTextView.hidden=YES;
	[m_ViewTopTextView.layer setCornerRadius:6];

	m_buttomTextView2.font=[UIFont fontWithName:kFontName size:13];
	m_buttomTextView.font=[UIFont fontWithName:kFontName size:13];
	l_textView.font=[UIFont fontWithName:kFontName size:13];
	
//	btnPreview.hidden = YES;
//	btnUndo.hidden = YES;
    
    // Filters
    [self loadFilters];

    
    // GDL
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
	scaleValue = 1.0f;
	
	isEditViewVisible=FALSE;
	m_LabCountChar.hidden=YES;
	
	//[m_ZoomBgView.layer setCornerRadius:6.0];
	
	m_zoomSliderView.hidden=YES;
	
	previousScaleValue=1.0f;
	
	//////////////top view//////////////////
	//if(![preview_view superview])
//	{
		//preview_view=[[UIView alloc] initWithFrame:CGRectMake(0,395, 320, 25)];
	//	preview_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		
		Preview_ViewLabel = [[UILabel alloc ] initWithFrame:CGRectMake(0,5,320,20)];
		Preview_ViewLabel.textAlignment =  UITextAlignmentCenter;
		Preview_ViewLabel.textColor = [UIColor whiteColor];
		Preview_ViewLabel.backgroundColor = [UIColor clearColor];
		//Preview_ViewLabel.font = [UIFont fontWithName:@"futura" size:(16.0)];
		[Preview_ViewLabel setFont:[UIFont boldSystemFontOfSize:20]];
		Preview_ViewLabel.text=@"Preview";
		[self.sharePhotoView addSubview:Preview_ViewLabel];
		
	//	[tmp_topViewLabel setTag:5];
		
		//[preview_view addSubview:Preview_ViewLabel];
		
		//[self.view addSubview:preview_view];
	//	preview_view.hidden=YES;
				
	//}
	m_imageViewCrop.frame=CGRectMake(-170, -250, 640, 920);
	m_imageViewCrop.image = [UIImage imageNamed:@"crop_back.png"];
	[m_imageViewCrop setHidden:YES];

	smoothnessSlider.hidden=YES;
	brightnessSlider.hidden=YES;
	saturationSlider.hidden=YES;
	

	m_zoomSliderView.hidden=YES;
	m_buttomTextView.hidden=NO;
	l_textView.hidden=YES;

    [smoothnessSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
	[brightnessSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
	[saturationSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
    [smoothnessSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateHighlighted];
	[brightnessSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateHighlighted];
	[saturationSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateHighlighted];
	[smoothnessSlider setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[smoothnessSlider setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	
	[brightnessSlider setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[brightnessSlider setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	
	[saturationSlider setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[saturationSlider setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	
	
	[m_sliderZooming setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[m_sliderZooming setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	[m_sliderZooming setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
	[m_sliderZooming setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateHighlighted];
	
	m_beautyViewBorder.hidden=YES;
	

	
	self.view.frame=[[UIScreen mainScreen] bounds];
	
    // create overlay view for 
	
    temp_View = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];

/*	UIImageView *temp_bottomImage=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,480)]; //(0, 426, 320, 55)];
	[temp_bottomImage setImage:[UIImage imageNamed:@"overlay_camera_back.png"]]; //@"image_edit_tab.png"]];
	[temp_View addSubview:temp_bottomImage];
	[temp_bottomImage release];
	
	UILabel *Preview_ViewLabel2 = [[UILabel alloc ] initWithFrame:CGRectMake(0,5,320,20)];
	Preview_ViewLabel2.textAlignment =  UITextAlignmentCenter;
	Preview_ViewLabel2.textColor = [UIColor darkGrayColor];
	Preview_ViewLabel2.backgroundColor = [UIColor clearColor];
	Preview_ViewLabel2.font = [UIFont fontWithName:kMyriadProBoldFont size:(16.0)];
	Preview_ViewLabel2.text=@"Take picture";
	[temp_View addSubview:Preview_ViewLabel2];
	[Preview_ViewLabel2 release];
	
	m_previewViewlabel3 = [[UILabel alloc ] initWithFrame:CGRectMake(0,380,320,24)];
	m_previewViewlabel3.textAlignment =  UITextAlignmentCenter;
	m_previewViewlabel3.textColor = [UIColor darkGrayColor];
	//m_previewViewlabel3.tag=29;
	
	m_previewViewlabel3.backgroundColor = [UIColor clearColor];
	m_previewViewlabel3.font = [UIFont fontWithName:kMyriadProBoldFont size:(16.0)];
    
    [self updateBasicTextInLabel:m_lblBasicText];
    [self updateBasicTextInLabel:m_previewViewlabel3];
    
	[temp_View addSubview:m_previewViewlabel3];
	
	[temp_View addSubview:self.m_buttomTextView2];
	self.m_buttomTextView2.frame = self.m_buttomTextView.frame;
	self.m_buttomTextView2.text = self.m_buttomTextView.text;


	UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
	[btnCancel addTarget:self action:@selector(dismissCameraView) forControlEvents:UIControlEventTouchUpInside];
	btnCancel.frame = CGRectMake(20,438,70, 29);
	[btnCancel setImage:[UIImage imageNamed:@"cancel_buzzButtton.png"] forState:UIControlStateNormal];
	[temp_View addSubview:btnCancel];

	
	UIButton *takePicButton  = [UIButton buttonWithType:UIButtonTypeCustom];
	[takePicButton addTarget:self action:@selector(takePicture:) forControlEvents:UIControlEventTouchUpInside];//cancel_btn.png
	takePicButton.frame = CGRectMake(120, 433, 80, 40);
	[takePicButton setImage:[UIImage imageNamed:@"gray_takePhoto_button.png"] forState:UIControlStateNormal];
	[temp_View addSubview:takePicButton];
	
	
	UIButton *existingPic = [UIButton buttonWithType:UIButtonTypeCustom];
	[existingPic addTarget:self action:@selector(btnPhotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
	existingPic.frame = CGRectMake(210, 438,105,29);
	[existingPic setImage:[UIImage imageNamed:@"choose_existing_btn.png"] forState:UIControlStateNormal];
	[temp_View addSubview:existingPic];*/
    
    CGRect overlayFrame = videoView.frame;
    
    overlayFrame.origin.y = temp_View.frame.size.height - videoView.frame.size.height;
    videoView.frame = overlayFrame;
    
	[temp_View addSubview:headerView];
    [temp_View addSubview:videoView];
    
    self.greyOverlay = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"grey-bgrd"]] autorelease];
    self.greyOverlay.frame = CGRectMake(0, videoView.frame.origin.y - 78, 320, 78);
    
    [temp_View addSubview:self.greyOverlay];
    
	self.m_imgPicker=[[[UIImagePickerController alloc]init]autorelease];
	
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.m_imgPicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
    } else
        self.m_imgPicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
    self.m_imgPicker.delegate = self;
    
    NSLog(@"Media types available %@", m_imgPicker.mediaTypes);
    
	sourceCamera = YES;
    [shootButton setHidden:!sourceCamera];
    [recordButton setHidden:sourceCamera];
    sourceSwitch.selected = NO;
		
	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150,200,20,20)];
	m_indicatorView.hidesWhenStopped=YES;
	[temp_View addSubview:m_indicatorView];
    
    flashOn = NO;
    rearCamera = YES;
    staticPicture = nil;
	self.flashButton  = [UIButton buttonWithType:UIButtonTypeCustom];
	[self.flashButton addTarget:self action:@selector(offFlash:) forControlEvents:UIControlEventTouchUpInside];//cancel_btn.png
	self.flashButton.frame = CGRectMake(30, headerView.frame.size.height, 73, 31);
	[self.flashButton setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];
	[temp_View addSubview:self.flashButton];
	
    [self.flashButton setHidden:YES];
	
	self.cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[self.cameraButton addTarget:self action:@selector(rotateCamera:) forControlEvents:UIControlEventTouchUpInside];
	self.cameraButton.frame = CGRectMake(320 - 73 - 30, headerView.frame.size.height, 73, 31);
	[self.cameraButton setImage:[UIImage imageNamed:@"camera-rotate"] forState:UIControlStateNormal];
	[temp_View addSubview:self.cameraButton];
    
    [self.cameraButton setHidden:YES];
	/////////////////////
	//btnUndoAction:
	[self performSelector:@selector(btnUndoAction:)];
}


- (IBAction)rotateCamera:(id)sender
{
    if (rearCamera) {
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            self.m_imgPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            rearCamera = NO;
        }
    } else {
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            self.m_imgPicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            rearCamera = YES;
        }
    }
}



- (void)updateBasicTextInLabel:(UILabel*)l{
    
    if (!l) {
        return;
    }

	// US237 : show nil for label
	l.text = nil;
	/* 
    
    if([m_strType isEqualToString:kCheckThisOut])
	{
		l.text=@"Check this out";
	}
    else if ([m_strType isEqualToString:kBuyItOrNot])
	{
		l.text=@"Buy it or not?";
	}
    else if ([m_strType isEqualToString:kFoundASale])
	{
		l.text=@"Found a sale!";
	}
    else if ([m_strType isEqualToString:kHowDoesThisLook])
	{
		//l.text=@"How do I look?";
		l.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
	}
    else if ([m_strType isEqualToString:kIBoughtIt])
	{
		l.text=@"I bought this !";
	}
	else 
	{
        NSLog(@"Could not find a match case for %@", m_strType);
		// US237: do not show anything
		//l.text=@"I Got it!";
		l.text=nil;
	}
	*/
    
}

-(void)startIndicator
{
	[temp_View setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.7]];
	[m_indicatorView startAnimating];
	
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

    [self removeAllTargets];
    
    [self resetFilters];
    recordingVideo = NO;

}

- (void) viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    self.wasCameraPickerActiveThenSkipCamLaunch = NO;
//    self.view = self.sharePhotoView;

}

- (void) viewDidAppear:(BOOL)animated
{
    if (self.wasCameraPickerActiveThenSkipCamLaunch == NO)
        [self performSelector:@selector(btnRetakeAction:) withObject:nil afterDelay:0.0];
    else
        self.wasCameraPickerActiveThenSkipCamLaunch = NO;
}


#pragma mark -
#pragma mark Custom methods


- (IBAction)btnPreviewAction:(id)sender {
    self.view = self.sharePhotoView;
	Preview_ViewLabel.text=@"Preview";
	
	if ( m_buttomTextView.text.length > 0 ) {
		m_buttomTextView.hidden = NO;
	}
	else {

		// US237 : show "Comments go here" message
		//m_buttomTextView.hidden = YES;
		m_buttomTextView.text = PLACEHOLDER_COMMENTS_MESSAGE;
		m_buttomTextView.hidden = NO;
	}

	//NSLog(@"m_sliderZooming value: %d",m_sliderZooming.value);
//	m_dictUndo=[NSDictionary dictionaryWithObjectsAndKeys:scaleValue,@"",nil];
	NSDictionary *tmpDictionary;
	
	if([m_sliderZooming value]!=1.00 && check==1)
	{
		check=-1;
		
		//tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:[m_sliderZooming value]],@"zoomValue",@"zoom",@"type",nil];
		tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:l_zoomValue],@"zoomValue",@"zoom",@"type",nil];
		[m_arrWithUndoDict addObject:tmpDictionary];
		
		l_zoomValue=[m_sliderZooming value];
		
	}
	
	if(([brightnessSlider value]!=0.00||[smoothnessSlider value]!=0.00||[saturationSlider value]!=1.00) && check==2)
	{
		check=-1;
		//tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:[brightnessSlider value]],@"brightnessValue",[NSNumber numberWithFloat:[smoothnessSlider value]],@"smoothnessValue",[NSNumber numberWithFloat:[saturationSlider value]],@"saturationValue",@"beauty",@"type",nil];
		tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:l_brightnessValue],@"brightnessValue",[NSNumber numberWithFloat:l_smoothnessValue],@"smoothnessValue",[NSNumber numberWithFloat:l_saturationValue],@"saturationValue",@"beauty",@"type",nil];
		[m_arrWithUndoDict addObject:tmpDictionary];
		
		l_brightnessValue=[brightnessSlider value];
		l_smoothnessValue=[smoothnessSlider value];
		l_saturationValue=[saturationSlider value];
		
	}
	
	
	//1 for zooming ,2 for beauty , 3 for comments
	m_imageViewCrop.hidden=YES;
	m_zoomSliderView.hidden=YES;
	//m_beautyView.hidden=YES;
	m_beautyViewBorder.hidden=YES;
	preview_view.hidden=NO;
//	btnPreview.hidden=YES;
//	btnUndo.hidden=YES;
	//m_view.hidden=YES;
	NSLog(@"preview btn");
	
}


-(IBAction)btnUndoAction:(id)sender
{
	//m_sliderZooming.hidden=YES;
	//m_beautyView.hidden=YES;
	
	checkUndoOrZoom=1;
//	NSLog(@"%@",[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]);
	/*if([m_arrWithUndoDict count]!=0)
	{
		if([[NSString stringWithString:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"type"]] isEqualToString:@"zoom"])
		{
			//NSLog(@"zooming previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"] floatValue]);
			NSLog(@"zoom block");
			[self zoomValueChanged:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"]];
			[m_sliderZooming setValue:[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"] floatValue]]; 
			
			[m_arrWithUndoDict removeLastObject];
		}
		else if([[NSString stringWithString:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"type"]] isEqualToString:@"beauty"])
		{
			NSLog(@"beauty block");
			[self brightnessforUndo:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"brightnessValue"]];
			[self smoothnessforUndo:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"smoothnessValue"]];
			[self saturationforUndo:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"saturationValue"]];
			//NSLog(@"beauty previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"brightnessValue"] floatValue]);
			//NSLog(@"beauty previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"smoothnessValue"] floatValue]);
			//NSLog(@"beauty previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"saturationValue"] floatValue]);
			[m_arrWithUndoDict removeLastObject];
		}
		else if([[NSString stringWithString:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"type"]] isEqualToString:@"comments"])
		{
			NSLog(@"comments block");
			//NSLog(@"comments previous value = %@",[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"]);
			[m_arrWithUndoDict removeLastObject];
		}
	}
	else if([m_arrWithUndoDict count]==0)
	{
		[self reset];
	}*/
    Preview_ViewLabel.text = @"Preview";
    [m_beautyViewBorder setHidden:YES];
    [m_zoomSliderView setHidden:YES];
    
    [self reset];
}

-(void)reset
{
	saturationSlider.value = 1.00;
    brightnessSlider.value = 0.00;
    smoothnessSlider.value = 0.00;
	m_sliderZooming.value = 1.00;
	
	l_saturationValue=1.0;
	l_brightnessValue=0.0;
	l_smoothnessValue=0.0;
	l_zoomValue=1.0;
	
	[self zoomValueChanged:[NSNumber numberWithInt:1.00]];
	[self saturationforUndo:[NSNumber numberWithInt:1.00]];
	[self smoothnessforUndo:[NSNumber numberWithInt:0.00]];
	[self brightnessforUndo:[NSNumber numberWithInt:0.00]];
}

// The user has tapped the Send to Runway button.
- (IBAction)sendToRunway:(id)sender {
	
	[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
	
	CGSize temp_size=CGSizeMake(320,320);
	
	UIGraphicsBeginImageContext(temp_size);
	[self.m_requiredView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	//[m_imageView setImage:newImage];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_buzzRequest=[UploadImageAPI SharedInstance];
	
	//l_request=[[ShopbeeAPIs alloc] init];
	
	m_personData=UIImageJPEGRepresentation(newImage, 0.5);
    
	
	NSUInteger len = [m_personData length];
	const char *raw=[m_personData bytes];
	
	//creates the byte array from image
	NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
	for (long int i = 0; i < len; i++)
	{
		[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
	} 
	
	NSString *tempCommentString=l_textView.text;
    
    /*
    if ([m_strType isEqualToString:kFoundASale]) {
        m_strType = kIBoughtIt;        
    }
    */

	NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",tempCommentString,@"comments",m_strType,@"type",@"",@"advtlocid",[tempByteArray JSONFragment],@"photo",nil];
    //	NSLog(@"%@",dict);
	l_buzzRequest.requestType=1;
	
	[l_buzzRequest sendBuzzRequest:dict];
    
	[tempByteArray release];
	tempByteArray=nil;
	
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)zoomValueChanged:(NSNumber *)zoomvalue
{
	scaleValue = [zoomvalue floatValue];
	//NSLog(@"scaleValue :%f", scaleValue);
	

	scale = scaleValue/previousScaleValue;
	previousScaleValue = scaleValue;
	
	CGAffineTransform transform = self.m_imageView.transform;
	transform = CGAffineTransformScale(transform, scale, scale);
	self.m_imageView.transform = transform;
	
	self.m_imageView.frame=CGRectMake(0, 0, self.m_imageView.frame.size.width, self.m_imageView.frame.size.height);
	[self.m_scrollView setContentSize:CGSizeMake(self.m_imageView.frame.size.width, self.m_imageView.frame.size.height)];
	
	
	//NSLog(@"frame: x: %f  y:%f height:%f width:%f",m_imageView.frame.origin.x,m_imageView.frame.origin.y,m_imageView.frame.size.height,m_imageView.frame.size.width);
}

-(IBAction)btnEditAction:(id)sender
{
	m_buttomTextView.hidden=YES;
	m_zoomSliderView.hidden=YES;
	m_beautyViewBorder.hidden=YES;
//	btnPreview.hidden=YES;
//	btnUndo.hidden=YES;
	Preview_ViewLabel.text=@"Edit";
	
//	if (![[self.view viewWithTag:101] superview] && isEditViewVisible==FALSE)
//	{
		//preview_view.hidden=YES;
		
//		btnPreview.hidden=NO;
//		btnUndo.hidden=NO;
		isEditViewVisible=TRUE;
		
		//[m_view setHidden:NO];

}

-(void)cropImage
{
	
//	((UILabel *)[preview_view viewWithTag:5]).text=@"Crop image";
	//m_buttomTextView.hidden=NO;	
//	m_beautyView.hidden=YES;	
//	l_textView.hidden=YES;
//	m_buttomTextView.hidden=YES;
//	m_view.hidden=YES;
//	m_LabCountChar.hidden=YES;
//	m_sliderZooming.hidden=YES;
//	smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
//	m_imageViewCrop.hidden=NO;
}


-(void)btnCommentAction:(id)sender
{
	[l_textView becomeFirstResponder];
	Preview_ViewLabel.text=@"Comment";
	
	check=3;
	m_ViewTopTextView.hidden=NO;
	m_LabCountChar.hidden=NO;
	m_imageViewCrop.hidden=YES;
	//count=1;
	//((UILabel *)[preview_view viewWithTag:5]).text=@"Add Comment";
	//preview_view.hidden=NO;
	m_beautyViewBorder.hidden=YES;
	m_buttomTextView.hidden=YES;
	l_textView.hidden=NO;
	l_textView.delegate=self;
	[l_textView.layer setCornerRadius:6];
	l_textView.backgroundColor=[UIColor whiteColor];
	[self.view addSubview:m_ViewTopTextView];
	//[self.view addSubview:l_textView];
}

-(IBAction)btnBeautyAction:(id)sender
{
    self.filtersToggleButton.selected = NO;
    self.filtersToggleButton.enabled = YES;
    self.m_BeautyBtn.selected = YES;
    self.m_ZoomBtn.selected = NO;
    [self hideFilters];

	check=2;
	m_imageViewCrop.hidden=YES;
	Preview_ViewLabel.text=@"Beauty";
	//count=3;
	//((UILabel *)[preview_view viewWithTag:5]).text=@"Beauty mode";
	m_beautyViewBorder.hidden=NO;	
	l_textView.hidden=YES;
	m_buttomTextView.hidden=YES;
	//m_view.hidden=YES;
	//preview_view.hidden=NO;
	m_LabCountChar.hidden=YES;
	m_zoomSliderView.hidden=YES;
	smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;

}

-(IBAction)btnZoomAction:(id)sender
{
    self.filtersToggleButton.selected = NO;
    self.filtersToggleButton.enabled = YES;
    [self hideFilters];
    self.m_BeautyBtn.selected = NO;
    self.m_ZoomBtn.selected = YES;

	checkUndoOrZoom=2;
	check=1;
	Preview_ViewLabel.text=@"Zoom";
	m_imageViewCrop.hidden=YES;
	//count=2;
	//((UILabel *)[preview_view viewWithTag:5]).text=@"Zoom image";
	m_beautyViewBorder.hidden=YES;
	//preview_view.hidden=NO;	
	l_textView.hidden=YES;
	m_buttomTextView.hidden=YES;
	//m_view.hidden=YES;
	m_zoomSliderView.hidden=NO;
	
	m_LabCountChar.hidden=YES;
	
		
	//previousScaleValue = 1.0f;
		
	[m_sliderZooming addTarget:self action:@selector(scaleChanged:) forControlEvents:UIControlEventValueChanged];
	[m_sliderZooming addTarget:self action:@selector(beginTracking:) forControlEvents:UIControlEventTouchDragInside];
	[m_sliderZooming addTarget:self action:@selector(endTracking:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
	
}

-(void)beginTracking:(id)sender
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.1];
	
	[UIView commitAnimations];
}

-(void)endTracking:(id)sender
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.1];
	[UIView commitAnimations];
}

-(void)scaleChanged:(id)sender
{
	scaleValue = [m_sliderZooming value];
	//NSLog(@"scaleValue :%f", scaleValue);
	
	scale = scaleValue/previousScaleValue;
	previousScaleValue = scaleValue;
	
	CGAffineTransform transform = self.m_imageView.transform;
	transform = CGAffineTransformScale(transform, scale, scale);
	self.m_imageView.transform = transform;
	//NSLog(@"frame: x: %f  y:%f height:%f width:%f",m_imageView.frame.origin.x,m_imageView.frame.origin.y,m_imageView.frame.size.height,m_imageView.frame.size.width);
	//NSLog(@"scroll view content: %f %f", m_scrollView.contentSize.height, m_scrollView.contentSize.width);
	
	self.m_imageView.frame=CGRectMake(0, 0, self.m_imageView.frame.size.width, self.m_imageView.frame.size.height);
	
	[self.m_scrollView setContentSize:CGSizeMake(self.m_imageView.frame.size.width, self.m_imageView.frame.size.height)];
	
#if 0
	if(self.m_scrollView.contentSize.width>=320)
		[self.m_scrollView scrollRectToVisible:CGRectMake((self.m_scrollView.contentSize.width-320)/2, (self.m_scrollView.contentSize.height -427)/2, 320, 427	) animated:NO];
	else
#endif
		self.m_imageView.center=CGPointMake(self.m_scrollView.frame.size.width/2, self.m_scrollView.frame.size.height/2);
	
    
}


-(IBAction)btnPhotoLibrary:(id)sender
{
	if(!(self.m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else {
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
	shouldUseEditedImageOfPhoto=TRUE;
	
    if (sourceCamera == NO) {
        if (moviePlayerController.view.superview != nil)
            [self.moviePlayerController.view removeFromSuperview];
        self.moviePlayerController = nil;
        [self changeSource:nil];
    }
    self.m_imgPicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
}


-(IBAction)btnCancelAction:(id)sender
{
	if(!(self.m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else {
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
	[self.m_imageView setImage:nil];
	//self.wasCameraPickerActiveThenSkipCamLaunch = NO;
    //[self btnRetakeAction:nil];
    [tnApplication goToNewsPage];
}


-(void)navigateToBackView
{
    self.m_imgPicker = nil;
    
	[self dismissModalViewControllerAnimated:NO];
	[self.navigationController popViewControllerAnimated:YES];
}


-(void)dismissCameraView
{
    [self performSelector:@selector(navigateToBackView)];
    //QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *)[[UIApplication sharedApplication] delegate];
    //mainDg.tabBarController.selectedIndex = RUNWAY_TAB_INDEX;
    [tnApplication goToNewsPage];
}

- (IBAction) takePicture:(id)sender
{
	m_lblselectAnImg.hidden=YES;
	self.m_imgPicker.allowsEditing = YES;
	
	[self performSelectorInBackground:@selector(startIndicator) withObject:nil];
	
	shouldUseEditedImageOfPhoto=FALSE;
 
	
	[self.m_imgPicker takePicture];
}

- (IBAction)startRecording:(id)sender
{
    if (!recordingVideo) {
        recordingVideo = [self.m_imgPicker startVideoCapture];
        [recordButton setImage:[UIImage imageNamed:@"recording-btn"] forState:UIControlStateNormal];
    } else {
        [self.m_imgPicker stopVideoCapture];
        recordingVideo = NO;
        [recordButton setImage:[UIImage imageNamed:@"record-btn"] forState:UIControlStateNormal];
    }
        
}

- (void) removePicker
{
    self.wasCameraPickerActiveThenSkipCamLaunch = YES;
    [self dismissModalViewControllerAnimated:NO];
}

- (IBAction) goToSlider:(id)sender
{
    /*self.wasCameraPickerActiveThenSkipCamLaunch = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        [self slideMenuButtonTouched];
    }];*/
    
//    if (self.modalViewController == m_imgPicker) {
//        self.wasCameraPickerActiveThenSkipCamLaunch = NO;
//    }
    [self dismissModalViewControllerAnimated:NO];
    //self.wasCameraPickerActiveThenSkipCamLaunch = NO;
    [self slideMenuButtonTouched];
}

- (IBAction)changeSource:(id)sender
{
    [self.m_imgPicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    sourceCamera = !sourceCamera;
    if (sourceCamera) {
        [self.m_imgPicker setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto];
        self.view = self.sharePhotoView;
        sourceSwitch.selected = NO;
    } else {
        [self.m_imgPicker setCameraCaptureMode:UIImagePickerControllerCameraCaptureModeVideo];
        self.view = self.shareView;
        sourceSwitch.selected = YES;
    }
    [shootButton setHidden:!sourceCamera];
    [recordButton setHidden:sourceCamera];
    [headerView setHidden:!sourceCamera];
    [self.greyOverlay setHidden:!sourceCamera];
    //[self.flashButton setHidden:sourceCamera];
    [self.cameraButton setHidden:sourceCamera];

}

- (void) highlightButton:(NSNumber *)number
{
    sourceSwitch.selected = [number boolValue];
}

-(IBAction)btnRetakeAction:(id)sender
{
    self.filtersToggleButton.selected = NO;
    self.filtersToggleButton.enabled = YES;
    [self hideFilters];
    if ([self.m_imgPicker isBeingPresented])
        return;
	//Bharat: 11/15/2011: Setting correct order of button display
    m_sendBtn.hidden = NO;
	
	m_sendBtn.enabled=YES;
    
//  saturationSlider.value = 1.00;
//  brightnessSlider.value = 0.00;
//  smoothnessSlider.value = 0.00;
//	saturation=1;
//	gamma=1;
//	smoothness=0;
	
	//m_imageView.image = [self.m_buzzImage imageWithSaturation:1 gamma:1 smoothing:0];

	m_LabCountChar.hidden=YES;
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		self.m_imgPicker.delegate=self;
		self.m_imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        self.m_imgPicker.mediaTypes =
        [UIImagePickerController availableMediaTypesForSourceType:
         UIImagePickerControllerSourceTypeCamera];
        self.m_imgPicker.cameraCaptureMode = (!sourceSwitch.selected) ? UIImagePickerControllerCameraCaptureModePhoto : UIImagePickerControllerCameraCaptureModeVideo;

        self.m_imgPicker.cameraDevice = (rearCamera) ? UIImagePickerControllerCameraDeviceRear : UIImagePickerControllerCameraDeviceFront;
        self.m_imgPicker.cameraFlashMode = ([UIImagePickerController isFlashAvailableForCameraDevice:m_imgPicker.cameraDevice]) ? UIImagePickerControllerCameraFlashModeAuto : UIImagePickerControllerCameraFlashModeOff;
		self.m_imgPicker.showsCameraControls=NO;
		self.m_imgPicker.cameraOverlayView = temp_View;
		//m_imgPicker.navigationBar.barStyle=UIBarStyleBlackOpaque;
		self.m_imgPicker.allowsEditing = YES	;
        playing = NO;
        [self presentViewController:self.m_imgPicker animated:NO completion:nil];
		self.wasCameraPickerActiveThenSkipCamLaunch = YES;
	}
	else 
	{
		[self performSelector:@selector(chooseImage:)];
	}
	
}


- (IBAction) smoothnessforUndo: (NSNumber *)smoothnessSliderValue 
{
	
	//smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
	
    // Just rounding to the nearest integer, making sure it works for negative integers.
    int newSmoothness = (int) ( [smoothnessSliderValue floatValue] + 10.5 ) - 10;
    if ( newSmoothness != smoothness ) {
        smoothness = newSmoothness;
        self.m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
	
}

- (IBAction) brightnessforUndo: (NSNumber *)brightnessSliderValue {
    
	
	//smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
	// Gamma is on a logarithmic scale, here ranging from 0.25 (2^-2) to 4 (2^2).
    float newGamma = powf(2,[brightnessSliderValue floatValue]);
    if ( newGamma != gamma ) {
        gamma = newGamma;
        self.m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}

- (IBAction) saturationforUndo: (NSNumber *)saturationSliderValue {
    //smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
	
	float newSaturation = [saturationSliderValue floatValue];
    if ( newSaturation != saturation ) {
        saturation = newSaturation;
        self.m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}



- (IBAction) chooseImage:(id) sender {	
	if(!(self.m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else {
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
    self.m_imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.m_imgPicker.allowsEditing = YES;
    shouldUseEditedImageOfPhoto = TRUE;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:self.m_imgPicker animated:NO completion:nil];
    });
	self.wasCameraPickerActiveThenSkipCamLaunch = YES;
}


- (IBAction) smoothness: (UISlider *)slider {
	
	smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;
	
    // Just rounding to the nearest integer, making sure it works for negative integers.
    int newSmoothness = (int) ( slider.value + 10.5 ) - 10;
    if ( newSmoothness != smoothness ) {
        smoothness = newSmoothness;
        self.m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
	
}

- (IBAction) brightness: (UISlider *)slider {
    
	
	smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;
	// Gamma is on a logarithmic scale, here ranging from 0.25 (2^-2) to 4 (2^2).
    float newGamma = powf(2,slider.value);
    if ( newGamma != gamma ) {
        gamma = newGamma;
        self.m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}

- (IBAction) saturation: (UISlider *)slider {
    smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;
	
	float newSaturation = slider.value;
    if ( newSaturation != saturation ) {
        saturation = newSaturation;
        self.m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}

-(void)showLoadingView
{
	NSAutoreleasePool *tempPool=[[NSAutoreleasePool alloc]init];
	
	[[LoadingIndicatorView SharedInstance]startLoadingView:self];
	
	[tempPool release];
}
#pragma mark - 
#pragma mark Clicking Next
// The user has tapped the Save button.
-(IBAction)btnSendAction:(id)sender {

    UIGraphicsBeginImageContext(CGSizeMake(320,320));
    
	CGContextRef c = UIGraphicsGetCurrentContext();
	//CGContextTranslateCTM(c, -contentOffset.x, -(contentOffset.y-scrollFrame.origin.y));
    CGContextTranslateCTM(c, 0.0f, -32.0f);
    
	[self.view.layer renderInContext:c];
	
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    self.wasCameraPickerActiveThenSkipCamLaunch = NO;
    
    BuzzButtonHeadlineFunctionality *viewController=[[BuzzButtonHeadlineFunctionality alloc] init];
    viewController.m_photoimage=viewImage;
    viewController.m_strType=self.m_strType;
    viewController.m_strMessage=self.l_textView.text;
    viewController.metadata = self.metadata;
    viewController.shareVideo = NO;
    [viewController setHidesBottomBarWhenPushed:NO];
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController release];
    
    
}

- (IBAction)trashMovie:(id)sender
{
    self.moviePath = nil;
    [self btnRetakeAction:nil];
}

- (IBAction)shareMovie:(id)sender
{
    // Save to Documents folder
    NSData *videoData = [NSData dataWithContentsOfURL: self.movieURL];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"/tempVid.MOV"];
    
    BOOL success = [videoData writeToFile:tempPath atomically:YES];
    if (!success) {
        NSLog(@"Couldn't write down a temporary movie file");
    }
    
    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (self.moviePath)) {
        UISaveVideoAtPathToSavedPhotosAlbum (
                                             self.moviePath, nil, nil, nil);
    }

    [thumbnailForMovie setHidden:NO];
    UIGraphicsBeginImageContext(CGSizeMake(320,320));
    
	CGContextRef c = UIGraphicsGetCurrentContext();
	//CGContextTranslateCTM(c, -contentOffset.x, -(contentOffset.y-scrollFrame.origin.y));
    CGContextTranslateCTM(c, 0.0f, -32.0f);
    
	[self.view.layer renderInContext:c];
	
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    self.wasCameraPickerActiveThenSkipCamLaunch = NO;
    
    BuzzButtonHeadlineFunctionality *viewController=[[BuzzButtonHeadlineFunctionality alloc] init];
    viewController.m_photoimage=thumbnailForMovie.image;
    viewController.m_strType=self.m_strType;
    viewController.m_strMessage=self.l_textView.text;
    viewController.metadata = self.metadata;
    viewController.shareVideo = success;
    [viewController setHidesBottomBarWhenPushed:NO];
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    [self.moviePlayerController.view removeFromSuperview];
    self.moviePlayerController = nil;
}

- (IBAction)play:(id)sender
{
    if (!playing) {
        if (!thumbnailForMovie.isHidden) {
            [thumbnailForMovie setHidden:YES];
        }
        [self.moviePlayerController play];
        playing = YES;
        [playButton setImage:[UIImage imageNamed:@"pause-btn"] forState:UIControlStateNormal];
    } else {
        [self.moviePlayerController pause];
        playing = NO;
        [playButton setImage:[UIImage imageNamed:@"play-icon"] forState:UIControlStateNormal];
    }
}

- (MPMoviePlayerController *) moviePlayerController
{
    if (!moviePlayerController) {
        self.moviePlayerController = [[[MPMoviePlayerController alloc] init] autorelease];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackComplete:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.moviePlayerController];
        [moviePlayerController.view setFrame:CGRectMake(0,
                                                        0,
                                                        movieEnclosureView.frame.size.width,
                                                        movieEnclosureView.frame.size.height)];
        [movieEnclosureView addSubview:self.moviePlayerController.view];
        moviePlayerController.shouldAutoplay = NO;
    }
    
    return moviePlayerController;
}


- (void)moviePlaybackComplete:(NSNotification *)notification
{
/*    MPMoviePlayerController *controller = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];*/
    [thumbnailForMovie setHidden:NO];
//    [controller.view removeFromSuperview];
//    [controller release];
}

- (IBAction)moviewPreviewAction:(id)sender
{
    self.view = self.shareView;
    NSURL    *fileURL    =   [NSURL fileURLWithPath:self.moviePath];
    
    self.moviePlayerController.contentURL = fileURL;
    
    UIImage *thumbnail = [self.moviePlayerController thumbnailImageAtTime:0
                                                timeOption:MPMovieTimeOptionExact];
    [thumbnailForMovie setHidden:NO];
    [thumbnailForMovie setImage:thumbnail];
    
    playing = NO;

}

/*
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];

	//m_view.hidden=YES;
//	[m_indicatorView stopAnimating];
//	[m_indicatorView release];
//	m_indicatorView=nil;
//	
//	[m_view removeFromSuperview];
//	[m_view release];
//	m_view=nil;
	
	if (temp_responseCode==500)
	{
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Internal error occured, please try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[dataReloadAlert show];
	}
	else if(temp_responseCode==200)
	{
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Buzz request sent successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[dataReloadAlert show];

	}
}
 */

#pragma mark -
#pragma mark textView methods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if ([text isEqualToString:@"\n"]) 
	{
		//preview_view.hidden=YES;
		
		
		[l_textView resignFirstResponder];
		
		
		Preview_ViewLabel.text=@"Preview";
		if(l_textView.text.length==0)
		{
			m_buttomTextView.hidden=YES;
			l_commentCheckMark.hidden=YES;
			
		}
		else
		{	
			m_buttomTextView.hidden=NO;
			
			l_commentCheckMark.hidden=NO;
			m_ViewTopTextView.hidden=YES;
			m_buttomTextView.hidden=NO;
			m_buttomTextView.text=textView.text; //str_textView;
		}
		
		
		m_ViewTopTextView.hidden=YES;
		
	}
	
	if(l_textView.text.length>=count_text1 && range.length==0)
	{
		return NO;
	}
	else
	{
		return YES;
	}
	return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
	m_LabCountChar.hidden=NO;
	int max_count;
	if (count_text1 == 0) 
	{
		NSLog(@"not editing\n");
		[l_textView setUserInteractionEnabled:FALSE];
	}
	//str_textView = l_textView.text;
	m_buttomTextView.text=l_textView.text;
	//[str_textView retain];
	max_count = count_text1-[l_textView.text length]; //[str_textView length];
	m_LabCountChar.text=[NSString stringWithFormat:@"%d",max_count];
	
}


#pragma mark -
#pragma mark textView methods

- (CGFloat)distancebetweentwopoints:(CGPoint)frompoint topoint:(CGPoint)topoint 
{
	
	float x = topoint.x - frompoint.x;
	float y = topoint.y - frompoint.y;
	
	return sqrt(x * x + y * y);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
	[super touchesBegan:touches withEvent:event];
	
	NSSet *all = [event allTouches];
	
	NSArray *allTouches = [all allObjects];
	
	int count = [allTouches count];
	if (count > 1) {
		touch1 = [[allTouches objectAtIndex:0] locationInView:self.view]; 
		touch2 = [[allTouches objectAtIndex:1] locationInView:self.view];
		
		initialDistance=[self distancebetweentwopoints:touch1 topoint:touch2];
		if (initialDistance==0)
		{
			initialDistance=1;
		}
		
		
		scaleValue = 1.0;
	}
	
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
	CGPoint currentTouch1;
	CGPoint currentTouch2;

//	UITouch *touch = [touches anyObject];
	NSSet *all = [event allTouches];
	NSArray *allTouches = [all allObjects];
	int count = [allTouches count]; 
	if(count ==1)
	{
		CGPoint newTouch = [[touches anyObject] locationInView:self.view];
		CGPoint lastTouch = [[touches anyObject] previousLocationInView: self.view];
        
        // GDL: Use %f for CGFloat values, not %i.
		float xDif = newTouch.x - lastTouch.x;
		float yDif = newTouch.y - lastTouch.y;
		
		CGAffineTransform translate = CGAffineTransformMakeTranslation(xDif, yDif);
		[m_imageViewCrop setTransform: CGAffineTransformConcat([m_imageViewCrop transform], translate)];
		
	}
	else if(count>1)
	{	
			
			
			if ((CGRectContainsPoint([m_imageViewCrop frame], [[allTouches objectAtIndex:0] locationInView:self.view])) ||
			(CGRectContainsPoint([m_imageViewCrop frame], [[allTouches objectAtIndex:1] locationInView:self.view]))) {
			
			currentTouch1 = [[allTouches objectAtIndex:0] locationInView:self.view]; 
			currentTouch2 = [[allTouches objectAtIndex:1] locationInView:self.view];
			CGFloat finalDistance = [self distancebetweentwopoints:currentTouch1 topoint:currentTouch2];

			
			//CGFloat finalDistance = [self distanceBetweenTwoPoints:currentTouch1 toPoint:currentTouch2];
			
			//Check if zoom in or zoom out.
			
			if (initialDistance!=0&&finalDistance!=0)
			{
				if(m_imageViewCrop.frame.size.width>=640 && m_imageViewCrop.frame.size.height>=920)
				{
					if(initialDistance > finalDistance) 
					{
						NSLog(@"Zoom Out");
						scaleValue-=0.02;
					}
				}	
				if (initialDistance < finalDistance){
					NSLog(@"Zoom In");
					scaleValue+=0.02;
				}
				NSLog(@"initial: %0.2f",initialDistance);
				NSLog(@"final: %0.2f",finalDistance);
				//CGAffineTransform scaleTransform = CGAffineTransformMakeScale( scaleValue, scaleValue);
				m_imageViewCrop.transform= CGAffineTransformMakeScale(scaleValue, scaleValue);
				//touch1 = currentTouch1;
//				touch2 = currentTouch2;
//				scaleValue=1.0;
//				
				initialDistance = finalDistance;
			}			
		}
	}
	
	
		
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"touches ended");
	[super touchesEnded:touches withEvent:event];
	
}
#pragma mark -
#pragma mark alertView methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex==0)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
	
}

#pragma mark -
#pragma mark Filter methods

-(void) loadFilters {
    for(int i = 0; i < 10; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d.png", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(60+10), 5.0f, 60.0f, 60.0f);
//        button.layer.cornerRadius = 7.0f;
        
        //use bezier path instead of maskToBounds on button.layer
//        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
//                                                 byRoundingCorners:UIRectCornerAllCorners
//                                                       cornerRadii:CGSizeMake(7.0,7.0)];
//        
//        CAShapeLayer *maskLayer = [CAShapeLayer layer];
//        maskLayer.frame = button.bounds;
//        maskLayer.path = bi.CGPath;
//        button.layer.mask = maskLayer;
//        
//        button.layer.borderWidth = 1;
//        button.layer.borderColor = [[UIColor blackColor] CGColor];
        
        [button addTarget:self
                   action:@selector(filterClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
//        [button setTitle:@"*" forState:UIControlStateSelected];
        [button setImage:[UIImage imageNamed:@"filter-img-selected"] forState:UIControlStateSelected];
        if(i == 0){
            [button setSelected:YES];
        }
		[self.filterScrollView addSubview:button];
	}
	[self.filterScrollView setContentSize:CGSizeMake(10 + 10*(60+10), 75.0)];
    
    filter = [[GPUImageFilter alloc] init];
}


-(void) showFilters {
    self.m_BeautyBtn.selected = NO;
    self.m_ZoomBtn.selected = NO;
    self.m_zoomSliderView.hidden = YES;
	self.m_beautyViewBorder.hidden=YES;
    if (staticPicture) {
        [self removeAllTargets];
        [staticPicture release];
    }
    
    staticPicture = [[GPUImagePicture alloc] initWithImage:self.m_imageView.image smoothlyScaleOutput:YES];

    [self prepareStaticFilter];
    
    [self.filtersBackgroundImageView setHidden:NO];
    [self.filterScrollView setHidden:NO];
    [self.filtersToggleButton setSelected:YES];
    self.filtersToggleButton.enabled = NO;
    CGRect sliderScrollFrame = self.filterScrollView.frame;
    sliderScrollFrame.origin.y = 430;
    sliderScrollFrame.origin.y -= self.filterScrollView.frame.size.height;
    CGRect sliderScrollFrameBackground = self.filtersBackgroundImageView.frame;
    sliderScrollFrameBackground.origin.y -=
    self.filtersBackgroundImageView.frame.size.height-3;
    
    self.filterScrollView.hidden = NO;
    self.filtersBackgroundImageView.hidden = NO;
    [UIView animateWithDuration:0.10
                          delay:0.05
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         self.filterScrollView.frame = sliderScrollFrame;
                         self.filtersBackgroundImageView.frame = sliderScrollFrameBackground;
                     }
                     completion:^(BOOL finished){
                         self.filtersToggleButton.enabled = YES;
                     }];
}

-(void) hideFilters {
    [self.filtersToggleButton setSelected:NO];
    CGRect sliderScrollFrame = self.filterScrollView.frame;
    sliderScrollFrame.origin.y += self.filterScrollView.frame.size.height;
    
    CGRect sliderScrollFrameBackground = self.filtersBackgroundImageView.frame;
    sliderScrollFrameBackground.origin.y += self.filtersBackgroundImageView.frame.size.height-3;
    
    [UIView animateWithDuration:0.10
                          delay:0.05
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         self.filterScrollView.frame = sliderScrollFrame;
                         self.filtersBackgroundImageView.frame = sliderScrollFrameBackground;
                     }
                     completion:^(BOOL finished){
                         
                         self.filtersToggleButton.enabled = YES;
                         self.filterScrollView.hidden = YES;
                         self.filtersBackgroundImageView.hidden = YES;
                     }];
}

-(IBAction) toggleFilters:(UIButton *)sender {
    sender.enabled = NO;
    if (sender.selected){
        [self hideFilters];
    } else {
        [self showFilters];
    }
    
}

- (void) resetFilters
{
    for(UIView *view in self.filterScrollView.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
        }
    }
    
    [self removeAllTargets];
    
    selectedFilter = 0;
    [self setFilter:0];
}

-(void) filterClicked:(UIButton *) sender {
    for(UIView *view in self.filterScrollView.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
        }
    }
    
    [sender setSelected:YES];
    [self removeAllTargets];
    
    selectedFilter = sender.tag;
    [self setFilter:sender.tag];
    [self prepareFilter];
}


-(void) setFilter:(int) index {
    if (filter) {
        [filter release];
        filter = nil;
    }
    switch (index) {
        case 1:{
            filter = [[GPUImageContrastFilter alloc] init];
            [(GPUImageContrastFilter *) filter setContrast:1.75];
        } break;
        case 2: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"crossprocess"];
        } break;
        case 3: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"02"];
        } break;
        case 4: {
            filter = [[GrayscaleContrastFilter alloc] init];
        } break;
        case 5: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"17"];
        } break;
        case 6: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"aqua"];
        } break;
        case 7: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"yellow-red"];
        } break;
        case 8: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"06"];
        } break;
        case 9: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"purple-green"];
        } break;
        default:
            filter = [[GPUImageFilter alloc] init];
            break;
    }
}

-(void) removeAllTargets {
    //regular filter
    [staticPicture removeAllTargets];
    [filter removeAllTargets];
}

-(void) prepareFilter {
    [self prepareStaticFilter];
}

//- (GPUImageView *) imageView
//{
//    if (!imageView) {
//        imageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 32, 320, 320)];
//        [imageView setBackgroundColor:[UIColor blackColor]];
//        [self.defaultView addSubview:_imageView];
//        //[self.view sendSubviewToBack:_imageView];
//    }
//    return imageView;
//}

-(void) prepareStaticFilter {
    
    [staticPicture addTarget:filter];
    
    [staticPicture processImage];
    UIImage *currentFilteredVideoFrame = [filter imageFromCurrentlyProcessedOutput];
    [self.m_imageView setImage:currentFilteredVideoFrame];
    
    
}

#pragma mark -
#pragma mark imagePickerController methods

- (UIImage *) cropImage:(CGRect) toRect
{
    UIGraphicsBeginImageContext(temp_View.frame.size);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGRect clippedRect = CGRectMake(0, headerView.frame.origin.x, 320, 320);
    CGContextClipToRect(currentContext, clippedRect);
    CGContextDrawImage(currentContext, temp_View.bounds, m_imageView.image.CGImage);
    UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cropped;
}

//-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//{
//	m_personImage=[[[UIImage alloc] init]autorelease];
//	m_personImage=image;
//	
//	if (m_personData) {
//		[m_personData release];
//		m_personData=nil;
//	}
//	m_personData=[NSData dataWithData:UIImagePNGRepresentation(m_personImage)];
//	
//	[m_personData retain];
//	[self dismissModalViewControllerAnimated:YES];
//}


- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
	//create a context to do our clipping in
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
	//create a rect with the size we want to crop the image to
	//the X and Y here are zero so we start at the beginning of our
	//newly created context
	CGRect clippedRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	CGContextClipToRect(currentContext, clippedRect);
	
	//create a rect equivalent to the full size of the image
	//offset the rect by the X and Y we want to start the crop
	//from in order to cut off anything before them
	CGRect drawRect = CGRectMake(0,
								 0,
								 imageToCrop.size.width,
								 imageToCrop.size.height);
	
	//draw the image to our clipped context using our offset rect
	CGContextDrawImage(currentContext, drawRect, imageToCrop.CGImage);
	
	//pull the image from our cropped context
	UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
	
	
	//pop the context to get back to the default
	UIGraphicsEndImageContext();
	
	//Note: this is autoreleased
	return cropped;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	for (id key in info) {
        NSLog(@"didFinishPickingMediaWithInfo: key: [%@], value: [%@]", key, [info objectForKey:key]);
    }
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        self.metadata = [[[NSMutableDictionary alloc] initWithInfoFromImagePicker:info] autorelease];
    } else {
        NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
        
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        self.metadata = [[[NSMutableDictionary alloc] init] autorelease];
        [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *representation = [asset defaultRepresentation];
            NSDictionary *metadataDict = [representation metadata]; 
            NSLog(@"%@",metadataDict);
            
            [self.metadata addEntriesFromDictionary:metadataDict];
            
        } failureBlock:^(NSError *error) {
            NSLog(@"%@",[error description]);
        }];
        [library release];
    }

    
	[m_indicatorView stopAnimating];
	[temp_View setBackgroundColor:[UIColor clearColor]];
	
    if (![[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
        saturationSlider.value = 1.00;
        brightnessSlider.value = 0.00;
        smoothnessSlider.value = 0.00;
        saturation=1;
        gamma=1;
        smoothness=0;
        
        m_sliderZooming.value = 1.00;
        
        l_saturationValue=1.0;
        l_brightnessValue=0.0;
        l_smoothnessValue=0.0;
        l_zoomValue=1.0;
        
        previousScaleValue=1.0f;
        
        self.m_imageView.transform = CGAffineTransformIdentity;
        self.m_imageView.frame=CGRectMake(0, 0, self.m_imageView.frame.size.width, self.m_imageView.frame.size.height);
        [self.m_scrollView setContentSize:CGSizeMake(self.m_imageView.frame.size.width, self.m_imageView.frame.size.height)];
        
        
        if(shouldUseEditedImageOfPhoto==TRUE)
        {
            if ([info objectForKey:UIImagePickerControllerEditedImage] != nil) {
                self.m_buzzImage=[[info objectForKey:UIImagePickerControllerEditedImage] scaledDownImageWithMaximumSize:CGSizeMake(320,480)];
            } else {
                self.m_buzzImage=[[info objectForKey:UIImagePickerControllerOriginalImage] scaledDownImageWithMaximumSize:CGSizeMake(320,480)]; 
            }

            if ((m_buzzImage.size.width >= 320) && (m_buzzImage.size.height >= 320)) {
                
                self.m_scrollView.contentSize=CGSizeMake(m_buzzImage.size.width, m_buzzImage.size.height);
            } else {
                self.m_scrollView.contentSize=CGSizeMake(320,320);
            }
        
        }
        else
        {
            self.m_buzzImage=[[info objectForKey:UIImagePickerControllerOriginalImage] scaledDownImageWithMaximumSize:CGSizeMake(320,480)];
            [self.m_scrollView setContentSize:CGSizeMake(320, 320)];
            self.m_imageView.frame=CGRectMake(0, 0, 320,320);
        }
        
        
        NSLog(@"%f %f",m_buzzImage.size.width,m_buzzImage.size.height);
        
//        CGImageRef imageRef = CGImageCreateWithImageInRect([self.m_buzzImage CGImage], CGRectMake(0, headerView.frame.origin.y, 320, 480 - headerView.frame.size.height));
//        // or use the UIImage wherever you like
//        [self.m_imageView setImage:[UIImage imageWithCGImage:imageRef]];
//        CGImageRelease(imageRef);
        self.m_imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.m_imageView setImage:self.m_buzzImage];
        
        self.wasCameraPickerActiveThenSkipCamLaunch = YES;
        
        [self dismissViewControllerAnimated:NO completion:nil];

        if(!(self.m_imageView.image))
        {
            m_lblselectAnImg.hidden=NO;
            btnEdit.enabled=FALSE;
            m_sendBtn.enabled=FALSE;
        }
        else {
            m_lblselectAnImg.hidden=YES;
            btnEdit.enabled=TRUE;
            m_sendBtn.enabled=TRUE;
            
        }
        
        m_buttomTextView.hidden=YES;
        l_commentCheckMark.hidden=YES;
        m_buttomTextView.text=@"";
        l_textView.text=@"";
        m_LabCountChar.text= [NSString stringWithFormat:@"%d", count_text1];
        [self performSelector:@selector(btnPreviewAction:)];
    } else {
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0)
            == kCFCompareEqualTo) {
            
            self.moviePath = [[info objectForKey:
                                    UIImagePickerControllerMediaURL] path];
            self.movieURL = [info objectForKey:
                             UIImagePickerControllerMediaURL];
        }
        self.wasCameraPickerActiveThenSkipCamLaunch = YES;
        
        [self dismissModalViewControllerAnimated:NO];
        [self performSelector:@selector(moviewPreviewAction:) withObject:nil afterDelay:0.5];

    }
	
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
  	[self performSelector:@selector(btnPreviewAction:)];
	
	[m_indicatorView stopAnimating];
	[temp_View setBackgroundColor:[UIColor clearColor]];
	
	//m_view.hidden=YES;
	[self dismissViewControllerAnimated:NO completion:^{
        [self.m_imageView setImage:nil];
        [tnApplication goToNewsPage];
    }];
#if TARGET_IPHONE_SIMULATOR
    return;
#endif

//#if TARGET_IPHONE_SIMULATOR == 1
//	[self.m_imageView setImage:nil];
//  	[tnApplication performSelector:@selector(goToNewsPage) withObject:nil afterDelay:0.2];
//    return;
	// US237: On cancel show/pop to Runway, if no image selected earlier
	//if((self.m_imageView == nil) || (self.m_imageView.image == nil)) 
    {
//		QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *) [[UIApplication sharedApplication] delegate];
//		mainDg.tabBarController.selectedIndex = RUNWAY_TAB_INDEX;
//        [tnApplication goToNewsPage];
	}
//#endif

    self.wasCameraPickerActiveThenSkipCamLaunch = NO;
    self.view = self.defaultView;
    [self performSelector:@selector(btnRetakeAction:) withObject:nil afterDelay:0.2];
	if(!(self.m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else {
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}
}


// GDL: Modified everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release all retained IBOutlets.
    self.m_scrollView = nil;
    self.m_requiredView = nil;
    self.defaultView = nil;
    self.m_zoomSliderView = nil;
    self.m_beautyViewBorder = nil;
    self.m_sendBtn = nil;
    self.m_lblselectAnImg = nil;
    self.m_ViewTopTextView = nil;
    self.btnEdit = nil;
    self.btnPreview = nil;
    self.btnUndo = nil;
    self.m_bottomBar = nil;
    self.m_imageViewCrop = nil;
    self.m_beautyView = nil;
    self.m_sliderZooming = nil;
    self.m_imageView = nil;
    self.m_buttomTextView = nil;
    self.m_userImage = nil;
    self.l_textView = nil;
    self.m_LabCountChar = nil;
    
    self.smoothnessSlider = nil;
    self.brightnessSlider = nil;
    self.saturationSlider = nil;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];    
    
}

- (void)dealloc {
    
    [metadata release];
    [m_imgPicker release];
    [m_ZoomBtn release];
    [m_userImage release];
    [l_textView release];
    [m_buttomTextView release];
    [m_buttomTextView2 release];
    [m_LabCountChar release];
    [m_imageView release];
    [m_imageViewCrop release];
    [m_sliderZooming release];
    [m_buzzImage release];
    [smoothnessSlider release];
    [brightnessSlider release];
    [saturationSlider release];
    [m_beautyView release];
    [m_beautyViewBorder release];
    [m_bottomBar release];
    [m_indicatorView release];
    [btnEdit release];
    [btnPreview release];
    [btnUndo release];
    [m_ViewTopTextView release];
    [m_arrWithUndoDict release];
    [m_ZoomDict release];
    [m_BeautyDict release];
    [m_CommentDict release];
    [m_strType release];
    [m_previewViewlabel3 release];
    
    // This was already released.
    //[m_personData release];
    
    [m_personImage release];
    [m_lblselectAnImg release];
    [m_sendBtn release];
    [m_zoomSliderView release];
    [m_requiredView release];
    [m_scrollView release];
    
    self.flashButton = nil;
    self.cameraButton = nil;
    self.shootButton = nil;
    self.recordButton = nil;
    self.sourceSwitch = nil;
    self.headerView = nil;
    self.moviePath = nil;
    self.sharePhotoView = nil;
    self.moviePlayerController = nil;
    self.greyOverlay = nil;
    
    if (staticPicture)
        [staticPicture release];
    if (filter)
        [filter release];

    
	[super dealloc];
}



@end
