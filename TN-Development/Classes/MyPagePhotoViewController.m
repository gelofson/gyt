//
//  MyPagePhotoViewController.m
//  QNavigator
//
//  Created by softprodigy on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyPagePhotoViewController.h"
#import "ViewMorePicsMyPage.h"
#import "QNavigatorAppDelegate.h"
#import "ShopbeeAPIs.h"
#import "Constants.h"
#import "AsyncImageView.h"
#import "FittingRoomLoadingView.h"
#import "NewsDataManager.h"
#import "DetailPageMessageController.h"
#import "CategoryData.h"
#import "Cell.h"
#import "LoadingIndicatorView.h"
#import "NewCell.h"
# define SectionHeaderHeight 35

@implementation MyPagePhotoViewController

ViewMorePicsMyPage     * l_morePics;
QNavigatorAppDelegate  * l_appDelegate;
ShopbeeAPIs            * l_request;
FittingRoomLoadingView * l_loadingView;

LoadingIndicatorView *l_FriendsIndicatorView;
@synthesize m_view;
@synthesize m_indicatorView;
@synthesize m_arrGetMyPage;
@synthesize tableViewGetPhoto;
@synthesize m_arrGetMypagePopularPhoto;
@synthesize m_FriendMailString;
@synthesize isFromViewFriends;
@synthesize m_HeaderLabel;
@synthesize m_FriendNameString;
@synthesize storyCount;
@synthesize footerView;
@synthesize table;
#pragma mark - Custom
#pragma mark

-(IBAction) m_goToBackView {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- UIGridViewDelegate

- (UIView *) gridFooterView
{
    if (!self.footerView) {
        self.footerView = [[[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 30)] autorelease];
        
    }
    return  self.footerView;
}



- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 80;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 115;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 4;
}


- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid
{
	return [m_arrGetMypagePopularPhoto count];
}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
	NewCell *cell = (NewCell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[[NewCell alloc] init] autorelease];
	}
	
    NSInteger indexInArray = rowIndex * 4 + columnIndex;
    
    NSDictionary *tempDict =[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray];
        
    	
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[tempDict valueForKey:@"photoThumbUrl"]];
    
/*    if ([[[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray] objectForKey:@"messageType"] caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame)  // for setting the type of the message in the table view
    {
        
        cell.headline.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it"];
    } else if([[[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray] objectForKey:@"messageType"] caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame)
    {
        
        cell.headline.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
        
    } else if([[[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray] objectForKey:@"messageType"] caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame)
    {
        
        cell.headline.text=[[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
        
    } else if([[[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray] objectForKey:@"messageType"] caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame)
    {
        
        cell.headline.text=[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
        
    } else if([[[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray] objectForKey:@"messageType"] caseInsensitiveCompare:kFoundASale] == NSOrderedSame)
    {
        
        cell.headline.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
        
    } else*/
       cell.headline.text=[[m_arrGetMypagePopularPhoto objectAtIndex:indexInArray] objectForKey:@"messageType"];

    cell.thumbnail.messageTag = indexInArray+1;
    cell.thumbnail.cropImage = YES;
    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:nil];
    
	return cell;
}

- (void) ProductBtnClicked:(id)sender
{
    int tag=((UIButton *)sender).tag-1;
    
    l_request=[[ShopbeeAPIs alloc]init];
	
        if (selectedIndex != tag) {
            [l_FriendsIndicatorView startLoadingView:self];
            NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
            NSString *tmp_String=[prefs2 stringForKey:@"userName"];
            
            selectedIndex = tag;
            [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_arrGetMypagePopularPhoto objectAtIndex:tag]valueForKey:@"id"]intValue]];
            
        }
	
	[l_request release];
    l_request=nil;
}

-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {		
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
		
        CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
        
        DetailPageMessageController *detailController = [[[DetailPageMessageController alloc] init] autorelease];
        detailController.m_data = catData;
        [detailController setMessagesIdsFromPopularPhotos:m_arrGetMypagePopularPhoto];
        detailController.m_Type = [[m_arrGetMypagePopularPhoto objectAtIndex:selectedIndex] objectForKey:@"messageType"];
        detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
        detailController.filter = POPULAR;

        NSDictionary *aData = [catData messageData:0];
        NSDictionary *tmp_Dict=[aData valueForKey:@"wsMessage"];
        NSString *custID=[tmp_Dict valueForKey:@"sentFromCustomer"];
        
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        if ([[custID lowercaseString] isEqualToString:[[prefs valueForKey:@"userName"] lowercaseString] ])
            detailController.mineTrack = YES;
        else
            detailController.mineTrack = NO;
        detailController.curPage = 1;
        
        detailController.m_CallBackViewController = self;
        [self.navigationController pushViewController:detailController animated:YES];
		
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[l_FriendsIndicatorView stopLoadingView];
    [l_morePics release];
    l_morePics=nil;

}


#pragma mark - AlertView Delegates
#pragma mark

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex==0) 
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}
	
#pragma mark - Callback Methods
#pragma mark

- (void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString *str;
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSArray *tempArray=[[str JSONValue] retain];
	NSLog(@"%@",tempArray);
	if ([responseCode intValue]==200)
	{
		NSDictionary *dict;//tagLine//wsMessage
		for(int i=0;i<[tempArray count];i++)
		{
			dict=[NSDictionary dictionaryWithObjectsAndKeys:[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"bodyMessage"],@"bodyMessage",[[tempArray objectAtIndex:i]valueForKey:@"buzzId"],@"buzzId",[[tempArray objectAtIndex:i]valueForKey:@"discountInfo"],@"discountInfo",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"elapsedTime"],@"elapsedTime",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"id"],@"id",[[tempArray objectAtIndex:i]valueForKey:@"tagLine"],@"tagLine",[[tempArray objectAtIndex:i]valueForKey:@"productName"],@"productName",[[tempArray objectAtIndex:i]valueForKey:@"photoUrl"],@"photoUrl",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"],@"messageType",[[tempArray objectAtIndex:i]valueForKey:@"photoThumbUrl"],@"photoThumbUrl",nil];
			[m_arrGetMyPage addObject:dict];
		}
		
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		[l_FriendsIndicatorView startLoadingView:self];
		
		l_request=[[ShopbeeAPIs alloc] init];
		if (isFromViewFriends==YES) 
		{
			[l_request getMyPagePopularPhotos:@selector(requestCallBackMethodforPopularPhoto:responseData:) tempTarget:self customerid:m_FriendMailString num:storyCount loginid:customerID];
		}
		else
		{
			[l_request getMyPagePopularPhotos:@selector(requestCallBackMethodforPopularPhoto:responseData:) tempTarget:self customerid:customerID num:storyCount loginid:customerID];
		}
		
		[l_request release];
		l_request=nil;
	}
	else if([responseCode intValue]==kLockedInfoErrorCode)
	{
		//[l_loadingView stopLoadingView];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kCannotViewFriendInfoTitle message:kCannotViewFriendInfoMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    //[l_loadingView stopLoadingView];
	
	
}

- (void)requestCallBackMethodforPopularPhoto:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString *str;
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSArray *tempArray=[[str JSONValue] retain];
	NSLog(@"%@",tempArray);
	
	
	if ([responseCode intValue]==200)
	{
		NSDictionary *dict;
		for(int i=0;i<[tempArray count];i++)
		{
            NSString *msgType = [[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"];
            
            if (![msgType isEqualToString:@"Q"]) {
                dict=[NSDictionary dictionaryWithObjectsAndKeys:[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"bodyMessage"],@"bodyMessage",[[tempArray objectAtIndex:i]valueForKey:@"buzzId"],@"buzzId",[[tempArray objectAtIndex:i]valueForKey:@"discountInfo"],@"discountInfo",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"elapsedTime"],@"elapsedTime",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"id"],@"id",[[tempArray objectAtIndex:i]valueForKey:@"tagLine"],@"tagLine",[[tempArray objectAtIndex:i]valueForKey:@"productName"],@"productName",[[tempArray objectAtIndex:i]valueForKey:@"photoUrl"],@"photoUrl",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"],@"messageType",[[tempArray objectAtIndex:i]valueForKey:@"photoThumbUrl"],@"photoThumbUrl",nil];
                NSLog(@"%@",dict);
                [m_arrGetMypagePopularPhoto addObject:dict];
            }
            
		}
		//NSLog(@"m_arrGetMypagePopularPhoto: %@",m_arrGetMypagePopularPhoto);
		[table reloadData];
	}
    
	else             
    {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	[l_FriendsIndicatorView stopLoadingView];
}

#pragma mark - View Lifecycle
#pragma mark

- (void)viewDidAppear:(BOOL)animated {
	
}

- (void)viewDidLoad {
	[super viewDidLoad];
    NSLog(@"story count %d",storyCount);
    
	m_arrGetMypagePopularPhoto=[[NSMutableArray alloc]init];
	m_arrGetMyPage=[[NSMutableArray alloc]init];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	l_FriendsIndicatorView=[LoadingIndicatorView SharedInstance];
	[l_FriendsIndicatorView startLoadingView:self];
	
	l_request=[[ShopbeeAPIs alloc] init];
	
	if (isFromViewFriends==YES) 
	{
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@'s: Photos",m_FriendNameString];
		[l_request getMyPageRecentPhotos:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:m_FriendMailString loginid:customerID];
	}
	else
	{
		m_HeaderLabel.text=[NSString stringWithFormat:@"My Page: Photos"];
		[l_request getMyPageRecentPhotos:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID loginid:customerID];
	}

	[l_request release];
	l_request=nil;
	
	
}

- (void)viewWillAppear:(BOOL)animated {
    selectedIndex = -1;
}

// GDL: Changed everything below.

#pragma mark - Memory Management
#pragma mark

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release retained IBOutlets.
    self.tableViewGetPhoto = nil;
    self.m_HeaderLabel = nil;
}

- (void)dealloc {
	[m_view release];
    [m_indicatorView release];
    [m_arrGetMyPage release];
    [m_arrGetMypagePopularPhoto release];
	[tableViewGetPhoto release];
	[m_FriendMailString release];
	[m_HeaderLabel release];
	[m_FriendNameString release];
	[footerView release];
    [table release];
	[super dealloc];
}

@end