//
//  IndyShopViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 30/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTopBarView.h"
//#import "CLLocation+AFExtensions.h"
#include <CoreLocation/CoreLocation.h>

@interface IndyShopViewController : UIViewController 
{
	UITableView					*m_tableView;
	UIView						*m_moreDetailView;
	UIActivityIndicatorView		*m_activityIndicator;
	NSMutableData				*m_mutIndyShopResponseData;
	NSURLConnection				*m_theConnection;
	NSURLConnection				*m_mallMapConnection;

	BOOL						m_isConnectionActive;
	BOOL						m_isMallMapConnectionActive;
	CLLocationManager			*m_locationManager;
	
	int					m_intResponseCode;
		
	UIScrollView		*m_scrollView;
	UISearchBar			*m_searchBar;
	UILabel				*m_lblCaption;
	BOOL				isCurrentIndyShopList;
	UITableView			*m_tblSearchResults;
	UIImageView			*m_exceptionPage;
}

@property (nonatomic,retain) IBOutlet UIImageView *m_exceptionPage;

@property (nonatomic,retain) IBOutlet UITableView *m_tblSearchResults;
@property BOOL isCurrentIndyShopList;
@property int m_intResponseCode;
@property (nonatomic,retain) IBOutlet UILabel *m_lblCaption;
@property (nonatomic,retain) IBOutlet UISearchBar *m_searchBar;
@property (nonatomic,retain) UIScrollView *m_scrollView;

@property (nonatomic,retain) IBOutlet		UITableView *m_tableView;
@property (nonatomic,retain) IBOutlet		UIActivityIndicatorView *m_activityIndicator;

@property (nonatomic,retain) NSMutableData	*m_mutIndyShopResponseData;
@property (nonatomic,retain) UIView			*m_moreDetailView;
@property (nonatomic,retain) NSURLConnection *m_theConnection;
@property (nonatomic,retain) NSURLConnection *m_mallMapConnection;
@property					 BOOL  			  m_isConnectionActive;
@property					 BOOL			m_isMallMapConnectionActive;

@property (nonatomic,retain)CLLocationManager *m_locationManager;

-(void)sendRequestForIndyShopData:(int)temp_indyPageNumber;
-(void)sendRequestToCheckMallMap:(NSString *)imageUrl;

@end
