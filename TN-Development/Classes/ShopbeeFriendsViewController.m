//
//  ShopbeeFriendsViewController.m
//  QNavigator
//
//  Created by softprodigy on 14/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ShopbeeFriendsViewController.h"
#import "SA_OAuthTwitterEngine.h" 
#import "SA_OAuthTwitterController.h"
#import "MGTwitterStatusesParser.h"
#import "MGTwitterXMLParser.h"

#import "MGTwitterEngine.h"
#import "QNavigatorAppDelegate.h"
#import "FBConnect.h"
#import "Constants.h"
#import "ATIconDownloader.h"
#import "CategoriesViewController.h"
#import"QNavigatorAppDelegate.h"
#import"ShopbeeAPIs.h"
#import "SBJSON.h"
#import "LoadingIndicatorView.h"
#import "AsyncImageView.h"
#import "NewsDataManager.h"

#define SectionHeaderHeight 25

//App ID
//207285035948301
//API Key
//52850ab435dfe5464a2164e662e9618d
//App Secret
//b97546c72f7826ace43b820647c5eb72

#define FacebookApiKey @"52850ab435dfe5464a2164e662e9618d"
#define FacebookApiSecret @"b97546c72f7826ace43b820647c5eb72"


//#define FacebookApiKey		@"a896ecf7cb5656dcc6ee3e5199d143f4"
//#define FacebookApiSecret	@"85edbbe757aa48d540522de01cdef79f"

@interface ShopbeeFriendsViewController ()
- (NSDictionary *)parseURLParams:(NSString *)query;

@end

@implementation ShopbeeFriendsViewController

@synthesize fbGraph;
@synthesize m_imgLogo;
@synthesize m_friendsDetails;
#ifdef OLD_FB_SDK
@synthesize m_loginDialog;
#endif
@synthesize facebookName;
@synthesize m_userDetails;
@synthesize table_AddFriends;

@synthesize m_view;
@synthesize m_indicatorView;
@synthesize m_strUids;

typedef long long MGTwitterEngineCursorID;
@synthesize m_searchBar;
@synthesize arr_str;
@synthesize backup_m_arrFrndsDic;
@synthesize m_shopBeeArray;
@synthesize m_nonshopBeeArray;
@synthesize m_intCurrentUserIndex;
@synthesize m_picUrlNonShopBee;
@synthesize m_picUrlShopBee;
@synthesize m_tableView;
@synthesize m_arrFrnds;
@synthesize btnFbclose;
@synthesize m_arrNonFrnds;

ShopbeeAPIs *l_request;
int countFilteredNameList=0;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

-(void)closeButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	m_shopBeeArray =[[NSMutableArray alloc]init];
	m_nonshopBeeArray=[[NSMutableArray alloc]init];		
	m_picUrlShopBee=[[NSMutableArray alloc]init];
	m_picUrlNonShopBee=[[NSMutableArray alloc]init];
	
	//[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"main_bg.png"]]];
	
	
#ifdef FB_OLD_SDK_USAGE
	NSString *client_id = FacebookApiKey;
	fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
	[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:) 
						 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
	
#else
    NSArray *permissions = [NSArray arrayWithObjects:@"user_photos",@"user_videos",@"publish_stream",@"read_stream",@"offline_access",@"user_checkins",@"friends_checkins", nil];
    
    [[tnApplication m_session] setSessionDelegate:self];
    
    if (![[tnApplication m_session] isSessionValid]) {
        [[tnApplication m_session] authorize:permissions];
    }
    else {
        tnApplication.isLoginFacebook = true;
        [self performSelector:@selector(getMeFriendsButtonPressed:)];
        LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
        [tempLoadingIndicator startLoadingView:self];
    }
    
#endif
	
	/* search bar background setting */
	[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		//[searchField setBackground: [UIImage imageNamed:@"search-bgrd.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	/***********************************/
	
	
	//if (l_appDelegate.countVal==2) 
//	{
//		m_session = [[FBSession sessionForApplication:FacebookApiKey secret:FacebookApiSecret delegate:self] retain];
//	}
//	else 
//	{
	//...	m_session = [[FBSession alloc] init];
//	}

	//....[m_session resume];
	//...m_searchBar.delegate=self;
	//....m_tableView.delegate=self;
	//....[self performSelector:@selector(loginButtonTapped)];
	//....[self performSelector:@selector(load_tableViewDataSource)];
	[super viewDidLoad];
	NSLog(@"%@",m_arrNonFrnds);
	
		
	//[self performSelector:@selector(getMeFriendsButtonPressed:)];
//	[self performSelector:@selector(getMeButtonPressed:)];

}

#pragma mark -
#pragma mark custom methods


-(void)btnAddAllAction:(id)sender
{
	NSLog(@"row no: %d",[sender tag]);
	if (m_shopBeeArray.count>0)
	{
		m_intCurrentUserIndex=-1;
		[self sendAddFriendRequest];
	}
	
}

-(void)load_tableViewDataSource{
			
	self.m_arrNonFrnds=[[[NSArray alloc]initWithArray:m_nonshopBeeArray] autorelease];
	self.m_arrFrnds=[[[NSMutableArray alloc] initWithArray:m_shopBeeArray] autorelease];
	[self.m_tableView reloadData];
		
}

#ifdef FB_OLD_SDK_USAGE

-(void)btnInviteAction:(id)sender
{
	[m_searchBar resignFirstResponder];

	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator performSelectorInBackground:@selector(startLoadingView:) withObject:self];
	
	NSLog(@"m_arrNonFrnds: %@",m_arrNonFrnds);
	NSLog(@"%@",[NSString stringWithFormat:@"%@/feed",[[m_arrNonFrnds objectAtIndex:[sender tag]] objectForKey:@"id"]]);
	//NSLog(@"m_arrFrnds: %@",m_arrFrnds);
	
	NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:3];
	[variables setObject:kTellAFriendMailBody2 forKey:@"message"];
 	[variables setObject:@"http://itunes.com/apps/fashiongram" forKey:@"link"];
 	[variables setObject:@"FashionGram" forKey:@"name"];
 	//[variables setObject:@"This is the plain text copy next to the image.  All work and no play makes Jack a dull boy." forKey:@"description"];
	
	//NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:4];
//	
//	[variables setObject:@"this is a test message: postMeFeedButtonPressed" forKey:@"message"];
// 	[variables setObject:@"http://bit.ly/bFTnqd" forKey:@"link"];
// 	[variables setObject:@"This is the bolded copy next to the image" forKey:@"name"];
// 	[variables setObject:@"This is the plain text copy next to the image.  All work and no play makes Jack a dull boy." forKey:@"description"];
	NSLog(@"%@",variables);
//	FbGraphResponse *fb_graph_response = [fbGraph doGraphPost:@"me/feed" withPostVars:variables];
	
	FbGraphResponse *fb_graph_response = [fbGraph doGraphPost:
										  [NSString stringWithFormat:@"%@/feed",[[m_arrNonFrnds objectAtIndex:[sender tag]] objectForKey:@"id"]] withPostVars:variables];
	
	NSLog(@"postMeFeedButtonPressed:  %@", fb_graph_response.htmlResponse);
	
    [tempLoadingIndicator stopLoadingView];
	[self showAlertView:@"" alertMessage:@"Invitation message posted on your friend's wall."];
}

#else

-(void)btnInviteAction:(id)sender
{
	[m_searchBar resignFirstResponder];
    
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator performSelectorInBackground:@selector(startLoadingView:) withObject:self];
	
	
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [[m_arrNonFrnds objectAtIndex:[sender tag]] objectForKey:@"id"], @"to",
                                   kTellAFriendMailBody2,  @"message",
                                   @"TinyNews", @"name",
                                   @"http://www.tinynews.me", @"link",
                                   @"Check this out", @"notification_text",
                                   nil];
	
    [[tnApplication m_session] enableFrictionlessRequests];
    currentAPICall =  kAPIPostOnFriendWall;
	[[tnApplication m_session] requestWithGraphPath:@"[friend_ID]/feed" andParams:params andHttpMethod:@"POST" andDelegate:self];
    [[tnApplication m_session] dialog:@"feed"
                      andParams:params
                    andDelegate:self];
	
    [tempLoadingIndicator stopLoadingView];
}

#endif
	
- (IBAction)setSelectedButton1:(id)sender 
{
	
#ifdef FB_OLD_SDK_USAGE 
    
    // Reset facebook Graph
	fbGraph.accessToken = nil;
	[fbGraph release];
	fbGraph =nil;
	
	NSHTTPCookie *cookie;
	NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	for (cookie in [storage cookies])
	{
		NSString* domainName = [cookie domain];
		NSRange domainRange = [domainName rangeOfString:@"facebook"];
		if(domainRange.length > 0)
		{
			[storage deleteCookie:cookie];
		}
	}
#endif
	
	[self.navigationController popViewControllerAnimated:YES];	
}

-(IBAction)btnLogoutAction:(id)sender
{
#ifdef FB_OLD_SDK_USAGE
	[m_session logout];
    l_appDelegate.isLogged = NO;
	[self.navigationController popViewControllerAnimated:YES];
	[m_session release];
#else    
    // This function is not in use but still replaced above code with new facebook sdk
    [[tnApplication m_session] logout];
     //gAppDelegate.isLogged = NO;
#endif
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btn_AddAction:(id)sender	
{
	self.m_intCurrentUserIndex=[sender tag];
	[self sendAddFriendRequest];
}

	
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


-(IBAction)btnbackAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}
	

#pragma mark FbGraph Callback Function
//- (void)setclosebutton {
//	[self.fbGraph.webView removeFromSuperview];	
//	//[facebookView removeFromSuperview];
//	[btnFbclose setSelected:NO];
//}

#pragma mark -
#pragma mark facebook

#ifdef FB_OLD_SDK_USAGE
-(void)loginButtonTapped
{
		// If we're not logged in, log in first...
    if (![m_session isConnected]) 
    {
			//self.m_loginDialog = nil;
			m_loginDialog = [[FBLoginDialog alloc] init];	
			[m_loginDialog show];	
		}
		// If we have a session and a name, post to the wall!
		else if (facebookName != nil) {
			//[self postToWall];
		}
		// Otherwise, we don't have a name yet, just wait for that to come through.
}
#endif



#pragma mark FBSessionDelegate

	
- (void)fbDidLogin  
{	
     NSLog(@"fbDidLogin()");
    tnApplication.isLoginFacebook = true;
    [tnApplication storeOAuthData];
    
    [self performSelector:@selector(getMeFriendsButtonPressed:)];
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator startLoadingView:self];
}


- (void)fbDidLogout 
{
    NSLog(@"fbDidLogout()");
    tnApplication.isLoginFacebook = false;
    [tnApplication removeOAuthData];
    facebookName = nil;
}

-(void)fbDidNotLogin:(BOOL)cancelled 
{
    NSLog(@"fbDidNotLogin()");
}


- (void)fbDidExtendToken:(NSString*)accessToken expiresAt:(NSDate*)expiresAt
{
     NSLog(@"fbDidExtendToken()");
    [tnApplication storeOAuthData];
}

- (void)fbSessionInvalidated {
    NSLog(@"fbSessionInvalidated()");
}


#pragma mark FBDialogDelegate

- (void)dialogCompleteWithUrl:(NSURL *)url {
    
    if (![url query]) {
        NSLog(@"User canceled dialog or there was an error");
        return;
}

    NSDictionary *params = [self parseURLParams:[url query]];
    //NSLog(@"Dialog Response: %@", params);
    
    switch (currentAPICall) {
        case kAPIPostOnFriendWall:
        {
            // Successful posts return a post_id
            if ([params valueForKey:@"post_id"]) {
                NSLog(@"Posted successfully.");
                [self showAlertView:nil alertMessage:@"Posted successfully."];
            }
            break;
        }
        case kAPISendInvitation:
{
            NSMutableArray *requestIDs = [[[NSMutableArray alloc] init] autorelease];
            for (NSString *paramKey in params) {
                if ([paramKey hasPrefix:@"request"]) {
                    [requestIDs addObject:[params objectForKey:paramKey]];
                }
            }
            if ([requestIDs count] > 0) {
                [self showAlertView:nil alertMessage:@"Sent request successfully."];
                NSLog(@"Request ID(s): %@", requestIDs);
            }
            break;
        }
    }
    
}


- (void)dialogDidCancel:(FBDialog*)dialog {
	NSLog(@"dialogDidCancel");
}


/**
 * Helper method to parse URL query parameters
 */
- (NSDictionary *)parseURLParams:(NSString *)query {
	NSArray *pairs = [query componentsSeparatedByString:@"&"];
	NSMutableDictionary *params = [[[NSMutableDictionary alloc] init] autorelease];
	for (NSString *pair in pairs) {
		NSArray *kv = [pair componentsSeparatedByString:@"="];
		NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
		[params setObject:val forKey:[kv objectAtIndex:0]];
	}
    return params;
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

#pragma mark Get Facebook Name Helper


#ifdef OLD_FB_SDK_USAGE

- (void)getFacebookName
{
	NSString* fql = [NSString stringWithFormat:@"SELECT is_app_user,name,pic_square,uid from user WHERE uid IN ( SELECT uid2 FROM friend WHERE uid1=%lld )",m_session.uid]; 
	//NSString* fql =@"SELECT is_app_user,name,pic_square,uid from user WHERE uid IN ( SELECT uid2 FROM friend WHERE uid1=100000181731610 )"; 
	//100000181731610
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
}
	
#else

- (void)getFacebookName 
{
    NSString* fql = [NSString stringWithFormat:@"SELECT is_app_user,name,pic_square,uid from user WHERE uid IN ( SELECT uid2 FROM friend WHERE uid1=%d )",0]; 
	
	NSMutableDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[tnApplication m_session] requestWithMethodName:@"fql.query"
                                     andParams:params
                                 andHttpMethod:@"POST"
                                   andDelegate:self];
}
#endif



#ifdef FB_OLD_SDK_USAGE

-(IBAction)getMeButtonPressed:(id)sender 
{
    
	FbGraphResponse *fb_graph_response = [fbGraph doGraphGet:@"me" withGetVars:nil];
	NSLog(@"getMeButtonPressed:  %@", fb_graph_response.htmlResponse);
	//NSString *strUid;
    //	strUid=[NSString stringWithFormat:@"listids=%@",[fb_graph_response.htmlResponse JSONFragment]];
    //	NSLog(@"%@",strUid);
	
}

-(IBAction)getMeFriendsButtonPressed:(id)sender {
	FbGraphResponse *fb_graph_response = [fbGraph doGraphGet:@"me/friends" withGetVars:nil];//friend_photos
	NSLog(@"getMeFriendsButtonPressed:  %@", fb_graph_response.htmlResponse);
	
	m_friendsDetails = [[[fb_graph_response.htmlResponse JSONValue] valueForKey:@"data"] retain];//[[NSMutableArray alloc] initWithArray:[fb_graph_response.htmlResponse JSONValue]];
	
	NSLog(@"facebook result----%@",m_friendsDetails);
	
	
	NSMutableArray	*tempArray=[[NSMutableArray alloc]init];
	
	for(int i=0;i<[m_friendsDetails count];i++)
	{
		[tempArray addObject:[NSString stringWithFormat:@"%@",[[m_friendsDetails objectAtIndex:i]valueForKey:@"id"]]];
	}
	
	self.m_strUids=[NSString stringWithFormat:@"listids=%@",[tempArray JSONFragment]];
	[tempArray release];
	tempArray=nil;
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	
    [l_request mapFaceBookID:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID facebookid:[NSString stringWithFormat:@"%lld",0]];
	
	[l_request release];
	l_request=nil;
	
}

#else

-(IBAction)getMeButtonPressed:(id)sender 
{
    currentAPICall = kAPIGetMe;
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"name,picture",  @"fields", nil];
    [[tnApplication m_session] requestWithGraphPath:@"me" andParams:params andDelegate:self];
}

-(IBAction)getMeFriendsButtonPressed:(id)sender {
    NSLog(@"getMeFriendsButtonPressed()");
    currentAPICall = kAPIGetFriends;
	[[tnApplication m_session] requestWithGraphPath:@"me/friends" andDelegate:self];
}
	
	
#endif
	
	
#pragma mark FBRequestDelegate

- (void)request:(FBRequest*)request didLoad:(id)result 
{
    //NSLog(@"facebook result----%@",result);
		
    if ([result isKindOfClass:[NSArray class]] && ([result count] > 0)) {
        result = [result objectAtIndex:0];
    }
	
    switch (currentAPICall) {
        case kAPIGetMe:
        {
            // Not in use
            break;
        }
        case kAPIGetFriends:
        {
            [[LoadingIndicatorView SharedInstance] stopLoadingView];
            NSLog(@"kAPIGetFriends : successful!!!");
            NSArray *friendsArray = [NSMutableArray arrayWithArray:[result objectForKey: @"data"]];
            //NSLog(@"facebook Friends----%@",friendsArray);
            NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            self.m_friendsDetails = [NSMutableArray arrayWithArray:[friendsArray sortedArrayUsingDescriptors:sortDescriptors]];
	
            if ([m_friendsDetails count] > 0) {
                NSMutableArray	*tempArray=[[NSMutableArray alloc]init];
                
                for(int i=0;i<[m_friendsDetails count];i++)
                {
                    [tempArray addObject:[NSString stringWithFormat:@"%@",[[m_friendsDetails objectAtIndex:i]valueForKey:@"id"]]];
                }
	
                self.m_strUids=[NSString stringWithFormat:@"listids=%@",[tempArray JSONFragment]];
                [tempArray release];
                tempArray=nil;
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *customerID= [prefs objectForKey:@"userName"];
                l_request=[[ShopbeeAPIs alloc] init];

                [l_request mapFaceBookID:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID facebookid:@"me"];

                [l_request release];
                l_request=nil;
            }
	
            break;
        }
        default:
            break;
    }
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    [self showAlertView:@"Tiny News" alertMessage:[[error userInfo] objectForKey:@"error_msg"]];
}
	
	
	

#pragma mark -
#pragma mark  callback methods

-(void)sendAddFriendRequest
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	m_view.hidden=NO;
	m_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
	m_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
	
	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140,200 , 30, 30)];
	
	[m_view addSubview:m_indicatorView];
	[self.view addSubview:m_view];
	[m_indicatorView startAnimating];
	
	l_request=[[ShopbeeAPIs alloc] init];
	
	NSMutableString *tempString;
	
	if (m_intCurrentUserIndex>=0)
	{
		tempString= [NSMutableString stringWithFormat:@"[\"%@\"]",[[m_arrFrnds objectAtIndex:m_intCurrentUserIndex] valueForKey:@"emailId"]];
	
		NSLog(@"%@",tempString);
	}
	else if(m_intCurrentUserIndex==-1)
	{
		NSMutableArray *tempArray=[[NSMutableArray alloc]init];
		for (int i=0; i<m_arrFrnds.count; i++)
		{
			[tempArray addObject:[[m_arrFrnds objectAtIndex:i]valueForKey:@"emailId"]];
		}
		tempString=[NSString stringWithString:[tempArray JSONRepresentation]];
		[tempArray release];
		NSLog(@"tempString: %@",tempString);			
			
	}
	
	[l_request sendRequestToAddFriend:@selector(requestCallBackMethodForAddFriend:responseData:) tempTarget:self custId:customerID tmpUids:tempString];//[m_shopBeeArray objectAtIndex:[sender tag]]
	
	[l_request release];
	l_request=nil;
	
}

-(void)requestCallBackMethodForListIds:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	int  temp_responseCode=[responseCode intValue];
	//NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];

	//[m_indicatorView stopAnimating];
//	[m_view removeFromSuperview];
//	[m_view release];
//	m_view=nil;
//	[m_indicatorView release];
//	m_indicatorView=nil;
	
	if(temp_responseCode==200)
	{
		NSString *str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding]autorelease];
		
		SBJSON *objJson=[SBJSON new];
		
		NSMutableArray *response=[objJson objectWithString:str];
		
		NSMutableArray	*tempArray_Uids=[[NSMutableArray alloc]init];
		
		for(int i=0;i<[response count];i++)
		{
			[tempArray_Uids addObject:[[response objectAtIndex:i]valueForKey:@"shopbee"]];
		}
		
		NSLog(@"FashionGram areeeeeeeeeeee>>>%@",tempArray_Uids);
		NSArray *arr = [tempArray_Uids objectAtIndex:0];
		NSString *tempString;
		
		BOOL tempStatus;
		tempStatus=FALSE;
		
		for(int j=0;j<[m_friendsDetails count];j++)
		{
			for(int i=0;i<[arr	count];i++)
			{	
				if([[[m_friendsDetails objectAtIndex:j] objectForKey:@"id"] isEqualToString:[[arr objectAtIndex:i] valueForKey:@"facebook"]])
				{
					tempString=[NSString stringWithString:[[arr objectAtIndex:i] valueForKey:@"email"]];
					tempStatus=TRUE;
					break;
					
				}
				else
				{
					tempStatus=FALSE;
					
				}
				
			}
			
			if (tempStatus==TRUE)
			{
				
				NSMutableDictionary *tempDict=[NSMutableDictionary dictionaryWithDictionary:[m_friendsDetails objectAtIndex:j]];
				[tempDict setValue:tempString forKey:@"emailId"];
				[m_shopBeeArray addObject:tempDict];
				//[m_picUrlShopBee addObject:[m_shopBeeArray valueForKey:@"pic_square"]];
				tempStatus=FALSE;
			}
			else 
			{
				[m_nonshopBeeArray addObject:[m_friendsDetails objectAtIndex:j]];
				//[m_picUrlNonShopBee addObject:[m_nonshopBeeArray valueForKey:@"pic_square"]];
			
			}
		}
		[[LoadingIndicatorView SharedInstance] stopLoadingView];

		[self load_tableViewDataSource];
				
	}
	else  {
        NSLog(@"ShopbeeFriendsViewController: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
	}

}


-(void)requestCallBackMethodForAddFriend:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	//NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	[m_indicatorView stopAnimating];
	[m_view removeFromSuperview];
	[m_view release];
	m_view=nil;
	[m_indicatorView release];
	m_indicatorView=nil;
		
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
    
	if (temp_responseCode==500)
	{
		NSLog(@"responseCode is 500");
		
        [tempLoadingIndicator stopLoadingView];
		[self showAlertView:@"Error!" alertMessage:@"Unable to add new friend. This user may be already in your friends list."];
		
		if (m_intCurrentUserIndex>=0 && m_arrFrnds.count>0) {
			[m_arrFrnds removeObjectAtIndex:m_intCurrentUserIndex];
			[self.m_tableView reloadData];
		}
		
		
	}
	else if(temp_responseCode==200)
	{
		NSLog(@"responseCode is 200");
						
		if (m_intCurrentUserIndex>=0 && m_arrFrnds.count>0)
		{
            
            [tempLoadingIndicator stopLoadingView];
			[self showAlertView:@"Friend request sent!" alertMessage:@"A new friend request has been sent."];
			[m_arrFrnds removeObjectAtIndex:m_intCurrentUserIndex];
			[self.m_tableView reloadData];
		}
		else if(m_intCurrentUserIndex==-1)
		{
            [tempLoadingIndicator stopLoadingView];
			[self showAlertView:@"Successful!" alertMessage:@"All friends have been sent friend request."];
			[m_arrFrnds removeAllObjects];
			[self.m_tableView reloadData];
		}
				
	}
	else {
        NSLog(@"ShopbeeFriendsViewController: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }

	

}

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	int  temp_responseCode=[responseCode intValue];
	
	m_view.hidden=YES;
	[m_indicatorView stopAnimating];
		
	if (temp_responseCode==500 || temp_responseCode==200)
	{
		m_view.hidden=NO;
		[m_indicatorView startAnimating];
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		l_request=[[ShopbeeAPIs alloc] init];
		
		[l_request getRegisteredFaceBookUsers:@selector(requestCallBackMethodForListIds:responseData:) tempTarget:self customerid:customerID listids:m_strUids];
		
		[l_request release];
		l_request=nil;
		
	}
	else {
        NSLog(@"ShopbeeFriendsViewController: %d", temp_responseCode);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	
}




#pragma mark -
#pragma mark  show alert message
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage
{
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];

	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

/*
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0)
	{
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator stopLoadingView];

	}
}
 */


#pragma mark -
#pragma mark  table view methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section                              
{
	if(section==0)
	{
		return [m_arrFrnds count];    
	}
	else
	{
		return [m_arrNonFrnds count];
	}
                                                                                                                      
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell %@",indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: SimpleTableIdentifier];
	
	if (cell == nil) 
	{ 
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
		
		cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow.png"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		CGRect frame;
		frame.size.width=30; frame.size.height=30;
		frame.origin.x=5; frame.origin.y=10;
		
		AsyncImageView* asyncImage = [[[AsyncImageView alloc]
									   initWithFrame:frame] autorelease];
		asyncImage.tag = 999;
		[cell.contentView addSubview:asyncImage];
		
		UILabel *temp_lblFriendName=[[UILabel alloc]init];
		temp_lblFriendName.frame=CGRectMake(45,6,185,25);
		temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
		temp_lblFriendName.tag=102;
		temp_lblFriendName.backgroundColor=[UIColor clearColor];
		//temp_lblFriendName.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
        temp_lblFriendName.textColor=[UIColor blackColor];
		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
		
	}
	else {
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		UIImageView *tempImageview=(UIImageView *)[asyncImage viewWithTag:101];
		[tempImageview setImage:nil];
		
		[(UILabel *)[cell.contentView viewWithTag:102] setText:@""];
	}
		
 if(indexPath.section==0)
 {		
	
	[(UILabel *)[cell.contentView viewWithTag:102] setText:
	 [NSString stringWithFormat:@"%@",[[m_arrFrnds objectAtIndex:indexPath.row] objectForKey:@"name"]]];
	
		 
	UIButton *temp_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
	[temp_addButton setImage:[UIImage imageNamed:@"add_in_add_friend_button.png"] forState:UIControlStateNormal];
	temp_addButton.tag=indexPath.row;
	[temp_addButton addTarget: self action:@selector(btn_AddAction:) forControlEvents:UIControlEventTouchUpInside];
	temp_addButton.frame=CGRectMake(250,13,42,23);
	[cell.contentView addSubview:temp_addButton];
		
	 NSDictionary *tempDict =[m_arrFrnds objectAtIndex:indexPath.row];  
	 NSURL *loadingUrl = [NSURL URLWithString:
						  [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",[tempDict valueForKey:@"id"]]];
						  //[tempDict valueForKey:@"pic_square"]];
	 
	 AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
	 [asyncImage loadImageFromURL:loadingUrl];
 }
 else
 {
     
	[(UILabel *)[cell.contentView viewWithTag:102] setText:
		[NSString stringWithFormat:@"%@",[[m_arrNonFrnds objectAtIndex:indexPath.row] objectForKey:@"name"]]];
	 
	 UIButton *temp_inviteButton=[UIButton buttonWithType:UIButtonTypeCustom];
	 [temp_inviteButton setImage:[UIImage imageNamed:@"btn_invite.png"] forState:UIControlStateNormal];
	 [temp_inviteButton addTarget: self action:@selector(btnInviteAction:) forControlEvents:UIControlEventTouchUpInside];
	 temp_inviteButton.frame=CGRectMake(250,12,54,25);
	 temp_inviteButton.tag=indexPath.row;
	 [cell.contentView addSubview:temp_inviteButton];
	 
	 NSDictionary *tempDict =[m_arrNonFrnds objectAtIndex:indexPath.row];  
	 NSURL *loadingUrl =  [NSURL URLWithString:
						   [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",[tempDict valueForKey:@"id"]]];
						  //[tempDict valueForKey:@"pic_square"]];

	 AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
	[asyncImage loadImageFromURL:loadingUrl];
}
	
	
return cell;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
		
	if (section==0) {
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, SectionHeaderHeight)];
		//tmp_view.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
        UIColor *color=[UIColor colorWithPatternImage:[UIImage imageNamed:@"friends-bgrd-bar.png"]];
        tmp_view.backgroundColor=color;
		[tmp_view autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(30,2, 260, 25)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:16];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%d friends are on Tiny News", m_arrFrnds.count];
		//tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];;
        tmp_headerLabel.textColor=[UIColor blackColor];
        tmp_headerLabel.backgroundColor=[UIColor clearColor];
		//tmp_headerLabel.textColor=[UIColor whiteColor];
		[tmp_view addSubview:tmp_headerLabel];
		
		
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_view.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"add_all.png"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnAddAllAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(240,4,50,23);
		[tmp_view addSubview:temp_addAll];
		[temp_addAll release];//18aug
		temp_addAll = nil;
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return tmp_view;
	}
	else {
		
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
		//tmp_headerForSecondSection.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
        UIColor *color=[UIColor colorWithPatternImage:[UIImage imageNamed:@"friends-bgrd-bar.png"]];
        tmp_headerForSecondSection.backgroundColor=color;
		[tmp_headerForSecondSection autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(30,2, 260, 25)] ;
		[tmp_headerLabel autorelease];
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:16 ];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%d friends not on Tiny News",m_arrNonFrnds.count];
		//tmp_headerLabel.textColor=[UIColor whiteColor];
		//tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
        tmp_headerLabel.textColor=[UIColor blackColor];
        tmp_headerLabel.backgroundColor=[UIColor clearColor];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
        
						
		return tmp_headerForSecondSection;
		
		
	}
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 
	
	if (section==0 && m_arrFrnds.count>0)
		return 30;
	else if(section==0 && m_arrFrnds.count==0)
		return 0;
	if (section==1 && m_arrNonFrnds.count>0)
		return 30;
	else if(section==1 && m_arrNonFrnds.count==0)
		return 0;
	return 0;
	
	
} 

#pragma mark -
#pragma mark Icon Downloader Methods

- (void)startIconDownload:(NSDictionary *)temp_dict forIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < m_arrNonFrnds.count)
	{
		ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
		iconDownloader.id_ = indexPath;
		
		[iconDownloader openConnection:[temp_dict valueForKey:@"pic_square"] withOtherUrlString:[temp_dict valueForKey:@"pic_square"]  withCallBackTarget:indexPath withDelegate:self];
	}
}

-(void) imageDownloadFinish:(UIImage*)image_ withId:(NSIndexPath*)id_
{
	if (id_.section == 0) 
	{																	
		//save the downloaded image in temp cache
				
		UITableViewCell *cell = [self.m_tableView cellForRowAtIndexPath:id_];
		NSLog(@"row number: %d",id_.row);
		
		// Display the newly loaded image
		if ((UIImageView*)[cell.contentView viewWithTag:5000])
		{
			[(UIImageView*)[cell.contentView viewWithTag:5000] setImage: image_];
		}
		
		
		NSMutableDictionary *tempDict=[[NSMutableDictionary alloc] initWithDictionary: [m_arrNonFrnds objectAtIndex:id_.row]];  //.............set image//
		[tempDict setObject:image_ forKey:@"image"];
		
		[m_friendsDetails replaceObjectAtIndex:id_.row withObject:tempDict];
		[tempDict release];
		
		[self.m_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:id_] withRowAnimation:UITableViewRowAnimationNone];
	}
	else 
	{
		NSLog(@"else block");
	}
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows
{
	if([m_arrNonFrnds count] > 0) 
	{
		NSArray *visiblePaths = [self.m_tableView	indexPathsForVisibleRows];
		for (NSIndexPath *indexPath in visiblePaths)
		{
			if(indexPath.row < m_arrNonFrnds.count)
			{
				NSDictionary *temp_dict  = [m_arrNonFrnds objectAtIndex:indexPath.row];
				[self startIconDownload:temp_dict forIndexPath:indexPath];
			}
		}
	}
}


// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
      //  [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

}

#pragma mark -
#pragma mark search bar methods
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
	[m_searchBar resignFirstResponder];
	[m_searchBar setShowsCancelButton:NO animated:YES];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)lclSearchBar
{
	self.m_arrNonFrnds=m_nonshopBeeArray;
	self.m_arrFrnds=m_shopBeeArray;
	NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",m_searchBar.text];
	NSArray *filteredNameList = [m_arrNonFrnds filteredArrayUsingPredicate:bPredicate];
	NSArray *filterednameList1=[m_arrFrnds filteredArrayUsingPredicate:bPredicate];
	NSLog(@"ARRAY=%@",filteredNameList);
	self.m_arrNonFrnds= filteredNameList;
	self.m_arrFrnds= [NSMutableArray arrayWithArray:filterednameList1];
	[self.m_tableView reloadData];
	[m_searchBar resignFirstResponder];
	[m_searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if(m_searchBar.text.length==0)
	{
		[self performSelector:@selector(load_tableViewDataSource)];
		
		[self.m_tableView reloadData];
		
		[m_searchBar resignFirstResponder];
	}
	[m_searchBar setShowsCancelButton:YES animated:YES];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
	[m_searchBar setShowsCancelButton:YES animated:YES];
	
	return YES;
	
}



#pragma mark -

-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated{
	
	//NSString *client_id = FacebookApiKey;
//
//	fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
//	
//	[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:) 
//						 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
//	
}

- (void)fbGraphCallback:(id)sender {
	
	if ( (fbGraph.accessToken == nil) || ([fbGraph.accessToken length] == 0) ) {
		
		NSLog(@"You pressed the 'cancel' or 'Dont Allow' button, you are NOT logged into Facebook...I require you to be logged in & approve access before you can do anything useful....");
		
		//restart the authentication process.....
		[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:) 
							 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
		//UIButton *btnFbclose;
		//btnFbclose = [UIButton buttonWithType:UIButtonTypeCustom];
//		[btnFbclose setFrame:CGRectMake(50, 50, 50, 50)];
//		[btnFbclose setBackgroundImage:[UIImage imageNamed:@"closeFBbtn.png"] forState:UIControlStateNormal];
//		[btnFbclose setBackgroundImage:[UIImage imageNamed:@"closeFBbtn.png"] forState:UIControlStateSelected];
//		//[btnFbclose setTag:1];
//		[btnFbclose addTarget:self action:@selector(setclosebutton) forControlEvents:UIControlEventTouchUpInside];
//		[fbGraph.webView addSubview:btnFbclose];
		//[self.view addSubview:fbGraph.webView]; 
		
		
	} else {
		//pop a message letting them know most of the info will be dumped in the log
		//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Note" message:@"For the simplest code, I've written all output to the 'Debugger Console'." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//		[alert show];
//		[alert release];
//		
		
		[self performSelector:@selector(getMeFriendsButtonPressed:)];
		//[self performSelector:@selector(getMeButtonPressed:)];
		
		NSLog(@"------------>CONGRATULATIONS<------------, You're logged into Facebook...  Your oAuth token is:  %@", fbGraph.accessToken);
		
	}
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];

	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];

    // Release retained IBOutlets.
    self.m_searchBar = nil;
    self.m_imgLogo = nil;
    self.m_tableView = nil;
    self.table_AddFriends = nil;
}

- (void)dealloc {
    [fbGraph release];
    [m_tableView release];
    [facebookName release];
    //[m_session release];
    [m_userDetails release];
    [myList release];
    [m_friendsDetails release];
    [m_imgLogo release];
    [m_searchBar release];
    [m_arrNonFrnds release];
    [m_arrFrnds release];
    [m_activityInidicator release];
    [m_strUids release];
    [m_view release];
    [m_shopBeeArray release];
    [m_nonshopBeeArray release];
    [m_picUrlShopBee release];
	[m_picUrlNonShopBee release];
#ifdef OLD_FB_SDK
	[m_loginDialog release];
#endif
	[btnFbclose release];
    
    [super dealloc];
}


@end
