//
//  BuzzButton ShareNowFunctionality.h
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
//#import "TwitterProcessing.h"
#import "JSON.h"


#import"SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "GAITrackedViewController.h"

#define CONTACT_ENTRY_SELECT_TAG 20011
#define CONTACT_ENTRY_NAME_TAG 20012
#define CONTACT_ENTRY_EMAIL_TAG 20013

#define SHARENOW_REQUEST_TYPE_ADDCONTACT 1111
#define SHARENOW_REQUEST_TYPE_DELCONTACT 2222
#define SHARENOW_REQUEST_TYPE_GETCONTACT 3333
#define SHARENOW_REQUEST_TYPE_SHARENOW	 4444

@class BuzzButtonAddFromAddressBook;
@interface BuzzButtonShareNowFunctionality : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,
		FBSessionDelegate, FBRequestDelegate, FBDialogDelegate, SA_OAuthTwitterControllerDelegate, SA_OAuthTwitterEngineDelegate> {
	
	NSString *  m_strType;
	NSString *  m_strMessage;
    NSString *m_whoMessage;
    NSString *m_whatMessage;
    NSString *m_whenMessage;
    NSString *m_whereMessage;
    NSString *m_howMessage;
    NSString *m_whyMessage;
    NSString *m_openMessage;
    NSString *m_headerMessage;
	IBOutlet UITableView *m_emailTableView;
	IBOutlet UIButton *m_facebookButton;
	IBOutlet UIButton *m_twitterButton;
	IBOutlet UIButton *m_selectAllButton;
	IBOutlet UIButton *m_selectEmailButton;
	IBOutlet UIView *m_sectionHeaderView;
	IBOutlet UIImageView *m_photoView;
    IBOutlet UIView *m_emailsView;
    IBOutlet UIView *m_shareNowView;
	UIImage * photoImage;
	
	NSMutableDictionary *m_DictOfContactDicts;
	NSMutableDictionary *m_newlyAddedContactsDict;
	NSMutableDictionary *m_deletedContactsDict;

	BuzzButtonAddFromAddressBook * addrBookVc;
	
	//TwitterProcessing * twitterProcessingVc;
	
    int currentAPICall;
	NSMutableData *m_mutResponseData;
	int m_intResponseCode;
	int m_intRequestType;
	NSString * m_photoUrl;
    BOOL shareVideo;
	SA_OAuthTwitterEngine *twitterEngine;
    NSString *buzzid;
    BOOL isPublishing;
}

@property (nonatomic,assign) SA_OAuthTwitterEngine * twitterEngine;
@property (nonatomic,copy) NSString *buzzid;


@property (nonatomic,assign) int m_intRequestType;
@property (nonatomic,assign) int m_intResponseCode;
@property (nonatomic,retain) NSString *m_photoUrl;
@property (nonatomic,retain) NSMutableData *m_mutResponseData;
@property (nonatomic,retain) NSString *m_strType;
@property (nonatomic,retain) NSString *m_strMessage;
@property(nonatomic,retain) NSString *m_whoMessage;
@property(nonatomic,retain)NSString *m_whatMessage;
@property(nonatomic,retain)NSString *m_whenMessage;
@property(nonatomic,retain)NSString *m_whereMessage;
@property(nonatomic,retain)NSString *m_howMessage;
@property(nonatomic,retain)NSString *m_whyMessage;
@property(nonatomic,retain)NSString *m_openMessage;
@property(nonatomic,retain)NSString *m_headerMessage;
//@property (nonatomic,retain) TwitterProcessing *twitterProcessingVc;
@property (nonatomic,retain) IBOutlet UITableView *m_emailTableView;
@property (nonatomic,retain) IBOutlet UIButton *m_facebookButton;
@property (nonatomic,retain) IBOutlet UIButton *m_twitterButton;
@property (nonatomic,retain) IBOutlet UIButton *m_selectAllButton;
@property (nonatomic,retain) IBOutlet UIButton *m_selectEmailButton;
@property (nonatomic,retain) IBOutlet UIView *m_sectionHeaderView;
@property (nonatomic,retain) IBOutlet UIImageView *m_photoView;
@property (nonatomic,retain) IBOutlet UIView *m_shareNowView;
@property (nonatomic,retain) IBOutlet UIView *m_emailsView;
@property (nonatomic,retain) UIImage *photoImage;
@property (nonatomic,retain) NSMutableDictionary *m_DictOfContactDicts;
@property (nonatomic,retain) NSMutableDictionary *m_newlyAddedContactsDict;
@property (nonatomic,retain) NSMutableDictionary *m_deletedContactsDict;
@property (nonatomic,retain) BuzzButtonAddFromAddressBook *addrBookVc;
@property BOOL shareVideo;

-(IBAction)goToBackView;
-(IBAction) contactsSelectAllToggleAction:(id) sender;
-(IBAction) facebookToggleAction:(id) sender;
-(IBAction) twitterToggleAction:(id) sender;
-(IBAction) contactSelectToggleAction:(id) sender;

-(IBAction) chooseEmail:(id) sender;
-(IBAction) backToShare:(id) sender;
-(IBAction) doShareNow:(id) sender;
-(IBAction) addNewEmailAction:(id) sender;
- (void) problemWithSendingVideo;
- (void) showFBInterface:(NSDictionary *)dict;

- (NSDictionary *)parseURLParams:(NSString *)query ;

@end


