//
//  MyFriendViewController.h
//  TinyNews
//
//  Created by jay kumar on 5/25/13.
//
//

#import <UIKit/UIKit.h>
#import "UIGridView.h"
#import "UIGridViewDelegate.h"

@interface MyFriendViewController : UIViewController<UIGridViewDelegate>
{
    NSArray *m_friendsArray;
    IBOutlet UIImageView *feedBackPage;//visible when no search records found;//**
    UIView *footerView;
   UIImage *m_image;
}
@property(nonatomic, retain) UIImage *m_image;
@property(nonatomic,retain)UIView *footerView;
@property (nonatomic, retain) IBOutlet UIGridView *table;
@property (nonatomic,retain) NSArray *m_friendsArray;
-(IBAction)backAction:(id)sender;
-(IBAction)moreclicked:(id)sender;
@end
