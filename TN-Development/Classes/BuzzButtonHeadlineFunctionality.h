//
//  BuzzButtonHeadlineFunctionality.h
//  TinyNews
//
//  Created by jay kumar on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuzzButtoncameraFunctionality.h"
#import "GAITrackedViewController.h"

@interface BuzzButtonHeadlineFunctionality : GAITrackedViewController <UITextViewDelegate, UITextFieldDelegate>
{
    UIImageView *m_headerimageView;
    UIImageView *m_imageView;
    UIImage *m_photoimage;
    UILabel *m_texteditingLBL;
    UILabel *m_balancetextLBL;
    UITextView *m_headingTextField;
    UITextView *m_opEdView;
    UILabel *m_headrLBL;
    NSString *m_strType;
    NSString *m_strMessage;
    UIButton *nextButton;
    UIButton *opEdButton;
    UIButton *selectCategoryButton;
    UIView *editHeadlineView;
    UIView *editStoryView;
    UIView *editOpEdView;
    UITextView *m_whotext;
    UITextView *m_whattext;
    UITextView *m_whentext;
    UITextView *m_wheretext;
    UITextView *m_howtext;
    UITextView *m_whytext;
    UITextView *currentTextField;
    UILabel *m_whoLBL;
    UILabel *m_whatLBL;
    UILabel *m_whenLBL;
    UILabel *m_whereLBL;
    UILabel *m_howLBL;
    UILabel *m_whyLBL;
    UIScrollView *scrollView;
    UILabel *placeLabel;
    BOOL    shouldShowKeyboard;
    BOOL    opEdShown;
    BOOL    openFalg;
    NSString *selectedType;
    NSMutableDictionary *metadata;
    IBOutlet UIButton *m_writestoryBTN;
    IBOutlet UIButton *m_writeopenBTN;
    IBOutlet UILabel *m_opentextcountLBL;
    int headlineCount;
    int openWriteCount;
    
    int heightfortextView;
    int heightforscroolView;
    UIScrollView *tempScroll;
    IBOutlet UILabel *m_headlinePlaceHolder;
    IBOutlet UILabel *m_whoPlaceholder;
    IBOutlet UILabel *m_whatPlaceholder;
    IBOutlet UILabel *m_whenPlaceholder;
    IBOutlet UILabel *m_wherePlaceholder;
    IBOutlet UILabel *m_howPlaceholder;
    IBOutlet UILabel *m_whyPlaceholder;
    IBOutlet UILabel *m_writeToOpenLBL;
    
    BOOL shareVideo;
}
@property(nonatomic,retain)IBOutlet UILabel *m_headlinePlaceHolder;
@property(nonatomic,retain)IBOutlet UILabel *m_whoPlaceholder;
@property(nonatomic,retain)IBOutlet UILabel *m_whatPlaceholder;
@property(nonatomic,retain)IBOutlet UILabel *m_whenPlaceholder;
@property(nonatomic,retain)IBOutlet UILabel *m_wherePlaceholder;
@property(nonatomic,retain)IBOutlet UILabel *m_howPlaceholder;
@property(nonatomic,retain)IBOutlet UILabel *m_whyPlaceholder;
@property(nonatomic,retain)IBOutlet UILabel *m_writeToOpenLBL;
@property(nonatomic,retain)UIScrollView *tempScroll;
@property(nonatomic,retain)IBOutlet UILabel *m_opentextcountLBL;
@property(nonatomic,retain)IBOutlet UIButton *m_writestoryBTN;
@property(nonatomic,retain)IBOutlet UIButton *m_writeopenBTN;
@property(nonatomic,retain)NSString *m_strType;
@property(nonatomic,retain)NSString *m_strMessage;
@property(nonatomic,retain)IBOutlet UIImageView *m_headerimageView;
@property(nonatomic,retain)IBOutlet UIImageView *m_imageView;
@property(nonatomic,retain)UIImage  *m_photoimage;
@property(nonatomic,retain)IBOutlet UILabel *m_texteditingLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_balancetextLBL;
@property(nonatomic,retain)IBOutlet UITextView *m_headingTextField;
@property(nonatomic,retain)IBOutlet UITextView *m_opEdView;
@property(nonatomic,retain)IBOutlet UILabel *m_headrLBL;
@property(nonatomic,retain)IBOutlet UIButton *nextButton;
@property(nonatomic,retain)IBOutlet UIButton *opEdButton;
@property(nonatomic,retain)IBOutlet UIButton *selectCategoryButton;
@property(nonatomic,retain)IBOutlet UITextView *m_whotext;
@property(nonatomic,retain)IBOutlet UITextView *m_whattext;
@property(nonatomic,retain)IBOutlet UITextView *m_whentext;
@property(nonatomic,retain)IBOutlet UITextView *m_wheretext;
@property(nonatomic,retain)IBOutlet UITextView *m_howtext;
@property(nonatomic,retain)IBOutlet UITextView *m_whytext;
@property(nonatomic,retain)         UITextView *currentTextField;
@property(nonatomic,retain)IBOutlet UILabel *m_whoLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_whatLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_whenLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_whereLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_howLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_whyLBL;
@property(nonatomic,retain)IBOutlet UILabel *placeLabel;
@property(nonatomic,retain)IBOutlet UIView *editStoryView;
@property(nonatomic,retain)IBOutlet UIView *editOpEdView;
@property(nonatomic,retain)IBOutlet UIView *editHeadlineView;
@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;
@property(nonatomic,retain) NSMutableDictionary *metadata;
@property(nonatomic,copy) NSString *selectedType;
@property BOOL shareVideo;
//-(IBAction)clickToBack:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)nextClicked:(id)sender;

- (IBAction)selectCategory:(id)sender;
//- (IBAction)showOpEd:(id)sender;
- (void) saveSelectedCategory:(NSString *)type;
- (void) updateNextButton;
-(IBAction)writeReportStory:(id)sender;
@end
