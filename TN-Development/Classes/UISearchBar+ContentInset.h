//
//  UISearchBar+ContentInset.h
//  QNavigator
//
//  Created by Greg Landweber on 7/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

// GDL: We're using the setContentInset method on UISearchBar, so I define it here.
// GDL: This may get the app rejected from the App Store, or it may not.

@interface UISearchBar (ContentInset)
@property (nonatomic) UIEdgeInsets contentInset;
@end
