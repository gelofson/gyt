//
//  BuzzButtonHeadlineFunctionality.m
//  TinyNews
//
//  Created by jay kumar on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BuzzButtonHeadlineFunctionality.h"
#import "QNavigatorAppDelegate.h"
#import "BuzzButtonPhotoStoryFunctionality.h"
#import "ChooseCategories.h"
#import "BuzzButtonShareNowFunctionality.h"
#import "NSMutableDictionary+ImageMetadata.h"
#import "NewsDataManager.h"
#import "NSString+URLEncoding.h"

@interface BuzzButtonHeadlineFunctionality()
{
@private
    BOOL isObservingKeyboard;
}

- (void) startObservingKeyboard;
- (void) stopObservingKeyboard;
- (void) setAnimatedLayout;

@end

@implementation BuzzButtonHeadlineFunctionality

@synthesize m_imageView;
@synthesize m_photoimage;
@synthesize m_texteditingLBL;
@synthesize m_balancetextLBL;
@synthesize m_headingTextField;
@synthesize m_headerimageView;
@synthesize m_headrLBL;
@synthesize m_strType;
@synthesize m_strMessage;
@synthesize nextButton;
@synthesize m_howtext;
@synthesize m_whattext;
@synthesize m_whentext;
@synthesize m_wheretext;
@synthesize m_whotext;
@synthesize m_whytext;
@synthesize m_whoLBL;
@synthesize m_whatLBL;
@synthesize m_whenLBL;
@synthesize m_whereLBL;
@synthesize m_howLBL;
@synthesize m_whyLBL;
@synthesize editStoryView;
@synthesize editOpEdView;
@synthesize editHeadlineView;
@synthesize opEdButton;
@synthesize scrollView;
@synthesize currentTextField;
@synthesize m_opEdView;
@synthesize selectedType;
@synthesize selectCategoryButton;
@synthesize placeLabel;
@synthesize metadata;
@synthesize m_writeopenBTN;
@synthesize m_writestoryBTN;
@synthesize m_opentextcountLBL;
@synthesize tempScroll;
@synthesize m_headlinePlaceHolder;
@synthesize m_whoPlaceholder;
@synthesize m_whatPlaceholder;
@synthesize m_whenPlaceholder;
@synthesize m_wherePlaceholder;
@synthesize m_howPlaceholder;
@synthesize m_whyPlaceholder;
@synthesize m_writeToOpenLBL;
@synthesize shareVideo;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.trackedViewName = @"Enter News Story";
   // [tnApplication.tabBarController setHidesBottomBarWhenPushed:NO];
    [self.m_imageView setImage:self.m_photoimage];
    editHeadlineView.layer.borderWidth = 1;
    editHeadlineView.layer.borderColor = [UIColor grayColor].CGColor;
    editHeadlineView.layer.cornerRadius = 6;
    
    editStoryView.layer.borderWidth = 1;
    editStoryView.layer.borderColor = [UIColor grayColor].CGColor;
    editStoryView.layer.cornerRadius = 6;
    editOpEdView.layer.borderWidth = 1;
    editOpEdView.layer.borderColor = [UIColor grayColor].CGColor;
    editOpEdView.layer.cornerRadius = 6;
    openFalg=NO;
    m_whotext.layer.borderWidth=1;
    m_whotext.layer.borderColor=[UIColor grayColor].CGColor;
    m_whotext.layer.cornerRadius=5;
    
    m_whattext.layer.borderWidth=1;
    m_whattext.layer.borderColor=[UIColor grayColor].CGColor;
    m_whattext.layer.cornerRadius=5;
    
    m_whentext.layer.borderWidth=1;
    m_whentext.layer.borderColor=[UIColor grayColor].CGColor;
    m_whentext.layer.cornerRadius=5;
    
    m_wheretext.layer.borderWidth=1;
    m_wheretext.layer.borderColor=[UIColor grayColor].CGColor;
    m_wheretext.layer.cornerRadius=5;
    
    m_howtext.layer.borderWidth=1;
    m_howtext.layer.borderColor=[UIColor grayColor].CGColor;
    m_howtext.layer.cornerRadius=5;
    
    m_whytext.layer.borderWidth=1;
    m_whytext.layer.borderColor=[UIColor grayColor].CGColor;
    m_whytext.layer.cornerRadius=5;
    
    m_headingTextField.textColor=[UIColor lightGrayColor];
    m_opEdView.textColor=[UIColor lightGrayColor];
    m_whotext.textColor=[UIColor lightGrayColor];
    m_whattext.textColor=[UIColor lightGrayColor];
    m_whentext.textColor=[UIColor lightGrayColor];
    m_wheretext.textColor=[UIColor lightGrayColor];
    m_howtext.textColor=[UIColor lightGrayColor];
    m_whytext.textColor=[UIColor lightGrayColor];
    // Do any additional setup after loading the view from its nib.
//    NSString * latString = [NSString stringWithFormat:@"%.2f", [tnApplication.m_objGetCurrentLocation m_latitude]];
//    
//    NSString * lngString = [NSString stringWithFormat:@"%.2f", [tnApplication.m_objGetCurrentLocation m_longitude]];
    
    heightforscroolView=21;
//    m_writestoryBTN.selected=YES;
//    opEdShown = YES;

    //self.tempScroll.contentSize=self.scrollView.contentSize;
    //placeLabel.text = [NSString stringWithFormat:@"%@ %@", latString, lngString];
    //[self.editOpEdView addSubview:m_opentextcountLBL];
}
     
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication]setStatusBarHidden:YES];
//    [self.tabBarController setHidesBottomBarWhenPushed:NO];
    self.scrollView.scrollEnabled = YES;
    //
    self.scrollView.contentSize=CGSizeMake(320, 920);
    [self writeReportStory:nil];
    self.tempScroll=[[[UIScrollView alloc]init] autorelease];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        [self startObservingKeyboard];
    }
    
    if ([self.metadata originalTime])
    {
        self.m_whenPlaceholder.hidden=YES;
        m_whentext.textColor=[UIColor blackColor];
        self.m_whentext.text = [self.metadata originalTime];
    } 
    else {
        NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
        [formatter setDateFormat:@"yyyy:MM:dd HH:mm:SS"];
        
        self.m_whentext.text = [formatter stringFromDate:[NSDate date]];
        //[self.m_whentext sizeToFit];
         self.m_whenPlaceholder.hidden=YES;
        m_whentext.textColor=[UIColor blackColor];
    }
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[tnApplication.m_objGetCurrentLocation m_latitude] longitude:[tnApplication.m_objGetCurrentLocation m_longitude]];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if ([placemarks count] > 0) 
        {
            CLPlacemark *address = [placemarks objectAtIndex:0];
            
            if ((address.thoroughfare!=nil) || (address.locality!=nil) || (address.country!=nil))
            {
                NSString *placemarkadd=[NSString stringWithFormat:@"%@, %@, %@", address.thoroughfare, address.locality, address.country];
                
                
                self.m_wheretext.text = placemarkadd;
                self.m_wherePlaceholder.hidden=YES;
                m_wheretext.textColor=[UIColor blackColor];
            }
           
           // CGSize fontSize=[placemarkadd sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
           // CGRect frame=[self.m_wheretext frame];
            
           //self.m_wheretext.frame=CGRectMake(frame.origin.x, frame.origin.y-5,frame.size.width , fontSize.height+15);
           
           
            
            //[self.m_wheretext sizeToFit];
        } else {
            NSString * latString = [NSString stringWithFormat:@"%.2f", [tnApplication.m_objGetCurrentLocation m_latitude]];
            NSString * lngString = [NSString stringWithFormat:@"%.2f", [tnApplication.m_objGetCurrentLocation m_longitude]];
            NSString *address=[NSString stringWithFormat:@"%@ %@", latString, lngString];

            self.m_wheretext.text = address;
            
            self.m_wherePlaceholder.hidden=YES;
            m_wheretext.textColor=[UIColor blackColor];
            
//            CGSize fontSize=[address sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
//            CGRect frame=[self.m_wheretext frame];
//            
//            self.m_wheretext.frame=CGRectMake(frame.origin.x, frame.origin.y-5,frame.size.width , fontSize.height+15);
                        
           
            //[self.m_wheretext sizeToFit];
        }
    }];
    
    [geocoder release];
    [location release];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopObservingKeyboard];
}

- (void) updateNextButton
{
    if ([self.m_headingTextField.text length] > 0 && self.m_headingTextField.textColor==[UIColor blackColor] && self.selectedType) {
        nextButton.enabled = YES;
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                        withAction:@"headline has value"
                                                         withLabel:nil
                                                         withValue:nil];
    } else {
        nextButton.enabled = NO;
    }
}

#pragma mark-
#pragma mark Text View delegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
   
    self.m_whotext.scrollEnabled=NO;
    self.m_whattext.scrollEnabled=NO;
    self.m_whentext.scrollEnabled=NO;
     self.m_wheretext.scrollEnabled=NO;
    self.m_howtext.scrollEnabled=NO;
    self.m_whytext.scrollEnabled=NO;    
    if(textView == m_headingTextField)
    {
        self.currentTextField = m_headingTextField;
        if (m_headingTextField.textColor==[UIColor lightGrayColor])
        {
            
            m_headingTextField.textColor=[UIColor blackColor];
            m_headlinePlaceHolder.hidden=YES;
        }
    
    }
    else if (textView ==m_whotext)
    {
        self.currentTextField=m_whotext;
        if (m_whotext.textColor==[UIColor lightGrayColor])
        {
            
            m_whotext.textColor=[UIColor blackColor];
            m_whoPlaceholder.hidden=YES;
        }
 
    }
    else if (textView ==m_whattext)
    {
        self.currentTextField=m_whattext;
        if (m_whattext.textColor==[UIColor lightGrayColor])
        {
            
            m_whattext.textColor=[UIColor blackColor];
            m_whatPlaceholder.hidden=YES;
        }
        
    }
    else if (textView ==m_whentext)
    {
        self.currentTextField=m_whentext;
        
        if (m_whentext.textColor==[UIColor lightGrayColor])
        {
            
            m_whentext.textColor=[UIColor blackColor];
            m_whenPlaceholder.hidden=YES;
        }
        
    }

    else if (textView ==m_wheretext)
    {
        self.currentTextField=m_wheretext;
        if (m_wheretext.textColor==[UIColor lightGrayColor])
        {
            
            m_wheretext.textColor=[UIColor blackColor];
            m_wherePlaceholder.hidden=YES;
        }
        
    }
    else if (textView ==m_howtext)
    {
        self.currentTextField=m_howtext;
        if (m_howtext.textColor==[UIColor lightGrayColor])
        {
            
            m_howtext.textColor=[UIColor blackColor];
            m_howPlaceholder.hidden=YES;
        }
        
    }
    else if (textView ==m_whytext)
    {
        self.currentTextField=m_whytext;
        if (m_whytext.textColor==[UIColor lightGrayColor])
        {
            
            m_whytext.textColor=[UIColor blackColor];
            m_whyPlaceholder.hidden=YES;
        }
        
    }
    else if(textView ==m_opEdView)
    {
        self.currentTextField=m_opEdView;
        if (m_opEdView.textColor==[UIColor lightGrayColor])
        {
            m_opEdView.textColor=[UIColor blackColor];
            m_writeToOpenLBL.hidden=YES;
        }
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    self.currentTextField = nil;
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if (textView==m_headingTextField)
    {
        m_headingTextField.textColor=[UIColor blackColor];
        m_headlinePlaceHolder.hidden=YES;
        
        headlineCount=[textView.text length];
        
        if(headlineCount <= 50 &&  m_headingTextField.textColor==[UIColor blackColor])
        {
            int i=headlineCount;
            m_texteditingLBL.text=[NSString stringWithFormat:@"%d/%d",i, 50 - i];
           
        }
        else if(headlineCount > 50 &&  m_headingTextField.textColor==[UIColor blackColor])
        {
            
            UIAlertView *alartView=[[UIAlertView alloc] initWithTitle:@"" message:@"Headline should not be more than 50 characters"
                                                             delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
            [alartView show];
            [alartView release];
            
        }
        if (m_headingTextField.text.length==0) 
        {
            int i=00;
            m_texteditingLBL.text=[NSString stringWithFormat:@"%d/%d",i, 50 - i];
            m_headingTextField.textColor=[UIColor lightGrayColor];
            m_headlinePlaceHolder.hidden=NO;
        }
        
    }
    else if (textView ==m_whotext)
    {
        if ([m_whotext.text length]==0)
        {
            
            m_whotext.textColor=[UIColor lightGrayColor];
            m_whoPlaceholder.hidden=NO;
        }
        else
        {
            m_whotext.textColor=[UIColor blackColor];
            m_whoPlaceholder.hidden=YES;
            CGSize fontSize=[m_whotext.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
            if (fontSize.height>=60)
            
            {
                
                CGPoint point=[self.scrollView contentOffset];
                point.y =point.y+75;
                [self.scrollView setContentOffset:point animated:YES];
                [self textViewShouldBeginEditing:m_whattext];
                [m_whattext setEditable:YES];
                //[self textViewShouldEndEditing:m_whotext];
                [m_whotext resignFirstResponder];
                [m_whattext becomeFirstResponder];
                return;
            }
        }
    }
    else if (textView ==m_whattext)
    {
        if ([m_whattext.text length]==0)
        {
            
            m_whattext.textColor=[UIColor lightGrayColor];
            m_whatPlaceholder.hidden=NO;
        }
        else
        {
            m_whattext.textColor=[UIColor blackColor];
            m_whatPlaceholder.hidden=YES;
            
            CGSize fontSize=[m_whattext.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
            if (fontSize.height>=60)
            {
                
                CGPoint point=[self.scrollView contentOffset];
                point.y =point.y+75;
                [self.scrollView setContentOffset:point animated:YES];
                //[self textViewShouldBeginEditing:m_howtext];
                [m_whentext setEditable:YES];
                //[self textViewShouldEndEditing:m_whattext];
                [m_whattext resignFirstResponder];
               // [m_howtext becomeFirstResponder];
                return;
            }

        }
    }

    else if (textView ==m_whentext)
    {
        if ([m_whentext.text length]==0)
        {
            
            m_whentext.textColor=[UIColor lightGrayColor];
            m_whenPlaceholder.hidden=NO;
        }
        else
        {
            m_whentext.textColor=[UIColor blackColor];
            m_whenPlaceholder.hidden=YES;
            
            CGSize fontSize=[m_whentext.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
            if (fontSize.height>=60)
            {
                
                CGPoint point=[self.scrollView contentOffset];
                point.y =point.y+75;
                [self.scrollView setContentOffset:point animated:YES];
               // [self textViewShouldBeginEditing:m_wheretext];
                [m_wheretext setEditable:YES];
               // [self textViewShouldEndEditing:m_whentext];
                [m_whentext resignFirstResponder];
                //[m_wheretext becomeFirstResponder];
                return;
            }

        }
    }

    else if (textView ==m_wheretext)
    {
        if ([m_wheretext.text length]==0)
        {
            
            m_wheretext.textColor=[UIColor lightGrayColor];
            m_wherePlaceholder.hidden=NO;
        }
        else
        {
            m_wheretext.textColor=[UIColor blackColor];
            m_wherePlaceholder.hidden=YES;
            
            CGSize fontSize=[m_wheretext.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
            if (fontSize.height>=60)
            {
                
                CGPoint point=[self.scrollView contentOffset];
                point.y =point.y+75;
                [self.scrollView setContentOffset:point animated:YES];
                [self textViewShouldBeginEditing:m_howtext];
                [m_howtext setEditable:YES];
                //[self textViewShouldEndEditing:m_wheretext];
                [m_wheretext resignFirstResponder];
                [m_howtext becomeFirstResponder];
                return;
            }

        }
    }

    else if (textView ==m_howtext)
    {
        if ([m_howtext.text length]==0)
        {
            
            m_howtext.textColor=[UIColor lightGrayColor];
            m_howPlaceholder.hidden=NO;
        }
        else
        {
            m_howtext.textColor=[UIColor blackColor];
            m_howPlaceholder.hidden=YES;
            CGSize fontSize=[m_howtext.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
            if (fontSize.height>=60)
            {
                
                CGPoint point=[self.scrollView contentOffset];
                point.y =point.y+75;
                [self.scrollView setContentOffset:point animated:YES];
                [self textViewShouldBeginEditing:m_whytext];
                [m_whytext setEditable:YES];
                //[self textViewShouldEndEditing:m_howtext];
                [m_howtext resignFirstResponder];
                [m_whytext becomeFirstResponder];
                return;
            }

        }
    }
    else if (textView ==m_whytext)
    {
        if ([m_whytext.text length]==0)
        {
            
            m_whytext.textColor=[UIColor lightGrayColor];
            m_whyPlaceholder.hidden=NO;
        }
        else
        {
            m_whytext.textColor=[UIColor blackColor];
            m_whyPlaceholder.hidden=YES;
            CGSize fontSize=[m_whytext.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] constrainedToSize:CGSizeMake(234, CGFLOAT_MAX)];
            if (fontSize.height>=60)
            {
                //[self textViewShouldEndEditing:m_whytext];
                [m_whytext resignFirstResponder];
                return;
            }

        }
    }

    
    else if(textView ==m_opEdView)
    {
        openWriteCount=[textView.text length];
        m_opEdView.textColor=[UIColor blackColor];
        m_writeToOpenLBL.hidden=YES;

        if(openWriteCount <= 500)
        {
            int i=openWriteCount;
            NSLog(@"%@",[NSString stringWithFormat:@"%d/%d",i, 500 - i]);
            m_opentextcountLBL.text=[NSString stringWithFormat:@"%d/%d",i, 500 - i];
        }
        else
        {
            UIAlertView *alartView=[[UIAlertView alloc] initWithTitle:@"" message:@"opEn text should not be more than 500 characters"
                                                             delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
            [alartView show];
            [alartView release];
        }
        if (m_opEdView.text.length==0) 
        {
            
            m_opEdView.textColor=[UIColor lightGrayColor];
            m_writeToOpenLBL.hidden=NO;
        }

    }
   
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if (textView==m_headingTextField)
        {
            if (m_headingTextField.text.length==0) 
            {
                m_headingTextField.textColor=[UIColor lightGrayColor];
               m_headlinePlaceHolder.hidden=NO;
               
            }
        }
        
        else if (textView==m_whotext)
        {
            if (m_whotext.text.length==0) 
            {
                m_whotext.textColor=[UIColor lightGrayColor];
                m_whoPlaceholder.hidden=NO;
                
            }
        }
        else if (textView==m_whattext)
        {
            if (m_whattext.text.length==0) 
            {
                m_whattext.textColor=[UIColor lightGrayColor];
                m_whatPlaceholder.hidden=NO;
                
            }
        }
        else if (textView==m_whentext)
        {
            if (m_whentext.text.length==0) 
            {
                m_whentext.textColor=[UIColor lightGrayColor];
                m_whenPlaceholder.hidden=NO;
                
            }
        }
        else if (textView==m_wheretext)
        {
            if (m_wheretext.text.length==0) 
            {
                m_wheretext.textColor=[UIColor lightGrayColor];
                m_wherePlaceholder.hidden=NO;
                
            }
        }
        else if (textView==m_howtext)
        {
            if (m_howtext.text.length==0) 
            {
                m_howtext.textColor=[UIColor lightGrayColor];
                m_howPlaceholder.hidden=NO;
                
            }
        }
        else if (textView==m_whytext)
        {
            if (m_whytext.text.length==0) 
            {
                m_whytext.textColor=[UIColor lightGrayColor];
                m_whyPlaceholder.hidden=NO;
                
            }
        }
        else if(textView ==m_opEdView)
        {
            if (m_opEdView.text.length==0) 
            {
                m_opEdView.textColor=[UIColor lightGrayColor];
                m_writeToOpenLBL.hidden=NO;
            }
            
        }
        [self updateNextButton];
        return NO;
    }
    if (textView == m_opEdView)
    {
           
    return YES;
    }
    
    return YES;
}


#pragma mark-
#pragma mark textField delegates


//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if (textField== m_whytext)
//    {
//        [UIView beginAnimations: @"upView" context: nil];
//        [UIView setAnimationDelegate: self];
//        [UIView setAnimationDuration: 0.4];
//        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
//        //self.view.frame = CGRectMake(0,-120, 320,460);
//        [self.scrollView setContentOffset:CGPointMake(0, 0)];
//        [UIView commitAnimations];
//    }
//    
//}

- (void)startObservingKeyboard
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) return;
    if (isObservingKeyboard) return;
    
    isObservingKeyboard = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)stopObservingKeyboard
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) return;
    if (!isObservingKeyboard) return;
    
    isObservingKeyboard = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark-
#pragma mark Button Action

-(IBAction)writeReportStory:(id)sender
{    
    int tempTag;
    if (sender==nil)
    {
        tempTag=1;
    }
    else
    {
        tempTag=[sender tag];
    }
    switch (tempTag)
    {
        case 1:
            opEdShown = NO;
                      m_writestoryBTN.selected=YES;
                       m_writeopenBTN.selected=NO;
                        
                    [UIView animateWithDuration:0.25 
                                   animations:^{[self setAnimatedLayout]; }];
                        
                        [opEdButton setTitle:@"Write Your Story Instead" forState:UIControlStateNormal];

            break;
        case 2:
                      opEdShown = YES;
                        m_writestoryBTN.selected=NO;
                        m_writeopenBTN.selected=YES;
                        [UIView animateWithDuration:0.25 
                                         animations:^{[self setAnimatedLayout]; }];
                    
                  [opEdButton setTitle:@"Write OP ED piece Instead" forState:UIControlStateNormal];
            break;

        default:
            break;
    }
   }


-(IBAction)cancelClicked:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)nextClicked:(id)sender {
	
    if ([m_headingTextField.text length]<=50)
    {
	BuzzButtonShareNowFunctionality * vc = [[BuzzButtonShareNowFunctionality alloc] init];
    vc.m_headerMessage=[m_headingTextField.text URLEncodedString];
	vc.photoImage = self.m_photoimage;
	vc.m_strType = [self.selectedType URLEncodedString] ;
	vc.m_strMessage = nil;
    vc.shareVideo = self.shareVideo;
    if (m_whotext.textColor==[UIColor lightGrayColor] ||[m_whotext.text length]==0)
        {
            vc.m_whoMessage= nil;
        }
        else
        {
            vc.m_whoMessage= [self.m_whotext.text URLEncodedString];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                        withAction:@"who has value"
                                                         withLabel:nil
                                                         withValue:nil];
        }
        if (m_whattext.textColor==[UIColor lightGrayColor] ||[m_whattext.text length]==0)
        {
            vc.m_whatMessage= nil;
        }
        else
        {
            vc.m_whatMessage=[self.m_whattext.text URLEncodedString];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                            withAction:@"what has value"
                                                             withLabel:nil
                                                             withValue:nil];
        }
        vc.m_whenMessage=[self.m_whentext.text URLEncodedString];
        vc.m_whereMessage=[self.m_wheretext.text URLEncodedString];
        if (m_howtext.textColor==[UIColor lightGrayColor] ||[m_howtext.text length]==0)
        {
            vc.m_howMessage=nil;
        }
        else
        {
            vc.m_howMessage=[self.m_howtext.text URLEncodedString];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                            withAction:@"how has value"
                                                             withLabel:nil
                                                             withValue:nil];
        }
        if (m_whytext.textColor==[UIColor lightGrayColor] ||[m_whytext.text length]==0)
        {
            vc.m_whyMessage=nil;
        }
        else
        {
            vc.m_whyMessage=[self.m_whytext.text URLEncodedString];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                            withAction:@"why has value"
                                                             withLabel:nil
                                                             withValue:nil];
        }
    
    if (opEdShown) {
        if (m_opEdView.textColor==[UIColor lightGrayColor] ||[m_opEdView.text length]==0)
        {
             vc.m_openMessage= nil;
        }
        else
        {
            vc.m_openMessage = [m_opEdView.text URLEncodedString];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                            withAction:@"oped has value"
                                                             withLabel:nil
                                                             withValue:nil];
        }
    } 
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
    }
    else
    {
        UIAlertView *alartView=[[UIAlertView alloc] initWithTitle:@"" message:@"Headline should not be more than 50 characters"
                                                         delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alartView show];
        [alartView release];
    
}

}


- (IBAction)selectCategory:(id)sender
{
    self.tempScroll.contentSize=self.scrollView.contentSize;
    ChooseCategories *aController = [[ChooseCategories alloc] initWithNibName:@"ChooseCategory" bundle:[NSBundle mainBundle]];
    
    if (self.selectedType) {
        aController.currentRow = [[NewsDataManager sharedManager] indexOfActiveCategoryByName:self.selectedType];
    } else {
        aController.currentRow = -1;
    }
    aController.controller = self;
    [self.navigationController pushViewController:aController animated:YES];
    
    [aController release];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzHeadline"
                                                    withAction:@"selectCategory"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (void) saveSelectedCategory:(NSString *)type
{
    self.selectedType = type;
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:type];
    
    if (type) {
        [self.selectCategoryButton setTitle:convertedName forState:UIControlStateNormal];
        
    } else {
        [self.selectCategoryButton setTitle:@"Select Category" forState:UIControlStateNormal];
    }
    [self updateNextButton];
}

- (void) setAnimatedLayout
{
    if (opEdShown) {
        CGRect frame1 = self.editStoryView.frame;
        
        frame1.origin.x = -320;
        [self.editStoryView setFrame:frame1];
        [self.editOpEdView setHidden:NO];
        self.tempScroll.contentSize=self.scrollView.contentSize;
        self.scrollView.contentSize=CGSizeMake(320, 700);
    } else {
        CGRect frame1 = self.editStoryView.frame;
        
        
        frame1.origin.x = +8;
        [self.editStoryView setFrame:frame1];
        [self.editOpEdView setHidden:YES];
        //self.scrollView.contentSize=self.tempScroll.contentSize;
        self.tempScroll.contentSize=self.scrollView.contentSize;
        self.scrollView.contentSize=CGSizeMake(320, 920);
        opEdShown=YES;
    }

}

//- (IBAction)showOpEd:(id)sender
//{
//    if (!opEdShown) {
//        [UIView animateWithDuration:0.25 
//                         animations:^{[self setAnimatedLayout]; }];
//
//        [opEdButton setTitle:@"Write Your Story Instead" forState:UIControlStateNormal];
//        opEdShown = YES;
//    } else {
//        [UIView animateWithDuration:0.25 
//                         animations:^{[self setAnimatedLayout]; }];
//        
//        [opEdButton setTitle:@"Write OP ED piece Instead" forState:UIControlStateNormal];
//        opEdShown = NO;
//    }
//    
//}

#pragma mark - Keyboard observation/animation
- (void)keyboardWillShow:(NSNotification *)aNotification 
{
    if (currentTextField == m_headingTextField) {
        return;
    }
    //self.scrollView.scrollEnabled=NO;
    if (self.currentTextField==m_howtext ||self.currentTextField==m_whytext)
    {
        CGRect frame=[self.view frame];
        frame.origin.y=-40;
        self.view.frame=frame;
    }
    UIView *viewToPop = (opEdShown) ? self.m_opEdView : currentTextField;
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.scrollView.frame;
    aRect.size.height -= (kbSize.height + 31);
    CGPoint newOrigin = [self.scrollView convertPoint:viewToPop.frame.origin fromView:viewToPop.superview];
       newOrigin.y += viewToPop.frame.size.height+50; 
    if (!CGRectContainsPoint(aRect, newOrigin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, newOrigin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    if (currentTextField == nil && !opEdShown) {
        return;
    }
    CGRect frame=[self.view frame];
    if (frame.origin.y==-40) 
    {
        frame.origin.y=0;
        self.view.frame=frame;
    }
    self.currentTextField = nil;
    [UIView animateWithDuration:0.25 
                     animations:^{ 
                    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
                    scrollView.contentInset = contentInsets;
                    scrollView.scrollIndicatorInsets = contentInsets;
                         //self.scrollView.scrollEnabled=YES;
                     }];
    
        
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    self.currentTextField = textField;    
//    
//    return YES ;
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    self.currentTextField = nil;
//}
//
//- (BOOL) textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//	return YES ;
//}



- (void)viewDidUnload
{
    [super viewDidUnload];
    self.m_writestoryBTN=nil;
    self.m_writeopenBTN=nil;
    self.m_opentextcountLBL=nil;
    self.m_whoLBL=nil;
    self.m_whatLBL=nil;
    self.m_whenLBL=nil;
    self.m_whereLBL=nil;
    self.m_howLBL=nil;
    self.m_whyLBL   =nil;
    self.m_headlinePlaceHolder=nil;
    self.m_whoPlaceholder=nil;
    self.m_whatPlaceholder=nil;
    self.m_wherePlaceholder=nil;
    self.m_wherePlaceholder=nil;
    self.m_howPlaceholder=nil;
    self.m_whyPlaceholder=nil;
    self.m_writeToOpenLBL=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc
{
    [metadata release];
    [m_photoimage release];
    [m_strMessage release];
    [m_strType release];
    [m_whotext release];
    [m_whattext release];
    [m_whentext release];
    [m_wheretext release];
    [m_howtext release];
    [m_whytext release];
    [m_whoLBL release];
    [m_whatLBL release];
    [m_whenLBL release];
    [m_whereLBL release];
    [m_howLBL release];
    [m_whyLBL release];
    [editHeadlineView release];
    [editStoryView release];
    [editOpEdView release];
    [opEdButton release];
    [m_opEdView release];
    [nextButton release];
    [selectCategoryButton release];
    [placeLabel release];
    self.currentTextField = nil;
    self.selectedType = nil;
    [m_writeopenBTN release];
    [m_writestoryBTN release];
    [m_opentextcountLBL release];
    [tempScroll release];
    [m_headlinePlaceHolder release];
    [m_whoPlaceholder release];
    [m_whatPlaceholder release];
    [m_whenPlaceholder release];
    [m_wherePlaceholder release];
    [m_howPlaceholder release];
    [m_whyPlaceholder release];
    [m_writeToOpenLBL release];
    [super dealloc];
}

@end
