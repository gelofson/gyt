//
//  AddAddressBookFriends.h
//  QNavigator
//
//  Created by softprodigy on 22/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import"Constants.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface AddAddressBookFriends : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate> {
    BOOL searchBarIsEditing;
	
	IBOutlet NSMutableArray *m_theNamesArray;
	
	NSMutableArray *m_listOfNames;
	
	NSString *m_theName;
	
	NSMutableArray *tmp_Names;
	
	NSMutableArray *m_mailIds;
	
	IBOutlet UILabel *firstName;
	
    IBOutlet UILabel *lastName;
	
	UIImageView *tmp_imageview;
	
	UIImage *image;
	
	NSArray *people;
	
	NSString *name;
	
	IBOutlet UITableView *table;
	
	NSString  *m_name;
	
	UIView *m_View;
	
	UIActivityIndicatorView *m_activityInidicator;
	
	int  AddButtonTag;
	
	int InviteButtonTag;
	
		
	ABAddressBookRef newaddressBook;
	
	NSString *m_personMail;
	NSMutableArray	 *email_Ids;
	NSMutableArray	 *m_arrAllContacts;
	NSMutableArray	 *m_arrShopbeeContacts;
	NSMutableArray	 *m_arrNonShopbeeContacts;
		
    IBOutlet UIImageView *feedBackPage;//visible when no search records found;//**
}
@property (nonatomic,retain) 	NSMutableArray	 *m_arrShopbeeContacts;

@property (nonatomic,retain) 	NSMutableArray	 *m_arrNonShopbeeContacts;

@property (nonatomic,retain) NSMutableArray *m_arrAllContacts;

@property(nonatomic, readwrite) ABAddressBookRef addressBook;

@property (nonatomic, retain) UILabel *firstName;

@property (nonatomic, retain) UILabel *lastName;

@property(nonatomic,retain) IBOutlet UIImageView *tmp_imageview;




-(IBAction) m_goToBackView;


-(IBAction)m_AddButtonAction:(id)sender;

-(IBAction)btnTellAFriendAction:(id)sender;

-(IBAction) btnAddAllAction;

-(IBAction) btnInviteAllAction;

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData;

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage;

-(void)fillContactsArrayFromAddressbook;

-(BOOL)checkShopbeeExistence:(NSString *)strEmail;
@end
