//
//  FittingRoomViewController.h
//  QNavigator
//
//  Created by softprodigy on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CategoryModelClass;
@class BigSalesModelClass;
@interface FittingRoomViewController : UIViewController {

	NSString *m_sendBuy,*m_PublicOrPrivate;
	UILabel *m_sendBuyLabel,*m_buyOrNot;
	UITextView *m_textView;
	NSInteger tagg;
	UIView *m_view;
	NSString *m_advtlocid;
	UIActivityIndicatorView *m_indicatorView;
	NSString *m_typeLabel;
	UIImage *m_Image;
	UIImageView *m_imgView;

	CategoryModelClass * m_catObjModel; 
	BigSalesModelClass * m_bigSalesModel; 

}
@property (nonatomic,retain) IBOutlet UIImageView *m_imgView;
@property (nonatomic,retain) UIImage *m_Image;
@property(nonatomic,retain)IBOutlet UIView *m_view;
@property(nonatomic,retain)NSString *m_sendBuy;
@property(nonatomic,retain)NSString *m_PublicOrPrivate;
@property(nonatomic,retain)IBOutlet UILabel *m_buyOrNot;
@property(nonatomic,retain)IBOutlet	UILabel *m_sendBuyLabel;
@property(nonatomic,retain)IBOutlet UITextView *m_textView;
@property(nonatomic,retain)NSString *m_advtlocid;
@property (nonatomic,retain)UIActivityIndicatorView *m_indicatorView;
@property(nonatomic,retain) NSString *m_typeLabel;
@property(nonatomic,retain) CategoryModelClass *m_catObjModel;
@property(nonatomic,retain) BigSalesModelClass *m_bigSalesModel;

@property (nonatomic,readwrite) NSInteger tagg;

-(IBAction)btnSendMessage;
-(IBAction) btnBackAction:(id)sender;
-(IBAction)cancelButtonAction:(id)sender;
-(IBAction)addToWishList:(id)sender;
//-(IBAction)LeaveItPublic:(id)sender;
//-(IBAction)LeaveItPrivate:(id)sender;
-(void)whereBtnPressed:(int)tag;


@end
