//
//  AddAddressBookFriends.m
//  QNavigator
//
//  Created by softprodigy on 22/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddAddressBookFriends.h"
#import <AddressBook/ABPerson.h>
#import"QNavigatorAppDelegate.h"
#import"SBJSON.h"
#import"ShopbeeAPIs.h"
#import"AuditingClass.h"
#import"JSON.h"
#import"LoadingIndicatorView.h"
#import "NewsDataManager.h"

# define SectionHeaderHeight 30

@implementation AddAddressBookFriends

@synthesize addressBook;

@synthesize firstName;

@synthesize lastName;

@synthesize tmp_imageview;

@synthesize m_arrAllContacts;
@synthesize m_arrNonShopbeeContacts;
@synthesize m_arrShopbeeContacts;

QNavigatorAppDelegate *l_appDelegate;

ShopbeeAPIs *l_requestObj;

NSArray *l_ShopbeeArray;
 
LoadingIndicatorView *l_indicatorView;
#pragma mark Custom methods


-(IBAction) m_goToBackView{
	[self.navigationController popViewControllerAnimated:YES];

}

-(IBAction)m_AddButtonAction:(id)sender{
		
	NSString *tmp_mailID;
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *nameString = [prefs objectForKey:@"userName"];
	tmp_mailID=[[m_arrShopbeeContacts objectAtIndex:[sender tag]]valueForKey:@"Email"];
	AddButtonTag=[sender tag];
	NSMutableArray *mail_Ids=[[NSMutableArray alloc] init];
	[mail_Ids addObject:tmp_mailID];
	NSString *tmp_String=[NSString stringWithFormat:@"%@",[mail_Ids JSONRepresentation]];
	NSLog(@"the mail is %@",tmp_mailID);
	NSLog(@"value is %@",m_name);
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_indicatorView startLoadingView:self];
	[l_requestObj sendRequestToAddFriend:@selector(AddRequestCallBackMethod:responseData:) tempTarget:self custId:nameString tmpUids:tmp_String];
	[l_requestObj release];
	l_requestObj=nil;
	[mail_Ids release];
	mail_Ids=nil;
	
}
-(IBAction)btnTellAFriendAction:(id)sender
{

	m_personMail=[[m_arrNonShopbeeContacts objectAtIndex:[sender tag]] valueForKey:@"Email"];
	
	InviteButtonTag=[sender tag];
	if (![m_personMail isEqualToString:@""]) {
		[self performSelector:@selector(showPicker)];
	}
	else if([m_personMail isEqualToString:@""]){
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Person email not exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
		
		
	}

}
-(IBAction) btnAddAllAction
{
	NSString *tmp_mail;
	NSMutableArray *tmp_array;
	NSString *customerId;
	tmp_array=[[NSMutableArray alloc] init];
	for (int i=0; i<[m_arrShopbeeContacts count];i++) {
		tmp_mail=[[m_arrShopbeeContacts objectAtIndex:i]valueForKey:@"Email"];
		[tmp_array addObject:tmp_mail];
	}
    
    // GDL: We don't need stringWithFormat: here. JSONRepresentation returns a string.
	NSString *strJson=[tmp_array JSONRepresentation];
	
	customerId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_indicatorView startLoadingView:self];
	[l_requestObj sendRequestToAddFriend:@selector(AddRequestCallBackMethod:responseData:) tempTarget:self custId:customerId tmpUids:strJson];
	
	[l_requestObj release];
	l_requestObj=nil;
	[tmp_array release];
	tmp_array=nil;
	AddButtonTag=-1;
}
-(IBAction) btnInviteAllAction{
	NSString *temp_MailId;
	InviteButtonTag=-1;
	m_mailIds=[[NSMutableArray alloc] init];
	for (int i=0; i<[m_arrNonShopbeeContacts count]; i++) {
if ([[m_arrNonShopbeeContacts objectAtIndex:i]valueForKey:@"Email"]) 
	{
		temp_MailId=[[m_arrNonShopbeeContacts objectAtIndex:i]valueForKey:@"Email"];
		[m_mailIds addObject:temp_MailId];
	}
		else {
			temp_MailId=@"";
			[m_mailIds addObject:temp_MailId];
		}

	}
	
	[self performSelector:@selector(showPicker)];	
}


-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}


-(void)fillContactsArrayFromAddressbook
{
	
	ABRecordRef tmp_recordObj;
	NSString	*tempName;
	UIImage		*tempImage;
	NSString	*tmp_mailID;
	int			tmp_count;
	
	ABMultiValueRef *tmp_mail;
	
	for (int i=0; i<people.count; i++)
	{
		tmp_recordObj =[people objectAtIndex:i];
		
		tmp_mail=(ABMultiValueRef *)ABRecordCopyValue(tmp_recordObj,kABPersonEmailProperty);
		tmp_count=ABMultiValueGetCount(tmp_mail);
		
		if (tmp_count>0)
		{
			tmp_mailID=(NSString *)ABMultiValueCopyValueAtIndex(tmp_mail,0);
			if ((tmp_mailID == nil) || ([tmp_mailID length] <= 0)) {
				CFRelease(tmp_mail);
				continue;
			}
		}
		else {
			CFRelease(tmp_mail);
			continue;
		}
		
	 	tempName = (NSString *)ABRecordCopyValue(tmp_recordObj,kABPersonFirstNameProperty);
		if(ABPersonHasImageData(tmp_recordObj))
		{
			tempImage =[UIImage imageWithData:(NSData *)ABPersonCopyImageData(tmp_recordObj)] ;
		}
		else {
			tempImage =[UIImage imageNamed:@"no-image"];
		}
		
		
		NSDictionary *tempDict=[NSDictionary dictionaryWithObjectsAndKeys:tempName,@"Name",tmp_mailID,@"Email",tempImage,@"Image",nil];
		[m_arrAllContacts addObject:tempDict];	
		CFRelease(tmp_mail);
		CFRelease(tmp_mailID);

	}

	NSLog(@"all contacts count:%d",m_arrAllContacts.count);
	
}




#pragma mark -
#pragma mark Mail Composer methods


// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
		

	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];
	if (InviteButtonTag==-1) {
		[toRecipients addObjectsFromArray:m_mailIds];
	}else {
	if (m_personMail) {
	[toRecipients addObject:m_personMail];
        }	
    }
    NSLog(@"values of  (to recipients) is %@",toRecipients);
	[picker setToRecipients:toRecipients];
	[picker setSubject:kTellAFriendMailSubject];
	
	NSString *emailBody;
	emailBody=[NSString stringWithFormat:kTellAFriendMailBody];
	
	[picker setMessageBody:emailBody isHTML:NO];
	[self presentModalViewController:picker animated:YES];
	
    [picker release];
	picker=nil;
	[toRecipients release];
	toRecipients=nil;
	[m_personMail release];
	m_personMail=nil;
	[m_mailIds release];
	m_mailIds=nil;
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	[self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


#pragma mark -
#pragma mark callBack methods

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog([NSString stringWithFormat:@"data downloaded %@",[NSString stringWithUTF8String:[responseData bytes]]]);
	
	if ([responseCode intValue]==200) 
	{
		
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSLog(@"response string: %@",tempString);
		NSArray *tempArray=[[tempString JSONValue] retain];
		NSDictionary *tempDict=[tempArray objectAtIndex:0];
		[tempArray release];
		tempArray=nil;
		l_ShopbeeArray=[[tempDict valueForKey:@"shopbee"] retain];
		
		for (int i=0;i<m_arrAllContacts.count; i++ )
		{
			if ([self checkShopbeeExistence:[[m_arrAllContacts objectAtIndex:i]valueForKey:@"Email"]])
			{
				[m_arrShopbeeContacts addObject:[m_arrAllContacts objectAtIndex:i]];
			}
			else 
			{
				[m_arrNonShopbeeContacts addObject:[m_arrAllContacts objectAtIndex:i]];
			}
		}
		
		[table reloadData];
		
		[tempString release];
		tempString=nil;
		[m_arrAllContacts release];
		m_arrAllContacts=nil;
		[l_indicatorView stopLoadingView];
		
		if ([m_arrShopbeeContacts count] + [m_arrNonShopbeeContacts count] == 0) {
			feedBackPage.hidden = NO;//Show 'Not found' image when no search records found
			table.hidden = YES;
		}
	}
	else 
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
}
-(void)AddRequestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSInteger Code=[responseCode integerValue];
	[l_indicatorView stopLoadingView];
	if(AddButtonTag==-1)
	{
		if (Code==200) 
		{
			[self showAlertView:@"Friend request sent to all friends." alertMessage:nil];	
			[m_arrShopbeeContacts removeAllObjects];
			[table reloadData];
			
		}
		else 
            [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

    }
	else 
	{
		if (Code==200) 
		{
			[l_indicatorView stopLoadingView];
			[self showAlertView:@"Friend Request Sent" alertMessage:@"Friend request has been sent ." ];
			[m_arrShopbeeContacts removeObjectAtIndex:AddButtonTag];
			[table reloadData];
		}
		else             
            [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
		
	}
	
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[tempString release];
	tempString=nil;
	
}	
-(BOOL)checkShopbeeExistence:(NSString *)strEmail
{
	NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@",strEmail];
	
	NSArray *filteredNameList = [l_ShopbeeArray filteredArrayUsingPredicate:bPredicate];
	
	if (filteredNameList.count>0)
		return YES;
	else 
		return NO;
	
}
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

#pragma mark tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
		
	NSLog(@"no. of persons %i",[people count]);
	if (section==0)
	{
		return m_arrShopbeeContacts.count;
	}
	
	return m_arrNonShopbeeContacts.count;
	
	
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *cellidentifier=@"cell";
	
	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
	if (cell == nil) 
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier] autorelease];
		cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_with_arrow_in_add_friend.png"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
	
	
	for(UIView *temp_view in cell.contentView.subviews)
	{
		if([temp_view isKindOfClass:[UIImageView class]])
		{
			[temp_view removeFromSuperview];
		}
		else if([temp_view isKindOfClass:[UILabel class]])
		{
			[temp_view removeFromSuperview];
		}
		else if([temp_view isKindOfClass:[UIButton class]])
		{
			[temp_view removeFromSuperview];
		}
	}
	
	UIImageView *m_imageView=[[UIImageView alloc]init];
	m_imageView.frame=CGRectMake(5,5,30,30); // for displaying the image in the cell
	m_imageView.contentMode=UIViewContentModeScaleAspectFit;
	
	UILabel *temp_lblFriendName=[[UILabel alloc]init];
	temp_lblFriendName.frame=CGRectMake(60,12,165,25);// for setting the frame of the tableview cell
	temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
	temp_lblFriendName.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
	
	if(indexPath.section==0)
	{
		m_imageView.image=[[m_arrShopbeeContacts objectAtIndex:indexPath.row] valueForKey:@"Image"];
		
		if ([[[m_arrShopbeeContacts objectAtIndex:indexPath.row]valueForKey:@"Name"] isEqualToString:@""]) 
		{
			temp_lblFriendName.text=@"No Name";
		}
		else 
		{
			temp_lblFriendName.text=[[m_arrShopbeeContacts objectAtIndex:indexPath.row]valueForKey:@"Name"];	
		}
		
			
		
		UIButton *m_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
		[m_addButton setImage:[UIImage imageNamed:@"add_in_add_friend_button.png"] forState:UIControlStateNormal];
		m_addButton.tag =indexPath.row;
		[m_addButton addTarget:self action:@selector(m_AddButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		m_addButton.frame=CGRectMake(220,0,60,50);
		
		[cell.contentView addSubview:m_addButton];
	}
	else if(indexPath.section==1) 
	{
		m_imageView.image=[[m_arrNonShopbeeContacts objectAtIndex:indexPath.row] valueForKey:@"Image"];
		
		if ([[[m_arrNonShopbeeContacts objectAtIndex:indexPath.row]valueForKey:@"Name"] isEqualToString:@""])
		{
				temp_lblFriendName.text=@"No Name";
		}
		else {
			temp_lblFriendName.text=[[m_arrNonShopbeeContacts objectAtIndex:indexPath.row]valueForKey:@"Name"];
			
		}
		
		
				
		UIButton *temp_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_addButton setImage:[UIImage imageNamed:@"invite_blue_button.png"] forState:UIControlStateNormal];
		[temp_addButton addTarget:self action:@selector(btnTellAFriendAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addButton.frame=CGRectMake(220,0,60,50);
		temp_addButton.tag=indexPath.row;
		[cell.contentView  addSubview:temp_addButton];
	}
	
	
	
	[cell.contentView addSubview:temp_lblFriendName];
		
	[cell.contentView addSubview:m_imageView];

	[m_imageView release];
	m_imageView=nil;
	[temp_lblFriendName release];
	temp_lblFriendName=nil; 
	return cell;

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
		
	if (section==0) {
		
	UIView *tmp_view;
		
	tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 350, SectionHeaderHeight)];
	tmp_view.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
	[tmp_view autorelease];
	UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)]  ;
	tmp_headerLabel.font=[UIFont fontWithName:kFontName size:14];
	tmp_headerLabel.text=[NSString stringWithFormat:@"%i" @" "@"Friends are on FashionGram",m_arrShopbeeContacts.count];
	tmp_headerLabel.textColor=[UIColor whiteColor];
	tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
	[tmp_view addSubview:tmp_headerLabel];
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_view.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"add_all.png"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnAddAllAction) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(238,3,44,25);
		[tmp_view addSubview:temp_addAll];
		[temp_addAll release];
		temp_addAll=nil;
		[tmp_headerLabel release];
		return tmp_view;
		
	}
	else {
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 350,	SectionHeaderHeight)];
		tmp_headerForSecondSection.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];

		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)] ;
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:14 ];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%i"@" "@"Friends not on FashionGram",m_arrNonShopbeeContacts.count];
		tmp_headerLabel.textColor=[UIColor whiteColor];
		
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];

		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_headerForSecondSection.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"btn_inviteAll"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnInviteAllAction) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(238,5,43,20);
		
		[tmp_headerForSecondSection addSubview:temp_addAll];
		[temp_addAll release];
		temp_addAll=nil;
				[tmp_headerLabel release];
		 tmp_headerLabel=nil;
		return [tmp_headerForSecondSection autorelease];
	}
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 
		if (section==0 && m_arrShopbeeContacts.count>0)
			return 30;
		else if(section==0 && m_arrShopbeeContacts.count==0)
			return 0;
		if (section==1 && m_arrNonShopbeeContacts.count>0)
			return 30;
		else if(section==1 && m_arrNonShopbeeContacts.count==0)
			return 0;
		return 0;
	
} 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}
#pragma mark -
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void)viewDidAppear:(BOOL)animated
{
	
}
-(void) viewDidLoad{
	[super viewDidLoad];
	l_indicatorView=[LoadingIndicatorView SharedInstance];
	
		
	m_arrAllContacts=[[NSMutableArray alloc]init];
	m_arrShopbeeContacts=[[NSMutableArray alloc]init];
	m_arrNonShopbeeContacts=[[NSMutableArray alloc]init];
	
	newaddressBook = ABAddressBookCreate();
	people=[(NSArray *) ABAddressBookCopyArrayOfAllPeople(newaddressBook)  retain];
	
	email_Ids=[[NSMutableArray alloc]init];
	int i;

	
	[self fillContactsArrayFromAddressbook];
	
		
	NSString *tempStr;
	
	for (i=0;i<[m_arrAllContacts count] ; i++) {
	
		tempStr=[[m_arrAllContacts objectAtIndex:i] valueForKey:@"Email"];
		if (!tempStr)
			tempStr=[NSString stringWithString:@""];
		
		[email_Ids addObject:tempStr];
	}	
	//[email_Ids removeAllObjects]; // to test the empty address book
	InviteButtonTag=0;
	NSLog(@"the email ids are%@",email_Ids);
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_indicatorView startLoadingView:self];
	[l_requestObj  getPhonebookList:@selector(requestCallBackMethod:responseData:) tempTarget:self  emailIds:email_Ids ];
	
	[email_Ids release];
	email_Ids=nil;
	[l_requestObj release];
	l_requestObj=nil;
	
	feedBackPage.hidden = YES; // Hide 'not found' view on startup
}
-(void)viewWillAppear:(BOOL)animated
{
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    [m_theNamesArray release]; m_theNamesArray = nil;
    [firstName release]; firstName = nil;
    [lastName release]; lastName = nil;
    [table release]; table = nil;
    self.tmp_imageview = nil;
	[feedBackPage release];

}

- (void)dealloc {
    
	[m_theNamesArray release];
    [m_listOfNames release];
    [m_theName release];
    [tmp_Names release];
    [m_mailIds release];
    [firstName release];
    [lastName release];
    [tmp_imageview release];
    [image release];
    [people release];
    [name release];
    [table release];
    [m_name release];
    [m_View release];
    [m_activityInidicator release];
    [m_personMail release];
    [email_Ids release];
    [m_arrAllContacts release];
    [m_arrShopbeeContacts release];
    [m_arrNonShopbeeContacts release];
	[feedBackPage release];
    
	[super dealloc];
}

@end
