//
//  BuzzButton AddFromAddressBook.h
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CONTACT_ENTRY_SELECT_TAG 20011
#define CONTACT_ENTRY_NAME_TAG 20012
#define CONTACT_ENTRY_EMAIL_TAG 20013

@interface BuzzButtonAddFromAddressBook : UIViewController <UITableViewDataSource, UITableViewDelegate> {

	IBOutlet UIView *m_sectionHeaderView;
	IBOutlet UITableView *m_emailTableView;
	IBOutlet UIButton *m_selectAllButton;
	
	NSMutableDictionary *m_addrBookEmailContacts;
}

@property (nonatomic,retain) IBOutlet UIButton *m_selectAllButton;
@property (nonatomic,retain) IBOutlet UIView *m_sectionHeaderView;
@property (nonatomic,retain) IBOutlet UITableView *m_emailTableView;
@property (nonatomic,retain) NSMutableDictionary *m_addrBookEmailContacts;

-(IBAction)goToBackView;
-(IBAction) contactsSelectAllToggleAction:(id) sender;


@end


