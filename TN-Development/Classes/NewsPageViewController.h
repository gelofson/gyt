//
//  NewsPageViewController.h
//  TinyNews
//
//  Created by Nava Carmon on 30/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentViewController.h"
#import "EGORefreshTableHeaderView.h"

@class MoreButton;
@class AsyncButtonView;
@class DetailPageViewController;
@class NewsMoreViewController;

@interface NewsPageViewController : ContentViewController <EGORefreshTableHeaderDelegate, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *newsTable;
    IBOutlet UIButton *notificationButton;
    IBOutlet UIImageView *m_exceptionPage;
    IBOutlet UIButton *m_exceptionButton;
    IBOutlet UIButton *popularButton;
    IBOutlet UIButton *friendsButton;
    IBOutlet UIButton *followingButton;
    IBOutlet UIButton *mineButton;
    NSInteger currentFilter;
    DetailPageViewController *detailController;
    NewsMoreViewController *moreController;
    NSMutableArray *tableCells;
    BOOL menuWasPressed;

	EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
}

@property(retain,nonatomic)IBOutlet UIButton *notificationButton;//**
@property(retain,nonatomic)IBOutlet UIImageView *m_exceptionPage;//**
@property(retain,nonatomic)IBOutlet UIButton *m_exceptionButton;//**
@property(retain,nonatomic)IBOutlet UITableView *newsTable;//**
@property(retain,nonatomic)NSMutableArray *tableCells;//**
@property(retain,nonatomic)IBOutlet DetailPageViewController *detailController;//**
@property(retain,nonatomic)NewsMoreViewController *moreController;//**

- (IBAction) clickOnPopular:(id) sender;
- (IBAction) clickOnFriends:(id) sender;
- (IBAction) clickOnFollowing:(id) sender;
- (IBAction) clickOnMine:(id) sender;
- (IBAction) viewNotification;
- (IBAction)moreButtonClicked:(MoreButton *)sender;
- (void) updateNotifications:(NSString *) count;
- (void) IconClickedAction:(AsyncButtonView *) sender;
- (void) noData;
-(IBAction)exceptionButtonAction:(id)sender;


- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

@end
