//
//  BuzzButton AddNewContact.m
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/CALayer.h>

#import "BuzzButtonAddNewContact.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "MoreButton.h"
QNavigatorAppDelegate *l_appDelegate;

@implementation BuzzButtonAddNewContact

@synthesize m_emailTableView;
@synthesize m_sectionHeaderView;
@synthesize m_emailTextField;
@synthesize m_nameTextField;
@synthesize m_DictOfContactDicts;
@synthesize m_newlyAddedContactsDict;


#define ALPHA                   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC                 @"1234567890"
#define ALPHA_NUMERIC           ALPHA NUMERIC

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
   
	if (textField == self.m_emailTextField) {
		NSCharacterSet *unacceptedInput = nil;
		if ([[textField.text componentsSeparatedByString:@"@"] count] > 1) {
			unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:[ALPHA_NUMERIC stringByAppendingString:@".-"]] invertedSet];
		} else {
			unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:[ALPHA_NUMERIC stringByAppendingString:@".!#$%&'*+-/=?^_`{|}~@"]] invertedSet];
		}
		return ([[string componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1);
	}

	return YES;
}  


- (BOOL) textFieldShouldReturn: (UITextField *) textField {

	[textField resignFirstResponder];
	if (textField == self.m_nameTextField)
		[self.m_emailTextField becomeFirstResponder];
	//else
		//[self addNewEntryAction:nil];
	
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{

	[super viewDidLoad];

//    CALayer * layer = self.m_emailTableView.layer;
//    layer.borderWidth = 3.0f;
//    layer.borderColor = [UIColor colorWithRed:104.0f/255.0f green:176.0f/255.0f blue:223.0f/255.0f alpha:1.0].CGColor;
//    layer.cornerRadius = 5.0f;

}
-(void)viewWillAppear:(BOOL)animated
{
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}
 
-(IBAction)goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
}

- (void)dealloc {
    
	self.m_emailTableView = nil;
	self.m_nameTextField = nil;
	self.m_emailTextField = nil;
	self.m_sectionHeaderView = nil;
	self.m_DictOfContactDicts = nil;
	self.m_newlyAddedContactsDict = nil;

    [super dealloc];
}

- (IBAction)removeRow:(id)sender
{
    MoreButton *button = (MoreButton *)sender;
    int row = [[button.data objectForKey:@"index"] intValue];
    
    NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    if ((sortedKeys != nil) && ([sortedKeys count] > row)) {
        NSString * keyStr = [sortedKeys objectAtIndex:row];
        [self.m_DictOfContactDicts removeObjectForKey:keyStr];
    }
    [self.m_emailTableView reloadData];
}


-(IBAction) addNewEntryAction:(id) sender {
    
    [self.m_emailTextField resignFirstResponder];
    [self.m_nameTextField resignFirstResponder];
 
    if ((self.m_emailTextField.text == nil) || ([self.m_emailTextField.text length] <= 0)) {
        
        [self.m_emailTextField becomeFirstResponder];
        
        UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        [av release];
        
        return;
    }
    
    if ((self.m_nameTextField.text == nil) || ([self.m_nameTextField.text length] <= 0)) {
        
        [self.m_nameTextField becomeFirstResponder];
        
        UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        [av release];

        return;
    }

   	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    if ([emailTest evaluateWithObject:self.m_emailTextField.text] == NO) {
		
		[self.m_emailTextField becomeFirstResponder];

        UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:@"Invalid email entered.\nPlease enter valid email\ne.g.  gelofson@fashiongram.com" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        [av release];

		return;
	}
	
    
	for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
        NSMutableDictionary * dict = [self.m_DictOfContactDicts objectForKey:keyStr];
        
        if ([[dict objectForKey:@"emailValue"] caseInsensitiveCompare:self.m_emailTextField.text] == NSOrderedSame) {
            
            UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Entry Exists" message:[NSString stringWithFormat:@"Contact by email address\n%@\nalready exists.",self.m_emailTextField.text] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
            [av release];
            
            return;
        }
        if ([keyStr caseInsensitiveCompare:self.m_nameTextField.text] == NSOrderedSame) {
            
            UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Entry Exists" message:[NSString stringWithFormat:@"Contact by name\n%@\nalready exists.",self.m_nameTextField.text] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
            [av release];
            
            return;
        }

	}
    
	NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:2];
	[dict setObject:self.m_emailTextField.text forKey:@"emailValue"];
	[dict setObject:self.m_nameTextField.text forKey:@"nameValue"];
	[dict setObject:@"1" forKey:@"selectionValue"];

    [self.m_DictOfContactDicts setObject:dict forKey:self.m_nameTextField.text];
    [self.m_newlyAddedContactsDict setObject:dict forKey:self.m_nameTextField.text];
	[dict release];

	self.m_nameTextField.text = nil;
	self.m_emailTextField.text = nil;

    [self.m_emailTableView reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [m_DictOfContactDicts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell * cell = nil;
	static NSString *cellIdentifier= @"AddNewContact_CellIdentifier";
	
	cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell==nil) {
		cell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
        MoreButton *button = [[MoreButton alloc] initWithFrame:CGRectMake(4,22,10,10)];
        [button setAdjustsImageWhenHighlighted:YES];
        [button setImage:[UIImage imageNamed:@"remove-icon"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(removeRow:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 20;
		[cell.contentView addSubview:button];

		UILabel *temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(20,2,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:kHelveticaBoldFont size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:22.0f/255.0f green:22.0f/255.0f blue:22.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"Greg Elofson";
		temp_lblTitle.tag = CONTACT_ENTRY_NAME_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
		temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(20,24,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:kHelveticaBoldFont size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:57./255.0f green:157.0f/255.0f blue:187.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"greg.elofson@gmail.com";
		temp_lblTitle.tag = CONTACT_ENTRY_EMAIL_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
	}
    
    MoreButton *button = (MoreButton *)[cell.contentView viewWithTag:20];
    button.data = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:indexPath.row] forKey:@"index"];
		
	NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
        
        UILabel * lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_NAME_TAG];
        NSString * nameValue = [contactDict objectForKey:@"nameValue"];
        lbl.text = nameValue;
        
        lbl=nil;
        lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_EMAIL_TAG];
        NSString * emailValue = [contactDict objectForKey:@"emailValue"];
        lbl.text = emailValue;

    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 55.0f;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
		if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        	NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
			[self.m_DictOfContactDicts removeObjectForKey:keyStr];
		}
        [self.m_emailTableView reloadData];
	} else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
