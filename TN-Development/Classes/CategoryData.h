//
//  CategoryData.h
//  TinyNews
//
//  Created by Nava Carmon on 29/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryData : NSObject
{
    NSArray *data;
    NSMutableArray *messageIDs;
    NSArray *categoryData;
}

@property (nonatomic, retain) NSArray *data;
@property (nonatomic, retain) NSArray *categoryData;
@property (nonatomic, retain) NSMutableArray *messageIDs;
@property (readonly) NSString *type;
@property (readonly) NSInteger numPages;
@property (readonly) NSInteger totalRecords;

+ (CategoryData *) convertMessageData:(NSDictionary *)messageDataDict;
- (id) initWithData:(NSArray *) aData;
- (id) initWithData:(NSArray *)aData fromList:(BOOL) fromFittingList;
- (void) addDataFromArray:(NSArray *)array;
- (NSDictionary *)wsMessage:(NSInteger)index;
- (NSDictionary *)wsBuzz:(NSInteger)index;
- (NSDictionary *)messageData:(NSInteger) index;
- (NSInteger) indexOfMessage:(NSInteger)messageId;
@end
