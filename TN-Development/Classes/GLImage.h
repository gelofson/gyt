//
//  GLImage.h
//  Category on UIImage with basic image filters and scaling.
//
//  Created by Greg Landweber on 5/14/11.
//  Copyright 2011 Gregory D. Landweber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GLImage)

// Saturation: 1.0 is normal, 0.0 is grayscale. Beyond 2.0 distorts the colors.

// Gamma: 1.0 is normal. On logarithmic scale. Reasonable range 0.2 to 5.0.

// Smoothing: 0 is normal. Reasonable range -2 to 2.
// Positive value is the number of times to apply a gaussian blur.
// Negative value is the number of times to apply a sharpen filter.
// Uses 3x3 convolution matrix, implemented by applying two 1D convolutions.

// NOTE: These filters works best on images sized for the screen, not full resolution photos.

// Returns a new UIImage, applying all three filters simultaneously.
- (UIImage *)imageWithSaturation:(float)saturation gamma:(float)gamma smoothing:(int)smoothing;

// Returns a new UIImage, applying filters individually, calling through to method above.
- (UIImage *)imageWithSaturation:(float)saturation;
- (UIImage *)imageWithGamma:(float)gamma;
- (UIImage *)imageWithSmoothing:(int)smoothing;

// Returns a new UIImage, scaling this image to size.
- (UIImage *)imageWithSize:(CGSize)size;

// Returns a new UIImage, scaling down only, maintaining the aspect ratio.
- (UIImage *)scaledDownImageWithMaximumSize:(CGSize)size;

@end
