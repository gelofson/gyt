//
//  GrouponWhereBox.m
//  QNavigator
//
//  Created by softprodigy on 08/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "MGTwitterStatusesParser.h"
#import "MGTwitterXMLParser.h"
#import "GrouponWhereBox.h"
#import "GrouponBuyNowWebViewController.h"
#import "QNavigatorAppDelegate.h"
#import "CustomTopBarView.h"
#import "CategoriesViewController.h"
#import "CategoryModelClass.h"
#import "AuditingClass.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

#import "SBJSON.h"
#import "ATIconDownloader.h"
#import "DBManager.h"
#import "StreetMallMapViewController.h"
#import "TellAFriendViewController.h"
#import "SA_OAuthTwitterEngine.h" 
#import"SA_OAuthTwitterController.h"
#import "ShopbeeFriendsViewController.h"
#import "MGTwitterEngine.h"
#import "TwitterFriendsViewController.h"
#import "AddFriendsMypageViewController.h"
#import "TwitterProcessing.h"
#import "FittingRoomViewController.h"
#import "ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"

//#import"WhereBoxShareNow.h"


#define POP_UP_OFFSET_X 1
#define POP_UP_OFFSET_Y 45

@implementation GrouponWhereBox

@synthesize m_lblBrandName;
@synthesize grouponRedeemLabel1;
@synthesize grouponRedeemLabel2;
@synthesize popUpforShareInfo;
@synthesize tagg;
@synthesize btnBackObj;
@synthesize m_moreDetailView;
@synthesize m_tableView;
@synthesize m_theConnection;
@synthesize m_mallMapConnection;
@synthesize m_isConnectionActive;
@synthesize	m_isMallMapConnectionActive;
@synthesize m_activityIndicator;
@synthesize m_intResponseCode;

@synthesize m_shareView;
@synthesize m_scrollView;
@synthesize m_mutCatResponseData;
@synthesize twitterEngine, tw; 
@synthesize m_FittingRoomView;
@synthesize m_emailFrnds,m_facebookFrnds,m_twitterFrnds,m_shopBeeFrnds;
	/* Bharat: 12/03/11
@synthesize m_btnSendBuyOrNot;
@synthesize m_btnSendIBoughtIt;
*/
@synthesize m_sharing_lable;
@synthesize m_imgShareView;
	/* Bharat: 12/03/11
@synthesize m_publicWishlist;
@synthesize m_privateWishlist;
	*/
@synthesize m_btnCancelWhereBox;
@synthesize m_privateOrPublic;
@synthesize m_BoughtItOrNot;
@synthesize m_productImage;
@synthesize m_DarkGreenBoxView;
@synthesize isFromIndyShopView;

CategoryModelClass *l_objCatModel;
QNavigatorAppDelegate *l_appDelegate;
CLLocation *l_cllocation;
TwitterFriendsViewController *Twitter_obj;
FittingRoomViewController *tempFittingRoom;
LoadingIndicatorView *l_whereboxIndicatorView;

NSString *temp_strCity;
NSString *temp_strState;
NSString *temp_strZip;
NSString *temp_strAdd2;



ShopbeeAPIs *l_request;
int counter;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewWillAppear:(BOOL)animated{
	//counter=0;
	[m_shareView setHidden:YES];
}

- (void)viewDidLoad 
{


	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	
	//delete the category sales images saved in temproray directory on applcation termination
	//NSError **tempErr;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
	//directoryContentsAtPath:tempPath];
	NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	if (onlyJPGs) 
	{	
		for (int i = 0; i < [onlyJPGs count]; i++) {
			//NSLog(@"Directory Count: %i", [onlyJPGs count]);
			NSString *contentsOnly = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [onlyJPGs objectAtIndex:i]];
			[fileManager removeItemAtPath:contentsOnly error:nil];
		}
	}
	//--------------
	
	counter=0;
	[super viewDidLoad];

	m_shareView.frame = CGRectMake(2,20,319,305);
	popUpforShareInfo.frame = CGRectMake(6,90,308,232);

    // Set font for objects defined in XIB here
	[m_lblBrandName setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_TITLEBAR_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_TITLEBAR_FONT_SIZE_KEY]]];
	[grouponRedeemLabel2 setTextColor:[UIColor colorWithRed:249.0f/255.0f green:229.0f/255.0f blue:38.0f/255.0f alpha:1.0]];
    
    
	l_whereboxIndicatorView=[LoadingIndicatorView SharedInstance];
	//[popUpforShareInfo.layer setCornerRadius:8.0f ];
	//popUpforShareInfo.hidden=YES;
	//m_DarkGreenBoxView=[[UIView alloc]init];
	[m_DarkGreenBoxView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"darkgreenbox"]]];
	//m_DarkGreenBoxView=[[UIView alloc]init];
	//m_DarkGreenBoxView.frame=CGRectMake(0, 0, 310, 277);
	m_mutCatResponseData=[[NSMutableData alloc] init];
	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bgNewWhereBox"]]];
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	/* Bharat: 12/03/11
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	*/
	[m_sharing_lable setHidden:YES];
	
	QNavigatorAppDelegate *obj=(QNavigatorAppDelegate*)[[UIApplication sharedApplication]delegate];
	NSLog(@"getFollowers   :  %@",[_engine getFollowersIncludingCurrentStatus:YES]);
	
	 // to download the category images and save them in the documents directory
	if (isFromIndyShopView==YES) // in case the user is from indyshop view we start downloading the indyshop images 
	{
		if ([l_appDelegate.m_arrIndyShopData count]>0) 
		{
			NSString *tmp_urlString=[[l_appDelegate.m_arrIndyShopData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
			[self sendRequestToLoadImages:tmp_urlString withContinueToNext:YES];
			
		}
		
	}
	else // else download images for the categories  
	{
		if ([l_appDelegate.m_arrCategorySalesData count]>0) 
		{
			NSString *tmp_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl2"];
            //Bharat: 11/10/2011: Added check to make sure image2 URL is not empty string.
			if ((tmp_urlString == nil) || ([tmp_urlString length] < 1)) {
				NSLog(@"GrouponWhereBox:m_strImageUrl2 is nil (Line 184)");
				tmp_urlString = [[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
			} else {
				NSLog(@"GrouponWhereBox:m_strImageUrl2=[%@] (Line 184)",tmp_urlString);
			}
			[self sendRequestToLoadImages:tmp_urlString withContinueToNext:YES];
			
		}
		
	}
	
				
	[self whereBtnPressed:tagg];
	
}

-(void)viewDidAppear:(BOOL)animated
{
}
-(void)viewWillDisappear:(BOOL)animated
{
	if (m_isConnectionActive)
	{
		[m_theConnection cancel];
		m_isConnectionActive=FALSE;
	}
	
	//remove the saved images to avoid inconsistency
		
	
}

#pragma mark -
#pragma mark Callback methods
-(void)requestCallBackMethodforWishList:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	//NSLog(@"Friend added. response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	[l_whereboxIndicatorView stopLoadingView];
	
	
	if([responseCode intValue]==200)
	{
		
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[m_shareView setHidden:YES];
		[dataReloadAlert show];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]==500)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Advertisement Expired!" message:@"Advertisement doesn't exists." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	
}


-(void)requestCallBackMethodForSendBuyItOrNot:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	[l_whereboxIndicatorView stopLoadingView];
	
	
	if([responseCode intValue]==200)
	{
		
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[dataReloadAlert show];
		[m_shareView setHidden:YES];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	
}



-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	[l_whereboxIndicatorView stopLoadingView];
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
    if ([responseCode intValue]==200) 
	{
        NSMutableArray *tempArray=[[tempString JSONValue] mutableCopy];
        
		if ([tempArray count]>0) 
		{
			ShareSalesWithFriends *tmp_obj=[[ShareSalesWithFriends alloc]init];
			tmp_obj.m_catObjModel=l_objCatModel;
			tmp_obj.m_bigSalesModel=nil;
			tmp_obj.m_friendsArray=tempArray;
			[self.navigationController pushViewController:tmp_obj animated:YES];	
			[tmp_obj release];
			tmp_obj=nil;
		}
		else 
		{
			UIAlertView *Tmp_alert=[[ UIAlertView alloc] initWithTitle:@"Oops!" message:@"No FashionGram friends found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[Tmp_alert show];
			[Tmp_alert release];
			Tmp_alert=nil;			
		}
        [tempArray release];

	}
	
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	[tempString release];
	tempString=nil;
	
}



-(IBAction)btnCloseOnThreeButtonViewAction
{
	[m_shareView setHidden:YES];
}


-(IBAction)btnTellAFriendAction
{
	[self performSelector:@selector(showPicker)];
}


#pragma mark -
#pragma mark mail composer methods



-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}



// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	[picker setSubject:@"Share \"FashionGram\""];
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];

	
	NSMutableString *emailBody;//=[[NSMutableString alloc] init];
	emailBody=[NSMutableString stringWithFormat:@"%@",l_objCatModel.m_strBrandName];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",l_objCatModel.m_strTitle];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",l_objCatModel.m_strDiscount];
	[emailBody appendFormat:@"%@",l_objCatModel.m_strAddress];
	
	
	//	working
	//
	UIImage *logoPic = [UIImage imageNamed:@"logo_for_share"];
	NSData *imageD = UIImageJPEGRepresentation(logoPic, 1);
	[picker addAttachmentData:imageD mimeType:@"image/jpg" fileName:@"logo_for_share"];
	
	[picker setToRecipients:toRecipients];
	[toRecipients release];
	toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:NO];
	
    [picker release];
	picker=nil;
	
}	


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			[m_shareView setHidden:YES];
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			//------------------------------------------------------------
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
				
			}
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"your message sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert setTag:2];
			
			[alert show];
			[alert release];
			
			[m_shareView setHidden:YES];

			//------------------------------------------------------------
					
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	[self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


#pragma mark -
#pragma mark custom methods

-(IBAction)btnShareInfoAction:(id)sender
{
	if(![popUpforShareInfo superview])
	{
		[self.view	addSubview:popUpforShareInfo];
		//[popUpforShareInfo setTransform:CGAffineTransformMakeTranslation(6,90)];
	}
	else
	{
		[popUpforShareInfo setHidden:NO];
		[self.view bringSubviewToFront:popUpforShareInfo];
	}
}

-(void)whereBtnPressed:(int)tag
{
	
	//for(UIView *tempView in m_scrollView.subviews)
	//{
		for (int i=1000; i<=1018; i++) 
		{
			if (([m_scrollView viewWithTag:i] != nil) && ([[m_scrollView viewWithTag:i] superview] != nil)) {
				[[m_scrollView viewWithTag:i] removeFromSuperview];
				//[[m_scrollView viewWithTag:i] release];
				//[m_scrollView viewWithTag:i=nil;
			}	
		}
		
	//}
	//for(UIView *tempView in popUpforShareInfo.subviews)
	//{
		for (int i=1008; i<=1017; i++) 
		{
			if (([popUpforShareInfo viewWithTag:i] != nil) && ([[popUpforShareInfo viewWithTag:i] superview] != nil)) {
				[[popUpforShareInfo viewWithTag:i] removeFromSuperview];
				//[[popUpforShareInfo viewWithTag:i] release];
				//[m_scrollView viewWithTag:i=nil;
			}	
		}
		
	//}
	if(l_appDelegate.m_intCurrentView==1)
	{	
		if ((tag >= 0) && (tag < [l_appDelegate.m_arrIndyShopData count])) {
			l_objCatModel= [l_appDelegate.m_arrIndyShopData objectAtIndex:tag];
			l_appDelegate.m_intIndyShopIndex=tag;
		} else {
			return;
		}
	}
	else
	{
		if ((tag >= 0) && (tag < [l_appDelegate.m_arrCategorySalesData count])) {
			l_objCatModel= [l_appDelegate.m_arrCategorySalesData objectAtIndex:tag];
			l_appDelegate.m_intCategorySalesIndex=tag;
		} else {
			return;
		}
	}
	//------------------------------------------------------------
	AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
	[temp_objAudit initializeMembers];
	NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
	[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
	
	if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
	{
		NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
								 @"Where button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",l_objCatModel.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
		[temp_objAudit.m_arrAuditData addObject:temp_dict];
	}
	
	if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
	{
		[temp_objAudit sendRequestToSubmitAuditData];
	}
	//------------------------------------------------------------
	
	[m_tableView setUserInteractionEnabled:FALSE];
	
	//UIImageView *tempDarkGreenBox=[[UIImageView alloc]initWithFrame:CGRectMake(5,5,312,277)];
//	[tempDarkGreenBox setImage:[UIImage imageNamed:@"darkgreenbox.png"]];
//	tempDarkGreenBox.contentMode=UIViewContentModeScaleAspectFit;
	
	
	//---------------------------scrollview------------------------------------
	m_scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(5,42,310,276)];
		
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	

	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	UISwipeGestureRecognizer *recogniser=[[UISwipeGestureRecognizer alloc] init]; // add a swipe gesture recogniser
	recogniser.direction=UISwipeGestureRecognizerDirectionLeft;
	[recogniser addTarget:self action:@selector(handleSwipeFrom:)];
	[m_scrollView addGestureRecognizer:recogniser];
	[recogniser release];
	recogniser=nil;
	
	UISwipeGestureRecognizer *recogniser1=[[UISwipeGestureRecognizer alloc] init];
	recogniser1.direction=UISwipeGestureRecognizerDirectionRight;
	[recogniser1 addTarget:self action:@selector(handleSwipeFrom:)];
	[m_scrollView addGestureRecognizer:recogniser1];
	[recogniser1 release];
	recogniser1=nil;
	
	CGFloat yOffset = 2.0f;

	UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(12,yOffset,286,44)];
	temp_lblDiscount.tag=1001;
	//temp_lblDiscount.lineBreakMode = UILineBreakModeWordWrap;
	[temp_lblDiscount setText:l_objCatModel.m_strDiscount]; //@"10% discount"];
	[temp_lblDiscount setTextAlignment:UITextAlignmentLeft];
	temp_lblDiscount.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblDiscount.numberOfLines = 0;
	[temp_lblDiscount setTextColor:[UIColor darkGrayColor]];
	[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
	//Bharat : 11/21:11
	//[temp_lblDiscount setFont:[UIFont fontWithName:kFontName size:14]];
	[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
	
	[m_scrollView addSubview:temp_lblDiscount];
	[temp_lblDiscount sizeToFit];
	yOffset += temp_lblDiscount.frame.size.height;
	[temp_lblDiscount release];
	temp_lblDiscount=nil;


	yOffset += 10.0f;
	//UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(140,7,157,157)];
	//m_ProductImageView=[[PhotosLoadingView alloc]initWithFrame:CGRectMake(12,48,220,132) ];
	m_ProductImageView=[[PhotosLoadingView alloc]initWithFrame:CGRectMake(12,yOffset,220,132) ];
	m_ProductImageView.tag=1000;
	m_ProductImageView.contentMode=UIViewContentModeScaleAspectFit;
	[m_scrollView addSubview:m_ProductImageView];
	//[temp_imgView setImage:[UIImage imageNamed:@"loading.png"]];
	if (isFromIndyShopView==YES) 
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tag];
		
		NSFileManager *temp_fileManger=[NSFileManager defaultManager];
		
		NSString *tmp_urlString=[[l_appDelegate.m_arrIndyShopData objectAtIndex:tag] valueForKey:@"m_strImageUrl"];	
		NSURL *tmp_imageUrl=[[NSURL alloc] initWithString:tmp_urlString];
		
		
		NSLog(@"%@",tmpPath);
		
		if ([temp_fileManger fileExistsAtPath:tmpPath])
		{
			[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:[UIImage imageWithContentsOfFile:tmpPath]];
		}
		else
		{
			[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:nil];
		}
		
		
		
	}
	else 
	{
		
	

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tag];
		
	NSFileManager *temp_fileManger=[NSFileManager defaultManager];
		
	NSString *tmp_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:tag] valueForKey:@"m_strImageUrl2"];
        //Bharat: 11/10/2011: Added check to make sure image2 URL is not empty string.
		if ((tmp_urlString == nil) || ([tmp_urlString length] < 1)) {
			NSLog(@"GrouponWhereBox:m_strImageUrl2 is nil (line 672)");
			tmp_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:tag] valueForKey:@"m_strImageUrl"];
		} else {
			NSLog(@"GrouponWhereBox:m_strImageUrl2=[%@] (line 672)",tmp_urlString);
		}
	NSURL *tmp_imageUrl=[[NSURL alloc] initWithString:tmp_urlString];

	// Bharat: poor coding. Better to update same variable with backup values
	//NSString *backup_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:tag] valueForKey:@"m_strImageUrl"];
	//NSURL *backup_imageUrl=[[NSURL alloc] initWithString:backup_urlString];


	NSLog(@"%@",tmpPath);
	
	// Check if temp image file exists and has content.
	if ([temp_fileManger fileExistsAtPath:tmpPath] && [UIImage imageWithContentsOfFile:tmpPath])
	{
		[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:[UIImage imageWithContentsOfFile:tmpPath]];
	}
	else
	{
		[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:nil];
	}
	
	//[m_scrollView addSubview:m_ProductImageView];
	}
//	[self.view addSubview:temp_imgView];


//	UILabel *temp_lblBrandName=[[UILabel alloc]initWithFrame:CGRectMake(107,15,195,20)];
//	

	NSMutableArray * arr = [[NSMutableArray alloc] initWithCapacity:3];
	if ([[Context getInstance] getFormatedGrouponAdRdeemTimingMessageWithStart:l_objCatModel.m_strStartRedemptionAt 
			withEnd:l_objCatModel.m_strEndRedemptionAt saveTo:arr] == YES) {
		grouponRedeemLabel1.text = [arr objectAtIndex:0];
		grouponRedeemLabel2.text = [arr objectAtIndex:1];
	} else {
		grouponRedeemLabel1.text = l_objCatModel.m_strStartRedemptionAt;
		grouponRedeemLabel2.text = l_objCatModel.m_strEndRedemptionAt;
	}
	[arr release];

	if(l_objCatModel.m_strBrandName.length>0)
		[m_lblBrandName setText:l_objCatModel.m_strBrandName];
	else
		[m_lblBrandName setText:@"Brand: N/A"];
	
	
	[m_lblBrandName setTextColor:[UIColor whiteColor]];
//	[m_lblBrandName setBackgroundColor:[UIColor clearColor]];
//	[m_lblBrandName setFont:[UIFont fontWithName:kArialBoldFont size:16]];

//	[m_scrollView addSubview:temp_lblBrandName];
//..	[m_DarkGreenBoxView addSubview:temp_lblBrandName];

//	[temp_lblBrandName release];
//	temp_lblBrandName=nil;
	
	
	//-------calculate distance-------

	NSLog (@"store-redemption-position: lat=[%@], lng=[%@]", l_objCatModel.m_strLatitude, l_objCatModel.m_strLongitude);

	l_cllocation=[[CLLocation alloc]initWithLatitude:[l_objCatModel.m_strLatitude floatValue] longitude:[l_objCatModel.m_strLongitude floatValue]];
	CLLocation *tempCurrentCL=[[CLLocation alloc] initWithLatitude:[l_appDelegate.m_objGetCurrentLocation m_latitude] longitude:[l_appDelegate.m_objGetCurrentLocation m_longitude]];
	NSLog (@"current-user-position: lat=[%f], lng=[%f]", [l_appDelegate.m_objGetCurrentLocation m_latitude], [l_appDelegate.m_objGetCurrentLocation m_longitude]);
	
	double tempDistance=[l_cllocation distanceFromLocation:tempCurrentCL];
	NSLog (@"distance calculated:%f miles",(tempDistance * 0.0006214f));
	
	NSString *temp_strDistance;
	
	if (tempDistance > 400) 
	{
		temp_strDistance=[NSString stringWithFormat:@"%.2f miles away",tempDistance * 0.0006214f];
	}
	else {
		temp_strDistance=[NSString stringWithFormat:@"%.0f steps away",tempDistance];
	}
	
	[l_cllocation release];
	[tempCurrentCL release];
	
	//-----------------------------------------------------------------
	
	UIButton *temp_btnShareIt=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_btnShareIt.tag=1003;
	[temp_btnShareIt setImage:[UIImage imageNamed:@"btn_groupon_shareit"] forState:UIControlStateNormal];
	[temp_btnShareIt addTarget:self action:@selector(shareBtnAction) forControlEvents:UIControlEventTouchUpInside];
	//[temp_btnShareIt setFrame:CGRectMake(245,54,43,36)];
	[temp_btnShareIt setFrame:CGRectMake(245,yOffset+6.0f,43,36)];
	[m_scrollView addSubview:temp_btnShareIt];
	
	UIButton *temp_btnMapIt=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_btnMapIt.tag=1018;
	[temp_btnMapIt setImage:[UIImage imageNamed:@"btn_groupon_mapit"] forState:UIControlStateNormal];
	[temp_btnMapIt addTarget:self action:@selector(btnMapItAction:) forControlEvents:UIControlEventTouchUpInside];
	//[temp_btnMapIt setFrame:CGRectMake(245,96,43,36)];
	[temp_btnMapIt setFrame:CGRectMake(245,yOffset+6.0f+42.0f,43,36)];
	[m_scrollView addSubview:temp_btnMapIt];
	
	
	UIButton *temp_btn_storeinfo=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_btn_storeinfo.tag=1004;
	[temp_btn_storeinfo setImage:[UIImage imageNamed:@"btn_groupon_storeinfo"] forState:UIControlStateNormal];
	[temp_btn_storeinfo addTarget:self action:@selector(btnShareInfoAction:) forControlEvents:UIControlEventTouchUpInside];
	//[temp_btn_storeinfo setFrame:CGRectMake(245,136,43,36)];
	[temp_btn_storeinfo setFrame:CGRectMake(245,yOffset+6.0f+84.0f,43,36)];
	[m_scrollView addSubview:temp_btn_storeinfo];

	
	yOffset += m_ProductImageView.frame.size.height;

	yOffset+= 4.0f;
	//Bharat: 11/22/11: Moving to right , up by 10 pixels, below product image
	//UILabel *temp_lblDistance=[[UILabel alloc]initWithFrame:CGRectMake(10,176,140,20)];
	//UILabel *temp_lblDistance=[[UILabel alloc]initWithFrame:CGRectMake(12,184,132,20)];
	UILabel *temp_lblDistance=[[UILabel alloc]initWithFrame:CGRectMake(12,yOffset,132,20)];
	temp_lblDistance.tag=1005;
	[temp_lblDistance setText:temp_strDistance];//[m_locationManager distanceToLocation:l_cllocation]];
	//[m_locationManager distanceAndDirectionTo:l_cllocation]];
	//[NSString stringWithFormat:@"(%.2f meters away)",([l_objCatModel.m_strDistance floatValue] * 1609.344f)]];
	[temp_lblDistance setTextColor:[UIColor darkGrayColor]];
	[temp_lblDistance setBackgroundColor:[UIColor clearColor]];
	[temp_lblDistance setFont:[UIFont fontWithName:kFontName size:12]];
	//[m_scrollView addSubview:];
	[m_scrollView addSubview:temp_lblDistance];

    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
	btnLeft.tag = 1016;
    [btnLeft setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"green arrow_left" ofType:@"png"]] forState:UIControlStateNormal];
    [btnLeft setFrame:CGRectMake(209.0,yOffset+5, 9.0,11.0)];
    [btnLeft addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
	[m_scrollView addSubview:btnLeft];

    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
	btnRight.tag = 1017;
    [btnRight setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"green arrow_right" ofType:@"png"]] forState:UIControlStateNormal];
    [btnRight setFrame:CGRectMake(220.0,yOffset+5, 9.0,11.0)];
    [btnRight addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
	[m_scrollView addSubview:btnRight];
	
	yOffset += temp_lblDistance.frame.size.height;
	[temp_lblDistance release];
	temp_lblDistance=nil;
	//--------------------------------------------------------------------
	

	yOffset += 10.0f;
	//Bharat: 11/22/11: Moving down and expanding the box
	//UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,82,120,40)];
	//UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(12,22,300,22)];
	UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(12,yOffset,286,44)];
	temp_lblTitle.tag=1006;
	temp_lblTitle.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblTitle.numberOfLines = 0;
	[temp_lblTitle setText:l_objCatModel.m_strTitle];
	//[temp_lblTitle setTextColor:[UIColor blackColor]];
	[temp_lblTitle setTextColor:[UIColor colorWithRed:53.0f/255.0f green:135.0f/255.0f blue:151.0f/255.0f alpha:1.0]];
	[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
	//[temp_lblTitle setFont:[UIFont fontWithName:kArialBoldFont size:12]];
	[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];

	//[m_moreDetailView addSubview:temp_lblTitle];
	[m_scrollView addSubview:temp_lblTitle];
	[temp_lblTitle sizeToFit];
	yOffset+= temp_lblTitle.frame.size.height;
	[temp_lblTitle release];
	temp_lblTitle=nil;
	
	yOffset+= 5.0f;
	//UITextView *textViewDescription = [[UITextView alloc] initWithFrame:CGRectMake(12, 200, 284, 70)];
	UITextView *textViewDescription = [[UITextView alloc] initWithFrame:CGRectMake(12, yOffset, 284, 70)];
	textViewDescription.tag=1007;
	textViewDescription.scrollEnabled = NO;
	textViewDescription.backgroundColor=[UIColor clearColor];
	textViewDescription.userInteractionEnabled = NO;
	textViewDescription.textColor=[UIColor blackColor];
	//[textViewDescription setFont:[UIFont fontWithName:kFontName size:12]];
	[textViewDescription setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_DESCRIPTIONLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_DESCRIPTIONLABEL_FONT_SIZE_KEY]]];
	textViewDescription.text = l_objCatModel.m_strOverview;
	[m_scrollView addSubview:textViewDescription];
	
		
	[textViewDescription setFrame:CGRectMake(10, yOffset, 290, textViewDescription.contentSize.height)];
	[textViewDescription setFrame:CGRectMake(10, yOffset, 290, textViewDescription.contentSize.height)];
	
	[m_scrollView setContentSize:CGSizeMake(310, yOffset + textViewDescription.frame.size.height)];
	[m_scrollView setContentSize:CGSizeMake(310, yOffset + textViewDescription.frame.size.height)];
	
	[textViewDescription release];
	textViewDescription=nil;

	
//	UILabel *temp_lblDescription=[[UILabel alloc]initWithFrame:CGRectMake(10,200,290,120)];
//	temp_lblDescription.lineBreakMode = UILineBreakModeWordWrap;
//	temp_lblDescription.numberOfLines = 0;
//	[temp_lblDescription setText:l_objCatModel.m_strDescription];
//	[temp_lblDescription setTextColor:[UIColor whiteColor]];
//	[temp_lblDescription setBackgroundColor:[UIColor grayColor]];
//	[temp_lblDescription setFont:[UIFont fontWithName:kFontName size:12]];
//	[m_scrollView addSubview:temp_lblDescription];
	
	
	
	UIImageView *temp_imgViewAddress=[[UIImageView alloc]initWithFrame:CGRectMake(12, 12, 20, 19)];
	temp_imgViewAddress.tag=1008;
	temp_imgViewAddress.image=[UIImage imageNamed:@"groupon_address icon"];
	//..[m_DarkGreenBoxView addSubview:temp_imgViewAddress];
	[popUpforShareInfo addSubview:temp_imgViewAddress];
	//[temp_imgViewAddress release];
//	temp_imgViewAddress=nil;
	
	UILabel *temp_lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(40,12,240,60)];
	temp_lblAddress.tag=1009;
	[temp_lblAddress setText:l_objCatModel.m_strAddress];
	[temp_lblAddress setTextColor:[UIColor whiteColor]];
	temp_lblAddress.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblAddress.textAlignment = UITextAlignmentLeft;
	[temp_lblAddress setNumberOfLines:0];
	[temp_lblAddress setBackgroundColor:[UIColor clearColor]];
	//[temp_lblAddress setFont:[UIFont fontWithName:kFontName size:13]];
	[temp_lblAddress setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
	[popUpforShareInfo addSubview:temp_lblAddress];
	[temp_lblAddress sizeToFit];

	[temp_lblAddress release];
	temp_lblAddress=nil;
	
	UIImageView *temp_imgViewPhone=[[UIImageView alloc]initWithFrame:CGRectMake(12, 74, 18, 18)];
	temp_imgViewPhone.image=[UIImage imageNamed:@"groupon_phone icon"];
	temp_imgViewPhone.tag=1010;
//..	[m_DarkGreenBoxView addSubview:temp_imgViewPhone];

	[popUpforShareInfo addSubview:temp_imgViewPhone];
	//[temp_imgViewPhone release];
//	temp_imgViewPhone=nil;
	
	UILabel *temp_lblPhone=[[UILabel alloc]initWithFrame:CGRectMake(40,74,240,20)];
	temp_lblPhone.tag=1011;
	if(l_objCatModel.m_strPhoneNo.length>0)
	{
		[temp_lblPhone setText:l_objCatModel.m_strPhoneNo];
	}
	else 
	{
		[temp_lblPhone setText:@""];
	}
	
	[temp_lblPhone setTextColor:[UIColor whiteColor]];
	[temp_lblPhone setBackgroundColor:[UIColor clearColor]];
	//[temp_lblPhone setFont:[UIFont fontWithName:kFontName size:13]];
	[temp_lblPhone setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
	temp_lblPhone.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblPhone.textAlignment = UITextAlignmentLeft;
	[temp_lblPhone setNumberOfLines:0];
	[popUpforShareInfo addSubview:temp_lblPhone];
	[temp_lblPhone sizeToFit];
	[temp_lblPhone release];
	temp_lblPhone=nil;
	
	UIImageView *temp_imgViewHours=[[UIImageView alloc]initWithFrame:CGRectMake(12, 118, 16, 16)];
	temp_imgViewHours.tag=1012;
	temp_imgViewHours.image=[UIImage imageNamed:@"groupon_hour"];
//..	[m_DarkGreenBoxView addSubview:temp_imgViewHours];
	[popUpforShareInfo addSubview:temp_imgViewHours];
	[temp_imgViewHours release];
	temp_imgViewHours=nil;
	
	UILabel *temp_lblHours=[[UILabel alloc]initWithFrame:CGRectMake(40,118,240,60)];
	temp_lblHours.tag=1013;
	if (l_objCatModel.m_strStoreHours.length>0)
	{
		[temp_lblHours setText:l_objCatModel.m_strStoreHours];
	}
	else 
	{
		[temp_lblHours setText:[NSString stringWithFormat:@"%@ %@", 
			grouponRedeemLabel1.text, grouponRedeemLabel2.text]];
	}
	
	[temp_lblHours setTextColor:[UIColor whiteColor]];
	[temp_lblHours setBackgroundColor:[UIColor clearColor]];
	//[temp_lblHours setFont:[UIFont fontWithName:kFontName size:13]];
	[temp_lblHours setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
//..	[m_DarkGreenBoxView addSubview:temp_lblHours];
	
	temp_lblHours.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblHours.textAlignment = UITextAlignmentLeft;
	[temp_lblHours setNumberOfLines:0];
	[popUpforShareInfo addSubview:temp_lblHours];
	[temp_lblHours sizeToFit];
	[temp_lblHours release];
	temp_lblHours=nil;
	
	UIButton *temp_closeButton=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_closeButton.tag=1014;
	[temp_closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
	[temp_closeButton addTarget:self action:@selector(btnClosePopUpforShareInfo:) forControlEvents:UIControlEventTouchUpInside];
	[temp_closeButton setFrame:CGRectMake(261,-11,50,50)];
	[popUpforShareInfo addSubview:temp_closeButton];
	
	
	UIImageView *temp_imgViewWebsite=[[UIImageView alloc]initWithFrame:CGRectMake(12, 194, 19, 17)];
	temp_imgViewWebsite.tag=1015;
	temp_imgViewWebsite.image=[UIImage imageNamed:@"groupon_website"];
//..	[m_DarkGreenBoxView addSubview:temp_imgViewWebsite];
	[popUpforShareInfo addSubview:temp_imgViewWebsite];
	[temp_imgViewWebsite release];
	temp_imgViewWebsite=nil;
	
	UILabel *temp_lblWebsite=[[UILabel alloc]initWithFrame:CGRectMake(40,194,240,20)];
	temp_lblWebsite.tag=1016;
	if (l_objCatModel.m_strStoreWebsite.length>0)
	{
		[temp_lblWebsite setText:l_objCatModel.m_strStoreWebsite];
	}
	else 
	{
		[temp_lblWebsite setText:@""];
	}
	
	[temp_lblWebsite setTextColor:[UIColor whiteColor]];
	[temp_lblWebsite setBackgroundColor:[UIColor clearColor]];
	//[temp_lblWebsite setFont:[UIFont fontWithName:kFontName size:13]];
	[temp_lblWebsite setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
//..	[m_DarkGreenBoxView addSubview:temp_lblWebsite];
	
	temp_lblWebsite.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblWebsite.textAlignment = UITextAlignmentLeft;
	[temp_lblWebsite setNumberOfLines:0];
	[popUpforShareInfo addSubview:temp_lblWebsite];
	[temp_lblWebsite sizeToFit];
	[temp_lblWebsite release];
	temp_lblWebsite=nil;
	
	//UITextView *temp_txtViewOverview=[[UITextView alloc]init]; 
	//	[temp_txtViewOverview setTextColor:[UIColor whiteColor]];
	//	[temp_txtViewOverview setFont:[UIFont fontWithName:kFontName size:13]];
	//	[temp_txtViewOverview setBackgroundColor:[UIColor clearColor]];
	//	[temp_txtViewOverview setEditable:FALSE];
	//	[temp_txtViewOverview setScrollEnabled:FALSE];
	//	[m_scrollView addSubview:temp_txtViewOverview];
	//...	[m_DarkGreenBoxView addSubview:temp_txtViewOverview];
	//[temp_txtViewOverview setFrame:CGRectMake(10,180,270,temp_txtViewOverview.contentSize.height)];
	//	[temp_txtViewOverview setFrame:CGRectMake(10,180,270,temp_txtViewOverview.contentSize.height)];
	//	
	//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width, m_scrollView.frame.size.height+ temp_txtViewOverview.contentSize.height);//-50);
	//	[temp_txtViewOverview release];
	//	temp_txtViewOverview=nil;
	//[m_DarkGreenBoxView addSubview:m_scrollView];
	//[m_scrollView addSubview:m_DarkGreenBoxView];

	[self.view addSubview:m_scrollView];
	
	/*// To add the arrow image on the scroll view 
	UIImageView *tmp_arrowImageView=[[UIImageView alloc] initWithFrame:CGRectMake(290,298, 17,17)];
	tmp_arrowImageView.image=[UIImage imageNamed:@"Arrow.png"];
	[self.view addSubview:tmp_arrowImageView];
	[tmp_arrowImageView release];
	tmp_arrowImageView = nil;*/
    
	//[m_scrollView release];
	//	m_scrollView=nil;

}

-(void)leftClick
{
    [self moveLeft];    
}

-(void)rightClick
{
    [self moveRight];
}

-(IBAction)btnClosePopUpforShareInfo:(id)sender
{
	[popUpforShareInfo setHidden:YES];
}
-(IBAction) btnFindTwitterFrnd
{
	TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
	temp_twitterPross.m_addFrndMypageVC=self;
	[temp_twitterPross btnTellFriendByTwitterAction:5];	
}	


-(void)btnMapItAction:(id)sender
{
	StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
	temp_streetMapViewCtrl.m_isShowMallMapTab=FALSE;	
	
	[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
	[temp_streetMapViewCtrl release];
	temp_streetMapViewCtrl=nil;
	
	[CATransaction begin];
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFromLeft;
	animation.duration = 0.6;
	//animation.delegate=self;
	[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
	[CATransaction commit];
		
}
-(void)checkForMallMapExistence
{
	if(m_isMallMapConnectionActive)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_isMallMapConnectionActive=FALSE;
		[m_mallMapConnection cancel];
	}
	
	 //represents other category sales view i.e. shoes, bags etc.
			
		if(l_appDelegate.m_intCurrentView==1)
		{
		
			if(l_appDelegate.m_arrIndyShopData.count > 0 && l_appDelegate.m_intIndyShopIndex!=-1) //l_appDelegate.m_intCategorySalesIndex!=-1)
			{
				CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrIndyShopData objectAtIndex:l_appDelegate.m_intIndyShopIndex];		
				[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];	
			}
		}
		else
		{
			if(l_appDelegate.m_arrCategorySalesData.count > 0 && l_appDelegate.m_intCategorySalesIndex!=-1)
			{
				CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
				[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];	
			}
		}

	
		
}

-(void)sendRequestToCheckMallMap:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[[LoadingIndicatorView SharedInstance]startLoadingView:self];
		//[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if (m_isMallMapConnectionActive==TRUE)
		{
			[m_mallMapConnection cancel];
		}
		
		m_mallMapConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
		
		[m_mallMapConnection start];
		m_isMallMapConnectionActive=TRUE;
		
		if(m_mallMapConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		m_isMallMapConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		//Bharat: 11/10/2011: On image download failure, the message for runway is being automatically generated
		// due to the delegate being set to 'self'.
		// Setting it 
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}



-(IBAction)back
{
	CategoriesViewController *categories_obj=[[CategoriesViewController alloc] init];
	[self.navigationController popViewControllerAnimated:YES];
	[categories_obj release];
	categories_obj=nil;
}

-(IBAction)shareBtnAction
{
	/*
US236: TA74 changes on hold for now, till Groupon share workflow is finalised
	WhereBoxShareNow * vc = [[WhereBoxShareNow alloc] init];
	vc.photoImage = m_ProductImageView.imageView.image;
	vc.m_intInputType = INPUT_TYPE_GROUPON_ADS;
	vc.m_catObjModel = l_objCatModel;

	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
	*/

	[m_emailFrnds setHidden:NO];
	[m_facebookFrnds setHidden:NO];
	[m_twitterFrnds setHidden:NO];
	[m_shopBeeFrnds setHidden:NO];
	
	/* Bharat: 12/03/11
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	
	[m_publicWishlist setHidden:YES];
	[m_privateWishlist setHidden:YES];
	*/
	[m_btnCancelWhereBox setHidden:YES];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"groupon_pop box"]];
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else {
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}

}

-(IBAction)btnTellFriendByEmailAction:(id)sender
{
	[self btnTellAFriendAction];
}

-(IBAction)btnTellShopBeeFriendsAction:(id)sender
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	[l_whereboxIndicatorView startLoadingView:self];
	[l_request getFriendsList:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID loginId:customerID];
	[l_request release];
	l_request=nil;
	
}
-(IBAction)btnFittingRoomViewAction
{
	
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	
	/* Bharat: 12/03/11
	[m_btnSendBuyOrNot setHidden:NO];
	[m_btnSendIBoughtIt setHidden:NO];
	[m_publicWishlist setHidden:YES];
	[m_privateWishlist setHidden:YES];
	*/
	[m_btnCancelWhereBox setHidden:NO];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_fittingroom-backgound"]];
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else 
	{
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
	
}

-(IBAction)btnWishListViewAction:(id)sender
{
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	
	/* Bharat: 12/03/11
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	[m_publicWishlist setHidden:NO];
	[m_privateWishlist setHidden:NO];
	*/
	[m_btnCancelWhereBox setHidden:NO];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_wishlist-background"]];
	
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else {
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
}

-(IBAction)btnSendIBoughtItAction
{
	m_BoughtItOrNot=@"   I bought it";
	//if(tempFittingRoom)
//	{
//		[tempFittingRoom release];
//		tempFittingRoom = nil;
//	}
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	
	tempFittingRoom.m_sendBuy=@"   I bought it";
	tempFittingRoom.m_typeLabel=@"I bought it";
	
	
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Would you like to leave a comment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
	[dataReloadAlert show];
	
}
-(IBAction)btnSendBuyOrNotAction
{
	m_BoughtItOrNot=@"   Buy or not";
	//if(tempFittingRoom)
//	{
//		[tempFittingRoom release];
//		tempFittingRoom = nil;
//	}
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	tempFittingRoom.m_sendBuy=@"   Buy or not";
	tempFittingRoom.m_typeLabel=@"Buy or not";
	
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Would you like to leave a comment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
	[dataReloadAlert show];
}



-(IBAction)btnPublicAction
{
	m_privateOrPublic=@"public";
	[self performSelector:@selector(addToWishList:)];
	
}	
-(IBAction)btnPrivateAction
{
	m_privateOrPublic=@"private";
	[self performSelector:@selector(addToWishList:)];
	
}


-(IBAction)addToWishList:(id)sender
{
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",l_objCatModel.m_strProductName,@"comments",m_privateOrPublic,@"visibility",nil];
	NSLog(@"%@",dic);
	[l_whereboxIndicatorView startLoadingView:self];
	[l_request addToWishList:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:dic];
	l_request=nil;
}
#pragma mark swipe methods
- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	[popUpforShareInfo setHidden:YES];
	[m_shareView setHidden:YES];
	if(recognizer.direction==UISwipeGestureRecognizerDirectionRight)
	{
		[self LeftMovement];
	}
	else
	{
		[self RightMovement];
	}
	
}

#pragma mark left swipe action

-(void)moveLeft
{
    if (tagg>0) 
	{
		tagg--;
		
	}
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tagg];
	NSLog(@"GrouponWhereBox:moveLeft: imagePath=[%@]",tmpPath);
	
	//NSFileManager *tempFileManager=[NSFileManager defaultManager];
	m_productImage=[UIImage imageWithContentsOfFile:tmpPath];
	[self whereBtnPressed:tagg];
}

-(IBAction)LeftMovement
{
    [self moveLeft];	
}
#pragma mark right swipe action 

-(void)moveRight
{
    if (isFromIndyShopView==YES) 
	{
		if (tagg<[l_appDelegate.m_arrIndyShopData count]-1) 
		{
			tagg++;
		}
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tagg];
		NSLog(@"GrouponWhereBox:moveRight: imagePath=[%@]",tmpPath);
		//NSFileManager *tempFileManager=[NSFileManager defaultManager];
		m_productImage=[UIImage imageWithContentsOfFile:tmpPath];
		[self whereBtnPressed:tagg];
		
	}
	else 
	{
		if (tagg<([l_appDelegate.m_arrCategorySalesData count]-1)) 
		{
			tagg++;
			
			NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			NSString *documentsDirectory = [paths objectAtIndex:0];
			NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tagg];
			NSLog(@"GrouponWhereBox:moveRight: imagePath=[%@]",tmpPath);
			//NSFileManager *tempFileManager=[NSFileManager defaultManager];
			m_productImage=[UIImage imageWithContentsOfFile:tmpPath];
			[self whereBtnPressed:tagg];
			
		}
	}

}

-(IBAction)RightMovement
{
    [self moveRight];	
}

-(void)sendRequestToLoadImages:(NSString *)imageUrl withContinueToNext:(BOOL) continueToNextFlag
{
    //NSLog(@"GrouponWhereBox:sendRequestToLoadImages:[%@]",imageUrl);
	if (continueToNextFlag == YES)
		ForPictureDownloading=YES;
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:25.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
		
		
		//[theRequest setHTTPBody:[imageUrl dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if(m_isConnectionActive)
			[m_theConnection cancel];
		
		m_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[m_theConnection start];
		
		m_isConnectionActive=TRUE;
		
		if(m_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		m_isConnectionActive=FALSE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		//Bharat: 11/10/2011: On image download failure, the message for runway is being automatically generated
		// due to the delegate being set to 'self'.
		// Setting it to nil will avoid false triggering.
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}

#pragma mark -
#pragma mark WebService Response Received methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	//NSLog(@"GrouponWhereBox:didReceiveResponse:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	
	if (connection == m_mallMapConnection&&ForPictureDownloading==NO) 
	{
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		long long temp_length=[response expectedContentLength];
		
		[m_mallMapConnection cancel];
		m_isMallMapConnectionActive=FALSE;
		
		StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
		
		if (temp_length ==0)
		{
			temp_streetMapViewCtrl.m_isShowMallMapTab=FALSE;	
		}
		else {
			temp_streetMapViewCtrl.m_isShowMallMapTab=TRUE;	
		}
		
		[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
		[temp_streetMapViewCtrl release];
		temp_streetMapViewCtrl=nil;
		
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromLeft;
		animation.duration = 0.6;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
		[CATransaction commit];
		
	}
	else 
	{
		m_intResponseCode = [httpResponse statusCode];
		NSLog(@"%d",m_intResponseCode);
		[m_mutCatResponseData setLength:0];
	}
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"GrouponWhereBox:didReceiveData:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	[m_mutCatResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"GrouponWhereBox:connectionDidFinishLoading:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	[self.view setUserInteractionEnabled:TRUE];
	m_isConnectionActive=FALSE;
	
	if (m_intResponseCode==401)
	{
		UIAlertView *temp_timeoutAlert=[[UIAlertView alloc]initWithTitle:@"Session timeout!" message:
										@"Session expired. Please login again." delegate:
										nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_timeoutAlert show];
		[temp_timeoutAlert release];
		temp_timeoutAlert=nil;
		
		[m_tableView reloadData];
		
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
		
		
	}
	else if(m_mutCatResponseData!=nil && m_intResponseCode==200&&ForPictureDownloading==NO)
	{	
		
				
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		NSLog(@"GrouponWhereBox::THIS PART OF CODE SHOULD NOT HAVE BEEN CALLED");		
		
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		[self.view setUserInteractionEnabled:TRUE];
		m_isConnectionActive=FALSE;
		
		[m_tableView reloadData];
		
		if(l_appDelegate.m_intCurrentView==1)
		{
			if (l_appDelegate.m_arrIndyShopData.count>0) 
			{ 
					[m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
			}
		}
		else
		{
				if (l_appDelegate.m_arrCategorySalesData.count>0) 
				{
					[m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
				
				}
		}
		
		//[self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:0.5];
		
	}
	else if (ForPictureDownloading==YES) 
	{
		ForPictureDownloading=NO;
		//NSString *temp_string=[[NSString alloc]initWithData:m_mutCatResponseData encoding:NSUTF8StringEncoding];
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
		NSLog(@"Doc Directory______Path_____%@",pathDoc);
		NSMutableString *tempEmail=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",pathDoc,counter];
		NSLog(@"tempEmail %@",tempEmail);
		//[l_indicatorView startLoadingView:self];
		[m_mutCatResponseData writeToFile:tempEmail atomically:YES];
		
		
		if (isFromIndyShopView==YES) 
		{
			if (counter<[l_appDelegate.m_arrIndyShopData count]-1)
			{
				counter++;
				NSString *_URLImageString=[[l_appDelegate.m_arrIndyShopData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
				[self sendRequestToLoadImages:_URLImageString withContinueToNext:YES];
				
			}
		}
		else 
		{
			if (counter<[l_appDelegate.m_arrCategorySalesData count]-1) 
		{
			counter++;
			NSString *URLImageString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl2"];
        	//Bharat: 11/10/2011: Added check to make sure image2 URL is not empty string.
            if ((URLImageString == nil) || ([URLImageString length] < 1)) {

				NSLog(@"GrouponWhereBox:m_strImageUrl2 is nil (URLImageString)");
				URLImageString = [[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
			} else {
				NSLog(@"GrouponWhereBox:m_strImageUrl2=[%@] (URLImageString)",URLImageString);
			}
			[self sendRequestToLoadImages:URLImageString withContinueToNext:YES];
			
		}
				
		}
	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"GrouponWhereBox:didFailWithError:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	m_isConnectionActive=FALSE;
	m_isMallMapConnectionActive=FALSE;
	
	self.view.userInteractionEnabled=TRUE;
	[m_tableView reloadData];
	
	//Bharat: 11/10/2011: On image download failure, the message for runway is being automatically generated
	// due to the delegate being set to 'self'.
	// Setting it to nil will avoid false triggering.
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
	
	//inform the user
    //NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	//NSLog(@"Exit :didFailWithError");
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark Facebookmethods

-(IBAction)ShopbeeFriends
{

	ShopbeeFriendsViewController *shopbee_obj=[[ShopbeeFriendsViewController alloc]init];
	[self.navigationController pushViewController:shopbee_obj animated:YES];
	[shopbee_obj release];
	shopbee_obj=nil;
}

- (IBAction)clickButtonTellFriendsByFacebook:(id)sender {
#ifdef OLD_FB_SDK
	if (l_appDelegate.m_session == nil) {
		l_appDelegate.m_session = [FBSession sessionForApplication:_APP_KEY 
											  secret:_SECRET_KEY delegate:self];
	}
	
	localFBSession = [FBSession session];
	
	if (fbLoginDialog == nil) {
		fbLoginDialog = [[[FBLoginDialog alloc] initWithSession:localFBSession] autorelease];
	}
    [fbLoginDialog show];
#endif
}

#ifdef OLD_FB_SDK
- (void)session:(FBSession*)session didLogin:(FBUID)uid {
	localFBSession =session;
    NSLog(@"User with id %lld logged in.", uid);
	[self getFacebookName];
}

- (void)getFacebookName {
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", localFBSession.uid];
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
//	self.post=YES;
}

- (void)request:(FBRequest*)request didLoad:(id)result {
	if ([request.method isEqualToString:@"facebook.fql.query"]) {
//		NSArray* users = result;
//		NSDictionary* user = [users objectAtIndex:0];
//		NSString* name = [user objectForKey:@"name"];
//		self.username = name;		
		
//		if (self.post) {
			[self postToFacebookWall];
//			self.post = NO;
//		}
	}
}

- (void)postToFacebookWall {
	
	NSLog(@"%@, %@, %@, %@, %@, %@", l_objCatModel.m_strBrandName, l_objCatModel.m_strTitle, l_objCatModel.m_strDiscount, l_objCatModel.m_strAddress, l_objCatModel.m_strImageUrl, l_objCatModel.m_strMallImageUrl);
	NSString *message = [NSString stringWithFormat:@"%@, %@, %@", l_objCatModel.m_strTitle, l_objCatModel.m_strDiscount, l_objCatModel.m_strAddress];
	//NSString *message2 = [NSString stringWithFormat:@"%@\r%@\r%@", l_objCatModel.m_strTitle, l_objCatModel.m_strDiscount, l_objCatModel.m_strAddress];

	FBStreamDialog *dialog = [[[FBStreamDialog alloc] init] autorelease];
	dialog.userMessagePrompt = @"Enter additional comment";
	dialog.delegate = self;
	
	// build attachment with JSONFragment
	NSString *customMessage = message; 
	NSString *postName = l_objCatModel.m_strBrandName; 
	NSString *serverLink = [NSString stringWithFormat:@"http://itunes.apple.com/app/fashiongram/id498116453?mt=8"];
	NSString *imageSrc = l_objCatModel.m_strImageUrl;
	
	NSMutableDictionary *dictionary = [[[NSMutableDictionary alloc] init]autorelease];
	[dictionary setObject:postName forKey:@"name"];
	[dictionary setObject:serverLink forKey:@"href"];
	[dictionary setObject:customMessage forKey:@"description"];
	
	NSMutableDictionary *media = [[[NSMutableDictionary alloc] init]autorelease];
	[media setObject:@"image" forKey:@"type"];
	[media setObject:serverLink forKey:@"href"];
	[media setObject:imageSrc forKey:@"src"];               
	[dictionary setObject:[NSArray arrayWithObject:media] forKey:@"media"];  
	
	dialog.attachment = [dictionary JSONFragment];
	[dialog show];
	
}

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidSucceed:(FBDialog*)dialog {
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message sent" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];	
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidCancel:(FBDialog*)dialog {
	// do nothing
}

- (void)fbDidLogin
{	
	 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Successful" message:@"login Successfull" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	 [alert show];
	 [alert release];	
	 NSLog(@"logged in.....................");
	
}
#pragma mark 
#pragma mark FBRequestDelegate 
/** 
 * Called when an error prevents the request from completing successfully. 
 */ 
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error{ 
	
} 

/** 
 * Called when a request returns and its response has been parsed into an object. 
 * The resulting object may be a dictionary, an array, a string, or a number, depending 
 * on thee format of the API response. 
 */ 
//- (void)request:(FBRequest*)request didLoad:(id)result { 
//   
//	NSLog(@"resultresult:-%@",result);
//			
//}

- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"received response");
}
#endif
#pragma mark -
#pragma mark alertView  

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(alertView.tag!=2)
	{
		
		if(buttonIndex==0)
		{
			if([m_BoughtItOrNot isEqualToString:@"   I bought it"])
			{	
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				l_request=[[ShopbeeAPIs alloc] init];
				NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",@"",@"comments",nil];
				NSLog(@"%@",dic);
				[l_whereboxIndicatorView startLoadingView:self];
				[l_request sendIBoughtItRequest:@selector(requestCallBackMethodForSendBuyItOrNot:responseData:) tempTarget:self tmpDict:dic];
				[l_request release];
				l_request=nil;
			}
			else 
			{
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				l_request=[[ShopbeeAPIs alloc] init];
				NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",@"",@"comments",nil];
				NSLog(@"%@",dic);
				[l_whereboxIndicatorView startLoadingView:self];
				[l_request sendBuyItOrNotRequest:@selector(requestCallBackMethodForSendBuyItOrNot:responseData:) tempTarget:self tmpDict:dic];
				
				[l_request release];
				l_request=nil;
				
				
			}
			[m_shareView setHidden:YES];
		}
		else
		{
			tempFittingRoom.m_Image=m_productImage;
			NSLog(@"this is one");//yes
			tempFittingRoom.m_catObjModel = l_objCatModel;
			tempFittingRoom.m_bigSalesModel = nil;
			[self.navigationController pushViewController:tempFittingRoom animated:YES];
			
			
		}
		
	}
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_lblBrandName = nil;
    self.popUpforShareInfo = nil;
    self.m_DarkGreenBoxView = nil;
    self.m_btnCancelWhereBox = nil;
    self.m_imgShareView = nil;
    self.m_sharing_lable = nil;
    self.m_emailFrnds = nil;
    self.m_facebookFrnds = nil;
    self.m_twitterFrnds = nil;
    self.m_shopBeeFrnds = nil;
	/* Bharat: 12/03/11
    self.m_btnSendBuyOrNot = nil;
    self.m_btnSendIBoughtIt = nil;
    self.m_publicWishlist = nil;
    self.m_privateWishlist = nil;
    */
	self.m_tableView = nil;
    self.m_shareView = nil;
    self.btnBackObj = nil;
    self.m_activityIndicator = nil;
    self.m_FittingRoomView = nil;
    
    [m_cancel release]; m_cancel = nil;
    [popupView release]; popupView = nil;
}

- (void)dealloc {
    
    [btnBackObj release];
    [m_moreDetailView release];
    [m_shareView release];
    [m_FittingRoomView release];
    [m_tableView release];
    [m_scrollView release];
    
   // This was already released.
   [m_mallMapConnection release];
    
    [m_activityIndicator release];
    [m_mutCatResponseData release];
    [_engine release];
    [m_cancel release];
    [popupView release];
    [twitterEngine release];
    [tw release];
    [m_emailFrnds release];
    [m_facebookFrnds release];
    [m_twitterFrnds release];
    [m_shopBeeFrnds release];
	/* Bharat: 12/03/11
    [m_btnSendBuyOrNot release];
    [m_btnSendIBoughtIt release];
    [m_publicWishlist release];
    [m_privateWishlist release];
    */
	[m_sharing_lable release];
    [m_imgShareView release];
    [m_btnCancelWhereBox release];
    [m_view release];
    [m_indicatorView release];
    [m_privateOrPublic release];
    [m_BoughtItOrNot release];
   // [m_productImage release];
    [m_DarkGreenBoxView release];
    [popUpforShareInfo release];
    [m_lblBrandName release];
    
	[grouponRedeemLabel1 release];
    [grouponRedeemLabel2 release];
    
    [super dealloc];
}


-(IBAction) grouponButtonClicked: (id) sender {
	NSLog(@"GrouponWhereBox::grouponButtonClicked:dealUrl=[%@]",l_objCatModel.m_strDealUrl);
	NSString * urlString = @"http://www.groupon.com";
	//if ((l_objCatModel.m_strDealUrl != nil) && ([l_objCatModel.m_strDealUrl length] > 0))
	//	urlString = l_objCatModel.m_strDealUrl;
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[urlString 
        stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]; 
}

-(IBAction) grouponBuyItButtonClicked: (id) sender {
	NSString * urlString = @"http://www.groupon.com";
	if ((l_objCatModel.m_strDealUrl != nil) && ([l_objCatModel.m_strDealUrl length] > 0)) {
		urlString = l_objCatModel.m_strDealUrl;

	}
	NSString * redirectUrlString = [NSString stringWithFormat:
		@"http://www.anrdoezrs.net/click-%@-10804307?url=%@", @"5416733", 
		[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSLog(@"GrouponWhereBox::grouponBuyItButtonClicked:redirectUrlString=[%@]",redirectUrlString);
	
	// Bharat: 12/05/11: Embedded Web view
	//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:redirectUrlString]];

	GrouponBuyNowWebViewController * bnv = [[GrouponBuyNowWebViewController alloc] init];
	bnv.urlString = redirectUrlString;
	bnv.titleString = l_objCatModel.m_strBrandName;
	[self.navigationController pushViewController:bnv animated:YES];
	[bnv release];

}

@end
