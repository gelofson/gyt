//
//  FansInfoViewController.h
//  QNavigator
//
//  Created by softprodigy on 11/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FansInfoViewController : UIViewController {
	
	UILabel *m_headerLabel;
	
	UILabel *m_Label1;
	
	NSInteger Follows;	// for displaying the following/follower properties on the fans info view controller

	UITableView *m_TableView;
	
	NSMutableArray *m_followArray;
	
	NSInteger intUnfollowTrack;
	
	BOOL m_followerBtnClicked;
	
	NSInteger m_FollowersTrack;
}
@property(nonatomic,retain) IBOutlet UILabel *m_headerLabel;

@property(nonatomic,retain) IBOutlet UILabel *m_Label1;

@property(nonatomic,readwrite) NSInteger Follows;

@property(nonatomic,retain) IBOutlet UITableView *m_TableView;

@property(nonatomic,retain) IBOutlet NSArray *m_followArray;

@property BOOL m_followerBtnClicked;

@property NSInteger intUnfollowTrack;

@property NSInteger m_FollowersTrack;

-(IBAction) m_goToBackView;

-(IBAction) UnFollowBtnAction:(id)sender;

@end
