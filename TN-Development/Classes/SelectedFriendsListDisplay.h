//
//  SelectedFriendsListDisplay.h
//  QNavigator
//
//  Created by softprodigy on 12/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGridView.h"
#import "UIGridViewDelegate.h"

@interface SelectedFriendsListDisplay : UIViewController {
	UITableView *m_theTable;
	NSArray *m_FriendsListArray;
	NSInteger LevelTrackSelectedFriend;
	NSMutableArray *m_AcceptedFriendsArray;
    UIView *footerView;
}
@property(nonatomic,retain)UIView *footerView;
@property (nonatomic, retain) IBOutlet UIGridView *table;
@property(nonatomic,retain) IBOutlet UITableView *m_theTable; 

@property(nonatomic,retain) IBOutlet NSArray *m_FriendsListArray;

@property(nonatomic,readwrite) NSInteger LevelTrackSelectedFriend;

-(IBAction)m_goToBackView;

-(IBAction)FilterFriends;

@end
