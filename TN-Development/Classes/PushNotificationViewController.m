//
//  PushNotificationViewController.m
//  QNavigator
//
//  Created by Mac on 9/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PushNotificationViewController.h"
#import "ShopbeeAPIs.h"
#import "QNavigatorAppDelegate.h"
#import "LoadingIndicatorView.h"
#import "Constants.h"
#import "NotificationResponse.h"
#import "Notification.h"
#import "ViewAFriendViewController.h"
#import "FittingRoomMoreViewController.h"
#import "FittingRoomBoughtItViewController.h"
#import "NewsDataManager.h"
#import "CategoryData.h"
#import "DetailPageViewController.h"

#pragma mark - Private Interface
#pragma mark

@interface PushNotificationViewController (PRIVATE)

- (void)loadMoreModal:(BOOL)modal;
- (BOOL)thereAreMoreRecordsToLoad;
- (void)removeItemFromTable:(NSIndexPath *)indexPath;
- (void)selectedProfileNotification:(NSString *)email withName:(NSString *)name andNotificationId:(NSString *)notificationId;
- (void)selectedRunwayNotification:(NSString *)messageIdString;
- (void)updateNotificationTray:(NSString *)notificationId;

@end

#pragma mark -
#pragma mark

@implementation PushNotificationViewController

QNavigatorAppDelegate             * l_appDelegate;
ShopbeeAPIs                       * l_requestObj;
LoadingIndicatorView              * l_indicatorView;
FittingRoomMoreViewController     * l_ViewRequestObj;
FittingRoomBoughtItViewController * l_boughtItRequest;
FittingRoomMoreViewController     * fittingRoomItemsViewController;
FittingRoomBoughtItViewController * boughtItViewController;
NSString                          * _messageIDString;
bool                                _rejectedInvitation;
bool                                _reloadingTable;
    
@synthesize notificationID = _notificationID;
@synthesize userEmail = _userEmail;
@synthesize userName = _userName;
@synthesize selectedIndexPath = _selectedIndexPath;
@synthesize currentType;

- (id) initWithDoneButton:(BOOL) button
{
    self = [self initWithNibName:nil bundle:nil];
    if (self)
    {
        doneButtonVisible = button;
    }
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        m_isLoading = NO;
        doneButtonVisible = NO;
        m_totalRecords = -1;
        m_currentPage = 1;
        m_itemsPerPage = 20; //Load 20 notifications per request
        m_notificationsArray = [[NSMutableArray alloc] init];
        
    }
    return self;
}

-(IBAction) m_goToBackView
{
    [tnApplication showSideMenu];
//	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) doneButtonPressed
{
    [l_appDelegate dismissNotifications];
}

#pragma mark - View Lifecycle
#pragma mark

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [m_notificationsArray removeAllObjects];
    m_totalRecords = 0;
    m_currentPage = 1;
    [self loadMoreModal:NO];
    
}

- (void)viewDidLoad {   
    [super viewDidLoad];
    
    //o_tableView.backgroundColor = [UIColor colorWithRed:0.9059f green:0.8980f blue:0.9020f alpha:1.0f];
    o_tableView.separatorColor = [UIColor colorWithRed:0.7608f green:0.7529f blue:0.7569f alpha:1.0f];
    
    l_indicatorView=[LoadingIndicatorView SharedInstance];

    if (doneButtonVisible)
    {
        doneButton.hidden = NO;
        backButton.hidden = YES;
        doneButtonVisible = NO;
    }

    _rejectedInvitation = NO;
    _notificationID = [[NSString alloc] init];
    _userEmail = [[NSString alloc] init];
    _userName = [[NSString alloc] init];
    _messageIDString = [[NSString alloc] init];
    self.selectedIndexPath = [[NSIndexPath alloc] init];
     
}

- (void)loadMoreModal:(BOOL)modal {
    
    if (m_isLoading)
    {
        return; //If I'm already loading more, wait until that request finishes
    }
    
//    if (![self thereAreMoreRecordsToLoad])
//    {
//        return;
//    }
    
    m_isLoading = YES;
        
    if (modal)
    {
        [[LoadingIndicatorView SharedInstance] startLoadingView:self];   
    }
    
    l_requestObj = [[ShopbeeAPIs alloc] init];
    [l_requestObj getNotificationsWithCallbackTarget:self
                             withCallbackSelector:@selector(onNotificationsResponse:data:) 
                                        forUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"] 
                                       pageNumber:m_currentPage 
                                  numberOfRecords:m_itemsPerPage];
    [l_requestObj release];
    l_requestObj = nil;
    
}

- (BOOL)thereAreMoreRecordsToLoad
{
    return (m_totalRecords == -1 || [m_notificationsArray count] < m_totalRecords);    
}

- (void)onNotificationsResponse:(NSNumber*)number data:(NSData*)data {
    [[LoadingIndicatorView SharedInstance] stopLoadingView];
    m_isLoading = NO;
    
    NSString* s = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    
    NSLog(@"Notifications response: %@",s);
    int temp_responseCode = [number intValue];
    
    if (temp_responseCode == 200) {
        
        NotificationResponse * response = [[[NotificationResponse alloc] init] autorelease];
        response.serverResponse = s;
        
        [response parse];
        
        m_currentPage++;
        m_totalRecords = response.totalRecords;
        
        [m_notificationsArray addObjectsFromArray:response.notifications];
        
        [o_tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
	}
	else if (temp_responseCode == 401) { // In case the session expires we have to make the user login again.
        [[NewsDataManager sharedManager] showErrorByCode:temp_responseCode fromSource:NSStringFromClass([self class])];
	}
	else if (temp_responseCode == kLockedInfoErrorCode)	{
		UIAlertView * tmp_AlertView = [[UIAlertView alloc] initWithTitle:kCannotViewFriendInfoTitle
                                                              message:kCannotViewFriendInfoMessage 
                                                             delegate:self 
                                                    cancelButtonTitle:@"OK" 
                                                    otherButtonTitles:nil];
		[tmp_AlertView show];
		[tmp_AlertView release];
		tmp_AlertView = nil;
	}
	else if (temp_responseCode != -1) {
		UIAlertView * alert = [[UIAlertView alloc] 
                            initWithTitle:kNetworkDownErrorTitle 
                            message:kNetworkDownErrorMsg 
                            delegate:nil 
                            cancelButtonTitle:@"OK" 
                            otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}

}

- (void)viewDidUnload {
    [o_tableView release];
    o_tableView = nil;
    [doneButton release];
    doneButton = nil;
    [backButton release];
    backButton = nil;
    [super viewDidUnload];
    [self.selectedIndexPath release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    if (m_notificationsArray) {
        [m_notificationsArray release];
        m_notificationsArray = nil;
    }
    [l_requestObj release];
    [o_tableView release];
    
    [_notificationID release];
    [_userEmail release];
    [_userName release];
    [_messageIDString release];
    [self.selectedIndexPath release];
    
    [super dealloc];
}




#pragma mark - TableView Methods
#pragma mark
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [m_notificationsArray count])
    {
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
        if (!cell) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"loadingCell"] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIActivityIndicatorView* v = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            CGRect r = v.frame;
            r.origin.y = (60.0f-r.size.height)/2;
            r.origin.x = (320.0f-r.size.width)/2;
            v.frame = r;
            [cell.contentView addSubview:v];
            [v startAnimating];
            [v release];
        }
        [self loadMoreModal:NO];
        return cell;
    }
    
	//Bharat: 11/14/2011: Reverse sort notifications
    Notification * n = [m_notificationsArray objectAtIndex:indexPath.row];
   // Notification * n = [m_notificationsArray objectAtIndex:([m_notificationsArray count] - indexPath.row -1)];
    
    UITableViewCell * cell = nil;
    
    if (n.notificationType == NotificationTypeMessage) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell"];
        
        if (!cell) {
            cell = [[[NotificationCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"notificationCell"] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            ((NotificationCell*)cell).delegate = self;
        }
        
        [((NotificationCell*)cell) setNotification:n];
		//Bharat: 11/16/2011: IF messageId missing, do not show accessory indicator
		if ((n.messageId != nil) && ([n.messageId length] > 0)) {
			((NotificationCell *)cell).m_arrowView.hidden = NO;
		} else {
			((NotificationCell *)cell).m_arrowView.hidden = YES;
		}
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"acceptOrRejectCell"];
        
        if (!cell) {
            cell = [[[AcceptRejectNotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"acceptOrRejectCell"] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            ((AcceptRejectNotificationCell*)cell).delegate = self;
        }
        
        [((AcceptRejectNotificationCell*)cell) setNotification:n];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([indexPath row] < [m_notificationsArray count] ) {
    
		//Bharat: 11/14/2011: Reverse sort notifications
        Notification * notification = [m_notificationsArray objectAtIndex:indexPath.row];
//        Notification * notification = [m_notificationsArray objectAtIndex:([m_notificationsArray count] - indexPath.row -1)];
        NSString * notificationMessageType = [[NSString alloc] initWithString:[notification messageType]];
        NSString * notificationId = [[NSString alloc] initWithString:[notification notificationId]]; 
        _notificationID = [notification notificationId];
        _userEmail = [notification email];
        _userName = [notification firstName];
        self.selectedIndexPath = indexPath;
        self.currentType = NONE_NOTIFICATION;
        _messageIDString = [notification messageId];
 		NSLog(@"PushNotificationViewController:didSelectRowAtIndexPath:notificationId=[%@], notificationType=[%d], messageId=[%@], messageType=[%@], userEmail=[%@], userName=[%@]",
				notificationId, [notification notificationType], _messageIDString, notificationMessageType, _userEmail, _userName);
        if (([notificationMessageType caseInsensitiveCompare:@"FollowingRequest"] == NSOrderedSame)      ||
            ([notificationMessageType caseInsensitiveCompare:@"AcceptedFriendRequest"] == NSOrderedSame)) {
            
			NSLog(@"PushNotificationViewController:didSelectRowAtIndexPath: messageType=[%@] selectedProfileNotification",notificationMessageType);
            self.currentType = FOLLOWING_NOTIFICATION;

            [self selectedProfileNotification:_userEmail withName:_userName andNotificationId:notificationId];
            
        }        
        else if (([notificationMessageType rangeOfString:@"CreateBuzzRequest"].length > 0)  ||
                 ([notificationMessageType rangeOfString:@"CommentOnFittingRoom"].length > 0) ||
                 ([notificationMessageType rangeOfString:@"CreateFittingRoom"].length > 0) ||
                 ([[NewsDataManager sharedManager] indexOfActiveCategoryByName:notificationMessageType]) != NSNotFound) {
                
				NSLog(@"PushNotificationViewController:didSelectRowAtIndexPath: messageType=[%@] selectedRunwayNotification",notificationMessageType);
                self.currentType = LIKE_TYPE_NOTIFICATION;

                [self selectedRunwayNotification:notificationId]; 
        }
        else if ([notificationMessageType caseInsensitiveCompare:@"AcceptOrReject"] == NSOrderedSame) {
        
			NSLog(@"PushNotificationViewController:didSelectRowAtIndexPath: messageType=[%@] selectedProfileWithEmail",notificationMessageType);
        //[self selectedProfileNotification:_userEmail withName:_userName andNotificationId:notificationId];
        //[self updateNotificationTray:notificationId];
            self.currentType = ACCEPT_REJECT_NOTIFICATION;
        [self selectedProfileWithEmail:_userEmail withName:_userName];
        
        } else {
 		
//			NSString * str = [NSString stringWithFormat:@"Bharat: MessageType/MessageId empty??.\n notificationId=[%@]\n notificationType=[%d]\n messageId=[%@]\n messageType=[%@]\n userEmail=[%@]\n userName=[%@]\n notificationMessage=[%@]",
//				notificationId, [notification notificationType], _messageIDString, notificationMessageType, _userEmail, _userName, [notification notificationMessage]];
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error"
														  message:@"This message is no longer available"
														 delegate:nil cancelButtonTitle:@"close" 
												otherButtonTitles: nil];
			[alert show];
			[alert release];
            
            [self performSelector:@selector(removeItemFromTable:) withObject:self.selectedIndexPath afterDelay:2];
            
        }
        
        [notificationMessageType release];
        [notificationId release];
        
    }
     
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"\n\nnumberOfRowsInSection");
    return [m_notificationsArray count] + ([self thereAreMoreRecordsToLoad] ? 1 : 0);

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row >= [m_notificationsArray count]) {
        
        return 60.0f; //Loading cell height
    
    }
    else {
        
		//Bharat: 11/14/2011: Reverse sort notifications
        Notification * n = [m_notificationsArray objectAtIndex:indexPath.row];
    	//Notification * n = [m_notificationsArray objectAtIndex:([m_notificationsArray count] - indexPath.row -1)];
        return [NotificationCell heightForNotification:n];
    
    }
    
}

- (void)removeItemFromTable:(NSIndexPath *)indexPath {
    
    if ([m_notificationsArray count] > [indexPath row]) {
        
		//Bharat: 11/14/2011: Reverse sort notifications
        [m_notificationsArray removeObjectAtIndex:[indexPath row]];
    	//[m_notificationsArray removeObjectAtIndex:([m_notificationsArray count] - indexPath.row -1)];
        m_totalRecords--;
        [o_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                                                           
    }
    
    [l_indicatorView stopLoadingView];
    
}

#pragma mark - Tapping Actions
#pragma mark

- (void)acceptInvitationFromNotification:(Notification *)notification {
    
//    self.selectedIndexPath = [NSIndexPath indexPathForRow:
//                              ([m_notificationsArray count] - [m_notificationsArray indexOfObject:notification] -1)
//                                                inSection:0];
    self.selectedIndexPath = [NSIndexPath indexPathForRow:[m_notificationsArray indexOfObject:notification]
                                                inSection:0];
    //selectedRow = [m_notificationsArray indexOfObject:notification];
    _notificationID = [notification notificationId];
    _userEmail = [notification email];
    _userName = [notification firstName];
    l_requestObj=[[ShopbeeAPIs alloc] init];
    goToProfile = YES;
    _rejectedInvitation = NO;
	NSString * acceptString = @"Y";
	NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
	NSString * tmp_UserId = [prefs valueForKey:@"userName"];
	NSString * tmp_FriendId = [notification email];
    [l_indicatorView startLoadingView:self];
	[l_requestObj answerFriendRequest:@selector(requestCallBackMethodForAcceptOrReject:responseData:) tempTarget:self userid:tmp_UserId friendid:tmp_FriendId acceptString:acceptString];

	[l_requestObj release];
	l_requestObj = nil;
    
}

- (void)denyInvitationFromNotification:(Notification *)notification {
    
//    self.selectedIndexPath = [NSIndexPath indexPathForRow:
//                              ([m_notificationsArray count] - [m_notificationsArray indexOfObject:notification] -1)
//                                                inSection:0];
    self.selectedIndexPath = [NSIndexPath indexPathForRow:[m_notificationsArray indexOfObject:notification]
                                                inSection:0];
    _notificationID = [notification notificationId];
    l_requestObj = [[ShopbeeAPIs alloc] init];
    goToProfile = YES;
    _rejectedInvitation = YES;
	NSString * acceptString = @"N";
	NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
	NSString * tmp_UserId = [prefs valueForKey:@"userName"];
	NSString * tmp_FriendId = [notification email];
	[l_indicatorView startLoadingView:self];
	[l_requestObj answerFriendRequest:@selector(requestCallBackMethodForAcceptOrReject:responseData:) tempTarget:self userid:tmp_UserId friendid:tmp_FriendId acceptString:acceptString];	
	[l_requestObj release];
	l_requestObj = nil;
    
}

- (void)selectedProfileWithEmail:(NSString *)email withName:(NSString *)name {
    
    ViewAFriendViewController * tempViewAFriend = [[ViewAFriendViewController alloc] init];
    tempViewAFriend.m_Email = email;
    tempViewAFriend.m_strLbl1 = name;
    tempViewAFriend.messageLayout = NO;
    [self.navigationController pushViewController:tempViewAFriend animated:YES];
    [tempViewAFriend release];
    tempViewAFriend = nil;

}

- (void)selectedProfileNotification:(NSString *)email withName:(NSString *)name andNotificationId:(NSString *) notificationId {
    
    goToProfile = YES;
    [self updateNotificationTray:notificationId];
}

- (void)selectedRunwayNotification:(NSString *)notificationId {
    
    //int messageId = [messageIdString intValue];
    //[self iconClickedAction:messageId];
    goToProfile = NO;
    [self updateNotificationTray:notificationId];
    
}

- (void)updateNotificationTray:(NSString *)notificationId {
	NSLog(@"PushNotificationViewController:updateNotificationTray:[notificationId=%@]",notificationId);
    [l_indicatorView startLoadingView:self];
    ShopbeeAPIs * requestObject = [[ShopbeeAPIs alloc] init];
    [requestObject updateNotificationTray:@selector(requestCallBackMethodForUpdateNotificationTray:responseData:) tempTarget:self notificationid:notificationId];
    [requestObject release];
    
}

#pragma mark - Callback Methods
#pragma mark

- (void)requestCallBackMethodForAcceptOrReject:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
    [l_indicatorView stopLoadingView];
	if ([responseCode intValue] == 200) {
        
		l_requestObj = [[ShopbeeAPIs alloc] init];
        [l_indicatorView stopLoadingView];
        [self updateNotificationTray:_notificationID];
        
		NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];

		[tempString release];
		tempString = nil;
		[l_requestObj release];
		l_requestObj = nil;
        
	}
	else if ([responseCode intValue] == 401) {  // In case the session expires we have to make the user login again.
        [l_indicatorView stopLoadingView];
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if ([responseCode intValue] != -1) {
        [l_indicatorView stopLoadingView];
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSRange range = [tempString rangeOfString:@"don't have any friend request from"];
        if (range.location == NSNotFound) {
            [alert show];
        }
        else {
            [alert setTitle:kCannotViewFriendInfoTitle];
            [alert setMessage:@"This friend request has already been accepted/rejected."];
            [alert show];
            goToProfile = YES;
            _rejectedInvitation = YES;
            [self updateNotificationTray:_notificationID];
        }
        
		[tempString release];
		tempString = nil;
        
		[alert release];
		alert = nil;
	}
	
}

- (void)requestCallBackMethodForUpdateNotificationTray:(NSNumber *)responseCode responseData:(NSData *)responseData {
    NSLog(@"PushNotificationViewController:requestCallBackMethodForUpdateNotificationTray:responseCode=[%d], data=[%@]",
			[responseCode intValue], [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	if ([responseCode intValue] == 200) {
        
        NSString * responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        [l_indicatorView stopLoadingView];
        if (goToProfile) {
            
            if (!_rejectedInvitation) {                
                ViewAFriendViewController * tempViewAFriend = [[ViewAFriendViewController alloc] init];
                tempViewAFriend.m_Email = _userEmail;
                tempViewAFriend.m_strLbl1 = _userName;
                tempViewAFriend.messageLayout = NO;
                [self.navigationController pushViewController:tempViewAFriend animated:YES];
                [tempViewAFriend release];
                tempViewAFriend = nil;
            }
            else {
                _rejectedInvitation = NO;
            }
            
        }
        else {
            int messageId = [_messageIDString intValue];
            
            NSUserDefaults * userPreferences = [NSUserDefaults standardUserDefaults];
            NSString * userName = [userPreferences stringForKey:@"userName"];
            
            if (messageId > 0) {
            //Bharat: 11/15/2011: Hmm.. Looks like we have to create a new API obj for each call, else thread blocks.
                ShopbeeAPIs * api = [[ShopbeeAPIs alloc] init];
                [api getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:userName messageId:messageId];
                [api release];
            }
        }
        
        if ([responseString caseInsensitiveCompare:@"[true]"] == NSOrderedSame) {
            [self performSelector:@selector(removeItemFromTable:) withObject:self.selectedIndexPath afterDelay:2];
        }
        
        [responseString release];
        responseString = nil;
        
	}
	else {
        [l_indicatorView stopLoadingView];
        m_isLoading = NO;
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    }
	
}

- (void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
	[l_indicatorView stopLoadingView];
	
    m_isLoading = NO;
	if ([responseCode intValue] == 200) {
		//[m_backup removeAllObjects];

		NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		[tempString release];
        
	}
	else 
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
}

- (void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
	NSLog(@"PushNotificationViewController:CallBackMethodForGetRequest:responseCode=[%d], data=[%@]",
			[responseCode intValue], [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
    
    m_isLoading = NO;
	if ([responseCode intValue] == 200) {
        
		NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray  * tmp_array = [tempString JSONValue];
        NSString * originalMessageId = nil;
        NSDictionary *tmp_dict = nil;
        
        if ([[tmp_array objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
            tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
            
            NSLog(@"tempString %@", tempString);
            // Bharat: 11/22/11: original message id check
            if (tmp_dict) {
                originalMessageId = [tmp_dict valueForKey:@"originalMessageId"];
            }
        }
		
		if ((originalMessageId != nil) && ([originalMessageId length] > 0)) {
			//_messageIDString = [NSString stringWithFormat:@"%d",[originalMessageId intValue]];
            NSUserDefaults * userPreferences = [NSUserDefaults standardUserDefaults];
            NSString * userName = [userPreferences stringForKey:@"userName"];
            ShopbeeAPIs * api = [[ShopbeeAPIs alloc] init];
            [api getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:userName messageId:[originalMessageId intValue]];
            [api release];
            api = nil;
			return;
		} 
			
        if (tmp_dict) {
            NSString *sentFromCustomer = [tmp_dict objectForKey:@"sentFromCustomer"];
            NSString *userName = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
            CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
            
            DetailPageViewController *detailController = [[[DetailPageViewController alloc] init] autorelease];
            detailController.m_data = catData;
            detailController.m_Type = catData.type;
            detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
            detailController.filter = POPULAR;
            detailController.mineTrack = [sentFromCustomer isEqualToString:userName];
            detailController.curPage = 1;
            //@@@@ To be changed later
            //    self.detailController.fetchingData = YES;
            detailController.m_CallBackViewController = self;
            [self.navigationController pushViewController:detailController animated:YES];
        }
        
        [tempString release];
        tempString = nil;
		
	}
	else  {
        [l_indicatorView stopLoadingView];
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    }
	
	
}

@end
