//
//  GetLocationAfterDistance.h
//  QNavigator
//
//  Created by softprodigy  on 22/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "CLLocation+AFExtensions.h"
#include <CoreLocation/CoreLocation.h>

@interface GetLocationAfterDistance : UIViewController <CLLocationManagerDelegate>//NSObject <CLLocationManagerDelegate>
{
	CLLocationManager	*m_locationManager;
	float				m_tmpLatitude;
	float				m_tmpLongitude;
	
	
}

@property (nonatomic,retain) CLLocationManager *m_locationManager;
@property float			m_tmpLatitude;
@property float			m_tmpLongitude;

-(void)initializeMembers;

@end
