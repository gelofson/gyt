//
//  BuzzButtonPhotoStoryFunctionality.m
//  TinyNews
//
//  Created by jay kumar on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BuzzButtonPhotoStoryFunctionality.h"
#import "QNavigatorAppDelegate.h"
#import "BuzzButtonEnterOpenFunctionality.h"
#import "BuzzButtonShareNowFunctionality.h"
#import "Constants.h"

@implementation BuzzButtonPhotoStoryFunctionality
QNavigatorAppDelegate *l_appDelegate;

@synthesize m_howtext;
@synthesize m_whattext;
@synthesize m_whentext;
@synthesize m_wheretext;
@synthesize m_whotext;
@synthesize m_whytext;
@synthesize m_photo;
@synthesize m_headerString;
@synthesize m_strType;
@synthesize m_strMessage;
@synthesize m_scrollView;

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	[l_appDelegate.m_customView setHidden:YES];
    QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *)[[UIApplication sharedApplication] delegate];
    [mainDg.tabBarController setHidesBottomBarWhenPushed:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
	[l_appDelegate.m_customView setHidden:YES];
	
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_scrollView.contentSize = CGSizeMake(m_scrollView.frame.size.width,650);
    // Do any additional setup after loading the view from its nib.
}


#pragma mark- Custome Button Action
/*
-(IBAction)clickToBack:(id)sender
{
    [self.m_whotext resignFirstResponder];
    [self.m_whattext resignFirstResponder];
    [self.m_whentext resignFirstResponder];
    [self.m_wheretext resignFirstResponder];
    [self.m_howtext resignFirstResponder];
    [self.m_whytext resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];   
}
 */

-(IBAction)clickToNext:(id)sender
{
     [self skipBTNClick]; 
}

-(IBAction)clickToCancel:(id)sender
{
    [self.m_whotext resignFirstResponder];
    [self.m_whattext resignFirstResponder];
    [self.m_whentext resignFirstResponder];
    [self.m_wheretext resignFirstResponder];
    [self.m_howtext resignFirstResponder];
    [self.m_whytext resignFirstResponder]; 
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(IBAction)openPriceClick
{
    BuzzButtonEnterOpenFunctionality *viewController=[[BuzzButtonEnterOpenFunctionality alloc]init];
    viewController.m_headerMessage=self.m_headerString;
    viewController.m_strMessage=self.m_strMessage;
    viewController.m_strType=self.m_strType;
    viewController.photoImage=m_photo;
    viewController.m_whoMessage=self.m_whotext.text;
    viewController.m_whatMessage=self.m_whattext.text;
    viewController.m_whenMessage=self.m_whentext.text;
    viewController.m_whereMessage=self.m_wheretext.text;
    viewController.m_howMessage=self.m_howtext.text;
    viewController.m_whyMessage=self.m_whytext.text;
    
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController release];
}

-(IBAction)skipBTNClick
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the best place for this new photo:" 
                                                             delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil 
                                                    otherButtonTitles:[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"], 
                                  [[Context getInstance] getDisplayTextFromMessageType:@"I Bought It!"],
                                  [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"],
                                  [[Context getInstance] getDisplayTextFromMessageType:@"Check This Out..."],
                                  
                                  [[Context getInstance] getDisplayTextFromMessageType:@"Buy It Or Not?"],
                                  
                                  nil];
    
	[actionSheet showFromTabBar:l_appDelegate.tabBarController.tabBar];
	[actionSheet release];
    
}


#pragma ActionheetDelegate method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex == 0) {
		self.m_strType = kHowDoesThisLook;
	}else if(buttonIndex == 1) {
		self.m_strType = kIBoughtIt;
	}else if(buttonIndex == 2) {
		self.m_strType = kFoundASale;
	}else if(buttonIndex == 3) {
		self.m_strType = kCheckThisOut;
	}else if(buttonIndex == 4) {
		self.m_strType = kBuyItOrNot;
	}else {
		self.m_strType = kCheckThisOut;
	}
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    if (buttonIndex != 5)
        [self finallyPerformRealShare];
    
}

-(void) finallyPerformRealShare {
	
	BuzzButtonShareNowFunctionality * vc = [[BuzzButtonShareNowFunctionality alloc] init];
	vc.photoImage = m_photo;
	vc.m_strType = self.m_strType;
	vc.m_strMessage = self.m_strMessage;
    vc.m_whoMessage= self.m_whotext.text;
    vc.m_whatMessage=self.m_whattext.text;
    vc.m_whenMessage=self.m_whentext.text;
    vc.m_whereMessage=self.m_wheretext.text;
    vc.m_howMessage=self.m_howtext.text;
    vc.m_whyMessage=self.m_whytext.text;
    vc.m_headerMessage=self.m_headerString;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
    
}

#pragma mark- textField delegate

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [UIView beginAnimations: @"downView" context: nil];
    [UIView setAnimationDelegate: self];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
  self.view.frame = CGRectMake(0,0, 320,460);
    [UIView commitAnimations];
    [textField resignFirstResponder];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the best place for this new photo:" 
                                                             delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil 
                                                    otherButtonTitles:[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"], 
                                  [[Context getInstance] getDisplayTextFromMessageType:@"I Bought It!"],
                                  [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"],
                                  [[Context getInstance] getDisplayTextFromMessageType:@"Check This Out..."],
                                  
                                  [[Context getInstance] getDisplayTextFromMessageType:@"Buy It Or Not?"],
                                  
                                  nil];
    
	[actionSheet showFromTabBar:l_appDelegate.tabBarController.tabBar];
	[actionSheet release];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField== m_whotext)
    {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //self.view.frame = CGRectMake(0,-20, 320,460);
        [self.m_scrollView setContentOffset:CGPointMake(0, 0)];
        [UIView commitAnimations];
    }
    else if (textField== m_whattext)
    {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //self.view.frame = CGRectMake(0,-40, 320,460);
        [self.m_scrollView setContentOffset:CGPointMake(0, 0)];
        [UIView commitAnimations];
    }

   else if (textField== m_whentext)
    {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //self.view.frame = CGRectMake(0,-60, 320,460);
        [self.m_scrollView setContentOffset:CGPointMake(0, 54)];
        [UIView commitAnimations];
    }
    else if (textField== m_wheretext)
    {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //self.view.frame = CGRectMake(0,-80, 320,460);
        [self.m_scrollView setContentOffset:CGPointMake(0, 109)];
        [UIView commitAnimations];
    }
    else if (textField== m_howtext)
    {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //self.view.frame = CGRectMake(0,-100, 320,460);
        [self.m_scrollView setContentOffset:CGPointMake(0, 165)];
        [UIView commitAnimations];
    }
    else if (textField== m_whytext)
    {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        //self.view.frame = CGRectMake(0,-120, 320,460);
        [self.m_scrollView setContentOffset:CGPointMake(0, 210)];
        [UIView commitAnimations];
    }

}



- (void)textFieldDidEndEditing:(UITextField *)textField
{
  if (textField== m_whytext)
  {
      [UIView beginAnimations: @"upView" context: nil];
      [UIView setAnimationDelegate: self];
      [UIView setAnimationDuration: 0.4];
      [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
      //self.view.frame = CGRectMake(0,-120, 320,460);
      [self.m_scrollView setContentOffset:CGPointMake(0, 0)];
      [UIView commitAnimations];
  }

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc
{
    //[m_whotext release];
    //[m_whattext release];
    //[m_whentext release];
   // [m_wheretext release];
   // [m_howtext release];
   // [m_whytext release];
    [m_scrollView release];
    [m_photo release];
    [m_headerString release];
    [m_strType release];
    [m_strMessage release];
    [super dealloc];
}
@end
