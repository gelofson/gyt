//
//  CategoryRequest.m
//  TinyNews
//
//  Created by Nava Carmon on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CategoryRequest.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "NewsDataManager.h"
#import "JSON.h"
#import "NSString+URLEncoding.h"

@implementation CategoryRequest

@synthesize data;

- (id) initWithData:(NSDictionary *) aData
{
    self = [super init];
    if (self) {
        self.data = aData;
    }
    return self;
}

- (void) main
{
    NSAutoreleasePool *aPool = [[NSAutoreleasePool alloc] init];
    
    if (![self isCancelled]) {
        [self performMain];
    }
    
    [aPool drain];
}

- (void) onGetCategoryData:(NSURLResponse *)response data:(NSData *)responseData error:(NSError *)error
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	NSInteger responseCode = [httpResponse statusCode];
	if (responseCode == 200) {
        NSString *tempString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSArray *anArray = [tempString JSONValue];
        

        //NSLog(@"CategoryRequest Data was recieved %@", anArray);
        
        if (anArray != nil) {
            [[NewsDataManager sharedManager] addCategoryData:[NSDictionary dictionaryWithObjectsAndKeys:anArray,@"array", [self.data objectForKey:@"delegate"], @"delegate",  [self.data objectForKey:@"operation"], @"operation", [self.data objectForKey:@"oneCategory"], @"oneCategory", nil]];
            if (![[self.data objectForKey:@"oneCategory"] boolValue]) {
                [[NewsDataManager sharedManager] checkCounter:[self.data objectForKey:@"delegate"]];
            }
            return;
        } else
            NSLog(@"CategoryRequest Data was recieved, but couldn't parse %@", tempString);
    } else if(responseCode==401 || (responseCode == 0 && error != nil)) { // In case the session expires we have to make the user login again.
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    } 

    NSLog(@"CategoryRequest for %@ Bad result was recieved %d", [self.data objectForKey:@"type"], responseCode);
    
    if (error)
        NSLog(@"CategoryRequest error %@", [error description]);
    
    [[NewsDataManager sharedManager] removeProblematicCategory: [self.data objectForKey:@"type"] delegate:[self.data objectForKey:@"delegate"]];
    if (![[self.data objectForKey:@"oneCategory"] boolValue]) {
        [[NewsDataManager sharedManager] checkCounter:[self.data objectForKey:@"delegate"]];
    }
}

- (void) performMain
{
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        NSString *userid=[prefs valueForKey:@"userName"];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSString *wsName;
        switch ([[self.data objectForKey:@"filter"] intValue]) {
            case POPULAR:
                wsName = @"getAllRequestSummaries";
                break;
            case FRIENDS:
                wsName = @"getFriendsRequestSummaries";
                break;
            case FOLLOWING:
                wsName = @"getFollowingRequestSummaries";
                break;
            case MINE:
                wsName = @"getMyRequestSummaries";
                break;
                
            default:
                break;
        }
        
        //We are sending a request to the server, set the activity indicator on
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

		NSString *temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=%@&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,wsName,userid,[[self.data objectForKey:@"type"] URLEncodedString],[[self.data objectForKey:@"number"] intValue],[[self.data objectForKey:@"numPage"] intValue]];
		temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"CategoryRequest %@",temp_url);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *adata, NSError *jsonError) {
            [self onGetCategoryData:response data:adata error:jsonError];
            if (jsonError != nil)
            {
                return;
            }
            
        }];
        //We are done with network data download, set the indicator off
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	}
}

- (void) dealloc
{
    self.data = nil;
    [super dealloc];
}

@end
