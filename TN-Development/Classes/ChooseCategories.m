//
//  ChooseCategories.m
//  TinyNews
//
//  Created by Nava Carmon on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChooseCategories.h"
#import "NewsDataManager.h"
#import "QNavigatorAppDelegate.h"
@implementation ChooseCategories

@synthesize tableView, currentRow, controller;
@synthesize m_bgImage;



- (IBAction)back:(id)sender
{
    if ([controller respondsToSelector:@selector(saveSelectedCategory:)]) {
        NSString *type = nil;
        if (currentRow >= 0) {
            type = [[NewsDataManager sharedManager] getAllCategoryIdAtIndex:currentRow];
        }             
        [controller performSelector:@selector(saveSelectedCategory:) withObject:type];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    UITapGestureRecognizer *reg=[[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)]autorelease];
    reg.numberOfTapsRequired=1;
    reg.delegate=self;
    //[m_bgImage addGestureRecognizer:reg];
    [tableView reloadData];
    
    
}

-(void)setAnimatedLayout
{
    m_bgImage.alpha=0.1;
    CGRect frame=[tableView frame];
    frame.origin.x=0;
    frame.size.width=320;
    tableView.frame=frame;
}

-(void)tap:(UITapGestureRecognizer*)reg
{
    [UIView animateWithDuration:0.25 
                     animations:^{[self setAnimatedLayout]; }];

    if ([controller respondsToSelector:@selector(saveSelectedCategory:)]) {
        NSString *type = nil;
        if (currentRow >= 0) {
            type = [[NewsDataManager sharedManager] getAllCategoryIdAtIndex:currentRow];
        }             
        [controller performSelector:@selector(saveSelectedCategory:) withObject:type];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)viewDidLoad
{
    [super viewDidLoad];
}
- (void) dealloc
{
    [controller release];
    [tableView release];
    //[m_bgImage release];
    
    [super dealloc];
}

#pragma mark UITableView delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
//    return [[NewsDataManager sharedManager] countAllCategories];
    return [[NewsDataManager sharedManager] countActiveCategories];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CategoryCellIdentifier = @"CategoryCell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CategoryCellIdentifier];
    if (!cell) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CategoryCellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
//    cell.textLabel.text = [[NewsDataManager sharedManager] getAllCategoryIdAtIndex:indexPath.row];
    NSString *titleString = [[NewsDataManager sharedManager] getCategoryIdAtIndex:indexPath.row];
    cell.textLabel.text = titleString;
    
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:titleString];
    
    if (convertedName)
        cell.textLabel.text = convertedName;

    if (currentRow == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentRow = indexPath.row;
    [atableView reloadData];
    
    
    if ([controller respondsToSelector:@selector(saveSelectedCategory:)]) {
        NSString *type = nil;
        if (currentRow >= 0) {
            type = [[NewsDataManager sharedManager] getCategoryIdAtIndex:currentRow];
        }             
        [controller performSelector:@selector(saveSelectedCategory:) withObject:type];
    }
//    [UIView animateWithDuration:0.25
//                     animations:^{[self setAnimatedLayout]; }];
    [self.navigationController popViewControllerAnimated:YES];
}



@end
