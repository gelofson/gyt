//
//  FittingRoomViewController.m
//  QNavigator
//
//  Created by softprodigy on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FittingRoomViewController.h"
#import "WhereBox.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "AuditingClass.h"
#import "CategoryModelClass.h"
#import "BigSalesModelClass.h"
#import "QNavigatorAppDelegate.h"
#import<QuartzCore/QuartzCore.h>
#import "ShopbeeAPIs.h"
#import"AsyncImageView.h"
#import"LoadingIndicatorView.h"

@implementation FittingRoomViewController
@synthesize m_sendBuy;
@synthesize m_sendBuyLabel;
@synthesize m_textView;
@synthesize tagg;
@synthesize m_view;
@synthesize m_buyOrNot;
@synthesize m_advtlocid;
@synthesize m_indicatorView;
@synthesize m_PublicOrPrivate;
@synthesize m_typeLabel;
@synthesize m_Image;
@synthesize m_imgView;
@synthesize m_catObjModel;
@synthesize m_bigSalesModel;

QNavigatorAppDelegate *l_appDelegate;
ShopbeeAPIs *l_request;

NSString *str_textView;
int count_text=140;

QNavigatorAppDelegate *l_appDelegate;
//CategoryModelClass *l_objCatModel;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    	self.m_catObjModel = nil;
    	self.m_bigSalesModel = nil;
	}
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidAppear:(BOOL)animated
{
	[l_appDelegate.m_customView setHidden:YES];
	
}
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	NSLog(@"%@",m_PublicOrPrivate);
	
	m_buyOrNot.text=m_sendBuy;
	NSLog(@"tagg value>>>>>--%d",l_appDelegate.m_intCategorySalesIndex);
	m_sendBuyLabel.text=@"140";
	[m_textView.delegate self ];
	
	
	NSLog(@"%@",m_sendBuy);
//	[self whereBtnPressed:l_appDelegate.m_intCategorySalesIndex];
	
	if ([m_typeLabel isEqualToString:@"     I bought it"]) 
	{
		m_buyOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
	}
	else if([m_typeLabel isEqualToString:@"     Buy or not"])
	{
		m_buyOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
	}
	
	m_imgView.image=m_Image;
	
	
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	[l_appDelegate.m_customView setHidden:YES];
	[m_view.layer setCornerRadius:8.0f];
	[m_view.layer setMasksToBounds:YES];
}

#pragma mark -
#pragma mark textview delegate methods


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
//{
//	int count_text;
//	str_textView = m_textView.text;
//	count_text =  [str_textView length];
//	//m_sendBuyLabel.text=[NSString stringWithFormat:@"%d",count_text];
//	NSLog(@"%d",count_text);
//	//NSLog(@"%@",m_sendBuyLabel.text);
//	
//	if (count_text == 140) {
//		NSLog(@"not editing\n");
//		[m_textView setUserInteractionEnabled:FALSE];
//	}
//}

- (void)textViewDidChange:(UITextView *)textView
{
	
	int max_count;
	if (count_text == 0) {
		NSLog(@"not editing\n");
		[m_textView setUserInteractionEnabled:FALSE];
	}
	str_textView = m_textView.text;
	[str_textView retain];
	max_count = count_text-[str_textView length];
	m_sendBuyLabel.text=[NSString stringWithFormat:@"%d",max_count];
	NSLog(@"%d",count_text);
	NSLog(@"%@",m_sendBuyLabel.text);
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{	// return NO to not change text
	
	if([text isEqualToString:@"\n"]) {
        [m_textView resignFirstResponder];
        return NO;
    }
	
	
	if(m_textView.text.length>=140 && range.length==0)
	{
		return NO;
	}
	else
	{
		return YES;
	}
}


#pragma mark -
#pragma mark Callback method
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	//NSLog(@"Friend added. response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	[m_textView resignFirstResponder];
	
	if([responseCode intValue]==200)
	{
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message was sent successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[dataReloadAlert show];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else 
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
}
#pragma mark -
#pragma mark alertView delegate


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0)
	{
		[m_textView resignFirstResponder];
		[self.navigationController popViewControllerAnimated:YES];
	}
}
#pragma mark -
#pragma mark Custom methods

-(IBAction)btnSendMessage
{
	if([m_sendBuy isEqualToString:@"   I bought it"])
	{	
		
		NSLog(@"%@",m_advtlocid);
		NSLog(@"%@",str_textView);
		
		//UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		//		[dataReloadAlert show];
		//str_textView
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		l_request=[[ShopbeeAPIs alloc] init];
		[[LoadingIndicatorView SharedInstance] startLoadingView:self];
		NSDictionary *dic= 	nil;
		if (self.m_catObjModel != nil) {
			dic=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",self.m_catObjModel.m_strId,@"advtlocid",str_textView,@"comments",nil];
		} else if (self.m_bigSalesModel != nil) {
			dic=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",self.m_bigSalesModel.m_strId,@"advtlocid",str_textView,@"comments",nil];
		}
		NSLog(@"%@",str_textView);
		NSLog(@"%@",dic);
		
		[l_request sendIBoughtItRequest:@selector(requestCallBackMethod:responseData:) tempTarget:self tmpDict:dic];
		//	sendBoughtItRequest&iboughtit={"custid":"demo32@info.com","comments":"I want to buy this Item. Please suggest me","advtlocid":"5015"}
		[l_request release];
		l_request=nil;
	}
	else 
	{
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		//m_view.hidden=NO;
//		m_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
//		m_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//		m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140,200 , 30, 30)];
//		[m_view addSubview:m_indicatorView];
//		[self.view addSubview:m_view];
//		[m_indicatorView startAnimating];
		l_request=[[ShopbeeAPIs alloc] init];
		[[LoadingIndicatorView SharedInstance] startLoadingView:self];
		NSDictionary *dic= 	nil;
		if (self.m_catObjModel != nil) {
			dic=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",self.m_catObjModel.m_strId,@"advtlocid",str_textView,@"comments",nil];
		} else if (self.m_bigSalesModel != nil) {
			dic=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",self.m_bigSalesModel.m_strId,@"advtlocid",str_textView,@"comments",nil];
		}
		NSLog(@"%@",str_textView);
		NSLog(@"%@",dic);
		
		[l_request sendBuyItOrNotRequest:@selector(requestCallBackMethod:responseData:) tempTarget:self tmpDict:dic];
		
		/*66.28.216.132/salebynow/json.htm?action=sendBuyItOrNotRequest&buyitornot={"custid":"demo9@info.com","comments":"I want to buy this Item. Please suggest me","advtlocid":"5015"}*/
		[l_request release];
		l_request=nil;
		
		
	}
	
}

-(IBAction)addToWishList:(id)sender
{
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	m_view.hidden=NO;
	m_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
	m_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140,200 , 30, 30)];
	[m_view addSubview:m_indicatorView];
	[self.view addSubview:m_view];
	[m_indicatorView startAnimating];
	l_request=[[ShopbeeAPIs alloc] init];
	//NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",str_textView,@"comments",m_PublicOrPrivate,@"visibility",nil];
	NSDictionary *dic= 	nil;
	if (self.m_catObjModel != nil) {
		dic=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",self.m_catObjModel.m_strId,@"advtlocid",
			str_textView,@"comments",m_PublicOrPrivate,@"visibility",nil];
	} else if (self.m_bigSalesModel != nil) {
		dic=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",self.m_bigSalesModel.m_strId,@"advtlocid",
			str_textView,@"comments",m_PublicOrPrivate,@"visibility",nil];
	}
	NSLog(@"%@",str_textView);
	NSLog(@"%@",dic);
	
	[l_request addToWishList:@selector(requestCallBackMethod:responseData:) tempTarget:self tmpDict:dic];
	/*66.28.216.132/salebynow/json.htm?action=addToWishList&wishlist={"custid":"demo9@info.com","comments":"This Product is very good .. ","advtlocid":"5015","visibility":"public"}	[l_request release];*/
	l_request=nil;
	
}

-(IBAction) btnBackAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
	
}

-(IBAction)cancelButtonAction:(id)sender
{
	//WhereBox *tempWhereBox=[[WhereBox alloc]init];
	[self.navigationController popViewControllerAnimated:YES];	
	//[tempWhereBox release];
	//tempWhereBox=nil;
}

-(void)whereBtnPressed:(int)tag
{
	m_advtlocid=self.m_catObjModel.m_strId;
	if(l_appDelegate.m_intCurrentView==1)
	{	
		self.m_catObjModel= [l_appDelegate.m_arrIndyShopData objectAtIndex:tag];//[sender tag]];
	}
	else
	{
		self.m_catObjModel= [l_appDelegate.m_arrCategorySalesData objectAtIndex:tag];//[sender tag]];
	}
	
	l_appDelegate.m_intCategorySalesIndex=tag;//[sender tag];
	
	//------------------------------------------------------------
	AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
	[temp_objAudit initializeMembers];
	NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
	[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
	
	if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
	{
		NSDictionary *temp_dict=nil;
		if (self.m_catObjModel != nil) {
			temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
								 @"Where button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",self.m_catObjModel.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
		} else if (self.m_bigSalesModel != nil) {
			temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
								 @"Where button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",self.m_bigSalesModel.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
		}
		[temp_objAudit.m_arrAuditData addObject:temp_dict];
		//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
	}
	
	if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
	{
		[temp_objAudit sendRequestToSubmitAuditData];
	}
	
		
	UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(13,119,80,60)];
	[temp_imgView setImage:[UIImage imageNamed:@"loading.png"]];
	temp_imgView.contentMode=UIViewContentModeScaleAspectFit;
	
    // GDL: This is unused.
	//NSFileManager *temp_fileManger=[NSFileManager defaultManager];
    
	NSString *temp_path=NSTemporaryDirectory();
    
    // GDL: This is unused.
	//NSString *tempLogoFile = [temp_path stringByAppendingPathComponent:[NSString stringWithFormat:@"%d_logo_%d.jpg",l_appDelegate.m_intCategoryIndex,tag]];//[sender tag]]];
    
	NSString *tempFile = [temp_path stringByAppendingPathComponent:[NSString stringWithFormat:@"%d_%d.jpg",l_appDelegate.m_intCategoryIndex,tag]];//[sender tag]]];
//	
//	NSLog(@"%@",tempFile);
//	
//	if([temp_fileManger fileExistsAtPath:tempLogoFile])
//	{
//		[temp_imgView setImage:[UIImage imageWithContentsOfFile:tempLogoFile]];
//	}
//	else if([temp_fileManger fileExistsAtPath:tempFile])
//	{
//		[temp_imgView setImage:[UIImage imageWithContentsOfFile:tempFile]];		
//	}
//	else if(m_productImage)
//	{
//		[temp_imgView setImage:m_productImage];
//	}
//	else 
//	{
//		[temp_imgView setImage:[UIImage imageNamed:@"no-image"]];
//	}
	
	
	CGRect frame;
	frame.size.width=70; frame.size.height=70;
	frame.origin.x=10; frame.origin.y=15;
	
	AsyncImageView* asyncImage = [[[AsyncImageView alloc] initWithFrame:frame] autorelease];
	
	NSMutableString *temp_strUrl;
	
	temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,tempFile];
	
	NSLog(@"%@",temp_strUrl);
	NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
	
	[asyncImage loadImageFromURL:tempLoadingUrl];
	[temp_imgView addSubview:asyncImage];
	//	[m_moreDetailView addSubview:temp_imgView];
	[self.view addSubview:temp_imgView];
	[temp_imgView release];
	temp_imgView=nil;
}	

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_imgView = nil;
    self.m_view = nil;
    self.m_buyOrNot = nil;
    self.m_sendBuyLabel = nil;
    self.m_textView = nil;
}

- (void)dealloc {
    [m_sendBuy release];
    [m_PublicOrPrivate release];
    [m_sendBuyLabel release];
    [m_buyOrNot release];
    [m_textView release];
    [m_view release];
    [m_advtlocid release];
    [m_indicatorView release];
    [m_typeLabel release];
    [m_Image release];
    [m_imgView release];
    
    [super dealloc];
}


@end
