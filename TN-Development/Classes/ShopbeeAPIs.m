
//
//  ShopbeeAPIs.m
//  QNavigator
//
//  Created by softprodigy on 20/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ShopbeeAPIs.h"
#import"SBJSON.h"
#import"Constants.h"
#import"QNavigatorAppDelegate.h"
#import "NewsDataManager.h"

@implementation ShopbeeAPIs

SEL callBackSelector;
id callBackTarget;

QNavigatorAppDelegate *l_appDelegate;

#pragma mark -
#pragma mark getMyPagePhotos
-(void)getMyPageRecentPhotos:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID  loginid:(NSString *) loginid
{
	int num=6;
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	
 	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getMyPageRecentPhotos&custid=%@&num=%d&loginid=%@",kServerUrl,customerID,num,loginid];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
		
}



-(void)getMyPagePopularPhotos:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID num:(int)num loginid:(NSString *) loginid
{
	//int num=20;
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getMyPagePopularPhotos&custid=%@&num=%d&loginid=%@",kServerUrl,customerID,num,loginid];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		
		[l_theConnection start];
		
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
		
}


#pragma mark WishList
-(void)getItemsOnFriendWishList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID friendId:(NSString *)friendId
{
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getItemsOnFriendWishList&custid=%@&friendid=%@",kServerUrl,customerID,friendId/*[m_dictionary valueForKey:@"userId"],[m_dictionary valueForKey:@"friendId"]*/];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

-(void)getNotificationsWithCallbackTarget:(id)tar withCallbackSelector:(SEL)selector forUserId:(NSString*)userId pageNumber:(int)pageNumber numberOfRecords:(int)numberOfRecords{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
    callBackTarget = tar;
    callBackSelector = selector;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
        NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getNotificationTrayDetails&custid=%@&num=%i&pagenum=%i",kServerUrl,userId,numberOfRecords,pageNumber];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}

//getWishListDetails
-(void)getWishListDetails:(SEL)tempSelector tempTarget:(id)tempTarget wishlistid:(NSInteger)wishlistid {
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
        
        // GDL: I think you mean %i not %@ for wishlistid.
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getWishListDetails&wishlistid=%i&lng=%f&lat=%f",kServerUrl,wishlistid,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	

}
-(void)deleteWishList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID wishlistid:(NSString *)wishlistid
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=deleteWishList&custid=%@&wishlistid=%@",kServerUrl,customerID,wishlistid/*[m_dictionary valueForKey:@"userId"],[m_dictionary valueForKey:@"friendId"]*/];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
				
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}

-(void)getItemsOnWishList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID type:(NSString *)type
{
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
	//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getItemsOnWishList&custid=%@&type=%@",kServerUrl,customerID,type/*[m_dictionary valueForKey:@"userId"],[m_dictionary valueForKey:@"friendId"]*/];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}


-(void)updateWishList:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary
{
	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=updateWishList",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSString *temp_strJson=[NSString stringWithFormat:@"wishlist=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}


-(void)addToWishListWithFittingRoomData:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary
{
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addToWishListWithFittingRoomData",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSString *temp_strJson=[NSString stringWithFormat:@"wishlist=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}

}

-(void)addWishListWithoutProduct:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary
{
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addWishListWithoutProduct",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSString *temp_strJson=[NSString stringWithFormat:@"wishlist=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}



#pragma mark sendRequesttoaddfriend
-(void)sendRequestToAddFriend:(SEL)tempSelector tempTarget:(id)tempTarget  custId:(NSString *)custId  tmpUids:(NSString *)tmpUids  //0: no, 1: yes
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addShopBeeFriends&custid=%@",kServerUrl,custId/*[m_dictionary valueForKey:@"userId"],[m_dictionary valueForKey:@"friendId"]*/];
		
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSString *temp_strJson=[NSString stringWithFormat:@"friendids=%@",tmpUids];
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		
		//	self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

//-(void)sendRequestToAddFriend:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary  //0: no, 1: yes
//{
//		[l_appDelegate CheckInternetConnection];
//	callBackSelector=tempSelector;
//	callBackTarget=tempTarget;
//	if(l_appDelegate.m_internetWorking==0)//0: internet working
//	{
//		m_mutResponseData=[[NSMutableData alloc] init];
//		
//	//	self.view.userInteractionEnabled=FALSE;
//		NSLog(@"The dictionary contains%@",m_dictionary);
//		//[m_activityIndicator startAnimating];
//		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//		NSMutableString *temp_url;
//		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addShopBeeFriends&custid=%@&friendid=%@",kServerUrl,[m_dictionary valueForKey:@"userId"],[m_dictionary valueForKey:@"friendId"]];
//		
//		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//		
//		NSLog(@"%@",temp_url);
//		
//		
//		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
//		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
//		//	NSLog(@"registration strJson: %@",temp_strJson);
//		
//		//[temp_objSBJson release];
//		//temp_objSBJson=nil;
//		
//		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
//																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
//		
//		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
//		
//		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
//		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
//		[theRequest setHTTPMethod:@"POST"];
//		
//		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
//		
//		[l_theConnection start];
//		
//		if(l_theConnection)
//		{
//			NSLog(@"Request sent to get data");
//		}
//		//[temp_strJson release];
//	}
//	else
//	{
//	//	self.view.userInteractionEnabled=TRUE;
//		//[m_activityIndicator stopAnimating];
//		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[netCheckAlert show];
//		[netCheckAlert release];
//	}
//	
//}
#pragma mark -
#pragma mark get friends list
-(void) getFriendsList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid loginId:(NSString*)loginId
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;

	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFriendsList&custid=%@&loginid=%@",kServerUrl,customerid,loginId];
		NSLog(@"value for name is%@",customerid);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark get added phonebook friends
-(void) getPhonebookList:(SEL)tempSelector tempTarget:(id)tempTarget emailIds:(NSArray*)Mail_IDs 
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *custid= [prefs objectForKey:@"userName"];
		m_mutResponseData=[[NSMutableData alloc] init];
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		NSLog(@"the email ids in the request are %@",Mail_IDs);
		//NSLog(@"the customer id in the request is %@",custid);
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=checkPhoneBookContacts&custid=%@",kServerUrl,custid];
		//NSLog(@"value for name is%@",customerid);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:Mail_IDs]];
		NSString *temp_strJson;
		if (Mail_IDs.count == 0) {
			NSMutableArray *copiedMailIDs = [[NSMutableArray alloc]init];
			[copiedMailIDs addObject:@""]; //Add a placeholder for empty array
			temp_strJson =[NSString stringWithFormat:@"contacts=%@",[copiedMailIDs JSONRepresentation]];
			[copiedMailIDs release];
		}
		else {
			temp_strJson =[NSString stringWithFormat:@"contacts=%@",[Mail_IDs JSONRepresentation]];
		}
		NSLog(@"the strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
//		temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark getRegisteredFaceBookUsers

-(void)getRegisteredFaceBookUsers:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID listids:(NSString *)uids {
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;

	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getRegisteredFaceBookUsers&custid=%@",kServerUrl,customerID];
		//NSLog(@"value for name is%@",customerID);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[uids dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}

-(void) mapFaceBookID:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID facebookid:(NSString *)facebookID
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=mapFaceBookID&custid=%@&facebookid=%@",kServerUrl,customerID,facebookID];
		NSLog(@"value for name is%@",customerID);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		
		
		
		
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}



-(void) mapTwitterID:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID twitterid:(NSString *)TwitterUid
{
	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=mapTwitterID&custid=%@&twitterid=%@",kServerUrl,customerID,TwitterUid];
		NSLog(@"value for name is%@",customerID);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}

-(void)getRegisteredTwitterUsers:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID listids:(NSString *)uids
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;                  
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getRegisteredTwitterUsers&custid=%@",kServerUrl,customerID];
		//NSLog(@"value for name is%@",customerID);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[uids dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark Rate FashionGram App
-(void) RateShopbeeApp:(SEL)tempSelector tempTarget:(id)tempTarget tmp_dictionary:(NSDictionary *)dictionary
{
	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		NSLog(@"the dictionary is %@",dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=rateApp",kServerUrl];
	//	NSLog(@"value for rating is%i",userRating);//
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSString *temp_strJson =[[NSString alloc]initWithFormat:@"",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		NSString *temp_strJson=[NSString stringWithFormat:@"apprating=%@",[dictionary JSONRepresentation]];
		NSLog(@"the strJson: %@",temp_strJson);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}
#pragma mark -
#pragma mark get user info
-(void) getUserInfo:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)custid loginId:(NSString*)loginId;
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getUserInfo&custid=%@&loginid=%@",kServerUrl,loginId,custid];
		NSLog(@"GetUserInfo ShopbeeAPI : \ncustID: %@\nloginID: %@\n\n",custid,loginId);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"GetUserInfo complete URL Request:\n%@\n\n", temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark Get Notifications Count
-(void) getNotificationsCount:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)custid;
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getNotificationsCount&custid=%@",kServerUrl,custid];
		NSLog(@"value for name is%@",custid);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

#pragma mark -
#pragma mark Update Notification Tray
-(void) updateNotificationTray:(SEL)tempSelector tempTarget:(id)tempTarget notificationid:(NSString *)notificationid;
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=updateNotificationTray&nid=%@",kServerUrl,notificationid];
		NSLog(@"Notification ID to be removed: %@\n\n", notificationid);
		temp_url = (NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"Complete URL of the updateNotificationTrayRequest: \n%@\n\n", temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

#pragma mark -
#pragma mark update user info 
-(void) updateUserInfo:(SEL)tempSelector tempTarget:(id)tempTarget tmp_dictionary:(NSDictionary *)updateDictionary
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=[tempTarget retain];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//NSLog(@"the dictionary is %@",updateDictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=updateUserInfo",kServerUrl];
		//	NSLog(@"value for rating is%i",userRating);//
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSString *temp_strJson =[[NSString alloc]initWithFormat:@"",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		NSString *temp_strJson=[NSString stringWithFormat:@"userinfo=%@",[updateDictionary JSONRepresentation]];
		//NSLog(@"the strJson: %@",temp_strJson);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
	
}

-(void)getRegisteredUsers:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID listids:(NSString *)uids
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		//getFriendsList&custid=%@&twitterid=%@",kServerUrl,customerID,TwitterUid
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getRegisteredUsers&custid=%@",kServerUrl,customerID];
		//NSLog(@"value for name is%@",customerID);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[uids dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

-(void)sendIBoughtItRequest:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=sendBoughtItRequest",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSString *temp_strJson=[NSString stringWithFormat:@"iboughtit=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}

-(void)addToWishList:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary
{
	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addToWishList",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSString *temp_strJson=[NSString stringWithFormat:@"wishlist=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}


-(void)sendBuyItOrNotRequest:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=sendBuyItOrNotRequest",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
						
		NSString *temp_strJson=[NSString stringWithFormat:@"buyitornot=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}

#pragma mark -
#pragma mark get followers list
-(void) getFollowerList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid loginid:(NSString*)loginid
{
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFollowers&custid=%@&loginid=%@",kServerUrl,customerid,loginid];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
				
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
	
}
#pragma mark -
#pragma mark get following list (the list of persons following you)
-(void) getFollowingList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid loginid:(NSString*)loginid
{	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
[l_appDelegate CheckInternetConnection];
callBackSelector=tempSelector;
callBackTarget=tempTarget;
if(l_appDelegate.m_internetWorking==0)//0: internet working
{
	//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
	m_mutResponseData=[[NSMutableData alloc] init];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	NSMutableString *temp_url;
	temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFollowing&custid=%@&loginid=%@",kServerUrl,customerid,loginid];
	temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSLog(@"%@",temp_url);
	
	
	
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
															  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
	
	NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
	
	//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
	[theRequest setAllHTTPHeaderFields:headerFieldsDict];
	[theRequest setHTTPMethod:@"POST"];
	
	l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
	
	[l_theConnection start];
	
	if(l_theConnection)
	{
		NSLog(@"Request sent to get data");
	}
	//[temp_strJson release];
}
else
{
	//self.view.userInteractionEnabled=TRUE;
	//[m_activityIndicator stopAnimating];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[netCheckAlert show];
	[netCheckAlert release];
	[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
}
	
}
#pragma mark -
#pragma mark add followers
-(void) addFollowing:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid followers:(NSArray *)followers{
	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addFollowing&custid=%@",kServerUrl,customerid];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		NSString *temp_strJson=[NSString stringWithFormat:@"following=%@",[followers JSONRepresentation]];
		NSLog(@"the strJson: %@",temp_strJson);

		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		[l_theConnection start];
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark delete a friend 
-(void) deletefriend:(SEL)tempSelector tempTarget:(id)tempTarget  custId:(NSString *)custId friendid:(NSString *)friendid
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;

	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
				//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=unFriend&custid=%@&friendid=%@",kServerUrl,custId,friendid];
		NSLog(@"value for name is%@",custId);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark - Get stories from near my location
-(void)getNearStories:(SEL)tempSelector tempTarget:(id)tempTarget Lattitude:(double) lattitude Longitude:(double) longitude Count:(int) count
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{

		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getNearStories&lat=%f&lng=%f&num=%d",kServerUrl, lattitude, longitude
                  , count];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		

		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];

		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

#pragma mark get Unread Request For Mine
-(void)getUnreadRequestsForMine:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getMyRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,userid,type,number,pageNo];
	//http://66.28.216.132/salebynow/json.htm?action=getMyRequestSummaries&custid=99&type=IBoughtIt&num=2&pagenum=1
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark get Unread Requests For Friends
-(void)getUnreadRequestsForFriends:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFriendsRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,userid,type,number,pageNo];
		//http://66.28.216.132/salebynow/json.htm?action=getFriendsRequestSummaries&custid=99&type=IBoughtIt&num=1&pagenum=1
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}
#pragma mark -
#pragma mark get unread requests for following
-(void)getUnreadRequestsForFollowing:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFollowingRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,userid,type,number,pageNo];
		//http://66.28.216.132/salebynow/json.htm?action=getFollowingRequestSummaries&custid=121&type=IBoughtIt&num=2&pagenum=1
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}
#pragma mark -
#pragma mark get unread request for popular
-(void)getUnreadRequestsForPopular:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,userid,type,number,pageNo];
		//http://66.28.216.132/salebynow/json.htm?action=getAllRequestSummaries&custid=99&type=IBoughtIt&num=2&pagenum=1		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}


#pragma mark -
#pragma mark getdata for a particular request
-(void)getDataForParticularRequest:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(NSInteger)messageId
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFittingRoomRequest&custid=%@&msgid=%d",kServerUrl,userid,messageId];
		//http://66.28.216.132/salebynow/json.htm?action=getRequest&custid=14&msgid=13
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}
#pragma mark -
#pragma mark add response for fitting room request
-(void)addResponseForFittingRoomRequest:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*) userid msgresponse:(NSMutableDictionary*)msgresponse
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=[tempTarget retain];

	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
				NSLog(@"the dictionary is %@",msgresponse);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addFittingRoomResponse&custid=%@",kServerUrl,userid];
		//	http://66.28.216.132/salebynow/json.htm?action=addResponse&custid=77&msgresponse={"isFavour":"Y","messageId":"16","responseWords":"My Response..","type":"BuyItOrNot"}
		
		
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSString *temp_strJson =[[NSString alloc]initWithFormat:@"",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		NSString *temp_strJson=[NSString stringWithFormat:@"msgresponse=%@",[msgresponse JSONRepresentation]];
		NSLog(@"the strJson: %@",temp_strJson);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

#pragma mark -
#pragma mark get Unread requests for nearby
-(void)getUnreadRequestsForNearby:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getNearByRequestSummaries&custid=%@&type=%@&dist=%d&num=%d&pagenum=%d",kServerUrl,userid,type,50,number,pageNo];
		//:http://66.28.216.132/salebynow/json.htm?action=getNearByRequestSummaries&custid=softprodigy@gmail.com&type=buyitornot&dist=10&num=4&pagenum=1	temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
}
#pragma mark -
#pragma mark unfollow a friend
-(void)unfollowAFriend:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*) userid following:(NSString*)personFollowing{
	{
			l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
		[l_appDelegate CheckInternetConnection];
		callBackSelector=tempSelector;
		callBackTarget=tempTarget;
		if(l_appDelegate.m_internetWorking==0)//0: internet working
		{
			//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
			m_mutResponseData=[[NSMutableData alloc] init];
			
			//	self.view.userInteractionEnabled=FALSE;
			//	NSLog(@"The Array contains%@",tmpUids);
			//[m_activityIndicator startAnimating];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
			NSMutableString *temp_url;
			temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=unFollow&custid=%@&following=%@",kServerUrl,userid,personFollowing];
			//http://66.28.216.132/salebynow/json.htm?action=unFollow&custid=134&following=133
			temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			
			NSLog(@"%@",temp_url);
			
			
			//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
			//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
			//	NSLog(@"registration strJson: %@",temp_strJson);
			
			//[temp_objSBJson release];
			//temp_objSBJson=nil;
			
			NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																	  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
			
			NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
			
			//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
			[theRequest setAllHTTPHeaderFields:headerFieldsDict];
			[theRequest setHTTPMethod:@"GET"];
			
			l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
			
			[l_theConnection start];
			
			if(l_theConnection)
			{
				NSLog(@"Request sent to get data");
			}
			//[temp_strJson release];
		}
		else
		{
			//self.view.userInteractionEnabled=TRUE;
			//[m_activityIndicator stopAnimating];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[netCheckAlert show];
			[netCheckAlert release];
			[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		}
	}
	
}
#pragma mark -
#pragma mark answer friend request	
-(void)answerFriendRequest:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*) userid friendid:(NSString*)friendid acceptString:(NSString*)acceptString{
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=answerFriendRequest&custid=%@&friendid=%@&accept=%@",kServerUrl,userid,friendid,acceptString];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		//http://66.28.216.132/salebynow/json.htm?action=answerFriendRequest&custid=shilpi2010@gmail.com&friendid=softprodigy@gmail.com&accept=Y
		
			
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
												  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

#pragma mark -
#pragma mark getMyPagePhotos
-(void)getSearchResults:(SEL)tempSelector tempTarget:(id)tempTarget searchText:(NSString *)searchText pageNo:(NSString *)pageNo
{
		//l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		//		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=shopBeeSearch&searchtxt=%@&pagenum=%@&num=%@",kServerUrl,searchText,pageNo,@"20"];
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=shopBeeSearch&searchtxt=%@&pagenum=%@&num=%@&lat=%f&lng=%f&dist=50",kServerUrl,searchText,pageNo,@"10",l_appDelegate.m_geoLatitude ,l_appDelegate.m_geoLongitude];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}

-(void)iboughtitforAparticularItem:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=announceChangeInBuyItOrNot&custid=%@&msgid=%d",kServerUrl,userid,messageId];
		// http://66.28.216.132/salebynow/json.htm?action=announceChangeInBuyItOrNot&msgid=79
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		
	}
}
#pragma mark -
#pragma mark change in mind action
-(void)announceAChangeInMind:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=announceChangeInIBoughtIt&custid=%@&msgid=%d",kServerUrl,userid,messageId];
		//http://66.28.216.132/salebynow/json.htm?action=announceChangeInIBoughtIt&msgid=294
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		
	}
	
}	
#pragma mark -
#pragma mark Turn Pings On or off
-(void)TurnPingsOnOrOff:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*)userid friendid:(NSString*)friendid pingStatus:(NSString*)pingStatus;
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;

	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
				//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSLog(@"value for name is%@",userid);
		NSLog(@"value for name is%@",friendid);
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=setPingStatus&custid=%@&friendid=%@&status=%@",kServerUrl,userid,friendid,pingStatus];
		//http://66.28.216.132/salebynow/json.htm?action=setPingStatus&custid=12&friendid=11&status=On
			temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
	
}
#pragma mark -
#pragma mark set Privacy Settings
-(void)setPrivacySettings:(SEL)tempSelector tempTarget:(id)tempTarget settingDict:(NSMutableDictionary*)settingDict
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=[tempTarget retain];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
	
		//NSLog(@"the dictionary is %@",updateDictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=setPrivacySettings",kServerUrl];
		//	NSLog(@"value for rating is%i",userRating);//http://66.28.216.132/salebynow/json.htm?action=setPrivacySettings&settings={"aboutMeSetting":"All","userId":"hello@yahoo.com"}
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSString *temp_strJson =[[NSString alloc]initWithFormat:@"",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		NSString *temp_strJson=[NSString stringWithFormat:@"settings=%@",[settingDict JSONRepresentation]];
		NSLog(@"the strJson: %@",temp_strJson);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}

}
#pragma mark -
#pragma mark flag a particular message
-(void)FlagAMessage:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId flagType:(NSString*)flagType
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=userFlagRequest&custid=%@&msgid=%d&type=%@",kServerUrl,userid,messageId,flagType];
		//http://66.28.216.132/salebynow/json.htm?action=userFlagRequest&msgid=31&type=prohibited
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		
	}
}	
#pragma mark -
#pragma mark forgotpassword

-(void)forgotPassword:(SEL)tempSelector tempTarget:(id)tempTarget email:(NSString *)email
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=forgotPassword&email=%@",kServerUrl,email];
		//NSLog(@"value for name is%@",customerID);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[uids dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		
	}
	
	
}
#pragma mark-
#pragma mark get privacy settings 
-(void)getPrivacySettings:(SEL)tempSelector tempTarget:(id)tempTarget custid:(NSString*)custid
{
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=tempTarget;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:FALSE];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//	self.view.userInteractionEnabled=FALSE;
		//	NSLog(@"The Array contains%@",tmpUids);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getPrivacySettings&custid=%@",kServerUrl,custid];
		//http://66.28.216.132/salebynow/json.htm?action=getPrivacySettings&custid=136
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
		
		//SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@",[temp_objSBJson stringWithObject:m_dictionary]];
		//	NSLog(@"registration strJson: %@",temp_strJson);
		
		//[temp_objSBJson release];
		//temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		//[temp_strJson release];
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		
	}
	
}
#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSLog(@"ShopbeeAPIs:didReceiveResponse");
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"%d",m_intResponseCode);
	
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	NSLog(@"ShopbeeAPIs:didReceiveData");
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"ShopbeeAPIs:connectionDidFinishLoading");
	//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
	NSString *str=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
	NSLog(@"String received from the Request: \n%@\n\n", str);
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
	[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:m_intResponseCode] withObject:m_mutResponseData];
	//if (m_mutResponseData)
//	{
//		[m_mutResponseData release];
//		m_mutResponseData=nil;
//	}
	[str release];
	str=nil;
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"ShopbeeAPIs:didFailWithError %@", [error localizedDescription]);
	//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
	if (m_mutResponseData)
	{
	[m_mutResponseData release];
		m_mutResponseData=nil;
	}
	
	[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
		
}

#pragma mark - memory management

- (void) dealloc {
    // I expect this was already released.
    //[l_theConnection release];
    
    [m_mutResponseData release];
    
    [super dealloc];
}

@end
