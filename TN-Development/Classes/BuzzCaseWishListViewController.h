//
//  BuzzCaseWishListViewController.h
//  QNavigator
//
//  Created by Mac on 9/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface BuzzCaseWishListViewController : UIViewController {
    AsyncImageView *buzzCaseViewPersonImage;
    AsyncImageView *buzzCaseViewProductImage;
    UILabel *buzzCaseViewTitleLabel;
    UILabel *buzzCaseViewCommentLabel;
    UIView *buzzCaseViewMoreInfo;
    NSDictionary *datadict;
    UILabel *titleLabel;
    IBOutlet UIScrollView *buzzyCaseScrollView;
    IBOutlet UILabel *m_headLineLBL;
    IBOutlet UITextView  *whoWhatTextView;
    IBOutlet UITextView *Whowhattitletext;
}
@property(retain,nonatomic) IBOutlet UILabel *titleLabel;
@property(retain,nonatomic)NSDictionary *datadict;
@property(retain,nonatomic)IBOutlet UIView *buzzCaseViewMoreInfo;
@property(retain,nonatomic)IBOutlet UILabel *buzzCaseViewTitleLabel;
@property(retain,nonatomic)IBOutlet UILabel *buzzCaseViewCommentLabel;
@property(retain,nonatomic)AsyncImageView *buzzCaseViewPersonImage;
@property(retain,nonatomic)AsyncImageView *buzzCaseViewProductImage;
@property(nonatomic,retain)IBOutlet UIScrollView *buzzyCaseScrollView;
@property(nonatomic,retain)IBOutlet UILabel *m_headLineLBL;
@property(nonatomic,retain)IBOutlet UITextView  *whoWhatTextView;
@property(nonatomic,retain)IBOutlet UITextView *Whowhattitletext;
-(IBAction) m_goToBackView;
- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict;
-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle;
-(NSString*)dateReFormated:(NSString*)date;
@end
