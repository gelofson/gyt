//
//  BuzzButton ShareNowFunctionality.m
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuzzButtonShareNowFunctionality.h"
#import "BuzzButtonCameraFunctionality.h"
#import "Constants.h"
#import "BuzzButtonAddNewContact.h"
#import "BuzzButtonAddFromAddressBook.h"
#import "QNavigatorAppDelegate.h"
#import "LoadingIndicatorView.h"
#import "UploadImageAPI.h"
#import "NewsDataManager.h"
#import "Context.h"
#import "UploadVideoOperation.h"
#import "NSString+URLEncoding.h"

QNavigatorAppDelegate *l_appDelegate;

@interface BuzzButtonShareNowFunctionality (PRIVATE)
- (void) sendToRunway;
- (void)pushBuzzScreenWithText:(NSString*)text;
- (void)postToFacebookWall:(NSString *) photoUrl;
- (void)postToTwitterWall:(NSString *) photoUrl;
- (void) performDeleteOfContactsIfAny;
- (void) performPhotoShare;
@end

@implementation BuzzButtonShareNowFunctionality

@synthesize twitterEngine;

@synthesize m_mutResponseData;
@synthesize m_intResponseCode;
@synthesize m_intRequestType;
@synthesize m_photoUrl;

@synthesize m_strType;
@synthesize m_strMessage;
@synthesize m_whoMessage;
@synthesize m_whatMessage;
@synthesize m_whenMessage;
@synthesize m_whereMessage;
@synthesize m_howMessage;
@synthesize m_whyMessage;
@synthesize m_openMessage;
@synthesize m_headerMessage;

@synthesize m_emailTableView;
@synthesize m_facebookButton;
@synthesize m_twitterButton;
@synthesize m_selectAllButton;
@synthesize m_sectionHeaderView;
@synthesize m_photoView;
@synthesize photoImage;
@synthesize m_DictOfContactDicts;
@synthesize m_newlyAddedContactsDict;
@synthesize m_deletedContactsDict;
@synthesize addrBookVc;
//@synthesize twitterProcessingVc;
@synthesize shareVideo;
@synthesize buzzid;
@synthesize m_emailsView;
@synthesize m_selectEmailButton;
@synthesize m_shareNowView;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{

	[super viewDidLoad];
    self.trackedViewName = @"Publish Page";
	addrBookVc = nil;
	m_photoUrl = nil;
	twitterEngine = nil;
    
       /* NSLog(@"header %@",self.m_strMessage);
        NSLog(@"who %@",self.m_whoMessage);
        NSLog(@"what %@",self.m_whatMessage);
        NSLog(@"when %@",self.m_whenMessage);
        NSLog(@"where %@",self.m_whereMessage);
        NSLog(@"how %@",self.m_howMessage);
        NSLog(@"why %@",self.m_whyMessage);
        NSLog(@"str %@",self.m_strType);
        NSLog(@"open %@",self.m_openMessage);
    NSLog(@"header %@",self.m_headerMessage);*/

	NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_DictOfContactDicts = dict;
	[dict release];
	dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_newlyAddedContactsDict = dict;
	[dict release];
	dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_deletedContactsDict = dict;
	[dict release];

	if (self.photoImage != nil) {
		[self.m_photoView setImage:self.photoImage];
        [self.m_photoView setContentMode:UIViewContentModeScaleAspectFill];
        self.m_photoView.clipsToBounds=YES;
	}
	
	self.m_facebookButton.selected = NO;
	self.m_twitterButton.selected = NO;
	self.m_selectAllButton.selected = YES;
    
    /*CALayer * layer = self.m_emailTableView.layer;
    layer.borderWidth = 3.0f;
    layer.borderColor = [UIColor colorWithRed:104.0f/255.0f green:176.0f/255.0f blue:223.0f/255.0f alpha:1.0].CGColor;*/
    
    self.m_photoView.layer.borderWidth = 2;
    self.m_photoView.layer.borderColor = [UIColor lightGrayColor].CGColor;
	
	[l_appDelegate CheckInternetConnection];
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		// Call FashionGram API to invoke share
		[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		NSString *temp_url;
		temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=getUserContacts&custid=%@",kServerUrl, customerID];
		temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];

		NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
		self.m_intRequestType = SHARENOW_REQUEST_TYPE_GETCONTACT;
		[conn start];
	}
    

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

#if TARGET_IPHONE_SIMULATOR == 1
#else 
	// Merge selected contacts from address book
	if (self.addrBookVc != nil) {
		
		if ([self.addrBookVc.m_addrBookEmailContacts count] > 0) {
			for (NSString * keyStr in [self.addrBookVc.m_addrBookEmailContacts allKeys]) {
				NSMutableDictionary * contactDict = [self.addrBookVc.m_addrBookEmailContacts objectForKey:keyStr];
				NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
				if ((selectionValue != nil) && ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
					NSMutableDictionary * existEntry = [self.m_DictOfContactDicts objectForKey:keyStr];
					if (existEntry != nil) {
						// entry exists, by name
						[existEntry setObject:@"1" forKey:@"selectionValue"];
						continue;
					}
					// check each email
					NSString * emailStr = [contactDict objectForKey:@"emailValue"];
					BOOL emailFound = NO;
					for (NSString * existKeyStr in [self.m_DictOfContactDicts allKeys]) {
						NSMutableDictionary * aDict = [self.m_DictOfContactDicts objectForKey:existKeyStr];
						NSString * aEmail = [aDict objectForKey:@"emailValue"];
						if ((aEmail != nil) && ([aEmail caseInsensitiveCompare:emailStr] == NSOrderedSame)) {
							[aDict setObject:@"1" forKey:@"selectionValue"];
							emailFound = YES;
							break;
						}
					}

					if (emailFound == NO) {
						[self.m_newlyAddedContactsDict setObject:contactDict forKey:keyStr];
						[self.m_DictOfContactDicts setObject:contactDict forKey:keyStr];
					}

				}
			}
		} 

		self.addrBookVc = nil;
	}
#endif

	BOOL selectedAll = YES;
	if ([self.m_DictOfContactDicts count] > 0) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
			NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
			if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
				selectedAll = NO;
				break;
			}
		}
	} else {
		selectedAll = NO;
	}

	self.m_selectAllButton.selected = selectedAll;

    [self.m_emailTableView reloadData];

}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
 
-(IBAction)goToBackView
{
	[self.navigationController popToRootViewControllerAnimated:YES];
    [tnApplication goToNewsPage];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
}

- (void)dealloc {
    
	self.m_photoUrl = nil;
	self.m_mutResponseData = nil;
	//self.twitterProcessingVc = nil;
	self.m_strMessage = nil;
    self.m_whoMessage=nil;
    self.m_whatMessage=nil;
    self.m_whenMessage=nil;
    self.m_whereMessage=nil;
    self.m_howMessage=nil;
    self.m_whyMessage=nil;
    self.m_openMessage=nil;
    self.m_headerMessage=nil;
	self.addrBookVc = nil;
	self.m_emailTableView = nil;
	self.m_facebookButton = nil;
	self.m_twitterButton = nil;
	self.m_selectAllButton = nil;
	self.m_sectionHeaderView = nil;
	self.m_photoView = nil;
	self.photoImage = nil;
	self.m_DictOfContactDicts = nil;
	self.m_newlyAddedContactsDict = nil;
	self.m_deletedContactsDict = nil;
    self.buzzid = nil;
    self.m_selectEmailButton = nil;
    self.m_shareNowView = nil;
    [super dealloc];
}

-(IBAction) contactsSelectAllToggleAction:(id) sender {
	UIButton* button = (UIButton*)sender;
    button.selected = !button.selected;
	self.m_intRequestType = 0;
	
	if ([self.m_DictOfContactDicts count] > 0) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];

			if (button.selected == YES)
				[contactDict setObject:@"1" forKey:@"selectionValue"];
			else
				[contactDict setObject:@"0" forKey:@"selectionValue"];
		}
	} 
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzShare"
                                                    withAction:@"contactsSelectAllToggleAction"
                                                     withLabel:nil
                                                     withValue:[NSNumber numberWithBool:button.selected]];

	[self.m_emailTableView reloadData];
}

#ifdef FB_OLD_SDK_USAGE
-(IBAction) facebookToggleAction:(id) sender {
	
	self.m_intRequestType = 0;
	if (self.m_facebookButton.selected == NO) {
		// User wants to enable FB share, check if FB login is available, else launch
		if (l_appDelegate.m_session == nil) {
			l_appDelegate.m_session = [FBSession sessionForApplication:_APP_KEY 
												  secret:_SECRET_KEY delegate:self];
		} else {
			if ([l_appDelegate.m_session.delegates indexOfObject:self] == NSNotFound)
				[l_appDelegate.m_session.delegates addObject:self];
		}
		
		localFBSession = [FBSession session];
		if (localFBSession.uid <= 0) {
            FBLoginDialog * fbLoginDialog = [[[FBLoginDialog alloc] initWithSession:localFBSession] autorelease];
			[fbLoginDialog show];
		} else {
			self.m_facebookButton.selected = YES;
		}
	} else {
		self.m_facebookButton.selected = NO;
	}
}
#else

-(IBAction) facebookToggleAction:(id) sender 
{
    self.m_intRequestType = 0;
	if (self.m_facebookButton.selected == NO) {
		// User wants to enable FB share, check if FB login is available, else launch
        tnApplication.m_session.sessionDelegate = self;
        if (![[tnApplication m_session] isSessionValid]) {
            NSArray *permissions =  [NSArray arrayWithObjects:@"user_photos",@"read_stream", @"publish_stream", @"offline_access",nil];
            [[tnApplication m_session] authorize:permissions];
        }
        
        else {
			self.m_facebookButton.selected = YES;
		}
	} else {
		self.m_facebookButton.selected = NO;
	}
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzShare"
                                                    withAction:@"facebookToggleAction"
                                                     withLabel:nil
                                                     withValue:[NSNumber numberWithBool:self.m_facebookButton.selected]];
}

#endif

-(IBAction) chooseEmail:(id) sender
{
    self.view = self.m_emailsView;
}

- (IBAction)backToShare:(id)sender
{
    self.view = self.m_shareNowView;
    if ([self thereAreSelectedContacts]) {
        m_selectEmailButton.selected = YES;
    } else
        m_selectEmailButton.selected = NO;
}


-(IBAction) twitterToggleAction:(id) sender {
	
	self.m_intRequestType = 0;
	if (self.m_twitterButton.selected == NO) {
		// User wants to enable TW share, check if TW login is available, else launch
		/*
		TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
		temp_twitterPross.m_addFrndMypageVC=self;
		self.twitterProcessingVc = temp_twitterPross;
		[temp_twitterPross release];
		
		[self.twitterProcessingVc btnTellFriendByTwitterAction:99];
		
		self.m_twitterButton.selected = YES;
		*/
		if(twitterEngine == nil)
		{  
			twitterEngine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];  
			twitterEngine.consumerKey    = kOAuthConsumerKey;  
			twitterEngine.consumerSecret = kOAuthConsumerSecret;  
		}
		
		//	BOOL isEmpty = [ self validateIsEmpty:txtField.text];                                               
		if(![twitterEngine isAuthorized])
		{  
			UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:twitterEngine delegate:self];  
			if (controller)
				[self presentModalViewController: controller animated: YES];
		} else {
			self.m_twitterButton.selected = YES;
		}
	} else {
		self.m_twitterButton.selected = NO;
	}
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzShare"
                                                    withAction:@"twitterToggleAction"
                                                     withLabel:nil
                                                     withValue:[NSNumber numberWithBool:self.m_twitterButton.selected]];
}

-(IBAction) contactSelectToggleAction:(id) sender {
	UIButton* button = (UIButton*)sender;
    button.selected = !button.selected;
	
	if (button.selected == YES) {
	}
}

- (BOOL) thereAreSelectedContacts
{
    BOOL selectedContacts = NO;
    if ((self.m_DictOfContactDicts) && ([self.m_DictOfContactDicts count] > 0)) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
			NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
			if ((selectionValue != nil) && ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
                selectedContacts = YES;
			}
		}
	}
    
    return selectedContacts;

}

-(void)showLoadingView
{
	[[LoadingIndicatorView SharedInstance]startLoadingView:self];
}

-(void)stopLoadingView
{
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
}

- (void) showMessageAlertOnMainThread:(NSString *) title message:(NSString *)message
{
    UIAlertView * av = [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [av   performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

- (void) onShareViaPhotoEmail:(NSURLResponse *) response data:(NSData *) data
{
    NSLog(@"BuzzButtonShareNowFunctionality:sharePhotoInfoViaEmail:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	self.m_intResponseCode = [httpResponse statusCode];
	
    if (self.m_intResponseCode == 200) {
        NSString *temp_string=[[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        SBJSON *temp_objSBJson = [[[SBJSON alloc]init] autorelease];
        NSArray *contArr = ![temp_string isEqualToString:@"[null]"] ? [temp_objSBJson objectWithString:temp_string] : nil;
        // ugly workaround
        BOOL success = [temp_string isEqualToString:@"[null]"];
        
        NSLog(@"BuzzButtonShareNowFunctionality temp_string = %@", temp_string);
        if (success || ((contArr != nil) && ([contArr isKindOfClass:[NSArray class]] == YES) && ([contArr count] > 0))) {
            
            if (!success && [contArr count] == 1) {
                NSLog(@"BuzzButtonShareNowFunctionality unsuccessful upload contArr = %@", contArr);
                
                NSString * str = [contArr objectAtIndex:0];
                if ((str == nil) || ([str isKindOfClass:[NSString class]] == NO) || (([str length] > 0) && ([str rangeOfString:@"\"false\""].location != NSNotFound)) ) {
                    
                }
                isPublishing = NO;
                
                return;
                
            } else {
                // Success
                if (success || [contArr count] > 1) {
                    if (self.m_facebookButton.selected) {
                        NSString * str = [contArr objectAtIndex:1];
                        self.m_photoUrl = [[NSString stringWithFormat:@"%@/%@",kServerUrl,str] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        [self postToFacebookWall:self.m_photoUrl];
                        return;
                    }
                    
                    NSLog(@"Posting successful - %@", temp_string);
                    
                    isPublishing = NO;
                    
                    return;
                } else {
                    NSString * str = [contArr objectAtIndex:1];
                    
                    self.m_photoUrl = [[NSString stringWithFormat:@"%@/%@",kServerUrl,str] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    [self postToFacebookWall:self.m_photoUrl];
                    
                    
                    [temp_string release]; temp_string = nil;
                    [temp_objSBJson release]; temp_objSBJson = nil;
                    
                    
                    return;
                }
            }
        }
        
        isPublishing = NO;
        
        NSLog(@"Invalid server response");
        
    } else {
        NSString * resp = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([resp rangeOfString:@"401"].length != 0) {
            [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
        } else {
            NSLog(@"Failed to upload photo.\n\n%@", resp);
            
        }
        isPublishing = NO;
        [[LoadingIndicatorView SharedInstance]stopLoadingView];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }

}

- (void) bgPublishPicture
{
    [self sendToRunway];
    if ([self.m_newlyAddedContactsDict count] > 0) {
        [l_appDelegate CheckInternetConnection];
        
        if(l_appDelegate.m_internetWorking==0)//0: internet working
        {
            // Call FashionGram API to invoke share
            m_mutResponseData=[[NSMutableData alloc] init];
            
            NSMutableArray * accArr = [[NSMutableArray alloc] initWithCapacity:3];
            if ((self.m_newlyAddedContactsDict) && ([self.m_newlyAddedContactsDict count] > 0)) {
                for (NSString * keyStr in [self.m_newlyAddedContactsDict allKeys]) {
                    NSMutableDictionary * contactDict = [self.m_newlyAddedContactsDict objectForKey:keyStr];
                    NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
                    NSString * nameAddr = [contactDict objectForKey:@"nameValue"];
                    NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:2];
                    [dict setObject:emailAddr forKey:@"email"];
                    [dict setObject:nameAddr forKey:@"name"];
                    [accArr addObject:dict];
                    [dict release];
                }
            }
            if ([accArr count] > 0)  {
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *customerID= [prefs objectForKey:@"userName"];
                NSString *temp_url;
                temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=addUserContacts&custid=%@",kServerUrl, customerID];
                temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"%@",temp_url);
                
                NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
                                                                          cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
                
                NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
                
                NSString *temp_strJson=[NSString stringWithFormat:@"contacts=%@",[accArr JSONRepresentation]];
                NSLog(@"temp_strJson=[%@]",temp_strJson);
                [theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
                [theRequest setAllHTTPHeaderFields:headerFieldsDict];
                [theRequest setHTTPMethod:@"POST"];
                
                NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
                self.m_intRequestType = SHARENOW_REQUEST_TYPE_ADDCONTACT;
                [conn start];
            }
            [accArr release];
        }
    } else {
        [self performDeleteOfContactsIfAny];
    }
}

-(IBAction) doShareNow:(id) sender {
	// Call FashionGram API to invoke share
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzShare"
                                                    withAction:@"Publish"
                                                     withLabel:nil
                                                     withValue:nil];
    [self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
    if (self.shareVideo) {
        if (!isPublishing) {
            isPublishing = YES;
            [self performSelectorInBackground:@selector(sendVideoToServer) withObject:nil];
        }
    } else {
        if (!isPublishing) {
            isPublishing = YES;
            [self performSelectorInBackground:@selector(bgPublishPicture) withObject:nil];
        }
    }
    
    // Optimistic message on picture posting and pop back
    
    [[LoadingIndicatorView SharedInstance]stopLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    NSString *message;
    if (self.shareVideo) {
        message = @"Your video was uploaded and should be available for viewing very soon.";
    } else {
        message = @"Your post was uploaded and should be available for viewing very soon.";
    }
    
    UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [av show]; [av release];
    [tnApplication goToNewsPage];
    
    
	// Send newly added email addresses
}

-(void) performDeleteOfContactsIfAny {
	
	// Send newly added email addresses
	if ([self.m_deletedContactsDict count] > 0) {
		[l_appDelegate CheckInternetConnection];
		
		if(l_appDelegate.m_internetWorking==0)//0: internet working
		{
			// Call FashionGram API to invoke share
			[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
			m_mutResponseData=[[NSMutableData alloc] init];
			
			NSMutableArray * accArr = [[NSMutableArray alloc] initWithCapacity:3];
			if ((self.m_deletedContactsDict) && ([self.m_deletedContactsDict count] > 0)) {
				for (NSString * keyStr in [self.m_deletedContactsDict allKeys]) {
					NSMutableDictionary * contactDict = [self.m_deletedContactsDict objectForKey:keyStr];
					NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
					[accArr addObject:emailAddr];
				}
			}
			if ([accArr count] > 0)  {
			
				[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				NSString *temp_url;
				temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=deleteUserContacts&custid=%@",kServerUrl, customerID];
				temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				NSLog(@"%@",temp_url);
				
				NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																		  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
				
				NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
			
				NSString *temp_strJson=[NSString stringWithFormat:@"listids=%@",[accArr JSONRepresentation]];
				NSLog(@"temp_strJson=[%@]",temp_strJson);
				[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
				[theRequest setAllHTTPHeaderFields:headerFieldsDict];
				[theRequest setHTTPMethod:@"POST"];

				NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
				self.m_intRequestType = SHARENOW_REQUEST_TYPE_DELCONTACT;
				[conn start];
			}
			[accArr release];
		}
	} else {
		[self performPhotoShare];
	}
}

-(void) performPhotoShare
{
	if (!self.buzzid) {
        return;
    }
    
	NSMutableDictionary * argDict = [[NSMutableDictionary alloc] initWithCapacity:14];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	[argDict setObject:customerID forKey:@"custid"];

	if ((self.m_strMessage != nil) && ([self.m_strMessage length] > 0))
		[argDict setObject:self.m_strMessage forKey:@"comments"];
	else
		[argDict setObject:@" " forKey:@"comments"];


	if(self.m_strType != nil)
		[argDict setObject:self.m_strType forKey:@"type"];
	else
        [argDict setObject:@"" forKey:@"type"];
	NSMutableArray * emailArr = [[NSMutableArray alloc] init];
	if ((self.m_DictOfContactDicts) && ([self.m_DictOfContactDicts count] > 0)) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
			NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
			if ((selectionValue != nil) && ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
				NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
				[emailArr addObject:emailAddr];			
			}
		}
	}
	if ([emailArr count] <= 0) {
		[emailArr addObject:customerID];
	}
	[argDict setObject:emailArr  forKey:@"emails"]; 
	[emailArr release];
	if (self.photoImage != nil) {
		NSData * d=UIImageJPEGRepresentation(self.photoImage, 0.5);
		NSUInteger len = [d length];
		const char *raw=[d bytes];
		//creates the byte array from image
		NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
		for (long int i = 0; i < len; i++) {
			[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
		}
		[argDict setObject:tempByteArray forKey:@"photo"];
		[tempByteArray release];
	}
    
    if (self.m_headerMessage !=nil)
    {
        [argDict setObject:self.m_headerMessage forKey:@"headline"];
    }
    if (self.m_whoMessage !=nil)
        [argDict setObject:self.m_whoMessage forKey:@"who"];
    else
        [argDict setObject:@"" forKey:@"who"];
    
    if (self.m_whatMessage !=nil)
        [argDict setObject:self.m_whatMessage forKey:@"what"];
    else
        [argDict setObject:@"" forKey:@"what"];
    
//    if (self.m_whenMessage !=nil)
//        [argDict setObject:self.m_whenMessage forKey:@"when"];
//    else
//        [argDict setObject:@"" forKey:@"when"];
//    
//    if (self.m_whereMessage !=nil)
//        [argDict setObject:self.m_whereMessage forKey:@"where"];
//    else
//        [argDict setObject:@"" forKey:@"where"];
    
    if (self.m_howMessage !=nil)
        [argDict setObject:self.m_howMessage forKey:@"how"];
    else
        [argDict setObject:@"" forKey:@"how"];
    
    if (self.m_whyMessage !=nil)
        [argDict setObject:self.m_whyMessage forKey:@"why"];
    else
        [argDict setObject:@"" forKey:@"why"];
    
//    if (self.m_openMessage !=nil)
//        [argDict setObject:self.m_openMessage forKey:@"oped"];
//    else
//        [argDict setObject:@"" forKey:@"oped"];
    
    [argDict setObject:self.buzzid forKey:@"buzzid"];
    
    [l_appDelegate CheckInternetConnection];
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		// Call FashionGram API to invoke share
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=sharePhotoInfoViaEmail",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		NSString *temp_strJson=[NSString stringWithFormat:@"emailinfo=%@",[argDict JSONRepresentation]];
		//NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		NSLog(@"temp_strJson=[%@]",temp_strJson);
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
        self.m_intRequestType = SHARENOW_REQUEST_TYPE_SHARENOW;
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *adata, NSError *jsonError) {
            if (jsonError) {
                NSLog(@"Couldn't send photo via email - %@", [jsonError localizedDescription]);
            } else
                [self onShareViaPhotoEmail:response data:adata];
        }] ;
	}
    
    [argDict release]; argDict = nil;

}

-(IBAction) addNewEmailAction:(id) sender {
    
//	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From my contact list",@"Enter new address",nil];
//    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
//    [actionSheet showInView:self.view];
//    [actionSheet release];
    
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzShare"
                                                    withAction:@"addNewEmailAction"
                                                     withLabel:nil
                                                     withValue:nil];
    BuzzButtonAddNewContact * vc = [[BuzzButtonAddNewContact alloc] init];
    vc.m_DictOfContactDicts = self.m_DictOfContactDicts;
    vc.m_newlyAddedContactsDict = self.m_newlyAddedContactsDict;
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [m_DictOfContactDicts count];
	//return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell * cell = nil;
	static NSString *cellIdentifier= @"ShareNow_CellIdentifier";
	
	cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell==nil) {
		cell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(13,5,15,15)];
		//[temp_imgView setImage:l_objIndyShopModel.m_imgItemImage]; //@"clothes.png"]];
		[temp_imgView setImage:[UIImage imageNamed:@"sharenow_checkbox_disabled"]];
		temp_imgView.contentMode=UIViewContentModeScaleAspectFit;
		temp_imgView.tag = CONTACT_ENTRY_SELECT_TAG;
		[cell.contentView addSubview:temp_imgView];
		[temp_imgView release];

		UILabel *temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(38,2,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:kHelveticaBoldFont size:16];
		temp_lblTitle.textColor=[UIColor blackColor];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"Greg Elofson";
		temp_lblTitle.tag = CONTACT_ENTRY_NAME_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
		/*temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(38,24,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:@"GillSans" size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:112.0f/255.0f green:112.0f/255.0f blue:112.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"greg.elofson@gmail.com";
		temp_lblTitle.tag = CONTACT_ENTRY_EMAIL_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];*/
	
	}
		
	NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
        

        
        UIImageView * iv = (UIImageView *) [cell.contentView viewWithTag:CONTACT_ENTRY_SELECT_TAG];

		NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
		if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
			[iv setImage:[UIImage imageNamed:@"sharenow_checkbox_disabled"]];
		}
		else {
			[iv setImage:[UIImage imageNamed:@"sharenow_checkbox_enabled"]];
		}
        
        UILabel * lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_NAME_TAG];
        NSString * nameValue = [NSString stringWithFormat:@"\"%@\" %@", [contactDict objectForKey:@"nameValue"], [contactDict objectForKey:@"emailValue"]];
        lbl.text = nameValue;
        
        /*lbl=nil;
        lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_EMAIL_TAG];
        NSString * emailValue = [contactDict objectForKey:@"emailValue"];
        lbl.text = emailValue;*/

    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 30.0f;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
		if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        	NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
			[self.m_deletedContactsDict setObject:[self.m_DictOfContactDicts objectForKey:keyStr] forKey:keyStr];
			[self.m_DictOfContactDicts removeObjectForKey:keyStr];
			BOOL selectedAll = YES;
			if ([self.m_DictOfContactDicts count] > 0) {
				for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
					NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
					NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
					if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
						selectedAll = NO;
						break;
					}
				}
			} else {
				selectedAll = NO;
			}

			self.m_selectAllButton.selected = selectedAll;
		}
        [self.m_emailTableView reloadData];
	} else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
        

        
        NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
        if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
            selectionValue = @"0";
        	[contactDict setObject:selectionValue forKey:@"selectionValue"];
			if (self.m_selectAllButton.selected == YES) {
				self.m_selectAllButton.selected = NO;
			}
		} else {
            selectionValue = @"1";
        	[contactDict setObject:selectionValue forKey:@"selectionValue"];
			BOOL selectedAll = YES;
			if ([self.m_DictOfContactDicts count] > 0) {
				for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
					NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
					NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
					if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
						selectedAll = NO;
						break;
					}
				}
			} else {
				selectedAll = NO;
			}

			self.m_selectAllButton.selected = selectedAll;
		}
        
        [contactDict setObject:selectionValue forKey:@"selectionValue"];
        
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"BuzzShare"
                                                        withAction:@"selectedExistingEmail"
                                                         withLabel:nil
                                                         withValue:nil];
        [tableView reloadData];
    }



}

- (void) sendVideoToServer
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"/tempVid.MOV"];
    
    NSString *str = [NSString stringWithFormat:@"%@/salebynow/upload",kServerUrl];
    UploadVideoOperation *op = [[[UploadVideoOperation alloc] init] autorelease];
    op.urlString = str;
    op.tempPath = tempPath;
    
    op.delegate = self;
    
    NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
    [queue addOperation:op];
    
}

- (void) problemWithSendingVideo
{
    [self showMessageAlertOnMainThread:@"Server Error" message:@"Video upload was unsuccessful.\nPlease try again later."];
}

- (void) sendVideoBuzz:(NSString *) videoFile
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	UploadImageAPI * l_buzzRequest=[UploadImageAPI SharedInstance];
	
	NSData * pData=UIImageJPEGRepresentation(self.photoImage, 0.5);
    
	
	NSUInteger len = [pData length];
	const char *raw=[pData bytes];
	NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
	for (long int i = 0; i < len; i++)
	{
		[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
	}
	
	NSString *tempCommentString=self.m_strMessage;
	if (tempCommentString == nil)
		tempCommentString = @"";
    
    NSMutableDictionary *argDict = [[[NSMutableDictionary alloc] init] autorelease];
    if (self.m_headerMessage !=nil)
    {
        [argDict setObject:self.m_headerMessage forKey:@"headline"];
    }
    if (self.m_whoMessage !=nil)
        [argDict setObject:self.m_whoMessage forKey:@"who"];
    else
        [argDict setObject:@"" forKey:@"who"];
    
    if (self.m_whatMessage !=nil)
        [argDict setObject:self.m_whatMessage forKey:@"what"];
    else
        [argDict setObject:@"" forKey:@"what"];
    
    if (self.m_whenMessage !=nil)
        [argDict setObject:self.m_whenMessage forKey:@"when"];
    else
        [argDict setObject:@"" forKey:@"when"];
    
    if (self.m_whereMessage !=nil)
        [argDict setObject:self.m_whereMessage forKey:@"where"];
    else
        [argDict setObject:@"" forKey:@"where"];
    
    if (self.m_howMessage !=nil)
        [argDict setObject:self.m_howMessage forKey:@"how"];
    else
        [argDict setObject:@"" forKey:@"how"];
    
    if (self.m_whyMessage !=nil)
        [argDict setObject:self.m_whyMessage forKey:@"why"];
    else
        [argDict setObject:@"" forKey:@"why"];
    
    if (self.m_openMessage !=nil)
        [argDict setObject:self.m_openMessage forKey:@"oped"];
    else
        [argDict setObject:@"" forKey:@"oped"];
    
    NSString * latString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_latitude]];
    
    NSString * lngString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_longitude]];
    NSLog(@"NewRegistration:lat=[%@], lng=[%@]",latString, lngString);
    
    [argDict setObject:latString forKey:@"latitude"];
    [argDict setObject:lngString forKey:@"longitude"];
    
	NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",tempCommentString,@"comments",m_strType,@"type",@"",@"advtlocid",[tempByteArray JSONFragment],@"photo", videoFile, @"vfile", [NSNumber numberWithInt:2], @"nature", nil];
    [argDict addEntriesFromDictionary:dict];
    NSLog(@"BuzzButtonShareNowFunctionality:sendToRunway:%@",dict);
	l_buzzRequest.requestType=99; // so that no alert is shown
	
	self.buzzid = [l_buzzRequest sendBuzzRequest:argDict];
    
    
	[tempByteArray release];
	tempByteArray=nil;


	if ([self.m_newlyAddedContactsDict count] > 0) {
		[l_appDelegate CheckInternetConnection];
		
		if(l_appDelegate.m_internetWorking==0)//0: internet working
		{
			// Call FashionGram API to invoke share
			[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
			m_mutResponseData=[[NSMutableData alloc] init];
			
			NSMutableArray * accArr = [[NSMutableArray alloc] initWithCapacity:3];
			if ((self.m_newlyAddedContactsDict) && ([self.m_newlyAddedContactsDict count] > 0)) {
				for (NSString * keyStr in [self.m_newlyAddedContactsDict allKeys]) {
					NSMutableDictionary * contactDict = [self.m_newlyAddedContactsDict objectForKey:keyStr];
					NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
					NSString * nameAddr = [contactDict objectForKey:@"nameValue"];
					NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:2];
					[dict setObject:emailAddr forKey:@"email"];
					[dict setObject:nameAddr forKey:@"name"];
					[accArr addObject:dict];
					[dict release];
				}
			}
			if ([accArr count] > 0)  {
                
				[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				NSString *temp_url;
				temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=addUserContacts&custid=%@",kServerUrl, customerID];
				temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				NSLog(@"%@",temp_url);
				
				NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																		  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
				
				NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
                
				NSString *temp_strJson=[NSString stringWithFormat:@"contacts=%@",[accArr JSONRepresentation]];
				NSLog(@"temp_strJson=[%@]",temp_strJson);
				[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
				[theRequest setAllHTTPHeaderFields:headerFieldsDict];
				[theRequest setHTTPMethod:@"POST"];
                
				NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
				self.m_intRequestType = SHARENOW_REQUEST_TYPE_ADDCONTACT;
				[conn start];
			}
			[accArr release];
		}
	} else {
		[self performDeleteOfContactsIfAny];
	}
}

// The user has tapped the Send to Runway button.
-(void) sendToRunway {
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	UploadImageAPI * l_buzzRequest=[UploadImageAPI SharedInstance];
	
	NSData * pData=UIImageJPEGRepresentation(self.photoImage, 0.5);
    
	
	NSUInteger len = [pData length];
	const char *raw=[pData bytes];
	NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
	for (long int i = 0; i < len; i++)
	{
		[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
	} 
	
	NSString *tempCommentString=self.m_strMessage;
	if (tempCommentString == nil)
		tempCommentString = @"";
    
    NSMutableDictionary *argDict = [[[NSMutableDictionary alloc] init] autorelease];
    if (self.m_headerMessage !=nil)
    {
        [argDict setObject:self.m_headerMessage forKey:@"headline"];
    }
    if (self.m_whoMessage !=nil)
        [argDict setObject:self.m_whoMessage forKey:@"who"];
    else
        [argDict setObject:@"" forKey:@"who"];
    
    if (self.m_whatMessage !=nil)
        [argDict setObject:self.m_whatMessage forKey:@"what"];
    else
        [argDict setObject:@"" forKey:@"what"];
    
    if (self.m_whenMessage !=nil)
        [argDict setObject:self.m_whenMessage forKey:@"when"];
    else
        [argDict setObject:@"" forKey:@"when"];
    
    if (self.m_whereMessage !=nil)
        [argDict setObject:self.m_whereMessage forKey:@"where"];
    else
        [argDict setObject:@"" forKey:@"where"];
    
    if (self.m_howMessage !=nil)
        [argDict setObject:self.m_howMessage forKey:@"how"];
    else
        [argDict setObject:@"" forKey:@"how"];
    
    if (self.m_whyMessage !=nil)
        [argDict setObject:self.m_whyMessage forKey:@"why"];
    else
        [argDict setObject:@"" forKey:@"why"];
    
    if (self.m_openMessage !=nil)
        [argDict setObject:self.m_openMessage forKey:@"oped"];
    else
        [argDict setObject:@"" forKey:@"oped"];
    
    NSString * latString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_latitude]];
    
    NSString * lngString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_longitude]];
    NSLog(@"NewRegistration:lat=[%@], lng=[%@]",latString, lngString);
    
    [argDict setObject:latString forKey:@"latitude"];
    [argDict setObject:lngString forKey:@"longitude"];
	NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",tempCommentString,@"comments",m_strType,@"type",@"",@"advtlocid",[tempByteArray JSONFragment],@"photo",nil];
    [argDict addEntriesFromDictionary:dict];
    NSLog(@"BuzzButtonShareNowFunctionality:sendToRunway:%@",dict);
	l_buzzRequest.requestType=99; // so that no alert is shown
	
	self.buzzid = [l_buzzRequest sendBuzzRequest:argDict];
    
    if (!self.buzzid) {
        NSLog(@"No buzzid - cannot post");
    }
    
	[tempByteArray release];
	tempByteArray=nil;
	
}

#pragma ActionheetDelegate method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex){
        [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }

    if (buttonIndex == 0) {
		// SHow contacts list
#if TARGET_IPHONE_SIMULATOR == 1
#else       
		if (self.addrBookVc == nil) {
			BuzzButtonAddFromAddressBook * vc = [[BuzzButtonAddFromAddressBook alloc] init];
			self.addrBookVc = vc;
			[self.navigationController pushViewController:vc animated:YES];
			[vc release];
		}
#endif

	} else if(buttonIndex == 1){
		// SHow add new contact page
		BuzzButtonAddNewContact * vc = [[BuzzButtonAddNewContact alloc] init];
		vc.m_DictOfContactDicts = self.m_DictOfContactDicts;
		vc.m_newlyAddedContactsDict = self.m_newlyAddedContactsDict;
		[self.navigationController pushViewController:vc animated:YES];
		[vc release];
    }
}

#ifdef FB_OLD_SDK_USAGE
- (void)getFacebookName {
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", localFBSession.uid];
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
//	self.post=YES;
}

#else
- (void)getFacebookName 
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"SELECT uid, name, pic FROM user WHERE uid=me()", @"query",
                                   nil];
    
    FBRequest *request = [[tnApplication m_session] requestWithMethodName:@"fql.query"
                                     andParams:params
                                 andHttpMethod:@"POST"
                                   andDelegate:self];
    
    if (request) {
        request.delegate = self;
    }
}

#endif


#pragma mark - FBSessionDelegate

- (void) fbDidLogin 
{
    NSLog(@"fbDidLogin()");
    tnApplication.isLoginFacebook = true;
    [tnApplication storeOAuthData];
	[self performSelectorOnMainThread:@selector(handleFBAuthSuccess) withObject:nil waitUntilDone:YES];
    [self getFacebookName];
}

/*
- (void)session:(FBSession*)session didLogin:(FBUID)uid {
	localFBSession =session;
	[FBSession setSession:session];
    NSLog(@"User with id %lld logged in.", uid);
 [self performSelectorOnMainThread:@selector(handleFBAuthSuccess) withObject:nil waitUntilDone:YES];
 [self getFacebookName];
 }
 */


- (void)fbDidLogout 
{
    tnApplication.isLoginFacebook = false;
    [tnApplication removeOAuthData];
}


-(void)fbDidNotLogin:(BOOL)cancelled {
    NSLog(@"fbDidNotLogin()");
}


- (void)fbSessionInvalidated {
    NSLog(@"fbSessionInvalidated()");
    tnApplication.isLoginFacebook = false;
    [tnApplication removeOAuthData];
}


- (void)fbDidExtendToken:(NSString*)accessToken expiresAt:(NSDate*)expiresAt
{
     NSLog(@"fbDidExtendToken()");
    [tnApplication storeOAuthData];
}


#pragma mark - FBRequestDelegate
	
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    //NSLog(@"received response");
}


- (void)request:(FBRequest*)request didLoad:(id)result {
	if ([request.httpMethod isEqualToString:@"facebook.fql.query"]) {
		NSArray* users = result;
		NSDictionary* user = [users objectAtIndex:0];
		NSString* name = [user objectForKey:@"name"];
		NSLog(@"FB Loggedin username=[%@]",name);
        isPublishing = NO;
		self.m_facebookButton.selected = YES;
	} else if ([request.url rangeOfString:@"https://graph.facebook.com/me/videos"].length > 0) {
        /* [[LoadingIndicatorView SharedInstance]stopLoadingView];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"You video was posted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        alertView=nil;
        [self.navigationController popToRootViewControllerAnimated:NO];
        
        [tnApplication goToNewsPage];*/
        isPublishing = NO;
       NSLog(@"You video was posted successfully.");
    } else if ([request.url rangeOfString:@"https://graph.facebook.com/me/feed"].length > 0) {
        /*[[LoadingIndicatorView SharedInstance]stopLoadingView];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"Posted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        alertView=nil;
        [self.navigationController popToRootViewControllerAnimated:NO];
        
        [tnApplication goToNewsPage];*/

        isPublishing = NO;
        NSLog(@"You video was posted successfully.");
    }
}


#pragma mark FBDialogDelegate

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidComplete:(FBDialog*)dialog {
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}
}

- (void)dialogCompleteWithUrl:(NSURL *)url {
    NSDictionary *params = [self parseURLParams:[url query]];
    
    switch (currentAPICall) {
        case kAPIPostToFriend:
        {
            // Successful posts return a post_id
            if ([params valueForKey:@"post_id"]) {
                /*[[LoadingIndicatorView SharedInstance]stopLoadingView];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"Posted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                [alertView release];
                alertView=nil;
                [self.navigationController popToRootViewControllerAnimated:NO];

                [tnApplication goToNewsPage];*/
                
                // Silent logging
                
                NSLog(@"FB posted successfully");
            }
            break;
        }
    }
    
}


/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidNotComplete:(FBDialog *)dialog
{
    NSLog(@"FB dialogDidNotComplete()");
    if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}
}

- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error{
    NSLog(@"FB didFailWithError()");
}


/**
 * Helper method to parse URL query parameters
 */
- (NSDictionary *)parseURLParams:(NSString *)query {
	NSArray *pairs = [query componentsSeparatedByString:@"&"];
	NSMutableDictionary *params = [[[NSMutableDictionary alloc] init] autorelease];
	for (NSString *pair in pairs) {
		NSArray *kv = [pair componentsSeparatedByString:@"="];
		NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
		[params setObject:val forKey:[kv objectAtIndex:0]];
	}
    return params;
}


#pragma mark - Posting News



- (void)postToTwitterWall:(NSString *) photoUrl {
	if ((self.m_twitterButton.selected == NO) || (photoUrl == nil) || ([photoUrl length] <= 0)
			|| (twitterEngine == nil)) {
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		// Bharat: DE86: pop to root
		//[self.navigationController popViewControllerAnimated:YES];
		[self.navigationController popToRootViewControllerAnimated:NO];
		
		// Bharat : US237, when send is completed, change tab to runway
		//UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:@"message sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		//[av show]; [av release];
		
        [tnApplication goToNewsPage];
		// Set image to nil
		UINavigationController * navC = [tnApplication.navControllersArray objectAtIndex:SHARE_TAB_INDEX];
		BuzzButtonCameraFunctionality * vc = (BuzzButtonCameraFunctionality *)[navC.viewControllers objectAtIndex:0];
		vc.wasCameraPickerActiveThenSkipCamLaunch = NO;
		[vc.m_imageView setImage:nil];
		[vc.l_textView setText:nil];
		[vc.m_buttomTextView setText:PLACEHOLDER_COMMENTS_MESSAGE];
		[vc.m_buttomTextView2 setText:PLACEHOLDER_COMMENTS_MESSAGE];

		return;
	}




	NSMutableString *tweetBody;
	tweetBody=[NSMutableString stringWithFormat:@"%@",[[Context getInstance] getDisplayTextFromMessageType:self.m_strType]];
	[tweetBody appendFormat:@"\n"];
	[tweetBody appendFormat:@"%@",self.m_strMessage];
	[twitterEngine sendUpdate:tweetBody];

    [[GAI sharedInstance].defaultTracker sendSocial:@"Twitter"
                                         withAction:@"tweet"
                                         withTarget:photoUrl];  // Send social interaction.
}
	
#ifdef FB_OLD_SDK_USAGE
- (void)postToFacebookWall:(NSString *) photoUrl {
	
	NSLog(@"BuzzButtonShareNowFunctionality:postToFacebookWall:[%@]",photoUrl);

	if ((self.m_facebookButton.selected == NO) || (photoUrl == nil) || ([photoUrl length] <= 0)) {
		[self postToTwitterWall:photoUrl];
		return;
	}
    

	NSString *customMessage = @"";
	if (self.m_strMessage != nil) {
		customMessage = [NSString stringWithFormat:@"%@", self.m_strMessage];
	}

	FBStreamDialog *dialog = [[[FBStreamDialog alloc] init] autorelease];
	dialog.userMessagePrompt = @"Enter additional comment";
	dialog.delegate = self;
	
	// build attachment with JSONFragment
	NSString *postName = [[Context getInstance] getDisplayTextFromMessageType:self.m_strType];
	NSString *serverLink = [NSString stringWithFormat:@"http://itunes.apple.com/app/fashiongram/id498116453?mt=8"];
	NSString *imageSrc = photoUrl; // Put image URL here
	
	NSMutableDictionary *dictionary = [[[NSMutableDictionary alloc] init]autorelease];
	[dictionary setObject:postName forKey:@"name"];
	[dictionary setObject:serverLink forKey:@"href"];
	[dictionary setObject:customMessage forKey:@"description"];
	
	NSMutableDictionary *media = [[[NSMutableDictionary alloc] init]autorelease];
	[media setObject:@"image" forKey:@"type"];
	[media setObject:serverLink forKey:@"href"];
	[media setObject:imageSrc forKey:@"src"];               
	[dictionary setObject:[NSArray arrayWithObject:media] forKey:@"media"];  
	
	NSLog(@"BuzzButtonShareNowFunctionality:postToFacebookWall:attachment=[%@]",[dictionary JSONFragment]);
	dialog.attachment = [dictionary JSONFragment];


	[dialog show];
	
}
#else
- (NSString *) formatWhoWhatText
{
    if ([self.m_openMessage length] > 0)
        return self.m_openMessage;
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    if ([self.m_whoMessage length] > 0) {
        [string appendFormat:@"Who: %@\n", self.m_whoMessage];
    }
    if ([self.m_whatMessage length] > 0) {
        [string appendFormat:@"What: %@\n", self.m_whatMessage];
    }
    if ([self.m_whereMessage length] > 0) {
        [string appendFormat:@"Where: %@\n", self.m_whereMessage];
    }
    if ([self.m_howMessage length] > 0) {
        [string appendFormat:@"How: %@\n", self.m_howMessage];
    }
    if ([self.m_whyMessage length] > 0) {
        [string appendFormat:@"Why: %@\n", self.m_whyMessage];
    }
    if ([self.m_whenMessage length] > 0) {
        [string appendFormat:@"When: %@\n", self.m_whenMessage];
    }
    return string;
}

- (void) showFBInterface:(NSDictionary *)dict
{
    NSString *message = [[self formatWhoWhatText] URLDecodedString];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"link", @"type",
                                   [dict objectForKey:@"postName"], @"name",
                                   [dict objectForKey:@"customMessage"], @"description",
                                   [dict objectForKey:@"serverLink1"], @"link",
                                   [dict objectForKey:@"photoUrl"], @"picture",
                                   message, @"description",
                                   nil];
    
    NSLog(@"params: %@",params);
    
    
    currentAPICall = kAPIPostToFriend;
    
    [[tnApplication m_session] dialog: @"feed"
                            andParams: params
                          andDelegate: self];

}

- (void)postToFacebookWall:(NSString *) photoUrl {
    
    if ((self.m_facebookButton.selected == NO) || (photoUrl == nil) || ([photoUrl length] <= 0)) {
		[self postToTwitterWall:photoUrl];
        isPublishing = NO;
		return;
	}
    
	NSString *customMessage = @"";
	if (self.m_strMessage != nil) {
		customMessage = [NSString stringWithFormat:@"%@", self.m_strMessage];
	}
    
    NSLog(@"Custom message: %@",customMessage);
    
	NSString *postName = [[Context getInstance] getDisplayTextFromMessageType:self.m_strType];
	NSString *serverLink = @"http://www.tinynews.me/";
    NSMutableDictionary *params = nil;
    
    if (!self.shareVideo) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/top.htm?action=getStory&storyId=%@", kServerUrl, self.buzzid]]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            if ([httpResponse statusCode] == 200) {
                NSString *temp_string=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                SBJSON *temp_objSBJson = [[SBJSON alloc]init];
                NSArray *contArr = [temp_objSBJson objectWithString:temp_string];
                
                NSLog(@"contArray - %@", contArr);
                
                NSString *messageId = [[contArr objectAtIndex:0] objectForKey:@"messageId"];
                NSString *serverLink1 = [NSString stringWithFormat:@"http://www.tinynews.me/fw/?lightboxId=%@", messageId];
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      postName, @"postName",
                                      customMessage, @"customMessage",
                                      serverLink1, @"serverLink1",
                                      photoUrl, @"photoUrl",
                                      nil];
                
                [self performSelectorOnMainThread:@selector(showFBInterface:) withObject:dict waitUntilDone:NO];
            }
            
        }];
        
        
    } else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"/tempVid.MOV"];
        NSData *videoData = [NSData dataWithContentsOfFile:tempPath];
        NSString *message = [[self formatWhoWhatText] URLDecodedString];
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       videoData, @"video.mov",
                                       @"video/quicktime", @"contentType",
                                       [self.m_headerMessage URLDecodedString], @"title",
                                       @"Video name", @"name",
                                       message, @"description",
                                       serverLink, @"link",
                                       nil];
        FBRequest *request = [[tnApplication m_session] requestWithGraphPath:@"me/videos"
                                             andParams:params
                                         andHttpMethod:@"POST"
                                           andDelegate:self];
        
        if (request) {
            request.delegate = self;
        }
    }
    
    
    
    [[GAI sharedInstance].defaultTracker sendSocial:@"Facebook"
             withAction:@"post"
             withTarget:photoUrl];  // Send social interaction.
}
#endif


-(void)gotAllFriendsFromTwitter:(NSArray *)tempFriendsArray
{
	NSLog(@"BuzzButtonShareNowFunctionality:gotAllFriendsFromTwitter:%@",tempFriendsArray);
	self.m_twitterButton.selected = YES;
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"BuzzButtonShareNowFunctionality:didReceiveResponse:statusCode=%d",m_intResponseCode);
	
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	
	NSLog(@"BuzzButtonShareNowFunctionality:didReceiveData");
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"BuzzButtonShareNowFunctionality:connectionDidFinishLoading");
	
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW)//request from mypage user pic change
	{

		NSLog(@"BuzzButtonShareNowFunctionality:sharePhotoInfoViaEmail:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
	
		if (self.m_intResponseCode == 200) {
			NSString *temp_string=[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding] autorelease];
			SBJSON *temp_objSBJson = [[[SBJSON alloc]init] autorelease];
			NSArray *contArr = ![temp_string isEqualToString:@"[null]"] ? [temp_objSBJson objectWithString:temp_string] : nil;
            // ugly workaround
            BOOL success = [temp_string isEqualToString:@"[null]"];
            
            NSLog(@"BuzzButtonShareNowFunctionality temp_string = %@", temp_string);
			if (success || ((contArr != nil) && ([contArr isKindOfClass:[NSArray class]] == YES) && ([contArr count] > 0))) {

				if (!success && [contArr count] == 1) {
                    NSLog(@"BuzzButtonShareNowFunctionality unsuccessful upload contArr = %@", contArr);

					NSString * str = [contArr objectAtIndex:0];
					if ((str == nil) || ([str isKindOfClass:[NSString class]] == NO) || (([str length] > 0) && ([str rangeOfString:@"\"false\""].location != NSNotFound)) ) {

					}
					
/*					UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[NSString stringWithFormat:@"Photo upload unsuccessful.\n%@",(temp_string) ? temp_string: @""]
						delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[av show];
					[av release];*/
                    isPublishing = NO;

					return;

				} else {
					// Success
					//if ((str == nil) || ([str length] <= 0)) {
                    if (success || [contArr count] > 1) {
                        if (self.m_facebookButton.selected) {
                            NSString * str = [contArr objectAtIndex:1];
                            self.m_photoUrl = [[NSString stringWithFormat:@"%@/%@",kServerUrl,str] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            [self postToFacebookWall:self.m_photoUrl];
                            return;
                        }
                        
                        NSLog(@"Posting successful - %@", temp_string);
                        
                        isPublishing = NO;

						return;
					} else {
                        NSString * str = [contArr objectAtIndex:1];
						
						self.m_photoUrl = [[NSString stringWithFormat:@"%@/%@",kServerUrl,str] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

						[self postToFacebookWall:self.m_photoUrl];
						
                        
						[temp_string release]; temp_string = nil;
						[temp_objSBJson release]; temp_objSBJson = nil;
                        
 /*                       UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Message sent..." message:temp_string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [av show];
                        [av release];*/

                        
                        //@@@@TEMP
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatePopular" object:nil];
						
                        
                        
						return;
					}
				}
			}
			
/*			[[LoadingIndicatorView SharedInstance]stopLoadingView];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			
			UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Invalid server response" message:temp_string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[av show];
			[av release];
						
			[temp_string release]; temp_string = nil;
			[temp_objSBJson release]; temp_objSBJson = nil;*/
            
            // Silent Failure
            isPublishing = NO;
           
            NSLog(@"Invalid server response");
			
			return;

		} else {
			NSString * resp = [[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
            
            if ([resp rangeOfString:@"401"].length != 0) {
                [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
            } else {
/*                UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[NSString stringWithFormat:@"Failed to upload photo.\n\n%@",
                            resp] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [av show];
                [av release];
                [resp release];*/
                // Silent Failure
                NSLog(@"Failed to upload photo.\n\n%@", resp);

            }
            isPublishing = NO;
			[[LoadingIndicatorView SharedInstance]stopLoadingView];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			return;
		}
	} else if ((m_intResponseCode==200) && (self.m_intRequestType == SHARENOW_REQUEST_TYPE_GETCONTACT))//request from mypage user pic change
	{

		NSLog(@"BuzzButtonShareNowFunctionality:getUserContacts:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
		SBJSON *temp_objSBJson = [[SBJSON alloc]init];
		NSArray *contArr = [temp_objSBJson objectWithString:temp_string];
        [temp_string release]; temp_string = nil;
        [temp_objSBJson release]; temp_objSBJson = nil;
		if ((contArr != nil) && ([contArr isKindOfClass:[NSArray class]] == YES) && ([contArr count] > 0)) {
			for (NSDictionary * dict in contArr) {
				NSString * email = [dict objectForKey:@"email"];
				NSString * name = [dict objectForKey:@"name"];

				NSMutableDictionary * d = [[NSMutableDictionary alloc] initWithCapacity:3];
				[d setObject:email forKey:@"emailValue"];
				[d setObject:name forKey:@"nameValue"];
				[d setObject:@"0" forKey:@"selectionValue"];

				[self.m_DictOfContactDicts setObject:d forKey:name];
                [d release]; d = nil;

			}
		}
		

		[temp_objSBJson release];
        [temp_string release];
		[self.m_emailTableView reloadData];
		
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

		return;
	} else if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_ADDCONTACT)//request from mypage user pic change
	{

		if (self.m_intResponseCode == 200) {
			NSLog(@"BuzzButtonShareNowFunctionality:addUserContacts:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
			[self.m_newlyAddedContactsDict removeAllObjects];
			[self performDeleteOfContactsIfAny];
		
			return;
		} else {
			
			NSMutableArray * emailArr = [[NSMutableArray alloc] initWithCapacity:3];
			if ((self.m_newlyAddedContactsDict) && ([self.m_newlyAddedContactsDict count] > 0)) {
				for (NSString * keyStr in [self.m_newlyAddedContactsDict allKeys]) {
					NSMutableDictionary * contactDict = [self.m_newlyAddedContactsDict objectForKey:keyStr];
					NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
					[emailArr addObject:emailAddr];
				}
			}
			NSString * resp = [[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
			UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[NSString stringWithFormat:@"Failed to add new contacts.\n%@\n\n%@",
						[emailArr componentsJoinedByString:@","], resp] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[av show];
			[av release];
			[emailArr release];
			[resp release];
	
			[[LoadingIndicatorView SharedInstance]stopLoadingView];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			return;
		}

	} else if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_DELCONTACT)//request from mypage user pic change
	{

		if (self.m_intResponseCode == 200) {
			NSLog(@"BuzzButtonShareNowFunctionality:deleteUserContacts:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
			[self.m_deletedContactsDict removeAllObjects];
			[self performPhotoShare];
			return;
		} else {
			NSMutableArray * emailArr = [[NSMutableArray alloc] initWithCapacity:3];
			if ((self.m_deletedContactsDict) && ([self.m_deletedContactsDict count] > 0)) {
				for (NSString * keyStr in [self.m_deletedContactsDict allKeys]) {
					NSMutableDictionary * contactDict = [self.m_deletedContactsDict objectForKey:keyStr];
					NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
					[emailArr addObject:emailAddr];
				}
			}
			NSString * resp = [[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
			UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Server Error" message:[NSString stringWithFormat:@"Failed to delete contacts.\n%@\n\n%@",
						[emailArr componentsJoinedByString:@","], resp] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[av show];
			[av release];
			[emailArr release];
			[resp release];
	
			[[LoadingIndicatorView SharedInstance]stopLoadingView];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			return;
		}

	}
	
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}


- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"BuzzButtonShareNowFunctionality:didFailWithError");
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    [self showMessageAlertOnMainThread:kNetworkDownErrorTitle message:kNetworkDownErrorMsg];
	
	if (m_mutResponseData)
	{
		[m_mutResponseData release];
		 m_mutResponseData=nil;
	}
}

-(void) handleFBAuthCancelled {
	self.m_facebookButton.selected = NO;
}

-(void) handleFBAuthFailure {
	self.m_facebookButton.selected = NO;

    [self showMessageAlertOnMainThread:@"login Unsuccesfull" message:@"login failed!"];
}

-(void) handleFBAuthSuccess {
	self.m_facebookButton.selected = YES;
}

-(void) handleTWAuthCancelled {
	self.m_twitterButton.selected = NO;
}

-(void) handleTWAuthFailure {
	self.m_twitterButton.selected = NO;

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"login Unsuccesfull" message:@"login failed!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];
}

-(void) handleTWAuthSuccess {
	self.m_twitterButton.selected = YES;
}

-(void) handleShareNowCompletionWithTWFailure {
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// Even on failure, do the pop  (DE113)
	[self.navigationController popToRootViewControllerAnimated:NO];
	// Bharat : US237, when send is completed, change tab to runway
	UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:@"message sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[av show]; [av release];
    
    [tnApplication goToNewsPage];
	// Set image to nil
	UINavigationController * navC = (UINavigationController *) [tnApplication.navControllersArray objectAtIndex:SHARE_TAB_INDEX];
	BuzzButtonCameraFunctionality * vc = (BuzzButtonCameraFunctionality *)[navC.viewControllers objectAtIndex:0];
	vc.wasCameraPickerActiveThenSkipCamLaunch = NO;
	[vc.m_imageView setImage:nil];
	[vc.l_textView setText:nil];
	[vc.m_buttomTextView setText:PLACEHOLDER_COMMENTS_MESSAGE];
	[vc.m_buttomTextView2 setText:PLACEHOLDER_COMMENTS_MESSAGE];
}
 
-(void) handleShareNowCompletionWithTWSuccess {
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
	// Bharat: DE86: pop to root
	//[self.navigationController popViewControllerAnimated:YES];
	[self.navigationController popToRootViewControllerAnimated:NO];
	// Bharat : US237, when send is completed, change tab to runway
	UIAlertView * av = [[UIAlertView alloc] initWithTitle:nil message:@"message sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[av show]; [av release];
    [tnApplication goToNewsPage];
	// Set image to nil
	UINavigationController * navC = (UINavigationController *) [tnApplication.navControllersArray objectAtIndex:SHARE_TAB_INDEX];
	BuzzButtonCameraFunctionality * vc = (BuzzButtonCameraFunctionality *)[navC.viewControllers objectAtIndex:0];
	vc.wasCameraPickerActiveThenSkipCamLaunch = NO;
	[vc.m_imageView setImage:nil];
	[vc.l_textView setText:nil];
	[vc.m_buttomTextView setText:PLACEHOLDER_COMMENTS_MESSAGE];
	[vc.m_buttomTextView2 setText:PLACEHOLDER_COMMENTS_MESSAGE];
}
 
#pragma mark FBDialogDelegate
/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidSucceed:(FBDialog*)dialog {
	
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToFacebookWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}

}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidCancel:(FBDialog*)dialog {
	// do nothing
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}

}

/** 
 * Called when an error prevents the request from completing successfully. 
 */ 
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error{ 
	
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}
    [[LoadingIndicatorView SharedInstance]stopLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    [self showMessageAlertOnMainThread:@"Error" message:[error localizedDescription]];
    
	
} 

/**
 * Asks if a link touched by a user should be opened in an external browser.
 *
 * If a user touches a link, the default behavior is to open the link in the Safari browser, 
 * which will cause your app to quit.  You may want to prevent this from happening, open the link
 * in your own internal browser, or perhaps warn the user that they are about to leave your app.
 * If so, implement this method on your delegate and return NO.  If you warn the user, you
 * should hold onto the URL and once you have received their acknowledgement open the URL yourself
 * using [[UIApplication sharedApplication] openURL:].
 */
- (BOOL)dialog:(FBDialog*)dialog shouldOpenURLInExternalBrowser:(NSURL*)url {
	return NO;
}



#pragma mark SA_OAuthTwitterController Delegate 

// following delegate methods are related with login process..
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) user_name {
	NSLog(@"Authenticated with user %@", user_name);
	[self performSelectorOnMainThread:@selector(handleTWAuthSuccess) withObject:nil waitUntilDone:YES];

}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Failure");
	[self performSelectorOnMainThread:@selector(handleTWAuthFailure) withObject:nil waitUntilDone:YES];
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Canceled");
	[self performSelectorOnMainThread:@selector(handleTWAuthCancelled) withObject:nil waitUntilDone:YES];
}

#pragma mark TwitterEngineDelegate

- (void) requestSucceeded: (NSString *) requestIdentifier {
	NSLog(@"Request %@ succeeded", requestIdentifier);
	[self performSelectorOnMainThread:@selector(handleShareNowCompletionWithTWSuccess) withObject:nil waitUntilDone:YES];
}

- (void) requestFailed: (NSString *) requestIdentifier withError: (NSError *) error {
	NSLog(@"Request %@ failed with error: %@", requestIdentifier, error);
	[self performSelectorOnMainThread:@selector(handleShareNowCompletionWithTWFailure) withObject:nil waitUntilDone:YES];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}


@end
