//
//  GetCurrentLocation.h
//  QNavigator
//
//  Created by softprodigy on 14/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <CoreLocation/CoreLocation.h>

@interface GetCurrentLocation : UIViewController <CLLocationManagerDelegate>//NSObject <CLLocationManagerDelegate>
{
	CLLocationManager *m_locationManager;
	float m_latitude;
	float m_longitude;
	
}
@property (nonatomic,retain) CLLocationManager *m_locationManager;

@property float m_latitude;
@property float m_longitude;

-(void)initializeMembers;
-(void)showLoginScreenOnSessionExpire:(id)sender;
@end
