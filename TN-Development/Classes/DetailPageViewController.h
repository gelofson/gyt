//
//  DetailPageViewController.h
//  TinyNews
//
//  Created by Nava Carmon on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "CategoryData.h"
#import "ShopbeeAPIs.h"
#import "AsyncButtonView.h"
#import "GAITrackedViewController.h"

@interface DetailPageViewController : GAITrackedViewController <UITextViewDelegate, UIAlertViewDelegate,UIActionSheetDelegate, UIWebViewDelegate>
{
    IBOutlet UILabel *m_headingLabel;
    IBOutlet UILabel *m_likeLabel;
    IBOutlet UILabel *m_dislikeLabel;
    IBOutlet UILabel *m_commentsLabel;

    IBOutlet UILabel      *m_originatorNameLabel;
    IBOutlet UILabel      *m_mineOriginatorNameLabel;
    IBOutlet UITextView   *m_whoWhatTextView;
    IBOutlet UITextView   *m_textViewSubject;
    IBOutlet UITextView   *m_mineTextViewSubject;
    IBOutlet UIView         *commentsGroup;
    IBOutlet UILabel      *viaLabel;
    IBOutlet UIButton     *linkButton;
    
    
    IBOutlet AsyncButtonView  *m_storyImageView;
    IBOutlet AsyncButtonView  *m_userImageView;
    IBOutlet AsyncButtonView  *m_mineUserImageView;


    IBOutlet UIView *m_customTabbar;
    IBOutlet UIView *commentsView;
    IBOutlet UIScrollView *m_scrollView;
    IBOutlet UIView *allCommentsView;

    IBOutlet UIButton *clipButton;
    IBOutlet UIButton *followButton;
    IBOutlet UIButton *likeButton;
    IBOutlet UIButton *dislikeButton;
    IBOutlet UIButton *commentButton;
    IBOutlet UIButton *viewAllCommentButton;
    IBOutlet UIButton *reply;
    IBOutlet UIButton *m_leftArrow;
    
    UIViewController *m_CallBackViewController;
    
    IBOutlet UILabel  *m_headerLabel;
    IBOutlet UIButton *m_rightArrow;
    NSString *m_userId;
    NSString *m_Type;
    NSInteger filter;
    NSMutableArray *m_messageIDsArray;
    NSMutableArray *statusesArray;
    CategoryData *m_data;
    NSInteger m_messageId;
    NSInteger m_currentMessageID;
    
    BOOL fetchingData;
    BOOL firstTime, GetPinnFlag;
    NSInteger cellHeight;
    int numPages, curPage;
    int totalRecords;
    NSInteger firstPictureID;
    ShopbeeAPIs *l_requestObj;
	UIView *m_ThePhotoView;
    UIImageView *m_ImageView;
    UIView *commentGroupMine;
    BOOL mineTrack;
    BOOL moreTrack;
    IBOutlet UIWebView *videoWebView;
    IBOutlet UIView *theWebView;
    NSString *storyUrl;
    IBOutlet UILabel *postingDate;
}

// cell
@property(nonatomic, retain) UIScrollView *m_scrollView;
@property(nonatomic, retain) UIView *m_customTabbar;
@property (nonatomic, retain) UIView *commentsGroup;
@property (nonatomic, retain) UIView *commentsView;
@property(nonatomic, retain) UILabel *m_headingLabel;
@property(nonatomic, retain) UILabel *m_likeLabel;
@property(nonatomic, retain) UILabel *m_commentsLabel;
@property(nonatomic, retain) UILabel *m_dislikeLabel;
@property(nonatomic, retain) UILabel *m_originatorNameLabel;
@property(nonatomic, retain) UILabel *m_mineOriginatorNameLabel;
@property(nonatomic, retain) UITextView *m_whoWhatTextView;
@property(nonatomic, retain) UITextView *m_textViewSubject;
@property(nonatomic, retain) UITextView *m_mineTextViewSubject;
@property(nonatomic, retain) UIView *allCommentsView;
@property(nonatomic, retain) AsyncButtonView *m_storyImageView;
@property(nonatomic, retain) AsyncButtonView *m_userImageView;
@property(nonatomic, retain) AsyncButtonView *m_mineUserImageView;
@property(nonatomic, retain) UIButton *clipButton;
@property(nonatomic, retain) UIButton *followButton;
@property(nonatomic, retain) UIButton *likeButton;
@property(nonatomic, retain) UIButton *dislikeButton;
@property(nonatomic, retain) UIButton *commentButton;
@property(nonatomic, retain) IBOutlet UIButton *viewAllCommentButton;
@property(nonatomic, retain) IBOutlet UIButton *reply;
@property(nonatomic,retain) IBOutlet	UIButton *m_leftArrow;
@property(nonatomic,retain) IBOutlet	UIButton *m_rightArrow;
@property(nonatomic,retain) IBOutlet UIView *m_ThePhotoView;
@property(nonatomic,retain) IBOutlet UIImageView *m_ImageView;
@property(nonatomic, retain) IBOutlet UIView *commentGroupMine;
@property (nonatomic, retain) UILabel  *m_headerLabel;
@property(nonatomic, retain) NSMutableArray *statusesArray;
@property(nonatomic, retain) UIViewController *m_CallBackViewController;
@property(nonatomic, retain)  UILabel      *viaLabel;
@property(nonatomic, retain)  UIButton     *linkButton;
@property(nonatomic, copy)  NSString *storyUrl;

@property(nonatomic,retain) NSString *m_userId;
@property(nonatomic,retain) NSString *m_Type;
@property (nonatomic) NSInteger filter;
@property (nonatomic) NSInteger firstPictureID;
@property(nonatomic,retain) NSMutableArray *m_messageIDsArray;
@property(nonatomic,readwrite) NSInteger m_messageId;
@property(nonatomic,retain) CategoryData *m_data;
@property NSInteger m_currentMessageID;
@property int curPage;
@property BOOL mineTrack;
@property BOOL fetchingData;
@property(nonatomic,retain) IBOutlet UIWebView *videoWebView;
@property(nonatomic,retain) IBOutlet UIView *theWebView;
@property (nonatomic, retain) IBOutlet UILabel *postingDate;

- (void) updateData;
- (IBAction)btnBackAction:(id)sender;
- (IBAction)clipThis:(id)sender;
- (IBAction)likeThis:(id)sender;
- (IBAction)dislikeThis:(id)sender;
- (IBAction)commentThis:(id)sender;
- (IBAction)follow:(id)sender;
- (IBAction)flagIt:(id)sender;
- (void)unfollowAction:(NSDictionary *)aData;
- (void)followAction:(NSDictionary *)aData;
- (NSMutableDictionary  *)findStatusByMessageId:(NSInteger)messageId;
- (void) sendLikeRequest:(NSString *)likeStatus messageID:(NSInteger)messageID;
- (void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles;
- (IBAction)moveRight:(id)sender;
- (IBAction)moveLeft:(id)sender;
- (void)commenterImagePressed:(id)sender;
- (void)ProductBtnClicked:(id)sender;
- (IBAction)CloseBtnAction:(id)sender; 
- (IBAction)CloseWebBtnAction:(id)sender;
- (void) setMineViewData;
- (IBAction)openUrl:(id)sender;
- (void) hideArrows;

@end
