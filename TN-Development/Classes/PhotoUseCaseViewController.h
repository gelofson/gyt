//
//  BuzzButtonCameraFunctionality.h
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyPageViewController.h"
#import "NewRegistration.h"

@class NewRegistration;
@class MyPageViewController;
@interface PhotoUseCaseViewController : UIViewController<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
	UIImagePickerController *m_imgPicker;
	UIImageView *m_userImage;
	UITextView *l_textView;
	UITextView *m_buttomTextView;
	UILabel *m_LabCountChar;
	UIImageView *m_imageView;
	UIImageView *m_imageViewCrop;

	UISlider *m_sliderZooming;
	float scale;
	float scaleValue;
	float previousScaleValue;	
	
	UIImage *m_buzzImage;
	UISlider *smoothnessSlider;
    UISlider *brightnessSlider;
    UISlider *saturationSlider;
    
    int smoothness;
    float gamma,initialDistance;
    float saturation;
	UIView *m_beautyView; 
	UIView *m_beautyViewBorder;
	
	CGPoint touch1,touch2;
	
	UIImageView *m_bottomBar;
	UIActivityIndicatorView *m_indicatorView;
	UIButton *btnEdit;
	UIButton *btnPreview;
	UIButton *btnUndo;
	UIView *m_ViewTopTextView;
	NSMutableArray *m_arrWithUndoDict;	
	NSDictionary *m_ZoomDict;
	NSDictionary *m_BeautyDict;
	NSDictionary *m_CommentDict;
	NSString *m_strType;
	NSData *m_personData;
	UIImage	*m_personImage;
	UILabel *m_lblselectAnImg;
	UIButton *m_sendBtn;
	UIView *m_zoomSliderView;
	UILabel *m_lblBasicText;
	UIView *m_requiredView;
	MyPageViewController *m_MypageViewController;
	NewRegistration *m_newRegistration;
	UIScrollView *m_scrollView;
	
	BOOL shouldUseEditedImage;
	
}

@property BOOL shouldUseEditedImage;
@property (nonatomic,retain) IBOutlet UIScrollView *m_scrollView;
@property (nonatomic,retain) NewRegistration *m_newRegistration;
@property (nonatomic,retain) MyPageViewController *m_MypageViewController;

@property (nonatomic,retain) IBOutlet UIView *m_requiredView;
@property (nonatomic,retain) IBOutlet UILabel *m_lblBasicText;
@property (nonatomic,retain) IBOutlet UIView *m_zoomSliderView;
@property (nonatomic,retain) IBOutlet UIView *m_beautyViewBorder;
@property (nonatomic,retain) IBOutlet UIButton *m_sendBtn;
@property (nonatomic,retain) IBOutlet UILabel *m_lblselectAnImg;
@property (nonatomic,retain)NSString *m_strType;
@property (nonatomic,retain)NSDictionary *m_ZoomDict;
@property (nonatomic,retain)NSDictionary *m_BeautyDict;
@property (nonatomic,retain)NSDictionary *m_CommentDict;
@property (nonatomic,retain) NSMutableArray *m_arrWithUndoDict;
@property (nonatomic,retain) IBOutlet UIView *m_ViewTopTextView;
@property (nonatomic,retain) IBOutlet	UIButton *btnEdit;
@property (nonatomic,retain) IBOutlet	UIButton *btnPreview;
@property (nonatomic,retain) IBOutlet	UIButton *btnUndo;
@property (nonatomic,retain) UIActivityIndicatorView *m_indicatorView;
@property (nonatomic,retain) IBOutlet UIImageView *m_bottomBar;
@property (nonatomic,retain) IBOutlet UIImageView *m_imageViewCrop;
@property (nonatomic, retain)IBOutlet UIView *m_beautyView;
@property (nonatomic, retain)IBOutlet UISlider *m_sliderZooming;
@property (nonatomic, retain)IBOutlet UIImageView *m_imageView;
@property(nonatomic,retain)IBOutlet UITextView *m_buttomTextView;
@property (nonatomic,retain) IBOutlet UIImageView *m_userImage;
@property(nonatomic,retain)IBOutlet UITextView *l_textView;
@property(nonatomic,retain)IBOutlet	UILabel *m_LabCountChar;



@property (nonatomic, retain) UIImage *m_buzzImage;
@property (nonatomic, retain) IBOutlet UISlider *smoothnessSlider;
@property (nonatomic, retain) IBOutlet UISlider *brightnessSlider;
@property (nonatomic, retain) IBOutlet UISlider *saturationSlider;

- (IBAction) chooseImage:(id) sender;
- (IBAction) smoothness: (UISlider *)slider;
- (IBAction) brightness: (UISlider *)slider;
- (IBAction) saturation: (UISlider *)slider;




-(IBAction)btnPhotoLibrary:(id)sender;
-(IBAction)btnRetakeAction:(id)sender;
-(IBAction)btnCancelAction:(id)sender;
- (IBAction)takePicture:(id)sender;
-(IBAction)btnEditAction:(id)sender;
-(IBAction)btnPreviewAction:(id)sender;
-(IBAction)btnUndoAction:(id)sender;
-(void)zoomValueChanged:(NSNumber *)zoomvalue;
-(IBAction) smoothnessforUndo: (NSNumber *)smoothnessSliderValue;
-(IBAction) brightnessforUndo: (NSNumber *)brightnessSliderValue;
-(IBAction) saturationforUndo: (NSNumber *)saturationSliderValue;
-(IBAction) btnSendAction:(id)sender;
-(IBAction) btnZoomAction:(id)sender;
-(IBAction) btnBeautyAction:(id)sender;

// GDL: This is not implemented, so I'm commenting it out.
//-(IBAction)btnSelectAction:(id)sender;

// GDL: We use these, so we should declare it.
-(void)reset;
-(void)sendPictureToWebservice:(UIImage *)newImage;


@end
