//
//  ChooseCategories.h
//  TinyNews
//
//  Created by Nava Carmon on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCategories : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *tableView;
    NSInteger  currentRow;
    id controller;
}

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) id controller;
@property (nonatomic) NSInteger  currentRow;

- (IBAction)back:(id)sender;


@end
