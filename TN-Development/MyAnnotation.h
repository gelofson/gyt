//
//  MyAnnotation.h
//  TinyNews
//
//  Created by jay kumar on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject<MKAnnotation>
{
    NSString *title;
    CLLocationCoordinate2D	coordinate;

}
@property (nonatomic, assign)	CLLocationCoordinate2D coordinate;
@property(nonatomic,copy)NSString *title;
@end
