//
//  MyAnnotation.m
//  TinyNews
//
//  Created by jay kumar on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation
@synthesize title;
@synthesize coordinate;

-(void)dealloc
{
    [title release];
    title=nil;
    [super dealloc];
}

@end
