//  Copyright _ All rights reserved.
//

#import "ATNetwork.h"
#import "ATConnection.h"

@interface ATNetwork (PRIVATE)
+ (NSMutableArray*)connectionsArray;
+ (NSMutableArray*)dataArray;
+ (NSMutableArray*)callBackTargetArray;
+ (NSMutableArray*)callBackSelectorArray;

- (void) closeConnection:(ATConnection *)connection ;
- (void) updateInterfaceWithReachability: (Reachability*) curReach;
@end


static NSMutableArray *__callBackTargetArray;
static NSMutableArray *__callBackSelectorArray;
static NSMutableArray *__connectionsArray;
static NSMutableArray *__dataArray;

@implementation ATNetwork


+ (NSMutableArray*)callBackTargetArray {
	@synchronized(self) {
		if(!__callBackTargetArray) {
			__callBackTargetArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __callBackTargetArray;
} 

+ (NSMutableArray*)callBackSelectorArray {
	@synchronized(self) {
		if(!__callBackSelectorArray) {
			__callBackSelectorArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __callBackSelectorArray;
} 

+ (NSMutableArray*)connectionsArray {
	@synchronized(self) {
		if(!__connectionsArray) {
			__connectionsArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __connectionsArray;
} 

+ (NSMutableArray*)dataArray {
	@synchronized(self) {
		if(!__dataArray) {
			__dataArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __dataArray;
} 


- (void) openConnection : (NSString *)urlString withCallBackTarget:(id)target_ withCallBackSelector:(SEL)selector_
{
//	(@"start openConnection");
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString]];
	ATConnection *connection = [[ATConnection alloc] initWithRequest:request delegate:self startImmediately:YES withTag:[NSString stringWithFormat:@"%d", connectionTag]];
	connectionTag++;
	NSMutableData *_data = [NSMutableData dataWithLength:0];
	
//	(@"%@",connection.tag);
	[[[self class] connectionsArray] addObject:connection];
	
	[[[self class] dataArray] addObject:_data];
	
	[[[self class] callBackTargetArray] addObject:target_];
	[[[self class] callBackSelectorArray] addObject:NSStringFromSelector(selector_)];

//	(@"stop");
}

- (void) closeConnection:(ATConnection *)connection 
{
	if(connection == nil){
		
		for (int index = 0; index < [[[self class] callBackTargetArray] count]; index++) {
			id target = [[[self class] callBackTargetArray] objectAtIndex:index];
			
			SEL selector = NSSelectorFromString([[[self class] callBackSelectorArray] objectAtIndex:index]);
			
			NSMutableData *data = [[[self class] dataArray] objectAtIndex:index];
			
			if([target respondsToSelector:selector]) {
				[target performSelector:selector withObject:data];
			}
		}
		
		
		[[[self class] callBackSelectorArray] removeAllObjects];
		[[[self class] callBackTargetArray] removeAllObjects];
		[[[self class] dataArray] removeAllObjects];
		[[[self class] connectionsArray] removeAllObjects];
		
	}
	else {
//		(@"start closeConnection");
//		(@"%@",connection);
//		(@"%@",connection.tag);
		NSInteger index = [[[self class] connectionsArray] indexOfObject:connection];
		[[[self class] connectionsArray] removeObject:connection];
		
		id target = [[[self class] callBackTargetArray] objectAtIndex:index];
		
		SEL selector = NSSelectorFromString([[[self class] callBackSelectorArray] objectAtIndex:index]);
		
		NSMutableData *data = [[[self class] dataArray] objectAtIndex:index];
		
		if([target respondsToSelector:selector]) {
			[target performSelector:selector withObject:data];
		}
		[[[self class] callBackSelectorArray] removeObjectAtIndex:index];
		[[[self class] callBackTargetArray] removeObjectAtIndex:index];
		[[[self class] dataArray] removeObjectAtIndex:index];
		[connection release];
	}
	
	if([[[self class] callBackSelectorArray] count] == 0){
		
		[[[self class] callBackSelectorArray] release];
		__callBackSelectorArray = nil;
	}
		
	
	if([[[self class] callBackTargetArray] count] == 0){
		[[[self class] callBackTargetArray] release];
		__callBackTargetArray = nil;
	}
		
	
	if([[[self class] dataArray] count] == 0){
		[[[self class] dataArray] release];
		__dataArray = nil;
	}
		
	
	if([[[self class] connectionsArray] count] == 0){
		[[[self class] connectionsArray] release];
		__connectionsArray = nil;
	}
		
	
}


- (void) configureNetworkReachability: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
	NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            statusString = @"Access Not Available";
            
            //Minor interface detail- connectionRequired may return yes, even when the host is unreachable.  We cover that up here...
			[self closeConnection:nil];
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
			break;
        }
        case ReachableViaWiFi:
        {
			statusString= @"Reachable WiFi";
            
            break;
		}
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self configureNetworkReachability: curReach];
	
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}


#pragma mark -
#pragma mark Asynchrous NSURLConnection Methods
- (void)connection:(ATConnection *)connection didReceiveData:(NSData *)data {

//	(@"start didReceiveData");
//	(@"%@",connection);
//	(@"%@",connection.tag);
//	(@"%@",[[self class] connectionsArray]);
	NSInteger index = [[[self class] connectionsArray] indexOfObject:connection];
	[[[[self class] dataArray] objectAtIndex:index] appendData:data];
}

- (void)connection:(ATConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
//	(@"start didReceiveResponse");
	if(connection == [[[self class] connectionsArray] objectAtIndex:0])
	{
//		(@"%@",response);
		
	}
}

- (void)connectionDidFinishLoading:(ATConnection *)connection {
//	(@"start connectionDidFinishLoading");
	[self closeConnection:connection];
	
}

- (void)connection:(ATConnection *)connection didFailWithError:(NSError *)error {
//	(@"start didFailWithError");
	[self closeConnection:connection];
}

@end
