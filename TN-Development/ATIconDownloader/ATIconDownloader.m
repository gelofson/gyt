//
//  ATIconDownloader.m
//  ATNetworks
//

//  Copyright __MyCompanyName__. All rights reserved.
//

#import "ATIconDownloader.h"

#import "ATConnection.h"
#import "Reachability.h"

@interface ATIconDownloader (PRIVATE)
+ (NSMutableArray*)connectionsArray;
+ (NSMutableArray*)dataArray;
+ (NSMutableArray*)callBackTargetArray;

- (void) closeConnection:(ATConnection *)connection ;
- (void) updateInterfaceWithReachability: (Reachability*) curReach;
@end


static NSMutableArray *__callBackTargetArray;
static NSMutableArray *__connectionsArray;
static NSMutableArray *__dataArray;
static NSMutableArray *__indexArray;

@implementation ATIconDownloader

@synthesize id_;
@synthesize imageUrl;


+ (NSMutableArray*)callBackTargetArray {
	@synchronized(self) {
		if(!__callBackTargetArray) {
			__callBackTargetArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __callBackTargetArray;
} 

+ (NSMutableArray*)connectionsArray {
	@synchronized(self) {
		if(!__connectionsArray) {
			__connectionsArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __connectionsArray;
} 

+ (NSMutableArray*)dataArray {
	@synchronized(self) {
		if(!__dataArray) {
			__dataArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __dataArray;
} 

+ (NSMutableArray*)indexArray {
	@synchronized(self) {
		if(!__indexArray) {
			__indexArray = [[NSMutableArray alloc] init];	
		}
	}
	
	return __indexArray;
} 


- (void) openConnection : (NSString *)urlString withOtherUrlString:(NSString*)urlString2 withCallBackTarget:(NSIndexPath*)target_ withDelegate:(id)delegate_
{
//	(@"start openConnection");
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString]];
	ATConnection *connection = [[ATConnection alloc] initWithRequest:request delegate:self startImmediately:YES withTag:[NSString stringWithFormat:@"%d", connectionTag]];
	connectionTag++;
	NSMutableData *_data = [NSMutableData dataWithLength:0];
	
	[[[self class] connectionsArray] addObject:connection];
	[[[self class] dataArray] addObject:_data];
	[[[self class] indexArray] addObject:target_];
	[[[self class] callBackTargetArray] addObject:delegate_];
	
	NSMutableURLRequest *request2 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: urlString2]];
	ATConnection *connection2 = [[ATConnection alloc] initWithRequest:request2 delegate:self startImmediately:YES withTag:[NSString stringWithFormat:@"%d", connectionTag]];
	connectionTag++;
	NSMutableData *_data2 = [NSMutableData dataWithLength:0];
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:target_.row inSection:1];
	
	[[[self class] connectionsArray] addObject:connection2];
	[[[self class] dataArray] addObject:_data2];
	[[[self class] indexArray] addObject:indexPath];
	[[[self class] callBackTargetArray] addObject:delegate_];
	
//	(@"stop");
}

- (void) closeConnection:(ATConnection *)connection 
{
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	if(connection == nil){
		//[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		for (int index = 0; index < [[[self class] callBackTargetArray] count]; index++) {
			id target = [[[self class] callBackTargetArray] objectAtIndex:index];
			
				
			NSMutableData *data = [[[self class] dataArray] objectAtIndex:index];
			UIImage *image = [UIImage imageWithData:data];
			[target imageDownloadFinish:image withId:[[[self class] indexArray] objectAtIndex:index]];
			
			}
		
		
		[[[self class] callBackTargetArray] removeAllObjects];
		[[[self class] dataArray] removeAllObjects];
		[[[self class] connectionsArray] removeAllObjects];
		
	}
	else {
		//		(@"start closeConnection");
		//		(@"%@",connection);
		//		(@"%@",connection.tag);
		
		
		
		NSInteger index = [[[self class] connectionsArray] indexOfObject:connection];
		[[[self class] connectionsArray] removeObject:connection];
		
		id target = [[[self class] callBackTargetArray] objectAtIndex:index];
		

		(@"%@",[[self class] indexArray]);
		(@"%@",[[self class] callBackTargetArray]);
		(@"%@",[[self class] dataArray]);

		NSMutableData *data = [[[self class] dataArray] objectAtIndex:index];
		UIImage *image = [UIImage imageWithData:data];
	
		[target imageDownloadFinish:image withId:[[[self class] indexArray] objectAtIndex:index]];
		
		[[[self class] callBackTargetArray] removeObjectAtIndex:index];
		[[[self class] dataArray] removeObjectAtIndex:index];
		[[[self class] indexArray] removeObjectAtIndex:index];
		
		if([[[self class] connectionsArray]count]<=0)
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		[connection release];
	}
	
	
	if([[[self class] callBackTargetArray] count] == 0){
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[[[self class] callBackTargetArray] release];
		__callBackTargetArray = nil;
	}
	
	if([[[self class] dataArray] count] == 0){
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[[[self class] dataArray] release];
		__dataArray = nil;
	}
	
	if([[[self class] connectionsArray] count] == 0)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[[[self class] connectionsArray] release];
		__connectionsArray = nil;
	}
	
}

//closes all the active connections
-(BOOL)closeAllActiveConnections
{
	ATConnection *temp_connection;
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	for(int i=0;i<[[[self class] connectionsArray] count];i++)
	{
		temp_connection=[[[self class]connectionsArray]objectAtIndex:i];
		if(temp_connection){
			[temp_connection cancel];
			temp_connection=nil;
		}
		
	}
	return YES;
}

//checks the n/w availability
- (void) configureNetworkReachability: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
	NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            statusString = @"Access Not Available";
            
            //Minor interface detail- connectionRequired may return yes, even when the host is unreachable.  We cover that up here...
			[self closeConnection:nil];
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
			break;
        }
        case ReachableViaWiFi:
        {
			statusString= @"Reachable WiFi";
            
            break;
		}
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self configureNetworkReachability: curReach];
	
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}


#pragma mark -
#pragma mark Asynchrous NSURLConnection Methods
- (void)connection:(ATConnection *)connection didReceiveData:(NSData *)data {
	
//	(@"start didReceiveData");
//	(@"%@",connection);
//	(@"%@",connection.tag);

	NSInteger index = [[[self class] connectionsArray] indexOfObject:connection];
	[[[[self class] dataArray] objectAtIndex:index] appendData:data];
}

- (void)connection:(ATConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
//	(@"start didReceiveResponse");
	if(connection == [[[self class] connectionsArray] objectAtIndex:0])
	{
//		(@"%@",response);
		
	}
}

- (void)connectionDidFinishLoading:(ATConnection *)connection
{
//	(@"start connectionDidFinishLoading");
	[self closeConnection:connection];
	
}

- (void)connection:(ATConnection *)connection didFailWithError:(NSError *)error {
//	(@"start didFailWithError");
	
	//NSInteger index = [[[self class] connectionsArray] indexOfObject:connection];
	
	//NSData *defaultImageData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"default_image" ofType:@"jpg"]];
	
	//[[[[self class] dataArray] objectAtIndex:index] appendData:defaultImageData];
	
	[self closeConnection:connection];
}

@end
