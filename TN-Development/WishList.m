//
//  WishListViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WishListViewController.h"
#import "DatabaseManager.h"
#import "tblWishList.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"

@implementation WishListViewController

@synthesize m_tblWishList;
@synthesize context = _context;

NSMutableArray *l_arrWishList;
BOOL l_isEditingTrue;
UITextView *l_txtInput;
QNavigatorAppDelegate *l_appDelegate;
DatabaseManager *l_db;
UILabel *l_lblRow=nil;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	l_db = [[DatabaseManager alloc] init];
	l_isEditingTrue=YES;
	[m_tblWishList setEditing:YES animated:YES];
	m_tblWishList.separatorColor=[UIColor clearColor];
}

-(void)viewWillAppear:(BOOL)animated{
	[m_myLabel setHidden:YES];
	[l_appDelegate.m_customView setHidden:YES];
	[self requestFetching];
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
	//[m_tblWishList setEditing:NO animated:YES];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc 
{
    [super dealloc];
	[m_tblWishList release];
	if(l_db!=nil)
	{
		[l_db release];
		l_db=nil;
	}
	
	if(l_arrWishList!=nil)
	{
		[l_arrWishList release];
		l_arrWishList=nil;
	}
}

#pragma mark -
#pragma mark Custom Methods

-(void)requestFetching
{
	_context=[l_db managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
								   entityForName:@"tblWishList" inManagedObjectContext:_context];
    [fetchRequest setEntity:entity];
    NSError *error;
	if(l_arrWishList!=nil)
	{
		[l_arrWishList release];
		l_arrWishList=nil;
	}
    l_arrWishList = [[NSMutableArray alloc] initWithArray:[_context executeFetchRequest:fetchRequest error:&error]];
	NSLog(@"%@",l_arrWishList);
    [fetchRequest release];
}

- (NSString *)getTextForIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row<=l_arrWishList.count)
	{
		return [[l_arrWishList objectAtIndex:indexPath.row-1]Wish_Text];
	}
	return @"";
}

- (CGSize)GetSizeOfText:(NSString *)text withFont:(UIFont *)font
{
	return [text sizeWithFont: font constrainedToSize:CGSizeMake(300,700)];
}

-(void)drawInputView
{
	m_customEntryView=[[UIView alloc]initWithFrame:CGRectMake(0,54,320,471)];
	m_customEntryView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7f];
	[self.view addSubview:m_customEntryView];
		
	l_txtInput=[[UITextView alloc]initWithFrame:CGRectMake(10,10,300,131)];
	[l_txtInput setTextColor:[UIColor blackColor]];
	[l_txtInput setFont:[UIFont fontWithName:kCursiveFontName size:25]];
	[m_customEntryView addSubview:l_txtInput];
	[l_txtInput becomeFirstResponder];
	[l_txtInput setDelegate:self];
	
	UIToolbar *temp_toolbar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,148,320,44)];
	[temp_toolbar setTintColor:[UIColor blackColor]];
	[m_customEntryView addSubview:temp_toolbar];
	
	UIBarButtonItem *btn_Save = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(btnSaveAction:)];
	UIBarButtonItem *btn_Cancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(btnCancelAction:)];
	UIBarButtonItem *btn_space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	
	btn_space.width=60;
	
	NSArray *temp_arrbBn=[[NSArray alloc]initWithObjects:btn_space,btn_Save,btn_Cancel,nil];
	[temp_toolbar setItems:temp_arrbBn]; 
	
	[btn_Save release];
	[btn_Cancel release];
	[btn_space release];
	[temp_arrbBn release];
	[temp_toolbar release];
	
	
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if(textView.text.length>=150 && range.length==0)
	{
		return NO;
	}
	return YES;
	
}

-(void)btnCancelAction:(id)sender
{
	[m_editButton setSelected:NO];
	[m_customEntryView removeFromSuperview];
}

-(void)btnSaveAction:(id)sender
{
	l_txtInput.text=[l_txtInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if(l_txtInput.text.length>0)
	{
		
		[m_editButton setSelected:NO];
		NSManagedObjectContext *temp_context = [l_db managedObjectContext];
		
		//if(l_isUpdateRow==NO) // Save Entries
		//{
		
		tblWishList *obj_wishList = [NSEntityDescription
									 insertNewObjectForEntityForName:@"tblWishList" 
									 inManagedObjectContext:temp_context];
		
		obj_wishList.Wish_Text=l_txtInput.text;
		NSError *error;
	    if (![temp_context save:&error]) {
	        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
	    }
		[m_tblWishList reloadData];
		[m_customEntryView removeFromSuperview];
		//[m_tblWishList setEditing:NO animated:YES];
		//l_isEditingTrue=NO;
	}
	else {
		UIAlertView *temp_alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input item description." 
														 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alert show];
		[temp_alert  release];
		
	}
}


-(void)btnAddNewAction:(id)sender
{
	[self drawInputView];	
}

-(void)btnEditAction:(id)sender
{
	// Deleting rows from tblwishlist	
	NSManagedObject *managedObject = [l_arrWishList objectAtIndex:[sender tag]-1]; //indexPath.row-1];
	[_context deleteObject:managedObject];
	
	// Commit the change.
	NSError *error;
	if (![_context save:&error]) 
	{
		// Handle the error.
	}
	[m_tblWishList reloadData];
	//[m_editButton setSelected:YES];
	//[m_tblWishList deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark -
#pragma mark Table View Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row==0)
		return 40;
	else if(indexPath.row <=l_arrWishList.count)
	{
		NSString *text;
		CGSize s;
		UIFont *f;
		text = [self getTextForIndexPath:indexPath];
		f = [UIFont fontWithName:kCursiveFontName size:25]; //systemFontOfSize:14];
		s = [self GetSizeOfText:text withFont: f];
		NSLog(@"Width: %f Height: %f", s.width, s.height);
		return s.height+10;	
			
	}
	
	return 40;
	

}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) 
	{
		// Deleting rows from tblwishlist	
		NSManagedObject *managedObject = [l_arrWishList objectAtIndex:indexPath.row-1];
        [_context deleteObject:managedObject];
					
		// Commit the change.
        NSError *error;
        if (![_context save:&error]) 
		{
            // Handle the error.
        }
		[m_editButton setSelected:YES];
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
	
	else if(editingStyle==UITableViewCellEditingStyleInsert)
	{
		[self drawInputView];		
	}
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	NSLog(@"ROW NO=%d",indexPath.row);
	if(indexPath.row==0)
	{
		l_lblRow.text=@"Add New Item";
		 return UITableViewCellEditingStyleInsert;
	}
	else {
			return UITableViewCellEditingStyleDelete;
		}

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *newCell;
	
	newCell=[tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
	
	if(newCell==nil)
		newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CustomCell"]autorelease];
	
	for (UIView *temp_view in newCell.contentView.subviews )
	{
		if([temp_view isKindOfClass:[UIButton class]])
		{
			[temp_view removeFromSuperview];
		}
		if([temp_view isKindOfClass:[UILabel class]])
		{
			[temp_view removeFromSuperview];
		}
		if([temp_view isKindOfClass:[UIImageView class]])
		{
			[temp_view removeFromSuperview];
		}
	}
	
	
	if(indexPath.row==0)
	{
		l_lblRow=newCell.textLabel;
		
		newCell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		l_lblRow.text=@"";
		[l_lblRow setFont:[UIFont fontWithName:kCursiveFontName size:25]];
		l_lblRow.numberOfLines=3;
		l_lblRow.lineBreakMode=UILineBreakModeCharacterWrap;
		
	}
	else
	{
		UILabel *temp_lblCellText;
		temp_lblCellText = newCell.textLabel;
		newCell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		temp_lblCellText.text=[[l_arrWishList objectAtIndex:indexPath.row-1] Wish_Text];
		 
		[temp_lblCellText setFont:[UIFont fontWithName:kCursiveFontName size:25]];
		
		temp_lblCellText.numberOfLines=50.0;
		temp_lblCellText.lineBreakMode=UILineBreakModeCharacterWrap;
		
	}
	if(indexPath.row!=0){
		UIImageView *temp_seperatorImgView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Seperator.png"]];
		//temp_seperatorImgView.frame=CGRectMake(0,newCell.frame.size.height,330,1);
		temp_seperatorImgView.frame=CGRectMake(0,-2,270,4);	
		[newCell.contentView addSubview:temp_seperatorImgView];
		[temp_seperatorImgView release];
	}
	
	return newCell;
	
}	
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	_context=[l_db managedObjectContext];
	[self requestFetching];
	return l_arrWishList.count+1;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
	
}

@end
