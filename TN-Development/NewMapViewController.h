//
//  NewMapViewController.h
//  TinyNews
//
//  Created by jay kumar on 11/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentViewController.h"
#import <MapKit/MapKit.h>
#import "MyAnnotation.h"

@class MoreButton;
@class DetailPageViewController;
@class CategoryData;
@interface NewMapViewController : ContentViewController<CLLocationManagerDelegate,MKMapViewDelegate,MKAnnotation,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIImageView *headerImage;
    IBOutlet UILabel *headertextLBL;
    IBOutlet UITextField *searchText;
    IBOutlet UIButton *categoryBTN;
    IBOutlet MKMapView *m_mapView;
    IBOutlet UIButton *moreButton;
    IBOutlet UIButton *closeByn;
    IBOutlet UIImageView *helpView;
    IBOutlet UILabel *headerLBL;
    IBOutlet UILabel *singleTapOnPin;
    IBOutlet UILabel *singleTapOnheadline;
    IBOutlet UILabel *firstDoubletap;
    IBOutlet UILabel *secondDoubletap;
    NSMutableArray* annotations;
    NSMutableArray *m_listArray;
    NSMutableArray *messageId;
    NSString *selectedType;
    NSInteger  currentRow;
    int pageCount;
    int annonationCount;
    int clickCount;
    CategoryData *m_data;
    BOOL serachTaG;
    BOOL zoomFlag;
}

@property(nonatomic,retain)IBOutlet UIImageView *headerImage;
@property(nonatomic,retain)IBOutlet UILabel *headertextLBL;
@property(nonatomic,retain) CategoryData *m_data;

@property(nonatomic,retain)IBOutlet UILabel *headerLBL;
@property(nonatomic,retain)IBOutlet UILabel *singleTapOnPin;
@property(nonatomic,retain)IBOutlet UILabel *singleTapOnheadline;
@property(nonatomic,retain)IBOutlet UILabel *firstDoubletap;
@property(nonatomic,retain)IBOutlet UILabel *secondDoubletap;
@property(nonatomic,retain) IBOutlet UIButton *closeByn;
@property(nonatomic,retain)IBOutlet UIImageView *helpView;
@property(nonatomic,retain)IBOutlet UIButton *moreButton;
@property(nonatomic,retain)IBOutlet UITextField *searchText;
@property(nonatomic,retain)IBOutlet UIButton *categoryBTN;
@property(nonatomic,copy) NSString *selectedType;
@property(nonatomic,retain)IBOutlet MKMapView *m_mapView;

-(IBAction)addMoreClicked;

-(IBAction)HelpClicked:(id)sender;
-(IBAction)getCategoryList:(id)sender;
-(void)drowPinsforcategorysearch:(NSArray*)serchArray;
-(IBAction)closeHelp:(id)sender;
-(void)show;
-(void)hide;
@end
